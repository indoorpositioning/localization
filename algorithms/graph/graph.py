import os

import numpy
from PIL import Image

import settings


class Graph:
    BLACK_THRESHOLD = 100  # type: int
    """The maximum brightness to recognize a pixel as a wall"""
    BOX_LENGTH_CM = 5  # type: int
    """Represents how many cm for one box in the grid"""

    def generate_floorplan_graph(self, path_to_image: str, pixels_per_cm: int, floorplan_id: int) -> str:
        """
        Generates a floorplan grid with a given image :py:data:`.BOX_LENGTH_CM` as the resolution. The generated
        image is a black & white representation on a lower resolution.

        Args:
            path_to_image:                  Path to the floorplan as image
            pixels_per_cm:                  Number of pixels that make up one cm in both dimensions
            floorplan_id:                   id to be used in the name to save the floorplan in a txt
        Returns: The filename of the saved boolean array
        """
        # Open image in 8-bit pixels black and white
        img = Image.open(path_to_image).convert("L")  # type: Image
        # Resize the image
        real_width_in_cm = img.size[0] / pixels_per_cm
        real_height_in_cm = img.size[1] / pixels_per_cm
        number_of_boxes_in_width = int(real_width_in_cm / self.BOX_LENGTH_CM)
        number_of_boxes_in_height = int(real_height_in_cm / self.BOX_LENGTH_CM)
        if number_of_boxes_in_height == 0 or number_of_boxes_in_width == 0:
            raise ValueError('pixels_per_cm to small (smaller than minimum tile length)')
        img = img.resize((int(real_width_in_cm), int(real_height_in_cm)), Image.BOX)
        # Convert image to one bit black and white pixel
        img = img.point(lambda x: 0 if x < self.BLACK_THRESHOLD else 255, '1')
        floorplan_filename = self._get_floorplan_filename(floorplan_id)
        numpy_array = numpy.array(img)
        # for inspecting purposes
        img.save(floorplan_filename + ".gif", "GIF")
        numpy.savetxt(floorplan_filename, numpy_array, fmt='%d')
        print("built " + floorplan_filename)
        return floorplan_filename

    @staticmethod
    def _get_floorplan_filename(floorplan_id):
        """Returns the filename to the floorplan boolean list with given id

        Args:
            floorplan_id:

        Returns: Filename to txt to boolean list
        """
        return os.path.join(settings.MAPS_BASE_PATH, str(floorplan_id) + settings.MAP_ARRAY_SUFFIX)

    @staticmethod
    def load_floorplan_array(floorplan_id: int) -> numpy.ndarray:
        """Retrieves the boolean floorplan array to be used in floorplan

        Args:
            floorplan_id: Id of an existing floorplan

        Returns: ndarray of booleans

        """
        return numpy.loadtxt(Graph._get_floorplan_filename(floorplan_id), 'bool')
