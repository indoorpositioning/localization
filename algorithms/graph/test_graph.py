import unittest

from algorithms.graph.graph import Graph


class TestGraph(unittest.TestCase):

    def test_update(self):
        """Doesn't test anything yet, only for demoing."""
        graph = Graph()
        result_filename = 'test_image_result.gif'
        input_filename = 'test_floorplan_edited.gif'
        pixels_per_cm = 2
        real_height_in_cm = 400
        result = graph.generate_floorplan_graph(input_filename, pixels_per_cm, 1)


if __name__ == '__main__':
    unittest.main()