import cProfile
import math
import operator
import os
import pickle
from typing import List, Callable, Dict

from algorithms.particlefilter.vector import Vector


class Bresenham:
    # All vectors that have the x/y ratio are equal
    ratio_octant_cache: Dict[float, List[List[Vector]]] = {}
    file_path: str = os.path.join(os.getcwd(), 'algorithms', 'particlefilter', 'ratio_ocatnt_cache.pk1')

    def __init__(self) -> None:
        Bresenham.ratio_octant_cache = {}
        self.used = {}
        self.failed = {}

    def write_cache_to_file(self):
        output = open(Bresenham.file_path, 'wb')
        pickle.dump(self.ratio_octant_cache, output)
        output.close()

    def load_from_cache(self):
        output = open(Bresenham.file_path, 'rb')
        self.ratio_octant_cache = pickle.load(self.ratio_octant_cache, output)
        output.close()

    def pre_compute_vectors(self, radius_from: int, radius_to: int):
        profile = cProfile.Profile()
        profile.disable()
        for radius in range(radius_from, radius_to + 1):
            # Uses Bresenham's to rasterize an arc, see https://en.wikipedia.org/wiki/Midpoint_circle_algorithm
            x = radius - 1  # type: int
            y = 0  # type: int
            dx = 1  # type: int
            dy = 1  # type: int
            error = dx - (radius << 1)  # type: int

            while x >= y:
                ratio: float = float(y) / float(x)
                if ratio not in self.ratio_octant_cache:
                    self.compute_part_vectors(Vector(x, y))
                if error <= 0:
                    y += 1
                    error += dy
                    dy += 2

                if error > 0:
                    x -= 1
                    dx += 2
                    error += dx - (radius << 1)
        profile.enable()

    def compute_part_vectors(self, vector: Vector) -> List[Vector]:
        """Creates vectors from pixel to each neighbour pixel using (Bresenham's line algorithm)[https://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm]
            If pattern of vector repeats, only one pattern is returned. E.g. (2,2) returns only (1,1)
        Args:
            vector (Vector): The vector from which to compute partial vectors

        Returns: The vectors that visit each box on the path of the input vector.
        """
        if vector == Vector(0, 0):
            return [vector]
        ratio: float = abs(float(vector.x)) / abs(float(vector.y)) if abs(vector.y) > 0 else float("inf")
        octant_dict: Dict[int, List[Vector]] = Bresenham.ratio_octant_cache.get(ratio, False)
        octant = self._determine_octant(vector)
        if octant_dict:
            var = self.used.get(ratio, 0)
            self.used[ratio] = var + 1
            return octant_dict[octant]
        # print("Not in " + str(vector) + " ||: " + "{:6.2f}".format(abs(vector)))
        var = self.failed.get(ratio, 0)
        self.failed[ratio] = var + 1
        shortened_vector: Vector
        shortened_vector, gcd = self._shorten_by_gcd_vector(vector)
        positions = self._simple_bresenham(vector)
        part_vectors = list(map(operator.sub, positions[1:], positions[:-1]))[::-1]
        # self.cache_computed_vectors(part_vectors, octant, ratio)
        return part_vectors

    def cache_computed_vectors(self, part_vectors, octant, ratio):
        octant_list = []
        for i in range(8):
            transformed = map(self._input_vector_transformation(octant), part_vectors)
            transformed = map(self._output_vector_transformation(i), transformed)
            octant_list.append(list(transformed))
        Bresenham.ratio_octant_cache[ratio] = octant_list

    def _shorten_by_gcd_vector(self, vector: Vector):
        """Shortens the vector by the vectors components greatest common divisor.
        Args:
            vector(Vector): The vector to shorten

        Returns: The shortened vector.
        """
        gcd: int = math.gcd(vector.x, vector.y)
        return vector / gcd, gcd

    def _simple_bresenham(self, vector: Vector) -> List[Vector]:
        """Returns all positions of the boxes if vector is rendered with Bresenham
        Args:
            vector (Vector): The vector from which to compute the positions

        Returns: The positions of each box on the path of the input vector.
        """
        octant = self._determine_octant(vector)
        output_transfromation = self._output_vector_transformation(octant)
        vector = self._input_vector_transformation(octant)(vector)
        positions = []  # type: List[Vector]
        d = 2 * vector.y - vector.x  # type: int
        y = 0  # type: int
        for x in range(0, int(vector.x) + 1):
            positions.append(output_transfromation(Vector(x, y)))
            if d > 0:
                y += 1
                d -= 2 * vector.x
            d += 2 * vector.y
        return positions

    def _determine_octant(self, vector: Vector) -> int:
        """Deteremines the octant of the vector.
         Octants:
          \2|1/
          3\|/0
         ---+---
          4/|\7
          /5|6\
          Args:
              vector (Vector): The vector
        """
        if vector.y > 0:
            if vector.x > 0:
                if vector.x > vector.y:
                    return 0
                else:
                    return 1
            else:
                if -vector.x < vector.y:
                    return 2
                else:
                    return 3
        else:
            if vector.x < 0:
                if vector.x < vector.y:
                    return 4
                else:
                    return 5
            else:
                if vector.x < -vector.y:
                    return 6
                else:
                    return 7

    def _input_vector_transformation(self, octant: int) -> Callable[[Vector], Vector]:
        """Returns the transformation function of the input vector to the bresenham algorithm based on the octant."""
        return {
            0: (lambda v: v),
            1: (lambda v: Vector(v.y, v.x)),
            2: (lambda v: Vector(v.y, -v.x)),
            3: (lambda v: Vector(-v.x, v.y)),
            4: (lambda v: Vector(-v.x, -v.y)),
            5: (lambda v: Vector(-v.y, -v.x)),
            6: (lambda v: Vector(-v.y, v.x)),
            7: (lambda v: Vector(v.x, -v.y)),
        }.get(octant)

    def _output_vector_transformation(self, octant: int) -> Callable[[Vector], Vector]:
        """Returns the transformation function of the output vectors of the bresenham algorithm based on the octant."""
        return {
            0: (lambda v: v),
            1: (lambda v: Vector(v.y, v.x)),
            2: (lambda v: Vector(-v.y, v.x)),
            3: (lambda v: Vector(-v.x, v.y)),
            4: (lambda v: Vector(-v.x, -v.y)),
            5: (lambda v: Vector(-v.y, -v.x)),
            6: (lambda v: Vector(v.y, -v.x)),
            7: (lambda v: Vector(v.x, -v.y)),
        }.get(octant)
