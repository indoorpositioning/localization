from typing import Dict

import numpy

from algorithms.particlefilter.vector import Vector
from data.models import RangingDevice, Location


class Floorplan(object):

    def __init__(self, tiles: numpy.ndarray, scale: float, ranging_devices: Dict[str, RangingDevice],
                 location: Location) -> None:
        super().__init__()
        self.location = location
        self.tiles = tiles
        self.ranging_devices = ranging_devices
        self.scale = scale

    def isBlocked(self, particle: Vector):
        # tile_Y = int(particle.y / self.scale)
        tile_Y = int(particle.y)
        # tile_X = int(particle.x / self.scale)
        tile_X = int(particle.x)
        return not (((0 <= tile_Y < len(self.tiles)) and (0 <= tile_X < len(self.tiles[0]))) and (
            self.tiles[tile_Y][tile_X]))
