from typing import Dict

from algorithms.graph.graph import Graph
from algorithms.particlefilter.floorplan import Floorplan
from algorithms.particlefilter.vector import Vector
from data.database_query import DatabaseQuery


class FloorplanPool:
    floorplans = {}  # type: Dict[int, Floorplan]

    def get_floorplan(self, id: int) -> Floorplan:
        if id not in self.floorplans:
            self._load_floorplan(id)
        return self.floorplans[id]

    def _load_floorplan(self, id: int):
        location = DatabaseQuery.get_location(id)
        vector = Vector(location.map_scale_end_x - location.map_scale_start_x,
                        location.map_scale_end_y - location.map_scale_start_y)
        tiles_boolean = Graph.load_floorplan_array(id)
        # scale = location.map_pixel_height / len(tiles_boolean)
        scale = 1
        ranging_devices = {ranging_device.identifier: ranging_device for ranging_device in location.ranging_devices}
        self.floorplans[id] = Floorplan(tiles_boolean, scale, ranging_devices, location)
