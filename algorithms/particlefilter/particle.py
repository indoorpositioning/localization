from typing import Dict

from algorithms.particlefilter.vector import Vector


class Particle(Vector):
    weight = None  # type: float
    access_node_likehood = None  # type: Dict[str, float]

    def __init__(self, x: int, y: int, weight: float) -> None:
        super().__init__(x, y)
        self.weight = weight
        self.access_node_likehood = {}
