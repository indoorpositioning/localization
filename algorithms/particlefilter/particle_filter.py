import copy
import operator
import random
from functools import reduce
from itertools import accumulate
from math import ceil
from typing import List, Dict
from typing import Tuple, Iterator

import numpy
from numpy.random.mtrand import RandomState
from scipy.stats import norm

from algorithms.particlefilter.bresenham import Bresenham
from algorithms.particlefilter.floorplan import Floorplan
from algorithms.particlefilter.particle import Particle
from algorithms.particlefilter.trilateration import Trilateration
from algorithms.particlefilter.vector import Vector
from data.models import RangingDevice, RangingData
from misc.measurement import RangingData


class ParticleFilter:
    _trilateration: Trilateration
    bresenham: Bresenham
    pdf_lookup: Dict[float, float]
    last_ranging_data: Dict[str, int] = None
    # NUMBER_OF_PARTICLES = 2200
    NUMBER_OF_PARTICLES_ = 500
    SIGMA_U_ = 133
    NEFF_THRESHOLD_ = 0.5
    MIN_PARTICLE_DISTANCE_TRAVEL = 50
    # When movement is shorter than this factors movement times step vector length
    MOVEMENT_RATIO = 0.6
    # Fraction of total particles to be alternatively resampled
    ALTERNATIVE_RESAMPLE_RATIO = 0.1

    particles: List[Particle]
    _active_particles: List[Particle]
    _alternative_position: Vector
    _known_ranging_devices: Dict[str, Vector]
    recent_ranging_values: Dict[RangingDevice, Dict[RangingData, float]]

    def __init__(self, floorplan: Floorplan, step_stretch_std_deviation, step_stretch_offset_factor,
                 step_angle_std_deviation, step_angle_offset, average_step_length, with_bresenham,
                 alternative_resample_ratio: float = ALTERNATIVE_RESAMPLE_RATIO,
                 start_position: Vector = None, neff_threshold: float = NEFF_THRESHOLD_, sigma_u: int = SIGMA_U_,
                 number_of_particles: int = NUMBER_OF_PARTICLES_, seed=None):
        self._trilateration = Trilateration()
        self._start_position = start_position
        self._last_position = start_position
        self.particles = list()
        self._active_particles = list()
        self.recent_ranging_values = {}
        self.floorplan = floorplan  # type: Floorplan
        self.SIGMA_U = sigma_u
        self.NEFF_THRESHOLD = number_of_particles * neff_threshold
        self.NUMBER_OF_PARTICLES = number_of_particles
        self.step_stretch_std_deviation = step_stretch_std_deviation
        self.step_stretch_offset_factor = step_stretch_offset_factor
        self.step_angle_std_deviation = step_angle_std_deviation
        self.step_angle_offset = step_angle_offset
        # self._known_ranging_devices = ranging_devices
        self.random = random.Random()
        self.numpy_random = numpy.random
        self.alternative_resample_spread = alternative_resample_ratio
        self.alternative_resample_particles = 0
        self.field_dimensions_cm = self.floorplan.location.get_dimensions_cm()
        if seed is not None:
            self.random.seed(int(seed))
            self.numpy_random = RandomState(int(seed))
        self.spread_particles()

        self.with_bresenham = with_bresenham
        # self.bresenham.load_from_cache()
        # print("Compute step vectors")
        if self.with_bresenham:
            self.bresenham = Bresenham()
            from_rad = max(int(average_step_length * (step_stretch_offset_factor - 3 * step_stretch_std_deviation)), 2)
            to_radius = ceil(average_step_length * (step_stretch_offset_factor + 3 * step_stretch_std_deviation))
            self.bresenham.pre_compute_vectors(from_rad, to_radius)
        # self.pdf_lookup = {x : norm.pdf(x , scale=self.SIGMA_U) for x in range(-10000, 10000)}

    @property
    def start_position(self):
        return self._start_position

    @start_position.setter
    def start_position(self, position: Vector):
        self._start_position = position
        self.spread_particles()

    def update(self, ranging_data: Dict[RangingDevice, Dict[RangingData, float]] = None, rotations: Vector = None):
        """Some documentation here.
        """
        kidnapped = False
        if rotations:
            # print("Got rotations " + str(rotations), flush=True)
            self._state_update(rotations)
            self.recent_ranging_values = {k: val for k, val in self.recent_ranging_values.items() if val is not None}
            previous_position = self._last_position
            self._calculate_position()
            # if previous_position and \
            #         abs(previous_position - self._last_position) < self.MOVEMENT_RATIO * abs(rotations):
            #     kidnapped = True
            #     print("Kidnapped!")
            #     self.alternative_resample_particles = int(self.ALTERNATIVE_RESAMPLE_RATIO * self.NUMBER_OF_PARTICLES)
            if ranging_data is None or len(self.recent_ranging_values) < 1:
                return self._last_position, "Kidnapped" if kidnapped else ""
        elif ranging_data is None:
            return None, None
        if ranging_data is not None:
            self.recent_ranging_values.update(ranging_data)
        if any([element is None for element in self.recent_ranging_values.values()]) \
                or (len(self.recent_ranging_values) < 3 and not rotations):
            return None, None
        self._individual_likehood(self.recent_ranging_values)
        inverted_distance_fractions = self._calculate_inverted_distance_fractions(self.recent_ranging_values)
        self._calculate_particle_unnormalized_weights(inverted_distance_fractions)
        self._normalize_weights(self.particles)
        self._compute_alternative_base_position()
        self._systematic_resampling()
        # print("Neff: " + str(self._neff()), end=", ", flush=True)
        while self._neff() < self.NEFF_THRESHOLD:
            self._systematic_resampling()
        self.last_ranging_data = ranging_data
        self.recent_ranging_values = {key: None for key in self.recent_ranging_values.keys()}
        self._calculate_position()
        return self._last_position, "Kidnapped" if kidnapped else ""

    def spread_particles(self):
        self.particles = []
        initial_particle_weight = 1 / self.NUMBER_OF_PARTICLES
        particle_range = range(self.NUMBER_OF_PARTICLES)
        for x in particle_range:
            position: Vector = Vector(0, 0)
            # Ensure no particles are on a wall or outside the building
            while True:
                if not self.start_position:
                    position = self._position_on_field()
                else:
                    position = self._position_around(self.start_position, 0)
                if not self.floorplan.isBlocked(position):
                    break
            self.particles.append(Particle(position.x, position.y, initial_particle_weight))

    def _position_around(self, position: Vector, spread: int):
        return Vector(int(self.numpy_random.normal(position.x, spread)),
                      int(self.numpy_random.normal(position.y, spread)))

    def _position_on_field(self):
        return Vector(int(self.random.randrange(0, int(self.field_dimensions_cm.x))),
                      int(self.random.randrange(0, int(self.field_dimensions_cm.y))))

    def _state_update(self, vector: Vector):
        """Moves all particles by the given step, but avoids invalid fields."""
        if abs(vector) == 0:
            return
        # i = 0
        # min_index_travel = (vector * (self.MIN_PARTICLE_DISTANCE_TRAVEL / abs(vector))).manhattan_norm()
        # print("Will loop over " + str(len(self.particles)))
        # print(str(len(self.particles)))
        for particle in self.particles:
            stretched_vector = copy.deepcopy(vector)
            # stretched_vector *= self.numpy_random.normal(1, 0.2)
            stretched_vector *= self.numpy_random.normal(self.step_stretch_offset_factor,
                                                         self.step_stretch_std_deviation)
            f = numpy.pi / 180
            stretched_vector.rotate(self.numpy_random.normal(self.step_angle_offset, self.step_angle_std_deviation) * f)
            # stretched_vector.rotate(self.numpy_random.normal(0, 5))
            stretched_vector.to_int()
            part_vectors = self.bresenham.compute_part_vectors(stretched_vector) if self.with_bresenham \
                else [stretched_vector]
            if self.floorplan.isBlocked(particle):
                # print("This shouldn't happen")
                break
            # print("  Particle " + str(particle) + " + " + str(stretched_vector) + " = ", end='')
            # print(particle, flush=True)
            new_position = Vector(particle.x, particle.y)
            new_position += stretched_vector
            traveled = 1
            not_blocked = len(part_vectors)
            position_set = False
            for part_vector in part_vectors:
                # print("Move to " + str(particle.__str__()))
                if position_set:
                    traveled += 1
                    if self.floorplan.isBlocked(new_position):
                        not_blocked -= 1
                else:
                    if not self.floorplan.isBlocked(new_position):
                        position_set = True
                        particle.x = new_position.x
                        particle.y = new_position.y
                new_position -= part_vector
                # self.alternative_resample_particles += 1 - weight_factor
                # print(self.alternative_resample_particles)
                #     particle.weight = 0.00000000000000000000000000000000000000000000001
                # particle.weight = 0
                #     if traveled < min_index_travel:
                #         particle.x = origin.x
                #         particle.y = origin.y
                #     else:
                #         particle -= part_vector
                #     break
            weight_factor = (1 / len(part_vectors)) ** 2 * traveled * not_blocked
            particle.weight *= weight_factor
            print(str(weight_factor), end=' ', flush=True)
            # i += 1
            # print("Particle id" + str(id(particle)) + str(particle))

    def _individual_likehood(self, ranging_datas: Dict[RangingDevice, Dict[RangingData, float]]):
        """Calculates the likelihood of each particle to each ranging device.

        Args:
            ranging_datas (Dict[RangingDevice, float]): Key = ranging device, value = real measured cm distance
        """
        # Collect all values then compute the pdf for the entire array and assign the likehoods.
        # Greatly increases efficiency
        pdf_inputs: List[float] = []
        for particle in self.particles:
            # particle.access_node_likehood = {}
            for ranging_device, rd_distance in ranging_datas.items():
                mu = abs(particle - ranging_device)
                pdf_inputs.append(rd_distance[RangingData.measured_cm] - mu)
        pdf_outputs: List[float] = norm.pdf(pdf_inputs, scale=self.SIGMA_U)
        i = 0
        for particle in self.particles:
            for ranging_device in ranging_datas.keys():
                particle.access_node_likehood[ranging_device] = pdf_outputs[i]
                i += 1

    @staticmethod
    def _calculate_inverted_distance_fractions(ranging_datas: Dict[RangingDevice, Dict[RangingData, float]]) \
            -> Dict[RangingDevice, float]:
        """Calculates the fraction of how much each inverted distance contributes to the total inverted distances

        Args:
            ranging_datas (Dict[int, float]): Key = id of ranging device, value = real measured cm distance

        Returns:
            Key = id of ranging device, value = part that the ranging device contributes to the total inverted distances
        """
        inverted_distances = {rd: 1 / rd_distance[RangingData.measured_cm] for rd, rd_distance in ranging_datas.items()}
        total_inverted_distances = sum(inverted_distances.values())
        return {rd_id: inverted_distances[rd_id] / total_inverted_distances for rd_id, rd_distance in
                ranging_datas.items()}

    def _calculate_particle_unnormalized_weights(self, inverted_distance_fractions: Dict[RangingDevice, float]):
        """Calculates the weights of the particles using the likelihoods and inverted distance fractions

        Args: inverted_distance_fractions: Key = id of ranging device, value = part that the ranging device
        contributes to the total inverted distances
        """
        for particle in self.particles:
            # weighted_rd_distances = [particle.access_node_likehood[rd_id] for rd_id in
            #                          particle.access_node_likehood.keys()]
            weighted_rd_distances = [particle.access_node_likehood[rd_id] ** inverted_distance_fractions[rd_id] for
                                     rd_id in inverted_distance_fractions.keys()]
            # particle.weight = sum(weighted_rd_distances)
            particle.weight = reduce(operator.mul, weighted_rd_distances)

    @staticmethod
    def _normalize_weights(particles: List[Particle]):
        """Normalizes the weights across the passed particles

        Args:
            particles: List of paricles to be normalized
        """
        total_weight = sum([particle.weight for particle in particles])
        for particle in particles:
            particle.weight /= total_weight

    def _systematic_resampling(self):
        """
            Resamples the particles. Copies the location of some randomly selected particles to others. Particles with a greater weight are
            more likely to be used as the new location.
        """
        # Kill dead particles
        threshold = 0
        self._active_particles = [particle for particle in self.particles if particle.weight > threshold]
        ParticleFilter._normalize_weights(self._active_particles)
        # Draw particles to be used for new particle position
        cumulative_sum = list(accumulate([particle.weight for particle in self._active_particles]))
        self.alternative_resample_particles = int(self.alternative_resample_particles)
        weights_to_draw = len(self.particles) - self.alternative_resample_particles
        # weights_to_draw = len(self.particles)
        if weights_to_draw > 0:
            drawn_weights = numpy.arange(self.random.random() / weights_to_draw, 1, 1 / weights_to_draw)
        else:
            drawn_weights = []
        drawn_weights = numpy.append(numpy.repeat(0, self.alternative_resample_particles), drawn_weights)
        cumulative_sum_particles_iterator = zip(cumulative_sum,
                                                self._active_particles)  # type: Iterator[Tuple[float, Particle]]
        weight_particle_tuple = next(cumulative_sum_particles_iterator)
        # Find and replace a particle to be replaced, set new weight to each particle
        for particle, drawn_cumulutative_weight in zip(self.particles, drawn_weights):
            if drawn_cumulutative_weight == 0:
                new_position = self._get_alternative_position()  # type: Vector
            else:
                while weight_particle_tuple[0] < drawn_cumulutative_weight:
                    weight_particle_tuple = next(cumulative_sum_particles_iterator)
                    pass
                new_position = weight_particle_tuple[1]  # type: Particle
                particle.weight = new_position.weight
            particle.x = new_position.x
            particle.y = new_position.y
        self.alternative_resample_particles = 0
        ParticleFilter._normalize_weights(self.particles)

    def _get_alternative_position(self):
        while True:
            # position = self._position_around(self._alternative_position, self.alternative_resample_spread)
            position = self._position_around(self._alternative_position, self.SIGMA_U)
            if not self.floorplan.isBlocked(position):
                break
        return position

    def _get_alternative_position2(self):
        while True:
            position = self._position_on_field()
            if not self.floorplan.isBlocked(position):
                break
        return position

    def _compute_alternative_base_position(self):
        self._alternative_position = self._trilateration.ap_distance(
            {device: value[RangingData.measured_cm] for device, value in self.recent_ranging_values.items()})
        # Get the closes position to the trilaterated that is not on a wall
        if self.floorplan.isBlocked(self._alternative_position):
            distance = 0
            found_positions = None
            while not found_positions:
                distance += 1
                found_positions = []
                for dx in range(-distance, distance + 1):
                    for dy in range(-distance, distance + 1):
                        position = self._alternative_position + Vector(dx, dy)
                        if not self.floorplan.isBlocked(position):
                            found_positions.append(position)
            self._alternative_position = self.random.choice(found_positions)

    def _neff(self) -> float:
        """Calculates the neff factor of all particles

        Returns: neff factor as a float

        """
        return 1 / sum([particle.weight ** 2 for particle in self.particles])

    def _calculate_position(self):
        self._last_position = sum([particle.weight * particle for particle in self.particles])
