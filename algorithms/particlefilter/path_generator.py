import json
import math
import time
from datetime import datetime
from typing import List, Dict, Union

import numpy as np
import scipy
from numpy.core.multiarray import ndarray
from scipy import signal

from algorithms.particlefilter.vector import Vector
from data.database_query import DatabaseQuery
from data.models import Track
from misc.utils import datetime_from_uploaded_timestamp

AVERAGE_STEP_LENGTH_CM = 70


class PathGenerator(object):
    exact_vectors_step_times: List[datetime]
    positions_on_transitions: Dict[int, List[Vector]]
    transition_vectors: ndarray
    step_ends: List[Vector]
    last_recorded_waypoint: int

    def __init__(self, track: Track) -> None:
        self.track = track
        DatabaseQuery.track_with_waypoints(self.track)
        self.location = self.track.location
        self.waypoint_numbers = [waypoint.position_number for waypoint in self.track.waypoints_fetched]
        self.measurements = json.loads(self.track.measurements)
        self.random_generator = np.random
        self.average_step_length = AVERAGE_STEP_LENGTH_CM
        self.positions_on_transitions = {}
        self.exact_vectors_step_times = []
        super().__init__()

    def generate_walk_vectors(self, offset_angle_mean: float, offset_angle_std_deviation: float,
                              average_step_length: float, std_deviation_step_length: float) -> [Vector, datetime]:
        # self.average_step_length = average_step_length
        exact_vectors = self.generate_exact_vectors()
        angles = self.random_generator.normal(offset_angle_mean, offset_angle_std_deviation, len(exact_vectors))
        deg_2_rad = scipy.math.pi / 180
        cos = np.cos(angles * deg_2_rad)
        sin = np.sin(angles * deg_2_rad)
        stretch = self.random_generator.normal(average_step_length, std_deviation_step_length, len(exact_vectors))
        stretched_vectors = exact_vectors * stretch
        rotated_vecs1 = [Vector(v.x * c - v.y * s, v.x * s + v.y * c) for (v, c, s) in zip(stretched_vectors, cos, sin)]
        return rotated_vecs1

    def generate_exact_vectors(self) -> [Vector]:
        """
            Generates step vectors with fixed step_length.
        Returns: List of steps

        """
        waypoints = [waypoint.position_cm for waypoint in self.track.waypoints_fetched]
        self.transition_vectors = np.array(waypoints[1:]) - waypoints[:-1]

        def generate_vectors_on_transition(transition: Vector, start_factor: float):
            transition_length = abs((1 - start_factor) * transition)
            number_of_fitting_vectors = int(transition_length / float(self.average_step_length))
            vector_on_transition = transition * float(self.average_step_length) / abs(transition)
            return [vector_on_transition for i in range(number_of_fitting_vectors)]

        def generate_crossing_vector(previous_end: Vector, transition_start: Vector, transition: Vector):
            v = transition_start - previous_end
            transition_dir = transition * 1 / abs(transition)
            lf = transition_dir * v
            alpha = math.acos(v * transition / (abs(v) * abs(transition)))
            c = self.average_step_length * math.sin(
                math.pi - alpha - math.asin(abs(v) * math.sin(alpha) / self.average_step_length)) / math.sin(alpha)
            return transition_start + c / abs(transition) * transition - previous_end, c / abs(transition)

        def save_positions(transition_index: int, positions: Union[ndarray, List[Vector]]):
            dict_list = dict.get(self.positions_on_transitions, transition_index, [])
            dict_list.extend(positions)
            self.positions_on_transitions[i] = dict_list

        i: int = 0
        start: float = 0.0
        new_positions: np.array = np.array([])
        positions: List[Vector] = [self.track.waypoints_fetched[0].position_cm]
        while i < len(self.transition_vectors):
            vectors: List[Vector] = np.cumsum(generate_vectors_on_transition(self.transition_vectors[i], start))
            new_positions = np.array([positions[-1] + vector for vector in vectors])
            positions.extend(new_positions)
            save_positions(i, new_positions)
            i += 1
            while i < len(self.transition_vectors):
                crossing_vector, start = generate_crossing_vector(positions[-1],
                                                                  self.track.waypoints_fetched[i].position_cm,
                                                                  self.transition_vectors[i])
                if crossing_vector:
                    position = positions[-1] + crossing_vector
                    positions.append(position)
                    save_positions(i, [position])
                    break
                i += 1

        self.step_ends = positions[1:]
        return np.array(self.step_ends) - positions[:-1]

    def generate_exact_vectors_step_times(self) -> List[datetime]:
        """
            Creates the step times. Generate_exact vectors has to be called before.

        """
        if not self.exact_vectors_step_times:
            transition_timestamps = self.parse_transition_timestamps()[1:]
            transition_durations = transition_timestamps[1:] - transition_timestamps[:-1]
            transition_positions = [self.track.waypoints_fetched[0].position_cm]
            transition_positions.extend(
                np.cumsum(self.transition_vectors) + self.track.waypoints_fetched[0].position_cm)
            for transition_index, positions_on_transition in self.positions_on_transitions.items():
                transition_position = transition_positions[transition_index]
                transition_vector_length = abs(self.transition_vectors[transition_index])
                transition_time = transition_timestamps[transition_index]
                transition_duration = transition_durations[transition_index]
                for position in positions_on_transition:
                    vector_to_position = position - transition_position
                    fraction = abs(vector_to_position) / transition_vector_length
                    timestamp = transition_time + fraction * transition_duration
                    self.exact_vectors_step_times.append(timestamp)
        return self.exact_vectors_step_times

    def parse_transition_timestamps(self) -> [datetime]:
        # Get the timestamp where the waypoint is hit
        timestamp_groups = np.repeat(datetime.now(), len(self.waypoint_numbers) + 1)
        current_trackpoint_id = -1
        position = 0
        for measurement in self.measurements:
            position = measurement['p']
            if current_trackpoint_id != position and current_trackpoint_id < position:
                current_trackpoint_id = position
                timestamp_groups[current_trackpoint_id] = datetime_from_uploaded_timestamp(measurement['t'])
        self.last_recorded_waypoint = position
        return timestamp_groups

    def get_start_position(self) -> Vector:
        if len(self.track.waypoints_fetched) > 0:
            position = [wp.position_cm for wp in self.track.waypoints_fetched if wp.position_number == 1][0]
        else:
            position = self.location.get_start_position_cm()
        return position


def convert_time_to_ms(upload_time: str):
    return int(time.mktime(datetime.datetime.strptime(upload_time[:-3], "%Y%m%d%H%M%S").timetuple())) * 1000 + int(
        upload_time[-3:])


if __name__ == '__main__':
    testrotations = PathGenerator(DatabaseQuery.track_with_waypoints(46))
    exact_vectors = testrotations.generate_exact_vectors()
    # testrotations.plot_vectors("exact", exact_vectors)
    # Distort them
    angles = (scipy.signal.gaussian(len(exact_vectors), 5) * 5) + 0.5 - 2.5
    deg_2_rad = scipy.math.pi / 180
    cos = np.cos(angles * deg_2_rad)
    sin = np.sin(angles * deg_2_rad)
    stretch = (scipy.signal.gaussian(len(exact_vectors), 0.1)) + 1
    stretched_vectors = exact_vectors * stretch
    rotated_vecs1 = [Vector(v.x * c - v.y * s, v.x * s + v.y * c) for (v, c, s) in zip(stretched_vectors, cos, sin)]
    # BEGIN DIRTY
    # do it again
    angles = (scipy.signal.gaussian(len(exact_vectors), 5) * 5) - 0.3 - 2.5
    deg_2_rad = scipy.math.pi / 180
    cos = np.cos(angles * deg_2_rad)
    sin = np.sin(angles * deg_2_rad)
    stretch = (scipy.signal.gaussian(len(exact_vectors), 0.2)) + 1.1
    stretched_vectors = exact_vectors * stretch
    rotated_vecs2 = [Vector(v.x * c - v.y * s, v.x * s + v.y * c) for (v, c, s) in zip(stretched_vectors, cos, sin)]
    ### END DIRTY
    testrotations.plot_vectors("all", [exact_vectors, rotated_vecs1, rotated_vecs2])
