import operator
from functools import reduce
from typing import List, Dict, Tuple, Union, Callable

import numpy
from matplotlib import pyplot
from matplotlib.ticker import FormatStrFormatter
from numpy.core.multiarray import ndarray

from algorithms.particlefilter.track_waypoint_result_extracter import TrackWaypointResultExtracter
from data.database_query import DatabaseQuery
from data.models import TrackGeneratorModel
from misc.measurement import PositionType


def compute_total_error(result: Dict[int, Tuple[List[PositionType], ndarray]]) -> float:
    error: int = 0
    for tuple in result.values():
        position_types = tuple[0]
        ground_truth_index = numpy.nonzero(position_types == PositionType.ground_truth)
        particle_filter_index = numpy.nonzero(position_types == PositionType.particle_filter)
        distances = tuple[1]
        error += distances[ground_truth_index, particle_filter_index]
    return error


def plot_cdf(extracters: List[TrackWaypointResultExtracter], position_type, marker):
    num_of_waypoints = 15
    waypoints = numpy.array(range(num_of_waypoints)) + 1
    distances = reduce(operator.concat, [[extracter.get_distance_between(
        num, position_type, PositionType.ground_truth) for num in waypoints]
        for extracter in extracters])
    distances_sorted = numpy.concatenate(([0], sorted(distances)))
    percentage = numpy.array([(i * 100) / (len(distances_sorted) - 1) for i, _ in enumerate(distances_sorted)])
    percentage = numpy.concatenate((percentage, [100]))
    distances_sorted = numpy.concatenate((distances_sorted, [900]))

    pyplot.plot(distances_sorted, percentage)

    pyplot.gca().xaxis.set_major_formatter(FormatStrFormatter('%d cm'))
    pyplot.gca().yaxis.set_major_formatter(FormatStrFormatter('%d %%'))


def plot_waypoint(models: Dict[TrackGeneratorModel, TrackWaypointResultExtracter]):
    # a_extractor = list(models.values())[0]
    num_of_waypoints = 15  # len(list(a_extractor.positions_by_waypoint))
    results: List[ndarray] = []
    for waypoint in range(num_of_waypoints):
        current_waypoint: ndarray = numpy.zeros(len(models))
        for i, extracter in enumerate(models.values()):
            try:
                current_waypoint[i] = extracter.get_distance_between(waypoint + 1, PositionType.particle_filter,
                                                                     PositionType.ground_truth)
            except:
                pass
        results.append(current_waypoint)

    pyplot.figure()

    bpl = pyplot.boxplot(results, showfliers=True, sym='k.')

    for flier in bpl['fliers']:
        flier.set(marker='o', color='#e7298a', alpha=0.5)

    # draw temporary red and blue lines and use them to create a legend
    # plt.plot([], c='#D7191C', label='Apples')
    # plt.plot([], c='#2C7BB6', label='Oranges')
    # plt.legend()

    # plt.xticks(xrange(0, len(ticks) * 2, 2), ticks)
    # plt.xlim(-2, len(ticks) * 2)
    # plt.ylim(0, 8)
    # plt.tight_layout()
    # plt.savefig('boxcompare.png')


def get_best_model(extracters_by_models: Dict[TrackGeneratorModel, Union[TrackWaypointResultExtracter, List]]):
    results = []
    for current_extracter in extracters_by_models.values():
        results.append(
            current_extracter.get_distance_between_all_average(PositionType.particle_filter, PositionType.ground_truth))
    lowest_error_index = int(numpy.argmin(results))
    return list(extracters_by_models.keys())[lowest_error_index]


def plot_model_error(model: TrackWaypointResultExtracter, type: PositionType, style: str):
    num_of_waypoints = 15  # len(list(model.positions_by_waypoint))
    waypoints = numpy.array(range(num_of_waypoints)) + 1
    points = [model.get_distance_between(i, PositionType.ground_truth, type) for i in waypoints]
    pyplot.plot(waypoints, points)
    pyplot.gca().yaxis.set_major_formatter(FormatStrFormatter('%d cm'))


def plot_error_by_particles(models_and_extracters: List[Tuple[TrackGeneratorModel, TrackWaypointResultExtracter]]):
    particles = [model[0].pf_number_of_particles for model in models_and_extracters]
    points = [model_and_extracter[1].get_distance_between_all_average(PositionType.particle_filter,
                                                                      PositionType.ground_truth) for model_and_extracter
              in models_and_extracters]
    pyplot.plot(particles, points,
                label=str(models_and_extracters[0][0].pdr_deviation_angle_offset) + "° deviation particle filter")
    points = [model_and_extracter[1].get_distance_between_all_average(PositionType.pdr,
                                                                      PositionType.ground_truth) for model_and_extracter
              in
              models_and_extracters]
    pyplot.plot(particles, points, label=str(models_and_extracters[0][0].pdr_deviation_angle_offset) + "° pdr only")


def same_pdr(model: TrackGeneratorModel, other_model: TrackGeneratorModel):
    return model.pdr_average_step_factor == other_model.pdr_average_step_factor and \
           model.pdr_deviation_step_factor == other_model.pdr_deviation_step_factor and \
           model.pdr_average_angle_offset == other_model.pdr_average_angle_offset and \
           model.pdr_deviation_angle_offset == other_model.pdr_deviation_angle_offset and \
           model.pdr_average_step_length == other_model.pdr_average_step_length


def same_particles(model: TrackGeneratorModel, other_model: TrackGeneratorModel):
    return model.pf_number_of_particles == other_model.pf_number_of_particles


def same_settings(model: TrackGeneratorModel, other_model: TrackGeneratorModel):
    return model.pf_alternative_resample_spread == other_model.pf_alternative_resample_spread and \
           model.number_of_tracks == other_model.number_of_tracks and \
           model.pf_sigma == other_model.pf_sigma and \
           model.pf_known_start == other_model.pf_known_start and \
           model.pf_with_bresenham == other_model.pf_with_bresenham and \
           model.pf_neff_threshold == other_model.pf_neff_threshold and \
           model.pdr_average_step_factor == other_model.pdr_average_step_factor and \
           model.pdr_deviation_step_factor == other_model.pdr_deviation_step_factor and \
           model.pdr_average_angle_offset == other_model.pdr_average_angle_offset and \
           model.pdr_deviation_angle_offset == other_model.pdr_deviation_angle_offset and \
           model.pdr_average_step_length == other_model.pdr_average_step_length and \
           model.pf_number_of_particles == other_model.pf_number_of_particles


def merge_models(models: List[TrackGeneratorModel],
                 merge_condition: Callable[[TrackGeneratorModel, TrackGeneratorModel], bool]) -> List[
    List[TrackGeneratorModel]]:
    results: List[List[TrackGeneratorModel]] = []
    checked_indices = []
    indices = [tuple[0] for tuple in enumerate(models)]
    for index1 in indices:
        model = models[index1]
        # Check existing clusters
        for list in results:
            if merge_condition(model, list[0]):
                list.append(model)
                checked_indices.append(index1)
                model = False
                break
        # Create a new cluster
        if model:
            resultings = [model]
            for index2 in range(index1):
                other_model = models[index2]
                if index2 not in checked_indices and merge_condition(model, other_model):
                    checked_indices.append(index2)
                    resultings.append(other_model)
            results.append(resultings)
    return results


def merge_extractors(grouped_models: List[List[TrackGeneratorModel]]):
    return {j[0]: sum([extractors_by_models[k] / len(j) for k in j]) for j in grouped_models if j}


if __name__ == '__main__':
    models: List[TrackGeneratorModel] = TrackGeneratorModel.select()
    # TrackGeneratorModel alway has only one track
    results: Dict[TrackGeneratorModel, float] = {}
    extractors_by_models: Dict[TrackGeneratorModel, TrackWaypointResultExtracter] = {}
    ids = []
    for model in models:
        extracters: List[TrackWaypointResultExtracter] = []
        for generatedTrack in model.generatedtracks:
            extracter = TrackWaypointResultExtracter(DatabaseQuery.track_with_waypoints(generatedTrack.track))
            extracter.compute_waypoint_positions()
            extracter.compute_waypoint_distances()
            extracter.compute_total_distances()
            extracter.compute_total_distances_average()
            # if extracter.get_distance_between_all_average(PositionType.particle_filter, PositionType.ground_truth) > 1000:
            # if generatedTrack.id not in ids:
            # ids.append(track.id)
            # print(str(model.id) + " " + str(generatedTrack.id) + " " + str(generatedTrack.track.id))
            # print(extracter.get_distance_between_all_average(PositionType.particle_filter, PositionType.ground_truth))
            extracters.append(extracter)
        if len(extracters) != 0:
            extractors_by_models[model] = sum(extracters) / len(extracters)
    print(sorted(ids))
        # extractors_by_models[model] = extracters[0]
    # grouped = merge_models(list(extractors_by_models.keys()),
    #                        same_settings)  # <- currently contains every model three times
    # extractors_by_models_merged = merge_extractors(grouped)
    # plot_waypoint(extractors_by_models_merged)
    # best_model = get_best_model(extractors_by_models_merged)
    # plot_model_error(extractors_by_models_merged[best_model], PositionType.particle_filter, 'rs')
    # plot_model_error(extractors_by_models_merged[best_model], PositionType.pdr, 'g^')
    # plot_model_error(extractors_by_models_merged[best_model], PositionType.rssi_trilaterated, 'bo')
    # pyplot.savefig("algorithms/particlefilter/statistics/alternative-resample.png", format='png')
    # pyplot.show()
    # print("Best model id " + str(best_model.id))
    # print("Error is " + str(extractors_by_models_merged[best_model].get_distance_between_all_average(
    #     PositionType.particle_filter, PositionType.ground_truth)))
    # print("Trilateration error is " + str(extractors_by_models_merged[best_model].get_distance_between_all_average(
    #     PositionType.rssi_trilaterated, PositionType.ground_truth)))
    # best_models_same = [l for l in grouped if best_model in l][0]
    # pyplot.figure()
    # plot_cdf([extractors_by_models[model] for model in best_models_same], PositionType.particle_filter, 'rs')
    # plot_cdf([extractors_by_models[model] for model in best_models_same], PositionType.pdr, 'g^')
    # plot_cdf([extractors_by_models[model] for model in best_models_same], PositionType.rssi_trilaterated, 'bo')
    # pyplot.savefig("algorithms/particlefilter/statistics/cdf-alternative-resample.png", format='png')
    # pyplot.show()
    # Plot number of particles performance
    models_by_same_pdr = merge_models(list(extractors_by_models.keys()), same_pdr)
    models_same_pdr_and_particles: List[List[List[TrackGeneratorModel]]] = []
    for models in models_by_same_pdr:
        same_number_of_particles = merge_models(models, same_particles)
        models_same_pdr_and_particles.append(same_number_of_particles)
    for models in models_same_pdr_and_particles:
        print("-")
        # extracters_same_particles = [(m[0], sum([extractors_by_models[model] for model in m]) / len(m)) for m in models]
        extracters = []
        for same_particle_model in models:
            print(" ".join(
                [str(model.pdr_deviation_angle_offset) + " " + str(str(model.pf_number_of_particles)) for model in
                 same_particle_model]))
            distances = [extractors_by_models[m].get_distance_between_all_average(
                PositionType.ground_truth, PositionType.particle_filter) for m in same_particle_model]
            lowest_index = numpy.argmin(distances)
            extracters.append((same_particle_model[0], extractors_by_models[same_particle_model[lowest_index]]))
        extracters_same_particles = [(m[0], sum([extractors_by_models[model] for model in m]) / len(m)) for m in models]
        plot_error_by_particles(extracters_same_particles)
        # plot_error_by_particles(extracters)
    pyplot.legend(loc='upper left')
    pyplot.show()
