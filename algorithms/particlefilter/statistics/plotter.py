import copy
from math import floor
from typing import List

from matplotlib import pyplot, patches

from algorithms.particlefilter.vector import Vector
from data.database_query import DatabaseQuery
from data.models import Location
from misc.measurement import Measurement, PositionType


class Plotter(object):

    def plot_vectors(self, name: str, positions: List[List[Vector]], location: Location):
        # Fetch resources
        # exact_vectors, _ = self.generate_exact_vectors()
        location_image_path = location.get_image_path_display()
        image = pyplot.imread(location_image_path)

        # Create the figure
        figure, axis = pyplot.subplots()
        axis.imshow(image)
        axis.axis('off')
        # Create arrows
        colors = ["xkcd:red", "xkcd:green", "xkcd:blue", "xkcd:mustard", "xkcd:rose", "xkcd:purple"]
        labels = [
            "std deviation 5°, mean 1°",
            "std deviation 10°, mean 2°",
            "std deviation 15°, mean 3°",
            "std deviation 20°, mean 4°",
            "std deviation 25°, mean 5°",
            "std deviation 30°, mean 6°",
        ]
        handles = []
        for (index, current_positions) in enumerate(positions):
            previous_position = current_positions[0]
            if index % 3 == 0:
                generator_index = floor(index / 3)
                color = colors[generator_index]
                handles.append(patches.Patch(color=color, label=labels[generator_index]))
            for position in current_positions[1:]:
                vector = position - previous_position
                axis.arrow(previous_position.x + index * 5, previous_position.y, vector.x, vector.y, linewidth=0.05,
                           color=color,
                           head_width=10,
                           head_length=5)
                # axis.arrow(previous_position.x, previous_position.y, vector.x, vector.y, linewidth=0.5, color='xkcd:red', arrowprops=dict(arrowstyle="->"))
                previous_position = position
        pyplot.legend(handles=handles, loc=3)
        pyplot.figure(figsize=(100, 100), dpi=300)
        pyplot.show()
        figure.savefig(f'location{location.id}-{name}_vectors', dpi=300)


if __name__ == '__main__':
    plotter: Plotter = Plotter()
    tracks = [DatabaseQuery.track_with_waypoints(i) for i in range(136, 154)]
    location = Location.select().where(Location.id == 8).get()
    start = DatabaseQuery.track_with_waypoints(135).waypoints_fetched[0].position_pixel
    pdr_positions_all = []
    for track in tracks:
        measurements: List[Measurement] = Measurement.from_track(track)
        last = copy.deepcopy(start)
        pdr_positions = [last]
        for measurement in measurements:
            if PositionType.pdr in measurement.positions_pixel:
                current_movement = measurement.positions_pixel[PositionType.pdr]
                pdr_positions.append(last + current_movement)
                last += current_movement
        pdr_positions.pop(len(pdr_positions) - 1)
        pdr_positions_all.append(pdr_positions)
    plotter.plot_vectors("pdr-tracks", pdr_positions_all, location)
