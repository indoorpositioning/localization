import copy
from math import sqrt
from typing import List, Dict
from unittest import TestCase

from algorithms.particlefilter.bresenham import Bresenham
from algorithms.particlefilter.vector import Vector


class TestBresenham(TestCase):

    def __init__(self, methodName='runTest'):
        super().__init__(methodName)
        self.bresenham = Bresenham()

    def test_compute_part_vectors1(self):
        vector = Vector(2, 1)
        vectors = self.bresenham.compute_part_vectors(vector)
        assert vectors[0].x == 1 and vectors[0].y == 1
        assert vectors[1].x == 1 and vectors[1].y == 0

    def test_compute_part_vectors2(self):
        vector = Vector(2, 2)
        vectors = self.bresenham.compute_part_vectors(vector)
        assert vectors[0].x == 1 and vectors[0].y == 1

    def test_compute_part_vectors3(self):
        vector = Vector(3, -5)
        vectors = self.bresenham.compute_part_vectors(vector)
        assert vectors[0].x == 1 and vectors[0].y == -1
        assert vectors[1].x == 0 and vectors[1].y == -1
        assert vectors[2].x == 1 and vectors[2].y == -1
        assert vectors[3].x == 0 and vectors[3].y == -1
        assert vectors[4].x == 1 and vectors[4].y == -1
        vector = Vector(3, 5)
        vectors = self.bresenham.compute_part_vectors(vector)
        assert vectors[0].x == 1 and vectors[0].y == 1
        assert vectors[1].x == 0 and vectors[1].y == 1
        assert vectors[2].x == 1 and vectors[2].y == 1
        assert vectors[3].x == 0 and vectors[3].y == 1
        assert vectors[4].x == 1 and vectors[4].y == 1
        vector = Vector(-3, 5)
        vectors = self.bresenham.compute_part_vectors(vector)
        assert vectors[0].x == -1 and vectors[0].y == 1
        assert vectors[1].x == 0 and vectors[1].y == 1
        assert vectors[2].x == -1 and vectors[2].y == 1
        assert vectors[3].x == 0 and vectors[3].y == 1
        assert vectors[4].x == -1 and vectors[4].y == 1
        vector = Vector(5, 3)
        vectors = self.bresenham.compute_part_vectors(vector)
        assert vectors[0].x == 1 and vectors[0].y == 1
        assert vectors[1].x == 1 and vectors[1].y == 0
        assert vectors[2].x == 1 and vectors[2].y == 1
        assert vectors[3].x == 1 and vectors[3].y == 0
        assert vectors[4].x == 1 and vectors[4].y == 1

    def test_compute_part_vectors4(self):
        vector = Vector(0, 2)
        vectors = self.bresenham.compute_part_vectors(vector)
        assert vectors[0].x == 0 and vectors[0].y == 1

    def test__determine_octant(self):
        assert self.bresenham._determine_octant(Vector(2, 1)) == 0
        assert self.bresenham._determine_octant(Vector(1, 2)) == 1
        assert self.bresenham._determine_octant(Vector(-1, 2)) == 2
        assert self.bresenham._determine_octant(Vector(-2, 1)) == 3
        assert self.bresenham._determine_octant(Vector(-2, -1)) == 4
        assert self.bresenham._determine_octant(Vector(-1, -2)) == 5
        assert self.bresenham._determine_octant(Vector(1, -2)) == 6
        assert self.bresenham._determine_octant(Vector(2, -1)) == 7

    def test_compute_part_vectors(self):
        vector_result: Dict[Vector, List[Vector]] = {}
        n = 50
        for x in range(-n, n):
            for y in range(-n, n):
                vector = Vector(x, y)
                result = self.bresenham.compute_part_vectors(vector)
                vector_result[vector] = result
                print(vector)
        max_length = sqrt(2 * 100 ** 2)
        for vector, vectors in vector_result.items():
            mod_vector = copy.deepcopy(vector)
            for i in range(0, 2 * n):
                mod_vector -= vectors[i % len(vectors)]
                if mod_vector == Vector(0, 0):
                    print("Success: " + str(vector))
                    break
            assert mod_vector == Vector(0, 0)
