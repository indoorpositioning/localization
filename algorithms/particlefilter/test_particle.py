from unittest import TestCase

from algorithms.particlefilter.particle import Particle
from algorithms.particlefilter.vector import Vector


class TestParticle(TestCase):
    def test___add__(self):
        particle = Particle(5, 5, 1.0)
        vector = Vector(1, 2)
        particle += vector
        assert particle.x == 6
        assert particle.y == 7
        assert particle.weight == 1.0