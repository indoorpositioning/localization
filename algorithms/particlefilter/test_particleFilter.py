from unittest import TestCase

import numpy as np
from algorithms.particlefilter.generate_test_vectors import PathGenerator

from algorithms.particlefilter.floorplan import Floorplan
from algorithms.particlefilter.floorplan_pool import FloorplanPool
from algorithms.particlefilter.particle import Particle
from algorithms.particlefilter.particle_filter import ParticleFilter
from algorithms.particlefilter.vector import Vector
from algorithms.ranging.ranging import Ranging
from data.models import Track


class TestParticleFilter(TestCase):
    def test_update(self):
        # self.fail()
        pass

    def test_init_particles(self):
        # self.fail()
        pass

    def test__state_update(self):
        floorplan = Floorplan(np.array([
            [False, False, False, False, False],
            [False, True, True, True, False],
            [False, True, True, False, False],
            [False, True, True, True, False],
            [False, False, False, False, False],
        ]), 1, {}, )  # type: Floorplan
        particle_filter = ParticleFilter(floorplan, None, None, None, None, None)
        particle_filter.particles = [Particle(2, 1, 1), Particle(2, 2, 1)]
        particle_filter._state_update(Vector(3, 0))
        assert particle_filter.particles[0].x == 3
        assert particle_filter.particles[0].y == 1
        assert particle_filter.particles[1].x == 2
        assert particle_filter.particles[1].y == 1

    def test__exact_vectors(self):
        self.floorplan_pool = FloorplanPool()
        track_ids_2_test = [102]
        tracks = Track.select().where(Track.id.in_(track_ids_2_test))
        for track in tracks:
            location_id = track.location.id
            particle_filter = ParticleFilter(self.floorplan_pool.get_floorplan(location_id), None, None, None, None,
                                             None)
            rotations = PathGenerator(track.id)
            ranging = Ranging(location_id)
            track_replayer = TrackReplayer(websocket, track, ranging, rotations, particle_filter)
            track_replayer.start()
