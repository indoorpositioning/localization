import copy
import json
from threading import Timer

from algorithms.particlefilter.floorplan_pool import FloorplanPool
from algorithms.particlefilter.particle_filter import ParticleFilter
from algorithms.particlefilter.track_generator import ParticleFilterComputer
from data.models import TrackGeneratorModel, Track, AccessPointSettingsGeneratedTrack, Location
from misc.json_encoder import APIJSONEncoder
from servers.api_server import get_display_location
from servers.websocket import WebSocket


# Location 8:
#   - f8:e9:03:03:20:02 (red)
#   - c0:a0:bb:18:88:66 (green)
#   - c0:a0:bb:18:83:a2 (blue)
#   - c0:a0:bb:18:83:8a (same room, pink)

def send_to_websocket(websocket: WebSocket):
    """
        This method can be adjusted to send json data to the websocket when a connection
        to /test is made.
    Args:
        websocket:
    """
    ap_next_room = 'f8:e9:03:03:20:02'
    ap_upper_room = 'c0:a0:bb:18:83:a2'
    ap_same_room = 'c0:a0:bb:18:83:8a'
    ap_diagonal_room = 'c0:a0:bb:18:88:66'
    floorplan_pool = FloorplanPool()
    location_id = 8
    location = get_display_location(location_id, 3)
    particle_filter = ParticleFilter(floorplan_pool.get_floorplan(location_id), None, None, None, None, None)
    websocket.send_message(json.dumps(location, cls=APIJSONEncoder))
    ranging = {
        ap_next_room: 800,
        ap_upper_room: 1200,
        ap_same_room: 500,
        ap_diagonal_room: 1400
    }
    position = particle_filter.update(ranging)
    measurement = {
        'particles': copy.deepcopy(particle_filter.particles),
        'particleFilterPosition': position,
        'r': {
            ap_next_room: {'cm': ranging[ap_next_room]},
            ap_upper_room: {'cm': ranging[ap_upper_room]},
            ap_same_room: {'cm': ranging[ap_same_room]},
            ap_diagonal_room: {'cm': ranging[ap_diagonal_room]}
        }
    }

    def send_particles():
        print("Send")
        websocket.send_message(json.dumps(measurement, cls=APIJSONEncoder))

    Timer(1, send_particles).start()


def generate_test_track_models():
    def get_template() -> TrackGeneratorModel:
        template_model = TrackGeneratorModel()
        template_model.location = 8
        template_model.route_track_model = Track.get(Track.id == 129)
        template_model.walk_speed_meter_per_sec = 1.4
        template_model.pdr_average_angle_offset = 0
        template_model.pdr_deviation_angle_offset = 20
        template_model.pdr_average_step_length = 70
        template_model.pdr_average_step_factor = 1.05
        template_model.pdr_deviation_step_factor = 0.2
        template_model.random_seed = 7
        template_model.pf_known_start = False
        template_model.number_of_tracks = 1
        template_model.pf_neff_threshold = 1.005
        template_model.pf_number_of_particles = 500
        template_model.pf_with_bresenham = False
        template_model.pf_sigma = 400
        template_model.pf_alternative_resample_spread = 100

        # template_model.save()

        # Ranging device settings
        def create_access_point_settings(average, variance, ranging_device):
            acccess_point_settings = AccessPointSettingsGeneratedTrack()
            acccess_point_settings.average = average
            acccess_point_settings.std_deviation = variance
            acccess_point_settings.ranging_device = ranging_device
            acccess_point_settings.track_generator_model = template_model
            acccess_point_settings.save()

        average_variance_dev_id = [
            (153.31, 229.52, 32),
            (118.97, 205.64, 33),
            (142.85, 229.52, 34),
            (134.95, 237.23, 35),
            (125.67, 202.87, 36),
            (80.01, 169.26, 37),
        ]

        # for average, variance, device_id in average_variance_dev_id:
        #     create_access_point_settings(average, variance, device_id)

        return template_model

    # model: TrackGeneratorModel = get_template()
    # model.save()

    # model = get_template()
    # model.pf_known_start = True
    # model.save()

    # model = get_template()
    # model.pf_known_start = False
    # model.pf_with_bresenham = True
    # model.save()

    # model = get_template()
    # model.pf_known_start = True
    # model.pf_with_bresenham = True
    # model.save()

    # Massive amount of tracks, id > 5
    step_offset = [0]
    step_deviation = [0.2]
    step_factor_av = [1]
    step_angle_dev = [5, 10, 15, 20, 25, 30]
    step_angle_average = range(1, 6 + 1)
    alternative_spread = [100, 200, 300]
    alternative_spread = [0]
    known_start = [True, False]
    known_start = [True]
    with_bresenham = [True]
    routes = range(136, 153 + 1)
    pf_sigma = list(range(50, 700, 100))
    pf_sigma = [133]
    neff = [1.0001, 1.001, 1.002, 1.005, 1.010, 1.1]
    neff = [1.0001, 1.001, 1.01, 1.1]
    neff = [0.5]
    particles = [500, 1000, 1500, 2000, 3000, 5000]
    particles = range(500, 5500, 500)
    number_of_tracks = 5  # 4

    # for so in step_offset:
    #     for step_offsetd in step_deviation:
    #         for sf in step_factor_av:
    #             for sd in step_factor_dev:
    for i, route in enumerate(routes):
        for p in particles:
            for wb in with_bresenham:
                for s in pf_sigma:
                    for n in neff:
                        for ks in known_start:
                            for so in step_offset:
                                for sd in step_deviation:
                                    for sfa in step_factor_av:
                                        deviation_angle = step_angle_dev[int(i / 3)]
                                        average_angle = step_angle_average[int(i / 3)]
                                        for aspr in alternative_spread:
                                            model = get_template()
                                            model.pdr_average_angle_offset = average_angle
                                            model.pdr_deviation_angle_offset = deviation_angle
                                            model.pdr_average_step_factor = sfa
                                            model.pdr_deviation_step_factor = sd
                                            model.pf_alternative_resample_spread = aspr
                                            model.pf_known_start = ks
                                            model.pf_neff_threshold = n
                                            model.pf_number_of_particles = p
                                            model.pf_with_bresenham = wb
                                            model.pf_sigma = s
                                            model.number_of_tracks = number_of_tracks
                                            model.route_track_model = route
                                            model.save()


def generate_tracks(generator_id: int):
    if isinstance(generator_id, int):
        track_generator_model = TrackGeneratorModel \
            .select(TrackGeneratorModel, Track, Location) \
            .where(TrackGeneratorModel.id == generator_id) \
            .join(Track).join(Location).get()
        # .select(TrackGeneratorModel, AccessPointSettingsGeneratedTrack, RangingDevice) \
        # .join(AccessPointSettingsGeneratedTrack).join(RangingDevice).get()
    else:
        track_generator_model = generator_id
    track_generator = ParticleFilterComputer()
    track_generator.track_generator_model = track_generator_model
    track_generator.generate_tracks()


if __name__ == '__main__':
    generate_test_track_models()
