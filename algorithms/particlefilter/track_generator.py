import json
import time
from datetime import datetime
from random import Random
from typing import List, Dict, Tuple

from numpy.random.mtrand import RandomState

from algorithms.particlefilter.floorplan import Floorplan
from algorithms.particlefilter.floorplan_pool import FloorplanPool
from algorithms.particlefilter.particle_filter import ParticleFilter
from algorithms.particlefilter.vector import Vector
from data.database_query import DatabaseQuery
from data.models import TrackGeneratorModel, AccessPointSettingsGeneratedTrack, RangingDevice, GeneratedTrack, Track
from misc.json_encoder import APIJSONEncoder
from misc.measurement import Measurement, PositionType, RangingData
from misc.track_preparer import TrackPreparer

GENERATOR_CLIENT_ID = 18


class ParticleFilterComputer(object):
    """
        Generates tracks with known waypoints and random measurements.
    """
    track_generator_model: TrackGeneratorModel
    seed_measurements_cache = {}

    def __init__(self) -> None:
        self.track_generator_model = TrackGeneratorModel()

    def load_from_id(self, id):
        self.track_generator_model = TrackGeneratorModel.get(TrackGeneratorModel.id == id)

    def generate_tracks(self):
        random_generator: Random = Random()
        random_generator.seed(self.track_generator_model.random_seed)
        seeds = [random_generator.randint(0, 10000) for x in range(int(self.track_generator_model.number_of_tracks))]
        for track_number in range(int(self.track_generator_model.number_of_tracks)):
            # print("Generate " + str(self.track_generator_model.id) + " + save with seed " + str(seeds[track_number]))
            # try:
            track, seconds = self.generate_track_particle_filter(seeds[track_number])
            self.save_generated_track(track, seconds, seeds[track_number])
            # except Exception as e:
            #     print(e)

    def generate_track_particle_filter(self, seed) -> Tuple[Track, float]:
        track_blueprint = self.track_generator_model.route_track_model
        track = Track()
        track.location = self.track_generator_model.location
        track.measurements = track_blueprint.measurements
        DatabaseQuery.track_with_waypoints(track_blueprint)
        start_position = next((wp for wp in track_blueprint.waypoints_fetched if wp.position_number == 1))
        floorplan: Floorplan = FloorplanPool().get_floorplan(8)
        if self.track_generator_model.pf_known_start:
            start_position: Vector = None
        particle_filter: ParticleFilter = ParticleFilter(floorplan,
                                                         float(self.track_generator_model.pdr_deviation_step_factor),
                                                         float(self.track_generator_model.pdr_average_step_factor),
                                                         float(self.track_generator_model.pdr_deviation_angle_offset),
                                                         float(self.track_generator_model.pdr_average_angle_offset),
                                                         float(self.track_generator_model.pdr_average_step_length),
                                                         bool(self.track_generator_model.pf_with_bresenham),
                                                         alternative_resample_ratio=
                                                         self.track_generator_model.pf_alternative_resample_spread,
                                                         start_position=start_position,
                                                         neff_threshold=float(
                                                             self.track_generator_model.pf_neff_threshold),
                                                         sigma_u=int(self.track_generator_model.pf_sigma),
                                                         number_of_particles=int(
                                                             self.track_generator_model.pf_number_of_particles),
                                                         seed=seed)
        track_preparer = TrackPreparer(track, particle_filter=particle_filter)
        start = time.time()
        track_preparer.append_particle_filter(with_particles=False)
        end = time.time()
        particle_filter_seconds = end - start
        track.measurements = json.dumps(track_preparer.get_measurements(), cls=APIJSONEncoder)
        return track, particle_filter_seconds

    def generate_rssi_readings(self, positions: List[Vector], seed: int) -> List[
        Dict[RangingDevice, Dict[RangingData, float]]]:
        ap_settings: List[AccessPointSettingsGeneratedTrack] = self.track_generator_model.access_point_settings
        random = RandomState(seed)
        distances: List[Dict[RangingDevice, Dict[RangingData, float]]] = []
        for i, position in enumerate(positions[:-1]):
            between_position: Vector = (position + positions[i + 1]) / 2
            this_distances = {}
            for ap_setting in ap_settings:
                this_distances[ap_setting.ranging_device] = {}
                distance = abs(between_position - ap_setting.ranging_device)
                error = -1
                while error < 0:
                    error = random.normal(ap_setting.average, ap_setting.std_deviation) * random.choice([-1, 1])
                this_distances[ap_setting.ranging_device][RangingData.measured_pixel] = distance + error
                this_distances[ap_setting.ranging_device][RangingData.measured_cm] = \
                    this_distances[ap_setting.ranging_device][RangingData.measured_pixel] \
                    / self.track_generator_model.location.get_pixels_per_cm()
            distances.append(this_distances)
        return distances

    def generate_timestamps(self, number_of_timestamps: int) -> List[datetime]:
        model = self.track_generator_model
        start_seconds = datetime.now().timestamp()
        seconds_for_step: float = float(model.walk_speed_meter_per_sec * model.pdr_average_step_length / 100)
        return [datetime.fromtimestamp(start_seconds + seconds_for_step * step) for step in range(number_of_timestamps)]

    def merge_to_measurements(self, timestamps: List[datetime], walk_positions: List[Vector],
                              rssi_distances: List[Dict[RangingDevice, Dict[RangingData, float]]],
                              trilaterated_positions: List[Vector], exact_positions: List[Vector]) \
            -> List[Measurement]:
        measurements = []
        timestamp_delta = timestamps[1] - timestamps[0]
        for timestamp, walk_position, rssi_distance, trilaterated_position, exact_position in \
                zip(timestamps, walk_positions, rssi_distances, trilaterated_positions, exact_positions):
            # Ranging later
            measurement = Measurement(timestamp)
            measurement.positions_cm[PositionType.pdr] = walk_position
            measurement.positions_cm[PositionType.ground_truth] = exact_position
            measurements.append(measurement)
            measurement = Measurement(timestamp + timestamp_delta)
            measurement.positions_cm[PositionType.rssi_trilaterated] = trilaterated_position
            measurement.ranging = rssi_distance
            measurements.append(measurement)
        return measurements

    def save_generated_track(self, track: Track, seconds: float, seed: int):
        track.client = GENERATOR_CLIENT_ID
        track.timestamp = datetime.now().timestamp()
        track.save()
        generated_track = GeneratedTrack()
        generated_track.computation_time_seconds = seconds
        generated_track.track = track
        generated_track.random_seed = seed
        generated_track.track_generator = self.track_generator_model
        generated_track.save()
        # print("Saved track " + str(track.id))
