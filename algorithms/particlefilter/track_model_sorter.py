from itertools import groupby
from typing import Any, Dict

from data.models import TrackGeneratorModel


class TrackGeneratorGrouper(object):
    models: Dict[TrackGeneratorModel, Any]

    def __init__(self, models: Dict[TrackGeneratorModel, Any]) -> None:
        self.models = models

    def _group(self, func):
        groups = {}
        data = sorted(self.models, key=func)
        for key, groups in groupby(data, func):
            groups[key] = list(groups)
        return groups

    def sort(self):
        pass
        # self.by_group = self._group(lambda x: x[0].)
