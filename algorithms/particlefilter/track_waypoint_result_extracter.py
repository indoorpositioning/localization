import itertools
from typing import Dict

import numpy
from numpy.core.multiarray import ndarray
from scipy.spatial.distance import pdist

from algorithms.particlefilter.vector import Vector
from data.models import Track
from misc.measurement import Measurement, PositionType


class TrackWaypointResultExtracter(object):
    track: Track
    position_type_order: Dict[PositionType, int] = {type: index for index, type in
                                                    enumerate([PositionType.ground_truth, PositionType.particle_filter,
                                                               PositionType.pdr, PositionType.rssi_trilaterated])}
    position_type_index_dict: Dict[PositionType, Dict[PositionType, int]] = {t: {} for t in position_type_order.keys()}

    for index, combo in enumerate(itertools.combinations(position_type_order.keys(), 2)):
        position_type_index_dict[combo[0]][combo[1]] = index
        position_type_index_dict[combo[1]][combo[0]] = index

    def __init__(self, track: Track = None) -> None:
        self.distances_total: ndarray = []
        self.distances_total_average: ndarray = []
        self.positions_by_waypoint: Dict[int, ndarray] = {}
        self.distances_by_waypoint: Dict[int, ndarray] = {}
        if track is not None:
            self.track = track
            self.track.measurements = Measurement.from_track(track)

    def _vectors_to_positions(self):
        """Adds the start locations and cumulates the vectors to become positions"""
        start_position = self.track.waypoints_fetched[0].position_cm
        last_ground_truth_position = start_position
        last_pdr_position = start_position
        for measurement in self.track.measurements:
            if PositionType.ground_truth in measurement.positions_cm:
                measurement.positions_cm[PositionType.ground_truth] += last_ground_truth_position
                last_ground_truth_position = measurement.positions_cm[PositionType.ground_truth]
            if PositionType.pdr in measurement.positions_cm:
                measurement.positions_cm[PositionType.pdr] += last_pdr_position
                last_pdr_position = measurement.positions_cm[PositionType.pdr]

    def compute_waypoint_positions(self):
        self._vectors_to_positions()
        current_waypoint: int = None
        find_positions = False
        positions_current_waypoint = {}
        for measurement in self.track.measurements:
            if measurement.last_waypoint and measurement.last_waypoint != current_waypoint:
                current_waypoint = measurement.last_waypoint
                positions_current_waypoint = numpy.empty(len(self.position_type_order), dtype=Vector)
                self.positions_by_waypoint[current_waypoint] = positions_current_waypoint
                find_positions = True
            if find_positions:
                for position_type, positions_current_measurement in measurement.positions_cm.items():
                    index = dict.get(self.position_type_order, position_type)
                    if index is not None and not positions_current_waypoint[index]:
                        positions_current_waypoint[index] = positions_current_measurement
                    if all(positions_current_waypoint) >= 4:
                        find_positions = False
        # Remove positions where not all position types are present
        self.positions_by_waypoint = {key: value for key, value in self.positions_by_waypoint.items() if all(value)}

    def compute_waypoint_distances(self):
        for id, position_dict in self.positions_by_waypoint.items():
            self.distances_by_waypoint[id] = pdist(numpy.array([v.elements for v in position_dict]), 'euclidean')

    def compute_total_distances(self):
        self.distances_total = numpy.sum(numpy.array(list(self.distances_by_waypoint.values())), axis=0)

    def compute_total_distances_average(self):
        self.distances_total_average = self.distances_total / len(self.distances_by_waypoint.keys())

    def get_distance_between(self, waypoint_id: int, type1: PositionType, type2: PositionType):
        return self.distances_by_waypoint[waypoint_id][self.position_type_index_dict[type1][type2]]

    def get_distance_between_all(self, type1: PositionType, type2: PositionType):
        return self.distances_total[self.position_type_index_dict[type1][type2]]

    def get_distance_between_all_average(self, type1: PositionType, type2: PositionType):
        return self.distances_total_average[self.position_type_index_dict[type1][type2]]

    def __truediv__(self, other):
        assert isinstance(other, float) or isinstance(other, int)
        extracter = TrackWaypointResultExtracter()

        def dict_divide_each_value(dict, divisor):
            return {k: v / divisor for k, v in dict.items()}

        extracter.positions_by_waypoint = dict_divide_each_value(self.positions_by_waypoint, other)
        extracter.distances_by_waypoint = dict_divide_each_value(self.distances_by_waypoint, other)
        extracter.positions_by_waypoint = dict_divide_each_value(self.positions_by_waypoint, other)
        extracter.distances_total_average = self.distances_total_average / other
        extracter.distances_total = self.distances_total / other
        return extracter

    def __radd__(self, other):
        if other is 0:
            return self
        assert isinstance(other, TrackWaypointResultExtracter)
        extracter = TrackWaypointResultExtracter()

        def add_dicts(this_dict, other_dict):
            return {k: v + other_dict[k] for k, v in this_dict.items() if k in other_dict}

        extracter.distances_by_waypoint = add_dicts(self.distances_by_waypoint, other.distances_by_waypoint)
        extracter.positions_by_waypoint = add_dicts(self.positions_by_waypoint, other.positions_by_waypoint)
        extracter.distances_total_average = self.distances_total_average + other.distances_total_average
        extracter.distances_total = self.distances_total + other.distances_total
        return extracter

    def __add__(self, other):
        return self.__radd__(other)


if __name__ == '__main__':
    pass
