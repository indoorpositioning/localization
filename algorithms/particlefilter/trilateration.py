from typing import List, Dict

from scipy.optimize import minimize

from algorithms.particlefilter.vector import Vector
from data.models import RangingDevice


class Trilateration(object):

    def _mean_squared_error(self, candidate, distances: Dict[RangingDevice, float]):
        error = 0.0
        candidate_vector = Vector.from_elements(candidate)
        for ranging_device, measured_distance in distances.items():
            error += (abs(ranging_device - candidate_vector) - measured_distance) ** 2
        return error / len(distances)

    def ap_distances(self, ap_distances: List[Dict[RangingDevice, float]]) -> List[Vector]:

        results: List[Vector] = []
        for ap_distances_single in ap_distances:
            position = self.ap_distance(ap_distances_single)
            results.append(position)

        return results

    def ap_distance(self, ap_distances_single: Dict[RangingDevice, float], start_position: Vector = None) -> object:
        if ap_distances_single and len(ap_distances_single) > 2:
            if not start_position:
                start_position: Vector = sum(ap_distances_single.keys()) * 1 / len(ap_distances_single)
            result = minimize(
                self._mean_squared_error,
                start_position.elements,
                args=ap_distances_single,
                method='L-BFGS-B',
                options={
                    'ftol': 1e-5,
                    'maxiter': 1e+7
                })
            position = Vector.from_elements(result.x)
        else:
            position = None
        return position
