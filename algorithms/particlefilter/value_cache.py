from typing import Dict, TypeVar

T = TypeVar('T')

"""
    Used to store older distance results needed for triangulation
"""


class RangingValueCache(object):
    cache: Dict[str, T]

    def __init__(self) -> None:
        self.cache = {}

    def cache_and_enhance(self, values: Dict[str, T]):
        self.cache.update(values)
        values.update(self.cache)
