import math
from typing import List, Union, Dict

import numpy


class Vector:
    def __init__(self, x: Union[int, float], y: Union[int, float]) -> None:
        self.elements = numpy.array([x, y])

    @classmethod
    def from_elements(cls, elements: List[Union[int, float]]):
        return cls(elements[0], elements[1])

    @classmethod
    def from_dict(cls, elements: Dict[str, Union[int, float]]):
        return cls(elements['position_x'], elements['position_y'])

    def __add__(self, other):
        return Vector.from_elements(self.elements + other.elements)

    def __radd__(self, other):
        return self if other == 0 else Vector.from_elements(self.elements + other.elements)

    def __sub__(self, other):
        return Vector.from_elements(self.elements - other.elements)

    def __pow__(self, power, modulo=None):
        return (self * self) ** (power - 1)

    def __mul__(self, other: Union[int, float, "Vector"]):
        if isinstance(other, Vector):
            return self.dot_product(other)
        return Vector.from_elements(self.elements * float(other))

    def __rmul__(self, other: Union[int, float, "Vector"]):
        if isinstance(other, Vector):
            return self.dot_product(other)
        return Vector.from_elements(self.elements * other)

    def __div__(self, other: Union[int, float]):
        return Vector.from_elements(self.elements / float(other))

    def __truediv__(self, other: Union[int, float]):
        return Vector.from_elements(self.elements / float(other))

    def __rdiv__(self, other: Union[int, float]):
        return Vector.from_elements(self.elements / other)

    def __iadd__(self, other):
        self.elements += other.elements
        return self

    def __isub__(self, other):
        self.elements -= other.elements
        return self

    def __imul__(self, other: Union[int, float, "Vector"]):
        if isinstance(self.elements[0], (int, numpy.int32, numpy.int64)) and isinstance(other, float):
            self.elements = numpy.round(self.elements.astype(float) * other).astype(int)
        else:
            self.elements *= other
        return self

    def dot_product(self, other: "Vector"):
        return sum(self.elements * other.elements)

    def __abs__(self):
        return math.sqrt(sum(self.elements ** 2))

    def __str__(self):
        return f"x: {self.x}, y: {self.y}"

    def __hash__(self):
        return hash((self.x, self.y))

    def __eq__(self, other):
        return (self.x, self.x) == (other.x, other.y)

    def __ne__(self, other):
        return not (self == other)

    def manhattan_norm(self):
        return abs(self.x) + abs(self.y)

    def rotate(self, radians: float):
        self.elements[0] = self.x * math.cos(radians) - self.y * math.sin(radians)
        self.elements[1] = self.x * math.sin(radians) + self.y * math.cos(radians)

    @property
    def x(self):
        return self.elements[0]

    @property
    def y(self):
        return self.elements[1]

    @x.setter
    def x(self, value: int):
        self.elements[0] = value

    @y.setter
    def y(self, value: int):
        self.elements[1] = value

    def to_int(self):
        self.elements = numpy.array(self.elements, dtype=int)
        return self
