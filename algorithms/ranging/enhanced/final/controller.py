import importlib
from algorithms.ranging.enhanced.final.room import Room
from data.models import RangingDevice, RangingLocation, RangingData
import math
from settings import ranging_settings
import operator
from random import shuffle
from settings import ranging_settings

client_id = 3
evaluation_location_id = 11
location_id = 10
regression_model = 'exponential'
max_training_locations = 100
trilateration_los_weight = 5
min_distance = 0
sample_count = ranging_settings['measurements_per_direction']
min_selection = 4
max_selection = 50
limiter = 400
use_dynamic_model = False
evaluation_ranging_device = 41



class EnhancedRanging:

    @classmethod
    def init(cls):
        cls.room = Room()
        cls.sample = {}
        cls.current_ranging_device = None
        cls.ranging_active = False
        cls.sample_multiplier = 1
        cls.models = {}
        cls.model_data = {}

        # Prepare models
        for model in ranging_settings['active_models']:
            cls.models[model] = {}

            module_ = importlib.import_module('algorithms.ranging.models.' + model)
            class_ = getattr(module_, model[0].upper() + model[1:])
            cls.models[model]['instance'] = class_(location_id=location_id)
            cls.models[model]['instance'].client_id = client_id

        # RangingDevice
        cls.ranging_devices = {}
        for ranging_device in RangingDevice.select().where(RangingDevice.location_id == location_id):
            cls.ranging_devices[ranging_device.id] = {
                'id': ranging_device.id,
                'identifier': ranging_device.identifier,
                'x': int(ranging_device.position_x),
                'y': int(ranging_device.position_y),
                'power_loss': ranging_device.power_loss,
                'path_loss_efficient': ranging_device.path_loss_efficient,
                'location_id': location_id
            }

        # Raging Device Translator for Evaluation
        cls.ranging_device_translator = {}
        cls.ranging_device_forward_translator = {}
        for rd in RangingDevice.select().where(RangingDevice.location_id == evaluation_location_id):
            for main_rd in cls.ranging_devices.values():
                if rd.identifier == main_rd['identifier']:
                    cls.ranging_device_translator[rd.id] = main_rd['id']
                    cls.ranging_device_forward_translator[main_rd['id']] = rd.id
                    break

        # if use_dynamic_model:
        #     cls.create_best_composite_model()
        # else:
        #     cls.create_complete_composite_Model()

    @classmethod
    def update(cls, data):

        # Init
        if not hasattr(cls, 'sample'):
            cls.init()

        if cls.current_ranging_device is None or cls.ranging_active is False:
            return

        # Create full sample
        sample = cls.create_sample(data)

        # If full sample, get position
        if sample:
            position = cls.room.get_position(cls.current_ranging_device, cls.ranging_devices, sample, cls.models, cls.model_data, trilateration_los_weight, client_id)

            if position is not False:
                location_count = cls.room.get_location_count(location_id)

                # If not enough locations yet, just add
                if location_count < max_training_locations:
                    #if cls.has_min_distance_from_locations(position):
                    cls.create_data(position, sample)

                # Else, find one to replace
                else:

                        replacement_id = cls.find_replacement_location_quality(position, sample)
                        #replacement_id = cls.find_replacement_location_proximity(position)
                        #replacement_id = cls.find_replacement_location_mix(position, sample)
                        if replacement_id:
                            query = RangingData.delete().where((RangingData.ranging_location_id == replacement_id) & (RangingData.client_id == client_id))
                            query.execute()

                            query = RangingLocation.delete().where(RangingLocation.id == replacement_id)
                            query.execute()

                            cls.create_data(position, sample)

                cls.create_complete_composite_model()
                if cls.model_data[evaluation_ranging_device]['model_data'] is not False:
                    errors = cls.compute_evaluation_error()
                    keys = []
                    items = []
                    for k, i in errors.items():
                        keys.append(k)
                        items.append(i)
                    print(";" + ';'.join(keys))
                    print(str(location_count+1) + ";" + ';'.join(items))

    @classmethod
    def create_complete_composite_model(cls):
        print('Creating static model')
        cls.model_data = {}

        model_data = cls.models[regression_model]['instance'].create_models(location_id=location_id, client_id=client_id)

        if model_data is not False:
            for rdid, md in model_data.items():
                cls.model_data[rdid] = {
                    'model_data': md,
                    'model': regression_model
                }


    @classmethod
    def create_best_composite_model(cls):
        stats = {}
        cls.model_data = {}

        print('Creating composite model')

        # Ranging Locations
        cls.ranging_locations = {}
        for ranging_location in RangingLocation.select().where(RangingLocation.location_id == location_id):
            cls.ranging_locations[ranging_location.id] = {
                'id': ranging_location.id,
                'x': ranging_location.position_x,
                'y': ranging_location.position_y
            }

        ranging_locations_translator = []
        for ranging_location in cls.ranging_locations.values():
            ranging_locations_translator.append(ranging_location['id'])
        num_locations = len(ranging_locations_translator)

        for model in ranging_settings['active_models']:
            print('- ' + model)
            stats[model] = {}

            data = cls.models[model]['instance'].get_data()

            for selection in range(min_selection, min(max_selection, num_locations)):

                # Prepare stats object
                if selection not in stats[model]:
                    stats[model][selection] = {
                        'overall': {
                            'values': []
                        },
                        'accesspoints': {}
                    }

                # Get combinations
                locations = list(range(num_locations))
                combinations = []
                for i in range(limiter):
                    shuffle(locations)
                    combinations.append(locations[0:selection])

                # For each combination...
                for combination in combinations:

                    location_results = []

                    for ranging_device_id, ranging_locations in data['distances'].items():

                        if ranging_device_id not in cls.model_data:
                            cls.model_data[ranging_device_id] = {
                                'min': 9999,
                                'model_data': None,
                                'model': None
                            }

                        if ranging_device_id not in stats[model][selection]['accesspoints']:
                            stats[model][selection]['accesspoints'][ranging_device_id] = {
                                'values': []
                            }

                        ranging_device_results = []

                        # Prepare ranging locations to be sent to model
                        test_ranging_locations = {}
                        for elm in combination:
                            try:
                                test_ranging_locations[ranging_locations_translator[elm]] = data['distances'][ranging_device_id][ranging_locations_translator[elm]]
                            except KeyError as e:
                                pass

                        # Get model data
                        model_data = cls.models[model]['instance'].compute_data(test_ranging_locations)
                        if model_data is False:
                            continue

                        # Use this model to compare computed distance to each ranging location to read distance and take average
                        for id, ranging_location in data['distances'][ranging_device_id].items():
                            distance = cls.models[model]['instance'].get_distance(ranging_device_id, ranging_location['signal'], client_id, model_data)
                            diff = abs(distance - ranging_location['distance'])

                            ranging_device_results.append(diff)
                            location_results.append(diff)

                        avg = sum(ranging_device_results) / len(ranging_device_results)
                        del ranging_device_results[:]

                        if avg < cls.model_data[ranging_device_id]['min']:
                            cls.model_data[ranging_device_id]['min'] = avg
                            cls.model_data[ranging_device_id]['model_data'] = model_data
                            cls.model_data[ranging_device_id]['model'] = model

        total = 0
        for rd in cls.model_data.values():
            total += rd['min']
        avg = total / len(cls.model_data)
        print("Error dynamic model")
        print("===================")
        print("Main: " + f'{avg:.2f}')
        print("Evaluation: " + str(cls.compute_evaluation_error()))
        print("")

    @classmethod
    def compute_evaluation_error(cls):
        cls.models[regression_model]['instance'].location_id = evaluation_location_id
        data = cls.models[regression_model]['instance'].get_data()
        cls.models[regression_model]['instance'].location_id = location_id

        # errors = []
        # for ranging_device_id, ranging_locations in data['distances'].items():
        #     for id, ranging_location in data['distances'][ranging_device_id].items():
        #         distance = cls.models[cls.model_data[cls.ranging_device_translator[ranging_device_id]]['model']]['instance'].get_distance(ranging_device_id, ranging_location['signal'], client_id, cls.model_data[cls.ranging_device_translator[ranging_device_id]]['model_data'])
        #         diff = abs(distance - ranging_location['distance'])
        #         errors.append(diff)

        errors = {}
        for id, ranging_location in data['distances'][cls.ranging_device_forward_translator[evaluation_ranging_device]].items():
            distance = cls.models[cls.model_data[evaluation_ranging_device]['model']]['instance'].get_distance(evaluation_ranging_device, ranging_location['signal'], client_id, cls.model_data[evaluation_ranging_device]['model_data'])
            diff = abs(distance - ranging_location['distance'])
            #errors.append(diff)
            errors[str(id)] = str(diff)
        return errors

        #avg = sum(errors) / len(errors)
        #return avg

    @classmethod
    def has_min_distance_from_locations(cls, position):
        for ranging_location in RangingLocation.select().where(RangingLocation.location_id == location_id):
            distance = math.sqrt(math.pow(ranging_location.position_x - position[0], 2) + math.pow(ranging_location.position_y - position[1], 2))
            distance_cm = cls.room.px2cm(distance, location_id)
            if distance_cm < min_distance:
                return False
        return True

    @classmethod
    def get_error(cls, ranging_locations, ranging_device, model_data):
        diff = []
        for id, ranging_location in ranging_locations:
            distance = cls.models[cls.model_data[ranging_device['id']]['model']]['instance'].get_distance(ranging_device['id'], ranging_location['signal'], client_id, model_data)
            diff.append(abs(distance - ranging_location['distance']))
        return sum(diff) / len(diff)

    @classmethod
    def update_ranging_device(cls, ranging_device_id):
        if ranging_device_id == cls.current_ranging_device:
            cls.current_ranging_device = None
        else:
            cls.current_ranging_device = int(ranging_device_id)

    @classmethod
    def ranging_control(cls, action):
        if action == 'Start':
            cls.ranging_active = True
        else:
            cls.ranging_active = False

    @classmethod
    def create_sample(cls, data):


        def get_sample_count():
            count = 0
            for mac in cls.sample.keys():
                if count == 0:
                    count = len(cls.sample[mac])
                else:
                    if len(cls.sample[mac]) != count:
                        return False
            return count

        # Determine multiplier
        sc = get_sample_count()
        if sc != False:
            if sc == 0:
                cls.sample_multiplier = 1
            elif sc == sample_count:
                cls.sample_multiplier = 2
            elif sc == 2*sample_count:
                cls.sample_multiplier = 3
            elif sc == 3*sample_count:
                cls.sample_multiplier = 4

        # Add samples
        for entry in data:
            for mac in entry['r'].keys():
                if mac not in cls.sample:
                    cls.sample[mac] = []
                if len(cls.sample[mac]) < cls.sample_multiplier*sample_count:
                    cls.sample[mac].append(entry['r'][mac][0])

        if len(cls.sample) == len(cls.ranging_devices):
            sc = get_sample_count()
            if sc == 4*sample_count:
                for mac in cls.sample.keys():
                    cls.sample[mac] = sum(cls.sample[mac]) / len(cls.sample[mac])
                ret = cls.sample
                cls.sample = {}
                cls.ranging_active = False
                cls.sample_multiplier = 1
                return ret
            if sc == 3*sample_count or sc == 2*sample_count or sc == sample_count:
                cls.ranging_active = False

        return False

    @classmethod
    def create_data(cls, position, sample):
        rl = RangingLocation(position_x=position[0], position_y=position[1], location_id=location_id)
        rl.save()
        for rd in cls.ranging_devices.values():
            RangingData.create(ranging_device_id=rd['id'], ranging_location_id=rl.id, client_id=client_id, value=sample[rd['identifier']])

        # if use_dynamic_model:
        #     cls.create_best_composite_model()
        # else:
        #     cls.create_complete_composite_Model()

    @classmethod
    def find_replacement_location_mix(cls, position, sample):
        proximity = cls.find_replacement_location_proximity(position, True)
        quality = cls.find_replacement_location_quality(position, sample, True)

        ranks = {}
        for pos in range(0, len(quality)):
            if proximity[pos] not in ranks:
                ranks[proximity[pos]] = 0
            if quality[pos] not in ranks:
                ranks[quality[pos]] = 0
            ranks[proximity[pos]] += pos
            ranks[quality[pos]] += pos

        sorted = sorted(ranks.values(), key=operator.itemgetter(1))
        return sorted[0]


    @classmethod
    def find_replacement_location_proximity(cls, position, return_list = False):
        min_id = 0
        min_distance = 99999

        distances = {}
        for ranging_location in RangingLocation.select().where(RangingLocation.location_id == location_id):
            distance = math.sqrt(math.pow(float(ranging_location.position_x) - position[0], 2) + math.pow(float(ranging_location.position_y) - position[1], 2))
            distances[ranging_location.id] = distance

            if distance < min_distance:
                min_distance = distance
                min_id = ranging_location.id

        if return_list is False:
            return min_id
        else:
            sorted_errors = sorted(distances.values(), key=operator.itemgetter(1))
            return sorted_errors


    @classmethod
    def find_replacement_location_quality(cls, position, sample, return_list=False):

        errors = {}
        for ranging_device in cls.ranging_devices.values():

            data = cls.models[cls.model_data[ranging_device['id']]['model']]['instance'].get_data()

            ranging_location_ids = list(data['distances'][ranging_device['id']].keys())

            # Create current/old model
            model_data = cls.models[cls.model_data[ranging_device['id']]['model']]['instance'].compute_data(data['distances'][ranging_device['id']])
            base_error = cls.get_error(data['distances'][ranging_device['id']].items(), ranging_device, model_data)

            # Create all alternative models
            for excluding_ranging_location_id in ranging_location_ids:
                test_ranging_locations = {}
                for ranging_location_id, ranging_location_data in data['distances'][ranging_device['id']].items():
                    if ranging_location_id != excluding_ranging_location_id:
                        test_ranging_locations[ranging_location_id] = ranging_location_data

                distance = cls.room.px2cm(math.sqrt(math.pow(position[0]+ranging_device['x'], 2) + math.pow(position[1]+ranging_device['y'], 2)), location_id) / 100

                test_ranging_locations['new'] = {
                    'signal': sample[ranging_device['identifier']],
                    'distance': distance
                }

                model_data = cls.models[cls.model_data[ranging_device['id']]['model']]['instance'].compute_data(test_ranging_locations)
                test_error = cls.get_error(data['distances'][ranging_device['id']].items(), ranging_device, model_data)

                if excluding_ranging_location_id not in errors:
                    errors[excluding_ranging_location_id] = 0
                errors[excluding_ranging_location_id] += abs(base_error - test_error)

        if return_list is False:
            max_error_id = max(errors, key=errors.get)
            if errors[max_error_id] > 0:
                return max_error_id

        else:
            sorted_errors = sorted(errors.values(), key=operator.itemgetter(1))
            return sorted_errors

        return False

    @classmethod
    def get_ranging_status(cls):
        data = []
        for mac in cls.sample.keys():

            for rd in cls.ranging_devices.values():
                if rd['identifier'] == mac:
                    id = rd['identifier']
                    break

            data.append({
                "ranging_device_id": id,
                "ranging_device_identifier": mac,
                "avg": float(sum(cls.sample[mac]) / len(cls.sample[mac])),
                "count": float(len(cls.sample[mac]))
            })
        return data

    @classmethod
    def test_data(cls):
        cls.init()
        data_main = cls.models[regression_model]['instance'].get_data()

        cls.regression.location_id = evaluation_location_id
        data_main = cls.regression.get_data()
        cls.regression.location_id = location_id

        for did in data_main['distances'].keys():
            for lid in data_main['distances'][did].keys():
                print(str(data_main['distances'][did][lid]['signal']) + "\t" + str(data_main['distances'][did][lid]['distance']))



#enhanced_ranging = EnhancedRanging()
#enhanced_ranging.test_data()

'''
Settings
--------

- TL replacement method: first worse, or worst
- Trilateration weight for LOS ranging device and others



- Build functions
    Get position
        - Does trilateration
            => [Get best ranging devices for triangulation]
        - If no LOS just fakes it
        - Returns position
    Get replacement
    Find TL to replace
        - If TL count < total amount: return false
        - Iterate through each existing TL
            - Replace one with largest improvement for *all* ranging devices

     model = Compute model
'''