from scipy.optimize import minimize
from itertools import combinations
import numpy as np
from numpy import *
from data.models import Location, RangingLocation



def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)


def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


def get_trilateration_ranging_devices(current_ranging_device_id, ranging_devices):
    pairs = []
    max_distance = 0
    min_distance = math.inf
    max_angle = 0
    min_angle = math.inf
    for pair in combinations([key for key in ranging_devices.keys() if key != current_ranging_device_id], 2):
        x1 = ranging_devices[current_ranging_device_id]['x'] - ranging_devices[pair[0]]['x']
        y1 = ranging_devices[current_ranging_device_id]['y'] - ranging_devices[pair[0]]['y']
        x2 = ranging_devices[current_ranging_device_id]['x'] - ranging_devices[pair[1]]['x']
        y2 = ranging_devices[current_ranging_device_id]['y'] - ranging_devices[pair[1]]['y']

        angle = angle_between((x1, y1), (x2, y2))
        distance1 = math.sqrt(math.pow(x1, 2)+math.pow(y1, 2))
        distance2 = math.sqrt(math.pow(x2, 2)+math.pow(y2, 2))
        pairs.append({
            'ranging_device_1': {
                'identifier': pair[0],
                'id': ranging_devices[pair[0]]['id'],
                'x': ranging_devices[pair[0]]['x'],
                'y': ranging_devices[pair[0]]['y'],
                'distance': distance1
            },
            'ranging_device_2': {
                'identifier': pair[1],
                'id': ranging_devices[pair[1]]['id'],
                'x': ranging_devices[pair[1]]['x'],
                'y': ranging_devices[pair[1]]['y'],
                'distance': distance2
            },
            'angle': angle
        })
        max_distance = max(max_distance, distance1)
        min_distance = min(min_distance, distance1)
        max_distance = max(max_distance, distance2)
        min_distance = min(min_distance, distance2)
        max_angle = max(max_angle, angle)
        min_angle = min(min_angle, angle)

    max_quality = 0
    best_pair = None
    for pair in pairs:
        distance_ratio_1 = 1 - (pair['ranging_device_1']['distance'] - min_distance) / (max_distance - min_distance)
        distance_ratio_2 = 1 - (pair['ranging_device_2']['distance'] - min_distance) / (max_distance - min_distance)
        angle_ratio = 1 - (abs((math.pi / 2) - pair['angle']) / (math.pi / 2))
        quality = 0.2*distance_ratio_1 + 0.2*distance_ratio_2 + 0.6*angle_ratio
        pair['distance_ratio_1'] = distance_ratio_1
        pair['distance_ratio_2'] = distance_ratio_2
        pair['angle_ratio'] = angle_ratio
        pair['quality'] = quality
        if quality > max_quality:
            max_quality = quality
            best_pair = pair

    return [
        best_pair['ranging_device_1']['id'],
        best_pair['ranging_device_2']['id']
    ]


def trilateration(anchors):

    def mean_squared_error(x, distances):
        mse = 0.0
        for ap in distances:
            dist2 = math.sqrt(math.pow(x[0] - ap['x'], 2) + math.pow(x[1] - ap['y'], 2))
            mse += ap['weight'] * math.pow(ap['distance'] - dist2, 2)
        return mse / len(distances)

    # Get starting position (average of anchors)
    average_position = [0, 0]
    for anchor in anchors:
        average_position[0] += anchor['x']
        average_position[1] += anchor['y']
    average_position[0] /= len(anchors)
    average_position[1] /= len(anchors)

    result = minimize(
        mean_squared_error,
        average_position,
        args=anchors,
        method='L-BFGS-B',
        options={
            'ftol': 1e-5,
            'maxiter': 1e+7
        })

    if result.success:
        return result.x
    else:
        return False


class Room:

    def __init__(self):
        self.last_position = None

    def get_position(self, current_ranging_device_id, ranging_devices, sample, models, model_data, trilateration_los_weight, client_id):

        if not current_ranging_device_id:
            return False

        trilateration_ranging_devices = get_trilateration_ranging_devices(current_ranging_device_id, ranging_devices)

        anchors = [{
            'distance': self.get_los_distance(ranging_devices[current_ranging_device_id], sample[ranging_devices[current_ranging_device_id]['identifier']]),
            'x': ranging_devices[current_ranging_device_id]['x'],
            'y': ranging_devices[current_ranging_device_id]['y'],
            'weight': trilateration_los_weight
        }]

        for ranging_device_id in trilateration_ranging_devices:
            #distance = models[model_data[ranging_device_id]['model']]['instance'].get_distance(ranging_device_id, sample[ranging_devices[ranging_device_id]['identifier']], client_id, model_data[ranging_device_id]['model_data'])
            #if not distance:
            distance = self.get_los_distance(ranging_devices[ranging_device_id], sample[ranging_devices[ranging_device_id]['identifier']])
            anchors.append({
                'distance': distance,
                'x': ranging_devices[ranging_device_id]['x'],
                'y': ranging_devices[ranging_device_id]['y'],
                'weight': 1
            })

        position = trilateration(anchors)

        return position

        # Only return if certain distance to last one
        # Doesn't matter now that's it's just static
        '''
        if self.last_position is not None:
            distance_to_last_px = math.sqrt(math.pow(position[0] + self.last_position[0], 2) + math.pow(position[1] + self.last_position[1], 2))
            distance_to_last_cm = self.px2cm(distance_to_last_px, ranging_devices[current_ranging_device_id]['location_id'])
        else:
            distance_to_last_cm = 9999

        if distance_to_last_cm > 150:
            self.last_position = position
            return position
        else:
            return False
        '''

    def get_location_count(self, location_id):
        return RangingLocation.select().where(RangingLocation.location_id == location_id).count()

    def get_los_distance(self, ranging_device, signal):
        distance_meter = (math.pow(10, ((float(ranging_device['power_loss']) - signal)/(10*float(ranging_device['path_loss_efficient'])))))
        distance_px = self.cm2px(distance_meter*100, ranging_device['location_id'])
        return distance_px


    def get_conversion_ratio(self, location_id):
        location = Location.get(Location.id == location_id)
        x_dist = location.map_scale_end_x - location.map_scale_start_x
        y_dist = location.map_scale_end_y - location.map_scale_start_y
        distance_px = math.sqrt(pow(x_dist, 2) + pow(y_dist, 2))
        ratio = float(location.map_width) / distance_px
        return ratio

    def cm2px(self, value, location_id):
        ratio = self.get_conversion_ratio(location_id)
        px = value / ratio
        return px

    def px2cm(self, value, location_id):
        ratio = self.get_conversion_ratio(location_id)
        cm = value * ratio
        return cm