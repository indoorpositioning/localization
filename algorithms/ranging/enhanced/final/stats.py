import importlib
from algorithms.ranging.enhanced.final.room import Room
from data.models import RangingDevice, RangingLocation, RangingData
import math
from settings import ranging_settings
import operator
from random import shuffle
from settings import ranging_settings
import itertools
from algorithms.ranging.enhanced.final.stats_helper import *


'''

define trilat aps
add new location
    create model [in various ways]
    get error

'''


client_id = 3
evaluation_location_id = 11
location_id = 10
regression_model = 'exponential'
trilateration_los_weight = 5
limiter = 10
local_evaluation_ranging_device = 41
remote_evaluation_ranging_device = 47
local_ranging_device = 39

########
# Init #
########

# Regression
module_ = importlib.import_module('algorithms.ranging.models.' + regression_model)
class_ = getattr(module_, regression_model[0].upper() + regression_model[1:])
regression = class_(location_id=location_id)
regression.client_id = client_id

# RangingDevices
ranging_devices = {}
for ranging_device in RangingDevice.select().where(RangingDevice.location_id == location_id):
    ranging_devices[ranging_device.id] = {
        'id': int(ranging_device.id),
        'identifier': ranging_device.identifier,
        'x': int(ranging_device.position_x),
        'y': int(ranging_device.position_y),
        'power_loss': ranging_device.power_loss,
        'path_loss_efficient': ranging_device.path_loss_efficient,
        'location_id': location_id
    }

# RangingLocations
ranging_locations = []
for ranging_location in RangingLocation.select().where(RangingLocation.location_id == location_id):
    ranging_locations.append({
        'id': ranging_location.id,
        'x': int(ranging_location.position_x),
        'y': int(ranging_location.position_y)
    })

# Create trilateration_combinations
trilat_devices = []
for rd in ranging_devices.values():
    if rd['id'] != local_evaluation_ranging_device and rd['id'] != local_ranging_device:
        trilat_devices.append(rd['id'])
trilat_combinations = []
for comb in itertools.combinations(trilat_devices, 2):
    trilat_combinations.append(list(comb))
for comb in itertools.combinations(trilat_devices, 3):
    trilat_combinations.append(list(comb))

# Create path through room (= order of ranging locations)
ranging_location_combinations = []
ids = list(range(len(ranging_locations)))
for i in range(limiter):
    shuffle(ids)
    ranging_location_combinations.append(ids)

#########
# Start #
#########
stats = {
    'by_trilateration': [],
    'by_n_measurements': {}
}
for trilat_combo in trilat_combinations:
    trilat_errors = []
    for ranging_location_combo in ranging_location_combinations:
        raw_data = {}
        for ranging_location_id in ranging_location_combo:
            signal, distance = get_location_metrics(trilat_combo, ranging_location_id, local_ranging_device, local_evaluation_ranging_device, ranging_devices, ranging_locations, client_id, trilateration_los_weight)
            raw_data[ranging_location_id] = {
                'signal': signal,
                'distance': distance
            }
            model_data = regression.compute_data(raw_data)
            error = get_model_error(model_data, regression, evaluation_location_id, remote_evaluation_ranging_device, client_id)
            trilat_errors.append(error)
            if len(raw_data) not in stats['by_n_measurements']:
                stats['by_n_measurements'][len(raw_data)] = []
            stats['by_n_measurements'][len(raw_data)].append(error)

    stats['by_trilateration'].append(sum(trilat_errors) / len(trilat_errors))

for n in stats['by_n_measurements'].keys():
    stats['by_n_measurements'][n] = sum(stats['by_n_measurements'][n]) / len(stats['by_n_measurements'][n])

print(stats)

'''
development over n measurements
difference between rang loc combo
difference between trilat combo
'''