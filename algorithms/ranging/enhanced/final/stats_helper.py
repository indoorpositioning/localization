from data.models import RangingDevice, RangingLocation, RangingData, fn, Location
import math
from scipy.optimize import minimize


def get_los_distance(ranging_device, signal):
    distance_meter = (math.pow(10, ((float(ranging_device['power_loss']) - signal)/(10*float(ranging_device['path_loss_efficient'])))))
    distance_px = cm2px(distance_meter*100, ranging_device['location_id'])
    return distance_px


def get_conversion_ratio(location_id):
    location = Location.get(Location.id == location_id)
    x_dist = location.map_scale_end_x - location.map_scale_start_x
    y_dist = location.map_scale_end_y - location.map_scale_start_y
    distance_px = math.sqrt(pow(x_dist, 2) + pow(y_dist, 2))
    ratio = float(location.map_width) / distance_px
    return ratio


def cm2px(value, location_id):
    ratio = get_conversion_ratio(location_id)
    px = value / ratio
    return px


def px2cm(value, location_id):
    ratio = get_conversion_ratio(location_id)
    cm = value * ratio
    return cm


def trilateration(anchors):

    def mean_squared_error(x, distances):
        mse = 0.0
        for ap in distances:
            dist2 = math.sqrt(math.pow(x[0] - ap['x'], 2) + math.pow(x[1] - ap['y'], 2))
            mse += ap['weight'] * math.pow(ap['distance'] - dist2, 2)
        return mse / len(distances)

    # Get starting position (average of anchors)
    average_position = [0, 0]
    for anchor in anchors:
        average_position[0] += anchor['x']
        average_position[1] += anchor['y']
    average_position[0] /= len(anchors)
    average_position[1] /= len(anchors)

    result = minimize(
        mean_squared_error,
        average_position,
        args=anchors,
        method='L-BFGS-B',
        options={
            'ftol': 1e-5,
            'maxiter': 1e+7
        })

    if result.success:
        return result.x
    else:
        return False


def get_location_metrics(trilat_combo, ranging_location_id, local_ranging_device, evaluation_ranging_device, ranging_devices, ranging_locations, client_id, trilateration_los_weight):

    # Get average signal to each access point
    data = {}
    for d in RangingData \
            .select(RangingData.ranging_device_id, RangingData.ranging_location_id,
                    fn.Avg(RangingData.value).alias('avg')) \
            .where((RangingData.ranging_location_id == ranging_locations[ranging_location_id]['id']) & (RangingData.client_id == client_id)) \
            .group_by(RangingData.ranging_device_id, RangingData.ranging_location_id):
        data[d.ranging_device_id] = float(d.avg)

    # Prepare data for trilateration
    anchors = [{
        'distance': get_los_distance(ranging_devices[local_ranging_device], data[local_ranging_device]),
        'x': ranging_devices[local_ranging_device]['x'],
        'y': ranging_devices[local_ranging_device]['y'],
        'weight': trilateration_los_weight
    }]

    for ranging_device_id in trilat_combo:
        anchors.append({
            'distance': get_los_distance(ranging_devices[ranging_device_id], data[ranging_device_id]),
            'x': ranging_devices[ranging_device_id]['x'],
            'y': ranging_devices[ranging_device_id]['y'],
            'weight': 1
        })

    # Trilateration
    position = trilateration(anchors)

    # Distance to evaluation ranging device
    distance = math.sqrt((ranging_devices[evaluation_ranging_device]['x']-position[0])**2 + (ranging_devices[evaluation_ranging_device]['y']-position[1])**2)

    return data[evaluation_ranging_device], distance


def get_model_error(model_data, regression, evaluation_location_id, evaluation_ranging_device, client_id):

    if model_data is False:
        return False

    # Get data
    data = RangingLocation \
        .select(
            RangingLocation.position_x,
            RangingLocation.position_y,
            RangingLocation.id,
            RangingData.ranging_location_id.alias('ranging_location_id'),
            RangingData.ranging_device_id.alias('ranging_device_id'),
            fn.Avg(RangingData.value).alias('avg')
        ) \
        .join(RangingData) \
        .where(RangingData.ranging_device_id == evaluation_ranging_device) \
        .group_by(RangingLocation.id, RangingData.ranging_device_id)

    room = {}
    for d in data:
        room[d.id] = {
            'x': int(d.position_x),
            'y': int(d.position_y),
            'id': int(d.id),
            'signal': float(d.avg)
        }

    evaluation_ranging_device_raw = RangingDevice.get(RangingDevice.id == evaluation_ranging_device)
    evaluation_ranging_device = {
        'id': int(evaluation_ranging_device_raw.id),
        'x': int(evaluation_ranging_device_raw.position_x),
        'y': int(evaluation_ranging_device_raw.position_y)
    }

    # Compute error for each location
    errors = []
    for ranging_location in room.values():
        real_distance_cm = px2cm(math.sqrt((evaluation_ranging_device['x'] - ranging_location['x'])**2 + (evaluation_ranging_device['y'] - ranging_location['y'])**2), evaluation_location_id)
        computed_distance_cm = px2cm(regression.get_distance(evaluation_ranging_device['id'], ranging_location['signal'], client_id, model_data), evaluation_location_id)
        error = abs(computed_distance_cm - real_distance_cm)
        errors.append(error)

    final_error = sum(errors) / len(errors)
    return final_error
