from data.models import RangingLocation, RangingData, RangingDevice
from settings import ranging_settings
import importlib

client_id = 3
location_source = 11
location_target = 10
ranging_device_source = 45
ranging_device_target = 39
ranging_locations_source = [165,163,164,168,166,167,169,170,172,171]

# Delete everything
ranging_location_ids = []
for rl in RangingLocation.select().where(RangingLocation.location_id == location_target):
    ranging_location_ids.append(rl.id)

for id in ranging_location_ids:

    query = RangingLocation.delete().where(RangingLocation.id == id)
    query.execute()

    query = RangingData.delete().where(RangingData.ranging_location_id == id)
    query.execute()

# Copy locations and their training locations

# First, get a translation dict from old to new ranging_devices - used to match ranging_data
ranging_device_translation = {}
source_ranging_devices = {}
for ranging_device_source in RangingDevice.select().where(RangingDevice.location_id == location_source):
    source_ranging_devices[ranging_device_source.identifier] = ranging_device_source.id

for ranging_device_target in RangingDevice.select().where(RangingDevice.location_id == location_target):
    ranging_device_translation[source_ranging_devices[ranging_device_target.identifier]] = ranging_device_target.id

# Then, do stuff...
location_translation = {}
for ranging_location_id in ranging_locations_source:
    ranging_location = RangingLocation.get(RangingLocation.id == ranging_location_id)

    rl = RangingLocation(position_x=ranging_location.position_x, position_y=ranging_location.position_y, location_id=location_target)
    rl.save()

    location_translation[rl.id] = ranging_location_id

    ranging_datas = []
    for ranging_data in RangingData.select().where((RangingData.ranging_location_id == ranging_location_id) & (RangingData.client_id == client_id)):
        ranging_datas.append({
            'ranging_device_id': ranging_device_translation[ranging_data.ranging_device_id],
            'ranging_location_id': rl.id,
            'client_id': client_id,
            'value': ranging_data.value
        })

    for data in ranging_datas:
        RangingData.create(ranging_device_id=data['ranging_device_id'],
                           ranging_location_id=data['ranging_location_id'],
                           client_id=data['client_id'],
                           value=data['value'])

# Check data
source_data = {}
destination_data = {}
for source_ranging_device_id in source_ranging_devices.values():
    if source_ranging_device_id not in source_data:
        source_data[source_ranging_device_id] = {}
    for d in RangingData.select().where(RangingData.ranging_device_id == source_ranging_device_id):
        if d.ranging_location_id not in source_data[source_ranging_device_id]:
            source_data[source_ranging_device_id][d.ranging_location_id] = 0
        source_data[source_ranging_device_id][d.ranging_location_id] += d.value

    if source_ranging_device_id not in destination_data:
        destination_data[source_ranging_device_id] = {}
    for d in RangingData.select().where(RangingData.ranging_device_id == ranging_device_translation[source_ranging_device_id]):
        if location_translation[d.ranging_location_id] not in destination_data[source_ranging_device_id]:
            destination_data[source_ranging_device_id][location_translation[d.ranging_location_id]] = 0
        destination_data[source_ranging_device_id][location_translation[d.ranging_location_id]] += d.value

    print(source_ranging_device_id)
    for lid in source_data[source_ranging_device_id].keys():
        print("   " + str(lid) + ": " + str(source_data[source_ranging_device_id][lid]) + " : " + str(destination_data[source_ranging_device_id][lid]))


for model in ranging_settings['active_models']:
    module_ = importlib.import_module('algorithms.ranging.models.' + model)
    class_ = getattr(module_, model[0].upper() + model[1:])
    instance = class_()
    instance.create_models(location_id=location_target, client_id=client_id)
