from data.models import RangingLocation, RangingData

location_id = 10

ranging_location_ids = []
for rl in RangingLocation.select().where(RangingLocation.location_id == location_id):
    ranging_location_ids.append(rl.id)

for id in ranging_location_ids:

    query = RangingLocation.delete().where(RangingLocation.id == id)
    query.execute()

    query = RangingData.delete().where(RangingData.ranging_location_id == id)
    query.execute()


print("done")