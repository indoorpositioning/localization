import importlib
from algorithms.ranging.enhanced.initial.room import Room
from data.models import RangingDevice

device_id = 3
location_id = 5
los_threshold = -30
regression_model = 'linear'

#ranging_ratio = RangingRatio(device_id=device_id, location_id=location_id)


class EnhancedRanging:

    def __init__(self):
        '''
         load regression
         load small version of image, as well as conversion
        '''

        # Load regression model
        module_ = importlib.import_module('algorithms.ranging.models.' + regression_model)
        class_ = getattr(module_, regression_model[0].upper() + regression_model[1:])
        self.regression = class_(location_id=location_id)

        # Prepare room
        self.room = Room(location_id=location_id)

        # RangingDevice Conversion from identifier to id
        self.ranging_devices = {}
        for ranging_device in RangingDevice.select().where(RangingDevice.location_id == location_id):
            self.ranging_devices[ranging_device.identifier] = {
                'id': ranging_device.id,
                'x': ranging_device.position_x,
                'y': ranging_device.position_y
            }

    def update(self, signal):

        signal = {
            'c0:a0:bb:18:83:a2': -45,
            'c0:a0:bb:18:88:66': -50,
            'c0:a0:bb:18:83:b0': -60,
            'c0:a0:bb:18:83:8a': -52,
            'f8:e9:03:03:20:02': -60,
            'c0:a0:bb:18:83:90': -70
        }

        position, update = self.room.get_position(signal=signal,
                                                  device_id=device_id,
                                                  regression=self.regression,
                                                  ranging_devices=self.ranging_devices)

        print(position)
        print(update)

        # Demo
        signal = {
            'c0:a0:bb:18:83:a2': -48,
            'c0:a0:bb:18:88:66': -50,
            'c0:a0:bb:18:83:b0': -60,
            'c0:a0:bb:18:83:8a': -52,
            'f8:e9:03:03:20:02': -50,
            'c0:a0:bb:18:83:90': -70
        }

        position, update = self.room.get_position(signal=signal,
                                                  device_id=device_id,
                                                  regression=self.regression,
                                                  ranging_devices=self.ranging_devices,
                                                  method='last_position')
        print(position)
        print(update)

        '''
        
        Process
            - load
                regression model
            - perform trilateration(signal)
                try to get distances based on model
                return (x,y) with error, or -1
            - room.get_current_room(signal, position from trilateration)
                checks based on available data
                returns -1 or rangingdevice_id
                    compares room by trying to find straight white line from rangingdevice to position
            - 
    
        Differentiate LOS/NLOS
            - Initially: High signal
            - Later: Add quality of location
    
        Find location:
            - If NLOS: drop
                ( - Before regression: Ratio | Don't update model )
                ( - Regression: Trilateration | Don't update model )
            - If LOS:
                - Try trilateration. Determine usage based on quality of approximation and closeness to curve.
                - If unavailable/insufficient / before regression:
                    - No data: Ratio and (Curve and ratio) | Don't update model
                    - Data: Movement and (Curve and ratio) | Update model
                - Else: Regression
                    - Curve combined with Regression based on three[?] closest points | Update model
    
        Improve regression
            - Should a location be replaced?
                Only if we already have enough
            - Which location to replace?
                Oldest one
            - Which relations to replace?
                All
                    - Because even those used for trilateration will be improved by LOS curve
                    - Because the quailty of a new point needs to be assessed by taking all ranging devices into consideration
            - How to take old data into account?
                Only indirectly where it's used for "trilateration". Beyond this it would be arbitrary.
            - How to weight relations?
                - Quality of location: Compute difference in position between regression-based and curve-based
                  The smaller this difference, the higher the accuracy => the higher the general weight of this turn
                  Range: 0 - 1
                    0: 2m (because if it's more, there's really not enough certainty for weights)
                    1: 0m
                - Question: Which access point is more positively affected by the new location?
                  => Perform regression without and with new location, add weight depending on improvement of relative error
                    - Get total error
                    - Get ratio for each access point so that total abs value = 1 (some will be positive, others negative)
                    - Normalize to values between 0 and 1
                    - Old weight * normalized ratio * quality of location * ratio
                        - ratio: 0.05(?)
    
        How to measure quality
            - R-Squared
            - Testing locations
            - Dynamic: Difference between regression-based location and new location (shows the system is working)
    
        Data
            - ranging_locations
                left, top pixel
                time
            - ranging_data
                ranging_location
                rangingdevice
                device_id
                signal
                
    
        Classes
            - room
                - convert pixel to cm
                - trilateration based on distances
                - handle image
    
        '''

        pass


        def websocket_send(self):
            pass


ranging = EnhancedRanging()
ranging.update(False)
