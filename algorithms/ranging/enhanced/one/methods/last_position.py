import operator
from data.models import RangingDevice
import numpy as np
from itertools import combinations
import itertools
import math
from numpy import *
from scipy.optimize import *
import warnings

warnings.filterwarnings('ignore', 'The iteration is not making good progress')
warnings.filterwarnings('ignore', 'The number of calls to function has reached maxfev')

def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


def get_los_distance(signal):
    power_loss = -30
    path_loss_efficient = 2.7
    distance_meter = math.pow(10, ((power_loss - signal)/(10*path_loss_efficient)))
    return distance_meter


def get_position_by_movement(last_position, last_signal, last_ranging_device, signal, location_id, cm2px):

    # Check if same ranging device
    closest_ranging_device = max(signal, key=signal.get)
    if last_ranging_device.identifier != closest_ranging_device:
        return False, False

    # Find best other ranging devices
    ranging_devices = {}
    for ranging_device in RangingDevice.select().where(RangingDevice.location_id==location_id):
        ranging_devices[ranging_device.identifier] = {
            'x': int(ranging_device.position_x),
            'y': int(ranging_device.position_y),
        }

    pairs = []
    max_distance = 0
    min_distance = math.inf
    max_angle = 0
    min_angle = math.inf
    for pair in combinations([key for key in ranging_devices.keys() if key not in {last_ranging_device.identifier}], 2):
        x1 = last_position[0] - ranging_devices[pair[0]]['x']
        y1 = last_position[1] - ranging_devices[pair[0]]['y']
        x2 = last_position[0] - ranging_devices[pair[1]]['x']
        y2 = last_position[1] - ranging_devices[pair[1]]['y']

        angle = angle_between((x1, y1), (x2, y2))
        distance1 = math.sqrt(math.pow(x1, 2)+math.pow(y1, 2))
        distance2 = math.sqrt(math.pow(x2, 2)+math.pow(y2, 2))
        pairs.append({
            'ranging_device_1': {
                'identifier': pair[0],
                'x': ranging_devices[pair[0]]['x'],
                'y': ranging_devices[pair[0]]['y'],
                'distance': distance1
            },
            'ranging_device_2': {
                'identifier': pair[1],
                'x': ranging_devices[pair[1]]['x'],
                'y': ranging_devices[pair[1]]['y'],
                'distance': distance2
            },
            'angle': angle
        })
        max_distance = max(max_distance, distance1)
        min_distance = min(min_distance, distance1)
        max_distance = max(max_distance, distance2)
        min_distance = min(min_distance, distance2)
        max_angle = max(max_angle, angle)
        min_angle = min(min_angle, angle)

    max_quality = 0
    best_pair = None
    for pair in pairs:
        distance_ratio_1 = 1 - (pair['ranging_device_1']['distance'] - min_distance) / (max_distance - min_distance)
        distance_ratio_2 = 1 - (pair['ranging_device_2']['distance'] - min_distance) / (max_distance - min_distance)
        angle_ratio = 1 - (abs((math.pi / 2) - pair['angle']) / (math.pi / 2))
        quality = 0.2*distance_ratio_1 + 0.2*distance_ratio_2 + 0.6*angle_ratio
        pair['distance_ratio_1'] = distance_ratio_1
        pair['distance_ratio_2'] = distance_ratio_2
        pair['angle_ratio'] = angle_ratio
        pair['quality'] = quality
        if quality > max_quality:
            max_quality = quality
            best_pair = pair

    # Compute direction of movement
    # Assume LOS because all that matters is ratio and there's no regression available
    diff1 = signal[best_pair['ranging_device_1']['identifier']] - last_signal[best_pair['ranging_device_1']['identifier']]
    diff2 = signal[best_pair['ranging_device_2']['identifier']] - last_signal[best_pair['ranging_device_2']['identifier']]

    v1 = ((best_pair['ranging_device_1']['x'] - last_position[0])/best_pair['ranging_device_1']['distance']*diff1,
          (best_pair['ranging_device_1']['y'] - last_position[1])/best_pair['ranging_device_1']['distance']*diff1)
    v2 = ((best_pair['ranging_device_2']['x'] - last_position[0])/best_pair['ranging_device_2']['distance']*diff2,
          (best_pair['ranging_device_2']['y'] - last_position[1])/best_pair['ranging_device_2']['distance']*diff2)
    v = (v1[0]+v2[0], v1[1]+v2[1])
    if v == (0, 0):
        return False, False

    m = v[1] / v[0]
    gamma = math.atan2(v[1], v[0])

    # Solve equations
    offset = {
        'x': int(last_ranging_device.position_x),
        'y': int(last_ranging_device.position_y)
    }

    curve_radius_current = cm2px(get_los_distance(signal[closest_ranging_device])*100, False)
    curve_radius_last = cm2px(get_los_distance(last_signal[closest_ranging_device])*100, False)
    distance = curve_radius_current - curve_radius_last

    def equations(start):
        x1 = start[0]
        y1 = start[1]
        x2 = start[2]
        y2 = start[3]

        f = empty(4)
        f[0] = (x1-offset['x'])**2+(y1-offset['y'])**2-curve_radius_current**2
        f[1] = (x2-offset['x'])**2+(y2-offset['y'])**2-curve_radius_last**2
        f[2] = (x2-x1)**2+(y2-y1)**2-(distance**2/math.cos(gamma))
        f[3] = (y2-y1)/(x2-x1)-m
        return f

    perm_values = []
    perm_values.append(int(offset['x']+curve_radius_last))
    perm_values.append(int(offset['x']+curve_radius_current))
    delta = (perm_values[1]-perm_values[0])/4
    perm_values.append(int(perm_values[0]+delta))
    perm_values.append(int(perm_values[0]+2*delta))
    perm_values.append(int(perm_values[0]+3*delta))

    permutations = itertools.permutations(perm_values, 4)
    results = []
    first_values = []
    for permutation in permutations:
        z = fsolve(equations, permutation)
        if int(z[0]) not in first_values:
            results.append(z)
            first_values.append(int(z[0]))

    # Check equations, find best one
    # Check direction
    # Find the one closest to last_position and go for some compromise

    best_distance = math.inf
    for r in results:

        tm = (r[3]-r[1])/(r[2]-r[0])
        if abs(tm-m) < 0.01:

            # Find direction of solution - we need to go from last to current circle
            error = abs(math.sqrt(math.pow(r[0]-offset['x'], 2) + math.pow(r[1]-offset['y'], 2)) - curve_radius_last)
            # Being that the error is based on the first two r-elements and the last radius, if there is an error,
            # we need to switch the direction for normalization
            if error > 1:
                tr0 = r[0]
                tr1 = r[1]
                r[0] = r[2]
                r[1] = r[3]
                r[2] = tr0
                r[3] = tr1

            # compare atan2 to make sure direction is right
            tgamma = math.atan2(r[3] - r[1], r[2] - r[0])
            if abs(gamma - tgamma) < 0.1:
                distance_to_last_position = math.sqrt(math.pow(last_position[0]-r[0], 2) + math.pow(last_position[1]-r[1], 2))
                if distance_to_last_position < best_distance:
                    best_distance = distance_to_last_position
                    best_result = r

    # Find point on circle that's between best result and last_position
    middle_point = (last_position[0] + (best_result[0] - last_position[0])*0.2, last_position[1] + (best_result[1] - last_position[1])*0.2)
    center_to_middle_point = (middle_point[0] - offset['x'], middle_point[1] - offset['y'])
    mag_center_to_middle_point = math.sqrt(center_to_middle_point[0]**2 + center_to_middle_point[1]**2)
    ratio = curve_radius_last / mag_center_to_middle_point
    new_origin = (offset['x']+(center_to_middle_point[0]*ratio), offset['y']+(center_to_middle_point[1]*ratio))

    # Add best result to new_origin
    new_location = (new_origin[0]+(best_result[2]-best_result[0]), new_origin[1]+(best_result[3]-best_result[1]))

    # Push to new curve
    mag_new_location = math.sqrt((new_location[0]-offset['x'])**2 + (new_location[1]-offset['y'])**2)
    ratio = curve_radius_current / mag_new_location
    position = (offset['x']+(ratio*(new_location[0]-offset['x'])), offset['y']+(ratio*(new_location[1]-offset['y'])))

    return position, curve_radius_current


