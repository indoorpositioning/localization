import math
from scipy.optimize import minimize


def trilateration(signal, device_id, regression_model, ranging_devices):

    def mean_squared_error(x, distances):
        mse = 0.0
        for ap in distances:
            dist2 = math.sqrt(math.pow(x[0] - ap['position'][0], 2) + math.pow(x[1] - ap['position'][1], 2))
            mse += math.pow(ap['distance'] - dist2, 2)
        return mse / len(distances)

    distances = []
    average_position = [0, 0]

    if len(signal) < 3:
        return False

    for mac, rssi in signal.items():
        distance = regression_model.get_distance(ranging_devices[mac]['id'], rssi, device_id)
        if distance is False:
            continue
        distances.append({
            'position': [float(ranging_devices[mac]['x']), float(ranging_devices[mac]['y'])],
            'distance': distance
        })
        average_position[0] += float(ranging_devices[mac]['x'])
        average_position[1] += float(ranging_devices[mac]['y'])

    if len(distances) < 3:
        return False

    average_position[0] /= len(distances)
    average_position[1] /= len(distances)

    result = minimize(
        mean_squared_error,
        average_position,
        args=distances,
        method='L-BFGS-B',
        options={
            'ftol': 1e-5,
            'maxiter': 1e+7
        })

    if result.success:
        return result.x
    else:
        return False