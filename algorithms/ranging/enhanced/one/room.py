from data.models import Location, RangingDevice
from PIL import Image
from algorithms.ranging.enhanced.initial.methods import trilateration
from algorithms.ranging.enhanced.initial.methods import get_position_by_movement
import math


grid_width = 500
clear_map_line_threshold = 10


class Room:

    def __init__(self, **kwargs):

        self.los_ranging_device_override = None
        self.last_position = None
        self.last_signal = None
        self.last_ranging_device = None
        self.curve_radius = None

        if 'location_id' in kwargs:
            self.location_id = kwargs['location_id']
            self.map_create()

    def get_conversion_ratio(self):
        location = Location.get(Location.id == self.location_id)
        x_dist = location.map_scale_end_x - location.map_scale_start_x
        y_dist = location.map_scale_end_y - location.map_scale_start_y
        distance_px = math.sqrt(pow(x_dist, 2) + pow(y_dist, 2))
        ratio = float(location.map_width) / distance_px
        return ratio

    def cm2px(self, value, small=True):
        ratio = self.get_conversion_ratio()
        px = value / ratio
        if small:
            px /= self.image_ratio
        return px

    def px2cm(self, value, small=True):
        ratio = self.get_conversion_ratio()
        cm = value * ratio
        if small:
            cm *= self.image_ratio
        return cm

    def get_closest_point_on_circle(self, center, radius, point):
        v = (point[0]-center[0], point[1]-center[1])
        magV = math.sqrt(math.pow(v[0], 2) + math.pow(v[1], 2))
        x = center[0] + radius * v[0] / magV
        y = center[1] + radius * v[1] / magV
        return (x, y)

    def get_los_distance(self, ranging_device, signal):
        r_device = RangingDevice.get((RangingDevice.identifier == ranging_device) & (RangingDevice.location_id == self.location_id))
        distance_meter = (math.pow(10, ((r_device.power_loss - signal)/(10*r_device.path_loss_efficient))))
        distance_px = self.cm2px(distance_meter*100, False)
        return distance_px

    def check_same_room(self, source, destination):

        pixels = []

        m = (destination[1] - source[1]) / (destination[0] - source[0])
        if abs(m) < 0.5:
            current_y = source[1] - (m/2)
            for x in range(source[0], destination[0], 1):
                current_y += m
                elm = (x, math.floor(current_y))
                if elm not in pixels:
                    pixels.append(elm)
                elm = (x+1, math.floor(current_y))
                if elm not in pixels:
                    pixels.append(elm)
        else:
            m = (destination[0] - source[0]) / (destination[1] - source[1])
            current_x = source[0] - (m/2)
            for y in range(source[1], destination[1], 1):
                current_x += m
                elm = (math.floor(current_x), y)
                if elm not in pixels:
                    pixels.append(elm)
                elm = (math.floor(current_x), y+1)
                if elm not in pixels:
                    pixels.append(elm)

        same_room = True
        counter = 0
        fraction = 0
        for pixel in pixels:
            counter += 1
            if self.grid[pixel] == (0, 0, 0) or self.grid[pixel] == (0, 0, 0, 255):
                same_room = False
                fraction = counter / len(pixels)
                break

        if same_room:
            return True, 1
        else:
            return False, fraction

    def get_position(self, **kwargs):
        signal = kwargs['signal']
        device_id = kwargs['device_id']
        regression = kwargs['regression']
        ranging_devices = kwargs['ranging_devices']

        method = None
        if 'method' in kwargs:
            method = kwargs['method']

        trilateration_position = trilateration(signal, device_id, regression, ranging_devices)
        if trilateration_position is not False and (method is None or method == 'trilateration'):
            # Find point on curve
            closest_ranging_device = max(signal, key=signal.get)
            curve_radius = self.get_los_distance(closest_ranging_device, signal[closest_ranging_device])
            r_device = RangingDevice.get((RangingDevice.identifier == closest_ranging_device) & (RangingDevice.location_id == self.location_id))
            ranging_device_position = (float(r_device.position_x), float(r_device.position_y))
            point_on_curve = self.get_closest_point_on_circle(ranging_device_position, curve_radius, trilateration_position)

            self.last_position = point_on_curve
            self.last_signal = signal
            self.last_ranging_device = r_device
            self.curve_radius = curve_radius

            # If we are in the right room because of override, it's easy
            if self.los_ranging_device_override is not False and r_device.id == self.los_ranging_device_override:
                return point_on_curve, True

            # Else, check that there's no wall between ranging device and position
            else:
                ranging_device_position_small = (int(ranging_device_position[0] / self.image_ratio), int(ranging_device_position[1] / self.image_ratio))
                point_on_curve_small = (int(point_on_curve[0] / self.image_ratio), int(point_on_curve[1] / self.image_ratio))
                trilateration_position_small = (int(trilateration_position[0] / self.image_ratio), int(trilateration_position[1] / self.image_ratio))
                same_room_curve, inside_fraction = self.check_same_room(ranging_device_position_small, point_on_curve_small)
                same_room_trilateration_position, inside_fraction = self.check_same_room(ranging_device_position_small, trilateration_position_small)

                if ((same_room_curve and same_room_trilateration_position) or
                        (same_room_curve and not same_room_trilateration_position and inside_fraction > 0.8)):
                    return point_on_curve, True
                else:
                    return point_on_curve, False

        elif self.last_position is not None and (method is None or method == 'last_position'):
            position, curve_radius = get_position_by_movement(self.last_position, self.last_signal, self.last_ranging_device, signal, self.location_id, self.cm2px)

            self.last_signal = signal
            if position:
                self.last_position = position
                self.curve_radius = curve_radius
                return position, True

            return False, False

        return False, False

    def set_los_ranging_device(self, ranging_device):
        if ranging_device > 0:
            self.los_ranging_device_override = ranging_device
        else:
            self.los_ranging_device_override = False

    def map_create(self):

        def change_size(img):
            orig_width = img.size[0]
            wpercent = (grid_width / float(orig_width))
            hsize = int((float(img.size[1]) * float(wpercent)))
            img = img.resize((grid_width, hsize))
            return img, (orig_width/grid_width)

        def change_contrast(img, level):
            factor = (259 * (level + 255)) / (255 * (259 - level))

            def contrast(c):
                #return 128 + factor * (c - 128)
                if c < 100:
                    return 0
                else:
                    return 255

            return img.point(contrast)

        def create_grid(img):

            width = img.size[0]
            height = img.size[1]
            threshold = width / 300

            grid = img.load()

            for x in range(width):
                black_range = []
                for y in range(height):
                    if grid[x, y] == (0, 0, 0, 255) or grid[x, y] == (0, 0, 0):
                        black_range.append(y)
                    elif len(black_range) > 0:
                        if len(black_range) < threshold:
                            for ty in black_range:
                                img.putpixel((x, ty), (255, 255, 255))
                        black_range = []

            for y in range(height):
                black_range = []
                for x in range(width):
                    if grid[x, y] == (0, 0, 0, 255) or grid[x, y] == (0, 0, 0):
                        black_range.append(x)
                    elif len(black_range) > 0:
                        if len(black_range) < threshold:
                            for tx in black_range:
                                img.putpixel((tx, y), (255, 255, 255))
                        black_range = []

            return img

        location = Location.get(Location.id == self.location_id)
        image = Image.open('data/maps/' + str(self.location_id) + '_display' + location.map_display)
        image, ratio = change_size(image)
        image = change_contrast(image, 200)
        image = create_grid(image)

        self.image = image
        self.grid = image.load()
        self.image_ratio = ratio
