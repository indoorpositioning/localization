import matplotlib.pyplot as plt

circle1 = plt.Circle((1500,700), 200, color='r', fill=False)
circle2 = plt.Circle((500,500), 300, color='g', fill=False)
fig, ax = plt.subplots()

ax.set_xlim((0,2000))
ax.set_ylim((0,1000))

ax.add_artist(circle1)
ax.add_artist(circle2)
fig.show()