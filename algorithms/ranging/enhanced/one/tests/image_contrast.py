from PIL import Image


def change_contrast(img, level):
    factor = (259 * (level + 255)) / (255 * (259 - level))

    def contrast(c):
        return 128 + factor * (c - 128)
    return img.point(contrast)


def improve_map(img):

    width = img.size[0]
    height = img.size[1]
    threshold = width / 300

    grid = img.load()

    print("Width")
    for x in range(width):
        black_range = []
        for y in range(height):
            if grid[x, y] == (0, 0, 0, 255) or grid[x, y] == (0, 0, 0):
                black_range.append(y)
            elif len(black_range) > 0:
                if len(black_range) < threshold:
                    for ty in black_range:
                        img.putpixel((x, ty), (255, 255, 255))
                black_range = []

    print("Height")
    for y in range(height):
        black_range = []
        for x in range(width):
            if grid[x, y] == (0, 0, 0, 255) or grid[x, y] == (0, 0, 0):
                black_range.append(x)
            elif len(black_range) > 0:
                if len(black_range) < threshold:
                    for tx in black_range:
                        img.putpixel((tx, y), (255, 255, 255))
                black_range = []

    return img


image = Image.open('data/maps/4_display.png')
image = change_contrast(image, 200)
image = improve_map(image)
image.show()
