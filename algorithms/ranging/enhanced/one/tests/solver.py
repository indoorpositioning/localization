'''
from sympy.solvers import solve
from sympy import Symbol
a = Symbol('a')
b = Symbol('b')
c = Symbol('c')
d = Symbol('d')
result = solve(((a-4)**2+(b-6)**2-16,(c-4)**2+(d-6)**2-36,(a-c)**2+(b-d)**2-9,(d-b)/(c-a)-1), (a,b,c,d), simplified=False)
print(result)
'''

from numpy import *
from scipy.optimize import *
import numpy as np
import itertools
import time

def myFunction(start):
    w = start[0]
    x = start[1]
    y = start[2]
    z = start[3]

    F = empty((4))
    F[0] = (w-400)**2+(x-600)**2-150**2
    F[1] = (y-400)**2+(z-600)**2-190**2
    F[2] = (w-y)**2+(x-z)**2-50**2
    F[3] = (z-x)/(y-w)-1
    return F

start = time.time()
permutations = itertools.permutations(range(150, 190, 10), 4)
results = []
first_values = []
for permutation in permutations:
    z = fsolve(myFunction, permutation)
    if int(z[0]) not in first_values:
        results.append(z)
        first_values.append(int(z[0]))
print(results)
print(time.time()-start)

# Check length
# Check direction
# Check distance to source


'''



7.81 x2
2.78 x2
'''