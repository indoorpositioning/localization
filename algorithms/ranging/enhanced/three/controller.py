from data.models import RangingDevice, RangingLocation, RangingData, Location
from itertools import combinations
import math
from numpy import *
from scipy.optimize import minimize
import importlib
import numpy as np

client_id = 3
location_id = 10
regression_model = 'exponential'
max_training_locations = 10
trilateration_los_weight = 5
sample_count = 10
evaluation_ranging_device = 41


def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)


def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::

            >>> angle_between((1, 0, 0), (0, 1, 0))
            1.5707963267948966
            >>> angle_between((1, 0, 0), (1, 0, 0))
            0.0
            >>> angle_between((1, 0, 0), (-1, 0, 0))
            3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


class EnhancedRanging():

    @classmethod
    def init(cls):
        cls.sample = {}
        cls.current_ranging_device = None
        cls.ranging_active = False
        cls.sample_multiplier = 1
        cls.real_position = {}

        # Load regression
        module_ = importlib.import_module('algorithms.ranging.models.' + regression_model)
        class_ = getattr(module_, regression_model[0].upper() + regression_model[1:])
        cls.regression = class_(location_id=location_id)
        cls.regression.client_id = client_id

        # RangingDevice
        cls.ranging_devices = {}
        for ranging_device in RangingDevice.select().where(RangingDevice.location_id == location_id):
            cls.ranging_devices[ranging_device.id] = {
                'id': ranging_device.id,
                'identifier': ranging_device.identifier,
                'x': int(ranging_device.position_x),
                'y': int(ranging_device.position_y),
                'power_loss': ranging_device.power_loss,
                'path_loss_efficient': ranging_device.path_loss_efficient,
                'location_id': location_id
            }



    @classmethod
    def update(cls, data):

        # Init
        if not hasattr(cls, 'sample'):
            cls.init()

        if cls.current_ranging_device is None or cls.ranging_active is False:
            return

        # Create full sample
        sample = cls.create_sample(data)

        # If full sample, get position
        if sample:
            position = cls.get_position(cls.current_ranging_device, cls.ranging_devices, sample, trilateration_los_weight, client_id)

            # Save data
            rl = RangingLocation(position_x=position[0],
                                 position_y=position[1],
                                 location_id=location_id,
                                 real_position_x=cls.real_position['x'],
                                 real_position_y=cls.real_position['y'])
            rl.save()

            for rd in cls.ranging_devices.values():
                RangingData.create(ranging_device_id=rd['id'], ranging_location_id=rl.id, client_id=client_id, value=sample[rd['identifier']])

            # Compute accuracy - which needs current ranging locations

            ranging_locations = {}
            for rl in RangingLocation.select().where(RangingLocation.location_id == location_id):
                ranging_locations[rl.id] = {
                    'x': float(rl.position_x),
                    'y': float(rl.position_y),
                    'real_x': float(rl.real_position_x),
                    'real_y': float(rl.real_position_y)
                }

            raw_data = {}
            for d in RangingData.select().where((RangingData.ranging_device_id == evaluation_ranging_device)& (RangingData.client_id == client_id)):
                raw_data[d.ranging_location_id] = {
                    'signal': float(d.value),
                    'distance': cls.get_distance_in_m(cls.ranging_devices[evaluation_ranging_device], ranging_locations[d.ranging_location_id])
                }
            model_data = cls.regression.compute_data(raw_data)

            # Check accuracy with available locations
            if model_data is not False:
                errors = []
                for id, loc in ranging_locations.items():
                    range = cls.regression.get_distance(evaluation_ranging_device, raw_data[id]['signal'], client_id, model_data=model_data)
                    distance = cls.get_distance_in_m(cls.ranging_devices[evaluation_ranging_device],
                                                     {'x': ranging_locations[id]['real_x'], 'y': ranging_locations[id]['real_y']})
                    error = abs(range - distance)
                    errors.append(error)

                final_error = sum(errors) / len(errors)
                print(final_error)




    @classmethod
    def update_ranging_device(cls, ranging_device_id):
        if ranging_device_id == cls.current_ranging_device:
            cls.current_ranging_device = None
        else:
            cls.current_ranging_device = int(ranging_device_id)


    @classmethod
    def ranging_control(cls, action, position):

        # Init
        if not hasattr(cls, 'sample'):
            cls.init()

        if action == 'Start':
            cls.ranging_active = True
            pos = position.split(',')
            cls.real_position = {
                'x': pos[0],
                'y': pos[1]
            }
        else:
            cls.ranging_active = False

    @classmethod
    def get_ranging_status(cls):

        # Init
        if not hasattr(cls, 'sample'):
            cls.init()

        data = []
        for mac in cls.sample.keys():

            for rd in cls.ranging_devices.values():
                if rd['identifier'] == mac:
                    id = rd['identifier']
                    break

            data.append({
                "ranging_device_id": id,
                "ranging_device_identifier": mac,
                "avg": float(sum(cls.sample[mac]) / len(cls.sample[mac])),
                "count": float(len(cls.sample[mac]))
            })
        return data

    @classmethod
    def get_position(cls, current_ranging_device_id, ranging_devices, sample, trilateration_los_weight, client_id):

        if not current_ranging_device_id:
            return False

        trilateration_ranging_devices = cls.get_trilateration_ranging_devices(current_ranging_device_id, ranging_devices)

        anchors = [{
            'distance': cls.get_los_distance(ranging_devices[current_ranging_device_id], sample[ranging_devices[current_ranging_device_id]['identifier']]),
            'x': ranging_devices[current_ranging_device_id]['x'],
            'y': ranging_devices[current_ranging_device_id]['y'],
            'weight': trilateration_los_weight
        }]

        for ranging_device_id in trilateration_ranging_devices:
            distance = cls.get_los_distance(ranging_devices[ranging_device_id], sample[ranging_devices[ranging_device_id]['identifier']])
            anchors.append({
                'distance': distance,
                'x': ranging_devices[ranging_device_id]['x'],
                'y': ranging_devices[ranging_device_id]['y'],
                'weight': 1
            })

        position = cls.trilateration(anchors)

        return position

    @classmethod
    def get_trilateration_ranging_devices(cls, current_ranging_device_id, ranging_devices):
        pairs = []
        max_distance = 0
        min_distance = math.inf
        max_angle = 0
        min_angle = math.inf
        for pair in combinations([key for key in ranging_devices.keys() if key != current_ranging_device_id], 2):
            x1 = ranging_devices[current_ranging_device_id]['x'] - ranging_devices[pair[0]]['x']
            y1 = ranging_devices[current_ranging_device_id]['y'] - ranging_devices[pair[0]]['y']
            x2 = ranging_devices[current_ranging_device_id]['x'] - ranging_devices[pair[1]]['x']
            y2 = ranging_devices[current_ranging_device_id]['y'] - ranging_devices[pair[1]]['y']

            angle = angle_between((x1, y1), (x2, y2))
            distance1 = math.sqrt(math.pow(x1, 2)+math.pow(y1, 2))
            distance2 = math.sqrt(math.pow(x2, 2)+math.pow(y2, 2))
            pairs.append({
                'ranging_device_1': {
                    'identifier': pair[0],
                    'id': ranging_devices[pair[0]]['id'],
                    'x': ranging_devices[pair[0]]['x'],
                    'y': ranging_devices[pair[0]]['y'],
                    'distance': distance1
                },
                'ranging_device_2': {
                    'identifier': pair[1],
                    'id': ranging_devices[pair[1]]['id'],
                    'x': ranging_devices[pair[1]]['x'],
                    'y': ranging_devices[pair[1]]['y'],
                    'distance': distance2
                },
                'angle': angle
            })
            max_distance = max(max_distance, distance1)
            min_distance = min(min_distance, distance1)
            max_distance = max(max_distance, distance2)
            min_distance = min(min_distance, distance2)
            max_angle = max(max_angle, angle)
            min_angle = min(min_angle, angle)

        max_quality = 0
        best_pair = None
        for pair in pairs:
            distance_ratio_1 = 1 - (pair['ranging_device_1']['distance'] - min_distance) / (max_distance - min_distance)
            distance_ratio_2 = 1 - (pair['ranging_device_2']['distance'] - min_distance) / (max_distance - min_distance)
            angle_ratio = 1 - (abs((math.pi / 2) - pair['angle']) / (math.pi / 2))
            quality = 0.2*distance_ratio_1 + 0.2*distance_ratio_2 + 0.6*angle_ratio
            pair['distance_ratio_1'] = distance_ratio_1
            pair['distance_ratio_2'] = distance_ratio_2
            pair['angle_ratio'] = angle_ratio
            pair['quality'] = quality
            if quality > max_quality:
                max_quality = quality
                best_pair = pair

        return [
            best_pair['ranging_device_1']['id'],
            best_pair['ranging_device_2']['id']
        ]

    @classmethod
    def get_los_distance(cls, ranging_device, signal):
        distance_meter = (math.pow(10, ((float(ranging_device['power_loss']) - signal)/(10*float(ranging_device['path_loss_efficient'])))))
        distance_px = cls.cm2px(distance_meter*100, ranging_device['location_id'])
        return distance_px

    @classmethod
    def get_conversion_ratio(self, location_id):
        location = Location.get(Location.id == location_id)
        x_dist = location.map_scale_end_x - location.map_scale_start_x
        y_dist = location.map_scale_end_y - location.map_scale_start_y
        distance_px = math.sqrt(pow(x_dist, 2) + pow(y_dist, 2))
        ratio = float(location.map_width) / distance_px
        return ratio

    @classmethod
    def cm2px(self, value, location_id):
        ratio = self.get_conversion_ratio(location_id)
        px = value / ratio
        return px

    @classmethod
    def px2cm(self, value, location_id):
        ratio = self.get_conversion_ratio(location_id)
        cm = value * ratio
        return cm

    @classmethod
    def trilateration(cls, anchors):

        def mean_squared_error(x, distances):
            mse = 0.0
            for ap in distances:
                dist2 = math.sqrt(math.pow(x[0] - ap['x'], 2) + math.pow(x[1] - ap['y'], 2))
                mse += ap['weight'] * math.pow(ap['distance'] - dist2, 2)
            return mse / len(distances)

        # Get starting position (average of anchors)
        average_position = [0, 0]
        for anchor in anchors:
            average_position[0] += anchor['x']
            average_position[1] += anchor['y']
        average_position[0] /= len(anchors)
        average_position[1] /= len(anchors)

        result = minimize(
            mean_squared_error,
            average_position,
            args=anchors,
            method='L-BFGS-B',
            options={
                'ftol': 1e-5,
                'maxiter': 1e+7
            })

        if result.success:
            return result.x
        else:
            return False


    @classmethod
    def get_distance_in_m(cls, source, destination):
        return math.sqrt((source['x']-destination['x'])**2 + (source['y']-destination['y'])**2)

    @classmethod
    def create_sample(cls, data):

        def get_sample_count():
            count = 0
            for mac in cls.sample.keys():
                if count == 0:
                    count = len(cls.sample[mac])
                else:
                    if len(cls.sample[mac]) != count:
                        return False
            return count

        # Determine multiplier
        sc = get_sample_count()
        if sc != False:
            if sc == 0:
                cls.sample_multiplier = 1
            elif sc == sample_count:
                cls.sample_multiplier = 2
            elif sc == 2*sample_count:
                cls.sample_multiplier = 3
            elif sc == 3*sample_count:
                cls.sample_multiplier = 4

        # Add samples
        for entry in data:
            for mac in entry['r'].keys():
                if mac not in cls.sample:
                    cls.sample[mac] = []

                if len(cls.sample[mac]) < cls.sample_multiplier*sample_count:
                    cls.sample[mac].append(entry['r'][mac][0])

        if len(cls.sample) == len(cls.ranging_devices):
            sc = get_sample_count()
            if sc == 4*sample_count:
                for mac in cls.sample.keys():
                    cls.sample[mac] = sum(cls.sample[mac]) / len(cls.sample[mac])
                ret = cls.sample
                cls.sample = {}
                cls.ranging_active = False
                cls.sample_multiplier = 1
                return ret
            if sc == 3*sample_count or sc == 2*sample_count or sc == sample_count:
                cls.ranging_active = False

        return False