from algorithms.ranging.models.regression import Regression
import math
import numpy


class Exponential(Regression):

    def __init__(self, **kwargs):
        super().__init__('exponential')

        if 'location_id' in kwargs:
            self.location_id = kwargs['location_id']

    def compute_data(self, ranging_locations):

        if len(ranging_locations) < 2:
            return False

        n = len(ranging_locations)

        sum_rss_ln_d = 0
        sum_rss = 0
        sum_ln_d = 0
        sum_rss_squared = 0
        for ranging_location_id, info in ranging_locations.items():
            sum_rss_ln_d += (info['signal'] * math.log(info['distance']))
            sum_rss += info['signal']
            sum_ln_d += math.log(info['distance'])
            sum_rss_squared += math.pow(info['signal'], 2)

        ln_d_avg = sum_ln_d / n
        rss_avg = sum_rss / n

        b = (n*sum_rss_ln_d - sum_rss*sum_ln_d) / (n*sum_rss_squared - math.pow(sum_rss, 2))
        a = ln_d_avg - b*rss_avg

        model_data = {
            'alpha': math.exp(a),
            'beta': b
        }

        return model_data

    def compute_data_batch_gradient(self, ranging_locations):

        l = 0.0001

        m = len(ranging_locations)
        if m < 2:
            return False

        alpha = 0
        beta = 0
        diff = 9999
        alpha_last = 10
        beta_last = 10

        while diff > 0.001:

            alpha_sum = 0
            beta_sum = 0
            for ranging_location_id, info in ranging_locations.items():
                alpha_sum += (alpha * math.exp(beta * info['signal']) - info['distance']) * math.exp(beta * info['signal'])
                beta_sum += (alpha * math.exp(beta * info['signal']) - info['distance']) * alpha * info['signal'] * math.exp(beta * info['signal'])
            alpha = alpha - l/m*alpha_sum
            beta = beta - l/m*beta_sum

            diff = max(abs(alpha-alpha_last), abs(beta-beta_last))
            alpha_last = alpha
            beta_last = beta

        model_data = {
            'alpha': alpha,
            'beta': beta
        }

        return model_data

    def compute_data_stochastic_gradient(self, ranging_locations, repetitions=1):

        l = 0.0001

        m = len(ranging_locations)
        if m < 2:
            return False

        alpha = 0
        beta = 0

        for n in range(repetitions):
            for ranging_location_id, info in ranging_locations.items():
                alpha_temp = alpha - (l * (alpha * math.exp(beta * info['signal']) - info['distance']) * math.exp(beta * info['signal']))
                beta_temp = beta - (l * (alpha * math.exp(beta * info['signal']) - info['distance']) * alpha * info['signal'] * math.exp(beta * info['signal']))
                alpha = alpha_temp
                beta = beta_temp

        model_data = {
            'alpha': alpha,
            'beta': beta
        }

        return model_data

    def get_distance(self, ranging_device_id, signal, client_id, model_data=None):

        if model_data is None:
            model_data = self.get_regression_data(ranging_device_id, client_id, 'exponential')

        if 'alpha' in model_data and 'beta' in model_data:
            distance = model_data['alpha'] * numpy.exp(signal*model_data['beta'])
            return distance
        else:
            return False
