from algorithms.ranging.models.regression import Regression


class Linear(Regression):

    def __init__(self, **kwargs):
        super().__init__('linear')

        if 'location_id' in kwargs:
            self.location_id = kwargs['location_id']

    def compute_data(self, ranging_locations):

        if len(ranging_locations) < 4:
            return False

        # Get mean signal and distance for each location, relative to one device
        # Note that the distances are relative to the ranging_device, but this is not relevant just for the computation.
        mean_signal = 0
        mean_distance = 0
        for ranging_location_id, info in ranging_locations.items():
            mean_signal += info['signal']
            mean_distance += info['distance']
        mean_signal /= len(ranging_locations)
        mean_distance /= len(ranging_locations)

        # Get Beta and Epsilon
        numerator = 0
        denominator = 0
        for ranging_location_id, info in ranging_locations.items():
            numerator += (info['signal'] - mean_signal) * (info['distance'] - mean_distance)
            denominator += pow(info['signal'] - mean_signal, 2)

        if denominator != 0:
            beta = numerator / denominator
            epsilon = mean_distance - (beta * mean_signal)

            # Create model
            model_data = {
                'beta': beta,
                'epsilon': epsilon
            }
            return model_data

        return False

    def compute_data_batch_gradient(self, ranging_locations):

        l = 0.0001

        m = len(ranging_locations)
        if m < 2:
            return False

        epsilon = 0
        beta = 0
        diff = 9999
        epsilon_last = 10
        beta_last = 10

        while diff > 0.01:

            epsilon_sum = 0
            beta_sum = 0
            for ranging_location_id, info in ranging_locations.items():
                epsilon_sum += epsilon + beta*info['signal'] - info['distance']
                beta_sum += (epsilon + beta*info['signal'] - info['distance']) * info['signal']
            epsilon = epsilon - l/m*epsilon_sum
            beta = beta - l/m*beta_sum

            diff = max(abs(epsilon-epsilon_last), abs(beta-beta_last))
            epsilon_last = epsilon
            beta_last = beta

        model_data = {
            'beta': beta,
            'epsilon': epsilon
        }

        return model_data

    def compute_data_stochastic_gradient(self, ranging_locations, repetitions=1):

        l = 0.0001

        m = len(ranging_locations)
        if m < 2:
            return False

        epsilon = 0
        beta = 0

        for n in range(repetitions):
            for ranging_location_id, info in ranging_locations.items():
                epsilon_temp = epsilon - (l * (epsilon + (beta * info['signal']) - info['distance']))
                beta_temp = beta - (l * ((epsilon + (beta * info['signal']) - info['distance']) * info['signal']))
                epsilon = epsilon_temp
                beta = beta_temp

        model_data = {
            'beta': beta,
            'epsilon': epsilon
        }

        return model_data

    def get_distance(self, ranging_device_id, signal, client_id, model_data=None):

        if model_data is None:
            model_data = self.get_regression_data(ranging_device_id, client_id, 'linear')

        if 'epsilon' in model_data and 'beta' in model_data:
            distance = (model_data['beta'] * signal) + model_data['epsilon']
            return distance
        else:
            return False
