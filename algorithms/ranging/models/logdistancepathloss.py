from algorithms.ranging.models.regression import Regression
from scipy.optimize import curve_fit
import numpy as np


def func(x, a, b):
    return np.power(10, (a - x)/(10 * b))


class Logdistancepathloss(Regression):

    def __init__(self, **kwargs):
        super().__init__('logdistancepathloss')

        if 'location_id' in kwargs:
            self.location_id = kwargs['location_id']

    def compute_data(self, ranging_locations):

        if len(ranging_locations) < 4:
            return False

        signal = list(map(lambda x: abs(x['signal']), ranging_locations.values()))
        distance = list(map(lambda x: x['distance'], ranging_locations.values()))
        try:
            popt, pcov = curve_fit(func, signal, distance)
        except RuntimeError as e:
            return False
        model_data = list(popt)
        return model_data

    def get_distance(self, ranging_device_id, signal, client_id, model_data=None):

        if model_data is None:
            model_data = self.get_regression_data(ranging_device_id, client_id, 'logdistancepathloss')

        distance = func(abs(signal), model_data[0], model_data[1])
        return distance
