from algorithms.ranging.models.regression import Regression
from scipy.optimize import least_squares
import numpy as np
import pickle


def generate_data(x, t0, t1, t2):
    return t0*np.power(x, 2) + t1*np.power(x, 1) + t2


def func(t, x, y):
    a = (t[0]*np.power(x, 2) + t[1]*np.power(x, 1) + t[2]) - y
    return a


class Poly2leastsquares(Regression):

    def __init__(self, **kwargs):
        super().__init__('poly2leastsquares')

        if 'location_id' in kwargs:
            self.location_id = kwargs['location_id']

    def compute_data(self, ranging_locations):

        if len(ranging_locations) < 4:
            return False

        x = []
        y = []
        for ranging_location_id, info in ranging_locations.items():
            x.append(info['signal'])
            y.append(info['distance'])

        t0 = np.ones(3)
        res_robust = least_squares(func, t0, loss='soft_l1', f_scale=0.1, args=(x, y))
        model_data = {
            'res_robust': list(res_robust.x)
        }
        return model_data

    def get_distance(self, ranging_device_id, signal, client_id, model_data=None):

        if model_data is None:
            model_data = self.get_regression_data(ranging_device_id, client_id, 'poly2leastsquares')

        sig = np.linspace(signal, signal, 1)
        distance = generate_data(sig, *model_data['res_robust'])
        return distance
