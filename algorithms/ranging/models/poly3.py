from algorithms.ranging.models.regression import Regression
import numpy


class Poly3(Regression):

    def __init__(self, **kwargs):
        super().__init__('poly3')

        if 'location_id' in kwargs:
            self.location_id = kwargs['location_id']

    def compute_data(self, ranging_locations):

        if len(ranging_locations) < 4:
            return False

        signal = list(map(lambda x: x['signal'], ranging_locations.values()))
        distance = list(map(lambda x: x['distance'], ranging_locations.values()))
        coefficients = numpy.polyfit(signal, distance, 3)
        model_data = list(coefficients)
        return model_data

    def get_distance(self, ranging_device_id, signal, client_id, model_data=None):

        if model_data is None:
            model_data = self.get_regression_data(ranging_device_id, client_id, 'poly3')
        distance = model_data[3] + model_data[2]*signal + model_data[1]*pow(signal, 2) + model_data[0]*pow(signal, 3)
        return distance
