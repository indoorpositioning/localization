from algorithms.ranging.models.regression import Regression
from scipy.optimize import curve_fit
import numpy as np


def power_func(x, a, b):
    return a * np.power(x, b)


class Power(Regression):

    def __init__(self, **kwargs):
        super().__init__('power')

        if 'location_id' in kwargs:
            self.location_id = kwargs['location_id']

    def compute_data(self, ranging_locations):

        if len(ranging_locations) < 4:
            return None

        signal = list(map(lambda x: abs(x['signal']), ranging_locations.values()))
        distance = list(map(lambda x: x['distance'], ranging_locations.values()))
        try:
            popt, pcov = curve_fit(power_func, signal, distance)
        except RuntimeError as e:
            return None
        model_data = list(popt)
        return model_data

    def get_distance(self, ranging_device_id, signal, client_id, model_data=None):

        if model_data is None:
            model_data = self.get_regression_data(ranging_device_id, client_id, 'power')

            if model_data is None:
                return None

        distance = power_func(abs(signal), model_data[0], model_data[1])
        return distance
