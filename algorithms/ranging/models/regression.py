import json
from math import *
from data.models import Location, RangingLocation, RangingData, fn, RangingDevice, RangingModel, Client
from peewee import *


class Regression(object):

    def __init__(self, name):
        self.ranging_location_id = None
        self.client_id = None
        self.location_id = None
        self.data = {}
        self.name = name

    def get_data(self):

        if self.ranging_location_id is None and self.location_id is None:
            return False

        # Fetch everything
        if self.location_id is not None:
            location = (Location.select().where(Location.id == self.location_id)).get()
        else:
            location = (Location.select().join(RangingLocation).where(RangingLocation.id == self.ranging_location_id)).get()

        if not location.map_width:
            return

        # Get all access points of a map
        ranging_devices = {}
        for ranging_device in location.ranging_devices:
            ranging_devices[ranging_device.id] = ranging_device

        # Get all ranging locations of a map
        ranging_locations = {}
        for ranging_location in location.ranging_locations:
            ranging_locations[ranging_location.id] = ranging_location

        # Fetch measurements - get the average for all measurements between all ranging devices and ranging locations
        data = {}
        for d in RangingData \
                .select(RangingData.ranging_device_id, RangingData.ranging_location_id,
                        fn.Avg(RangingData.value).alias('avg')) \
                .join(RangingDevice) \
                .where((RangingData.client_id == self.client_id) & (RangingDevice.location_id == location.id)) \
                .group_by(RangingData.ranging_device_id, RangingData.ranging_location_id):
            if d.ranging_device_id not in data:
                data[d.ranging_device_id] = {}
            data[d.ranging_device_id][d.ranging_location_id] = {'signal': float(d.avg)}

        # Get radio between meter and pixel
        x_dist = location.map_scale_end_x - location.map_scale_start_x
        y_dist = location.map_scale_end_y - location.map_scale_start_y
        distance = sqrt(pow(x_dist, 2) + pow(y_dist, 2))
        ratio = float(location.map_width) / distance

        # Get Distances
        for ranging_device_id, ranging_locs in data.items():
            for ranging_location_id, info in ranging_locs.items():
                x_dist = float(ranging_locations[ranging_location_id].position_x) - float(ranging_devices[ranging_device_id].position_x)
                y_dist = float(ranging_locations[ranging_location_id].position_y) - float(ranging_devices[ranging_device_id].position_y)
                distance = sqrt(pow(x_dist, 2) + pow(y_dist, 2)) * ratio
                info['distance'] = distance

        return {
            'ranging_devices': ranging_devices,
            'distances': data
        }

    def create_models(self, **kwargs):

        if 'ranging_location_id' in kwargs:
            self.ranging_location_id = kwargs['ranging_location_id']

        if 'location_id' in kwargs:
            self.location_id = kwargs['location_id']

        self.client_id = kwargs['client_id']


        # For one ranging location, get all data
        self.data = self.get_data()

        if self.data:

            models = {}

            # For each ranging device...
            for ranging_device_id, ranging_locations in self.data['distances'].items():
                model_data = self.compute_data(ranging_locations)
                models[ranging_device_id] = model_data
                if model_data:
                    self.create_db(self.name, self.data['ranging_devices'][ranging_device_id], model_data)

            return models

    def get_regression_data(self, ranging_device_id, client_id, model_name):

        if 'model' not in self.data:
            self.data['model'] = {}

        if ranging_device_id not in self.data['model']:

            if client_id:
                if isinstance(client_id, int):
                    model_data_count = RangingModel.select().where((RangingModel.name == model_name) & (RangingModel.client_id == client_id) & (RangingModel.ranging_device_id == ranging_device_id)).count()
                    if model_data_count:
                        model = RangingModel.get((RangingModel.name == model_name) & (RangingModel.client_id == client_id) & (RangingModel.ranging_device_id == ranging_device_id))
                        model_data = json.loads(model.data)
                    else:
                        model_data = None
                else:
                    model_data_count = RangingModel.select().join(Client, JOIN.LEFT_OUTER).where((RangingModel.name == model_name) & (Client.identifier == client_id) & (RangingModel.ranging_device_id == ranging_device_id)).count()
                    if model_data_count:
                        model = RangingModel.select().join(Client, JOIN.LEFT_OUTER).where((RangingModel.name == model_name) & (Client.identifier == client_id) & (RangingModel.ranging_device_id == ranging_device_id)).get()
                        model_data = json.loads(model.data)
                    else:
                        model_data = None
            else:
                fields = {}
                for model in RangingModel.select().where((RangingModel.name == model_name) & (RangingModel.ranging_device_id == ranging_device_id)):
                    model_data_temp = json.loads(model.data)
                    for key, value in model_data_temp.items():
                        if not hasattr(fields, key):
                            fields[key] = []
                        fields[key].append(model_data_temp[key])

                model_data = {}
                for key, values in fields.items():
                    model_data[key] = sum(values) / len(values)

            self.data['model'][ranging_device_id] = model_data

        return self.data['model'][ranging_device_id]

    def get_r_squared(self, ranging_device_id, client_id, ranging_data, model_data = False):

        # Get mean
        distances = []
        for elm in ranging_data.values():
            distances.append(elm['distance'])
        avg_distance = sum(distances) / len(distances)

        # Get residual and total
        total = 0
        residual = 0
        for elm in ranging_data.values():
            total += pow(elm['distance']-avg_distance, 2)
            residual += pow(elm['distance'] - self.get_distance(ranging_device_id, elm['signal'], client_id, model_data), 2)

        # Get r-squared
        r_squared = 1 - (residual/total)
        return r_squared

    def create_db(self, name, ranging_device, data):

        # Peewee doesn't support upserts
        query = RangingModel.delete().where((RangingModel.name == name) & (RangingModel.client_id == self.client_id) & (RangingModel.ranging_device == ranging_device))
        query.execute()

        client = Client.get(Client.id == self.client_id)
        RangingModel.create(name=name, client=client, ranging_device=ranging_device, data=json.dumps(data))
