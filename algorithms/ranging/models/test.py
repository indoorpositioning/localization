from data.models import RangingData, fn, RangingDevice

for d in RangingData \
        .select(RangingData.ranging_device_id, RangingData.ranging_location_id ,
                fn.Avg(RangingData.value).alias('avg')) \
        .join(RangingDevice) \
        .where((RangingData.client_id == 2) & (RangingDevice.location_id == 6)) \
        .group_by(RangingData.ranging_device_id, RangingData.ranging_location_id):
    # if d.ranging_device_id not in data:
    #     data[d.ranging_device_id] = {}
    # data[d.ranging_device_id][d.ranging_location_id] = {'signal': float(d.avg)}
    print(d)
