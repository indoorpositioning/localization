import importlib
import time
from collections import deque

from data.models import RangingDevice
from misc.utils import get_or_list
from settings import ranging_settings


def current_milli_time():
    return int(round(time.time() * 1000))


def current_micro_time():
    return int(round(time.time() * 1000000))


class Ranging:

    def __init__(self, location_id):
        self.data = {}
        self.location_id = location_id

        # Get ranging_identifier => ranging_device_id for this location
        self.ranging_device_map = {}
        for ranging_device in RangingDevice.select().where(RangingDevice.location_id == location_id):
            self.ranging_device_map[ranging_device.identifier] = ranging_device.id

        # Load appropriate regression model
        regression_model = ranging_settings['live_model']
        module_ = importlib.import_module('algorithms.ranging.models.' + regression_model)
        class_ = getattr(module_, regression_model[0].upper() + regression_model[1:])
        self.regression = class_(location_id=location_id)

    def update(self, new_data, upload_id):
        """Some documentation here.
        """

        # Add values to data
        current_time = current_milli_time()
        #start_time = current_micro_time()

        for batch in new_data:
            if 'r' not in batch:
                continue
            for ranging_identifier, signals in batch['r'].items():
                if ranging_identifier in self.ranging_device_map:
                    if ranging_identifier not in self.data:
                        self.data[ranging_identifier] = {'last': 0, 'values': deque(
                            maxlen=ranging_settings['aggregate_last_n_measurements']), 'weighted_avg': 0}

                    for signal in get_or_list(signals):
                        self.data[ranging_identifier]['values'].append(signal)

                    self.data[ranging_identifier]['last'] = current_time

        # Compute distance to each ranging device
        summary = {}
        for ranging_identifier in self.data:

            # Get averages and weight by age
            self.data[ranging_identifier]['weighted_avg'] = \
                sum(self.data[ranging_identifier]['values']) / len(self.data[ranging_identifier]['values']) * \
                (1 - (current_time - self.data[ranging_identifier]['last'])/5000)

            # Get distance
            summary[ranging_identifier] = self.regression.get_distance(self.ranging_device_map[ranging_identifier], self.data[ranging_identifier]['weighted_avg'], upload_id)

        #print("4: " + str(current_micro_time()-start_time))
        # print(summary)
        return summary

