import importlib

from data.models import Location, RangingLocation, RangingData, Client
from misc.utils import get_or_list
from settings import ranging_settings


class RangingMeasurements:

    ranging_location = None
    ranging_devices = {}
    client = None

    @classmethod
    def start_measurements(cls, ranging_location_id, client_id):

        # Load APs of this location
        location = (Location.select().join(RangingLocation).where(RangingLocation.id == ranging_location_id)).get()
        cls.ranging_devices = {}
        for ranging_device in location.ranging_devices:
            cls.ranging_devices[ranging_device.identifier] = ranging_device

        # Get Ranging Location
        cls.ranging_location = RangingLocation.get(RangingLocation.id == ranging_location_id)

        # Get Device
        if client_id != '-1':
            cls.client = Client.get(Client.id == client_id)

        # Init counter
        cls.measurement_counter = {}

    @classmethod
    def end_measurements(cls, ranging_location_id, client_id):

        for model in ranging_settings['active_models']:
            module_ = importlib.import_module('algorithms.ranging.models.' + model)
            class_ = getattr(module_, model[0].upper() + model[1:])
            instance = class_()
            instance.create_models(ranging_location_id=ranging_location_id, client_id=client_id)

        cls.ranging_location = None
        cls.client = None

    @classmethod
    def gather_data(cls, data):
        """
        Gathers data from ranging locations which form the basis for the regression models.

        Args:
            data: The data; format:
                    {
                        device: [
                            measurement 1,
                            measurement 2,
                            ...
                        ]
                    }

        Example:
            {"t":"r","d":{"AP0":[-52,-50,-48,-49],"AP1":[-23,-24,-25],"AP2":[-30],"AP3":[-10]}}

        """
        if cls.ranging_location is not None:

            for batch in data:
                for key, measurements in batch['r'].items():
                    if key in cls.ranging_devices:
                        for measurement in get_or_list(measurements):
                            if key not in cls.measurement_counter:
                                cls.measurement_counter[key] = 0
                            if cls.measurement_counter[key] < ranging_settings['measurements_per_direction']:
                                cls.measurement_counter[key] += 1
                                RangingData.create(ranging_device=cls.ranging_devices[key],
                                                   ranging_location=cls.ranging_location,
                                                   client=cls.client,
                                                   value=measurement)
