from typing import TypeVar, Dict, Generic

import numpy as np

X = TypeVar('X')
Y = TypeVar('Y')
Content = TypeVar('Content')


# 2 Dimensional Dictionary backed by a numpy array
class NPNDict(Generic[X, Y, Content]):
    x_index_lookup: Dict[X, int]
    y_index_lookup: Dict[Y, int]
    array: np.array

    def __init__(self, x_keys: [X], y_keys: [Y], array: [Content] = None) -> None:
        self.x_index_lookup = {x: index for (index, x) in x_keys}
        self.y_index_lookup = {y: index for (index, y) in y_keys}
        self.array = np.ndarray((len(x_keys), len(y_keys))) if array is None else array

    def set(self, x: X, y: Y, value: Content):
        self.array[self.x_index_lookup[x], self.y_index_lookup[y]] = value

    def get(self, x: X, y: Y) -> Content:
        return self.array[self.x_index_lookup[x], self.y_index_lookup[y]]
