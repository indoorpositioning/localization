import importlib
import matplotlib.pyplot as plt
import numpy as np


model = 'linear'
batch = True
location_id = 10
client_id = 3


module_ = importlib.import_module('algorithms.ranging.models.' + model)
class_ = getattr(module_, model[0].upper() + model[1:])
instance = class_(location_id=location_id)
instance.client_id = client_id
data = instance.get_data()

for ranging_device_id, ranging_data in data['distances'].items():
    model_data_conventional = instance.compute_data(data['distances'][ranging_device_id])
    print(model_data_conventional)

    if batch:
        model_data_batch = instance.compute_data_batch_gradient(data['distances'][ranging_device_id])
        print(model_data_batch)

    model_data_stochastic = instance.compute_data_stochastic_gradient(data['distances'][ranging_device_id])
    print(model_data_stochastic)



    ########
    # Plot #
    ########

    signal = []
    distance = []
    for elm in ranging_data.values():
        signal.append(elm['signal'])
        distance.append(elm['distance'])
    plt.plot(signal, distance, 'bo')

    sig = np.arange(min(signal), max(signal), 0.01)

    distance = instance.get_distance(ranging_device_id, sig, client_id, model_data_conventional)
    r_squared = instance.get_r_squared(ranging_device_id, client_id, ranging_data, model_data_conventional)
    plt.plot(sig, distance, label="Conventional: " + str(r_squared))

    if batch:
        distance = instance.get_distance(ranging_device_id, sig, client_id, model_data_batch)
        r_squared = instance.get_r_squared(ranging_device_id, client_id, ranging_data, model_data_batch)
        plt.plot(sig, distance, label="Batch: " + str(r_squared))

    distance = instance.get_distance(ranging_device_id, sig, client_id, model_data_stochastic)
    r_squared = instance.get_r_squared(ranging_device_id, client_id, ranging_data, model_data_stochastic)
    plt.plot(sig, distance, label="Stochastic: " + str(r_squared))

    plt.legend()
    #plt.savefig("algorithms/ranging/statistics/plots/regression_analysis_" + model + ".pdf", format='pdf')
    plt.show()

