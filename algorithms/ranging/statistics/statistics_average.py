import itertools
import importlib
import time
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from data.models import RangingLocation, Location
from PIL import Image, ImageDraw
from settings import ranging_settings
import pprint
from random import shuffle

limiter = 400
location_id = 11
client_id = 3
min_selection = 5
max_selection = 10
model_colors = {
    'linear': 'b',
    'exponential': 'g',
    'power': 'r',
    'poly2': 'c',
    'poly3': 'm',
    'leastsquaressoftl1': 'y',
    'poly2leastsquares': 'k'
}


def compute_stats(obj, measurements, combination, ranging_locations_translator):

    if len(measurements):
        avg_measurements = sum(measurements) / len(measurements)
        if avg_measurements < obj['min']['val']:
            obj['min']['val'] = avg_measurements
            obj['min']['combination'] = list(map(lambda x: ranging_locations_translator[x], combination))
        if avg_measurements > obj['max']['val']:
            obj['max']['val'] = avg_measurements
            obj['max']['combination'] = list(map(lambda x: ranging_locations_translator[x], combination))
        obj['avg'] = ((obj['avg'] * obj['num']) + avg_measurements) / (obj['num'] + 1)
        obj['num'] += 1
        return avg_measurements
    return False


def add_positions(positions, combination, distance, ranging_locations_translator):
    amount_locations = len(combination)
    if amount_locations not in positions:
        positions[amount_locations] = {}

    for ranging_location in combination:
        ranging_location_id = ranging_locations_translator[ranging_location]
        if not ranging_location_id in positions[amount_locations]:
            positions[amount_locations][ranging_location_id] = 0
        positions[amount_locations][ranging_location_id] += (distance / 100)


def add_combinations(data, combination, distance, ranging_location_translator):
    amount_locations = len(combination)
    if amount_locations not in data:
        data[amount_locations] = []

    data[amount_locations].append({
        'combination': list(map(lambda x: ranging_locations_translator[x], combination)),
        'distance': distance
    })


def plot():

    # One Chart with all averages
    fig, ax = plt.subplots(figsize=(6, 3.75))
    for axis in [ax.xaxis, ax.yaxis]:
        axis.set_major_locator(ticker.MaxNLocator(integer=True))
    #plt.figure(figsize=(6, 3.75))
    for model, data in stats.items():
        x = list(data['values'].keys())
        for kind in ['min', 'max', 'avg']:
            y = []
            for locations in x:
                if kind == 'avg':
                    y.append(data['values'][locations]['all'][kind])
                else:
                    y.append(data['values'][locations]['all'][kind]['val'])
            c = model_colors[model] + 'o-'
            if kind == 'avg':
                #print(model)
                #print(y)
                plt.plot(x, y, c, label=model)
            #else:
            #    plt.plot(x, y, c)


    #plt.title('Error by Number of Locations')
    plt.ylabel('Error (cm)')
    plt.xlabel('Locations')
    plt.legend()

    #print(plt.ylim())
    plt.ylim(120, 300)
    fig.savefig("algorithms/ranging/statistics/plots/model_comparison.pdf", format='pdf')
    plt.show()
    exit()
    # Min
    fig, ax = plt.subplots()
    for axis in [ax.xaxis, ax.yaxis]:
        axis.set_major_locator(ticker.MaxNLocator(integer=True))

    for model, data in stats.items():
        x = list(data['values'].keys())
        y = []
        for locations in x:
            y.append(data['values'][locations]['all']['min']['val'])
            #print(str(locations) + ": " + str(data['values'][locations]['all']['min']['combination']))
        if model == 'poly3':
            print(y)
        c = model_colors[model] + 'o-'
        plt.plot(x, y, c, label=model)

    plt.title('Min Error by Number of Locations')
    plt.ylabel('Error (cm)')
    plt.xlabel('Locations')
    plt.legend()

    #print(plt.ylim())
    fig.savefig("algorithms/ranging/statistics/plots/model_comparison_minima.pdf", format='pdf')
    plt.show()

    '''
        Color training/testing locations on map:
        
        var training = [...];
        $('#locations_map > span.oi-audio').each(function(i, elm) {
            if (jQuery.inArray($(elm).data('id'), training) !== -1) {
                $(elm).css('border', '3px solid red');
            }
        });
    '''


    for model, data in stats.items():

        fig, ax = plt.subplots()
        for axis in [ax.xaxis, ax.yaxis]:
            axis.set_major_locator(ticker.MaxNLocator(integer=True))

        x = list(data['values'].keys())
        for kind in ['min', 'max', 'avg']:
            y = []
            for locations in x:
                if kind == 'avg':
                    y.append(data['values'][locations]['all'][kind])
                else:
                    y.append(data['values'][locations]['all'][kind]['val'])
            if kind == 'min' and model == 'poly3':
                print(y)

            c = model_colors[model] + 'o-'
            if kind == 'avg':
                plt.plot(x, y, c, label=model)
            else:
                plt.plot(x, y, c)

        plt.title('Error by Number of Locations')
        plt.ylabel('Error (cm)')
        plt.xlabel('Locations')
        plt.legend()

        plt.ylim(100, 400)
        fig.savefig("algorithms/ranging/statistics/plots/model_comparison_" + model + ".pdf", format='pdf')
        plt.show()



stats = {}

# We want to get data for every model available
for model in ranging_settings['active_models']:
    print("Model: " + model)
    stats[model] = {
        'values': {},
        'positions': {},
        'combinations': {}
    }
    module_ = importlib.import_module('algorithms.ranging.models.' + model)
    class_ = getattr(module_, model[0].upper() + model[1:])
    instance = class_(location_id=location_id)
    instance.client_id = client_id
    data = instance.get_data()

    first_ranging_device_key = (list(data['distances']))[0]
    num_locations = len(data['distances'][first_ranging_device_key])

    ranging_locations_translator = []
    for id, ranging_location in data['distances'][first_ranging_device_key].items():
        ranging_locations_translator.append(id)

    # Find the best combination on ranging locations possible => test them all
    for selection in range(min_selection, max_selection):

        print("Selection: " + str(selection))
        start_time = time.time()

        # Build stats
        stats[model]['values'][selection] = {}
        stats[model]['values'][selection]['all'] = {
            'min': {
                'val': 99999,
                'combination': []
            },
            'max': {
                'val': 0,
                'combination': []
            },
            'avg': 0,
            'num': 0
        }
        for ranging_device_id, ranging_locations in data['distances'].items():
            stats[model]['values'][selection][ranging_device_id] = {
                'min': {
                    'val': 99999,
                    'combination': []
                },
                'max': {
                    'val': 0,
                    'combination': []
                },
                'avg': 0,
                'num': 0
            }

        stats_all_storage = []

        locations = list(range(num_locations))
        combinations = []
        for i in range(limiter):
            shuffle(locations)
            combinations.append(locations[0:selection])

        for combination in combinations:

            #print(combination)

            # Test each combination with each ranging device individually
            # (and later with all of them to see if the same combinations work best)
            for ranging_device_id, ranging_locations in data['distances'].items():

                ranging_device_results = []

                # Prepare ranging locations to be sent to model
                test_ranging_locations = {}
                for elm in combination:
                    try:
                        test_ranging_locations[ranging_locations_translator[elm]] = data['distances'][ranging_device_id][ranging_locations_translator[elm]]
                    except KeyError as e:
                        pass
                        #print(e)

                # Get model data
                model_data = instance.compute_data(test_ranging_locations)
                if model_data is False:
                    continue

                # Use this model to compare computed distance to each ranging location to read distance and take average
                for id, ranging_location in data['distances'][ranging_device_id].items():
                    #if id not in test_ranging_locations.keys():
                    distance = instance.get_distance(ranging_device_id, ranging_location['signal'], client_id, model_data)
                    diff = abs(distance - ranging_location['distance'])

                    #ranging_device_results.append(diff)
                    stats_all_storage.append(diff)
                    #print(diff)

                #diff_ranging_device = compute_stats(stats[model]['values'][selection][ranging_device_id], ranging_device_results, combination, ranging_locations_translator)
                #stats_all_storage.append(diff_ranging_device)
                #del ranging_device_results[:]

            avg_distance = compute_stats(stats[model]['values'][selection]['all'], stats_all_storage, combination, ranging_locations_translator)
            #add_positions(stats[model]['positions'], combination, avg_distance, ranging_locations_translator)
            #add_combinations(stats[model]['combinations'], combination, distance, ranging_locations_translator)
            del stats_all_storage[:]

        elapsed_time = time.time() - start_time
        print("-- Time: " + str(elapsed_time))

plot()
#evaluate_positions()
#evaluate_combinations()
pp = pprint.PrettyPrinter(indent=4)
#pp.pprint(stats)
