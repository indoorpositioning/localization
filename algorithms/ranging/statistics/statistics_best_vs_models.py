import itertools
import importlib
import time
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from data.models import RangingLocation, Location
from PIL import Image, ImageDraw
from settings import ranging_settings
import pprint
from random import shuffle


'''
for each model
    for each number of locations:
        get combinations
            find average error over all combinations per model (= one number per model and number of locations)
            find min error per access point

find smallest error per access point
find perfect min value

'''

limiter = 400
location_id = 8
client_id = 3
min_selection = 5
max_selection = 43
model_colors = {
    'linear': 'b',
    'exponential': 'g',
    'power': 'r',
    'poly2': 'c',
    'poly3': 'm',
    'leastsquaressoftl1': 'y',
    'poly2leastsquares': 'k'
}


ranging_locations_translator = []
for ranging_location in RangingLocation.select().where(RangingLocation.location_id == location_id):
    ranging_locations_translator.append(ranging_location.id)
num_locations = len(ranging_locations_translator)

training_locations = list(range(num_locations))
shuffle(training_locations)
training_locations = training_locations[0:max_selection]

stats = {}
for model in ranging_settings['active_models']:
    print("Model: " + model)
    stats[model] = {}

    module_ = importlib.import_module('algorithms.ranging.models.' + model)
    class_ = getattr(module_, model[0].upper() + model[1:])
    instance = class_(location_id=location_id)
    instance.client_id = client_id
    data = instance.get_data()

    for selection in range(min_selection, max_selection):

        print(selection)

        # Prepare stats object
        if selection not in stats[model]:
            stats[model][selection] = {
                'overall': {
                    'values': []
                },
                'accesspoints': {}
            }

        # Get combinations
        #locations = list(range(num_locations))
        locations = training_locations
        combinations = []
        for i in range(limiter):
            shuffle(locations)
            combinations.append(locations[0:selection])

        # For each combination...
        for combination in combinations:

            location_results = []

            for ranging_device_id, ranging_locations in data['distances'].items():

                if ranging_device_id not in stats[model][selection]['accesspoints']:
                    stats[model][selection]['accesspoints'][ranging_device_id] = {
                        'values': []
                    }

                ranging_device_results = []

                # Prepare ranging locations to be sent to model
                test_ranging_locations = {}
                for elm in combination:
                    try:
                        test_ranging_locations[ranging_locations_translator[elm]] = data['distances'][ranging_device_id][ranging_locations_translator[elm]]
                    except KeyError as e:
                        pass

                # Get model data
                model_data = instance.compute_data(test_ranging_locations)
                if model_data is False:
                    continue

                # Use this model to compare computed distance to each ranging location to read distance and take average
                for id, ranging_location in data['distances'][ranging_device_id].items():
                    distance = instance.get_distance(ranging_device_id, ranging_location['signal'], client_id, model_data)
                    diff = abs(distance - ranging_location['distance'])

                    ranging_device_results.append(diff)
                    location_results.append(diff)

                avg = sum(ranging_device_results) / len(ranging_device_results)
                del ranging_device_results[:]
                stats[model][selection]['accesspoints'][ranging_device_id]['values'].append(avg)

            avg = sum(location_results) / len(location_results)
            del location_results[:]
            stats[model][selection]['overall']['values'].append(avg)

# Get min and avg and find min per access point

accesspoint_min = {}

for model, selections in stats.items():
    for selection in selections.keys():
        stats[model][selection]['overall']['min'] = min(stats[model][selection]['overall']['values'])
        stats[model][selection]['overall']['avg'] = sum(stats[model][selection]['overall']['values']) / len(stats[model][selection]['overall']['values'])
        stats[model][selection]['overall'].pop('values')

        for accesspoint in stats[model][selection]['accesspoints'].keys():
            stats[model][selection]['accesspoints'][accesspoint]['min'] = min(stats[model][selection]['accesspoints'][accesspoint]['values'])
            stats[model][selection]['accesspoints'][accesspoint]['avg'] = sum(stats[model][selection]['accesspoints'][accesspoint]['values']) / len(stats[model][selection]['accesspoints'][accesspoint]['values'])
            stats[model][selection]['accesspoints'][accesspoint].pop('values')

            # accesspoint_min
            if selection not in accesspoint_min:
                accesspoint_min[selection] = {}

            if accesspoint not in accesspoint_min[selection]:
                accesspoint_min[selection][accesspoint] = {
                    'min': 9999
                }

            if stats[model][selection]['accesspoints'][accesspoint]['min'] < accesspoint_min[selection][accesspoint]['min']:
                accesspoint_min[selection][accesspoint] = {
                    'model': model,
                    'min': stats[model][selection]['accesspoints'][accesspoint]['min']
                }

global_min = 9999
for selection in accesspoint_min.keys():
    ap_sum = 0
    for ap in accesspoint_min[selection].values():
        ap_sum += ap['min']
    accesspoint_min[selection]['avg'] = ap_sum / len(accesspoint_min[selection])
    if accesspoint_min[selection]['avg'] < global_min:
        global_min = accesspoint_min[selection]['avg']

# Find global cumulative min
aps_global_min = {}
for selection in accesspoint_min.keys():
    for ap in accesspoint_min[selection].items():
        if ap[0] != 'avg':
            if ap[0] not in aps_global_min:
                aps_global_min[ap[0]] = {'v': 9999}
            if ap[1]['min'] < aps_global_min[ap[0]]['v']:
                aps_global_min[ap[0]] = {
                    'v': ap[1]['min'],
                    'm': ap[1]['model'],
                    's': selection
                }

pp = pprint.PrettyPrinter(indent=4)
pp.pprint(stats)
pp.pprint(accesspoint_min)
print("Global minimum: " + str(global_min))
pp.pprint(aps_global_min)
print("Global Composite Minimum: ")
print(sum(item['v'] for item in aps_global_min.values()) / len(aps_global_min))

# Graphs
for model, data in stats.items():

    fig, ax = plt.subplots(figsize=(6, 3.75))
    for axis in [ax.xaxis, ax.yaxis]:
        axis.set_major_locator(ticker.MaxNLocator(integer=True))

    x = list(data.keys())
    x = x[:-3]
    y = list(map(lambda z: data[z]['overall']['avg'], x))
    #y = y[:-3]
    c = model_colors[model] + 'o-'
    plt.plot(x, y, c, label='Average')

    y = list(map(lambda z: data[z]['overall']['min'], x))
    c = model_colors[model] + 'x-'
    plt.plot(x, y, c, label='Minimum')

    y = list(map(lambda z: accesspoint_min[z]['avg'], x))
    c = model_colors[model] + '.-'
    plt.plot(x, y, c, label="Composite Minimum")

    #plt.title('Model Error vs Composite Error: ' + model.title())
    plt.ylabel('Error (cm)')
    plt.xlabel('# Locations')
    plt.legend()

    #print(plt.ylim())
    #plt.ylim(120, 200)
    fig.savefig("algorithms/ranging/statistics/plots/composite_error_vs_model_error_" + model + ".pdf", format='pdf')
    plt.show()