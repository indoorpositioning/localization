import importlib
import itertools
import statistics
import time

import matplotlib.pyplot as plt

location_id = 6
client_id = 2
limiter = 1000
location_amount_start = 5
location_amount_end = 5
n_bins = 100
model_colors = {
    'linear': 'b',
    'exponential': 'g',
    'power': 'r',
    'poly2': 'c',
    'poly3': 'm',
    'leastsquaressoftl1': 'y',
    'poly2leastsquares': 'k'
}



def plot():

    # By model
    for model, selections in stats.items():

        fig, ax = plt.subplots(figsize=(8, 8))

        for selection, errors in selections.items():
            n, bins, patches = ax.hist(errors, n_bins, normed=1, histtype='step', cumulative=True, label='(' + f'{statistics.mean(errors):.2f}' + ' / ' + f'{statistics.median(errors):.2f}' + ')')
            n, bins, patches = ax.hist(additional_stats, n_bins, normed=1, histtype='step', cumulative=True, label='(' + f'{statistics.mean(additional_stats):.2f}' + ' / ' + f'{statistics.median(additional_stats):.2f}' + ')')

        # Tidy up
        #plt.xlim(0, 1000)
        ax.grid(True)
        ax.legend(loc='lower right')
        ax.set_title('CDF Ranging Error: ' + model)
        ax.set_xlabel('Error (cm)')
        ax.set_ylabel('CDF (%)')

        plt.show()
        fig.savefig("algorithms/ranging/statistics/plots/cdn_" + model + ".pdf", format='pdf')




stats = {}
additional_stats = []

# We want to get data for every model available
for model in ['linear']: #ranging_settings['active_models']:
    print("Model: " + model)
    stats[model] = {

    }
    module_ = importlib.import_module('algorithms.ranging.models.' + model)
    class_ = getattr(module_, model[0].upper() + model[1:])
    instance = class_(location_id=location_id)
    instance.client_id = client_id
    data = instance.get_data()

    first_ranging_device_key = (list(data['distances']))[0]
    num_locations = len(data['distances'][first_ranging_device_key])

    ranging_locations_translator = []
    for id, ranging_location in data['distances'][first_ranging_device_key].items():
        ranging_locations_translator.append(id)

    # Find the best combination on ranging locations possible => test them all
    for selection in range(location_amount_start, min(num_locations+1, location_amount_end+1)):

        linmin = 9999
        linmax = 0

        print("Selection: " + str(selection))
        start_time = time.time()

        # Build stats
        stats[model][selection] = []

        # Get number of combinations (for limiter)
        combinations = itertools.combinations(range(num_locations), selection)
        count = 0
        for combination in combinations:
            count += 1
        limiter_local = int(count / limiter)
        #print("(" + str(count) + "/" + str(limiter_local) + ")")

        combinations = itertools.combinations(range(num_locations), selection)

        count = 0
        for combination in combinations:
            count += 1
            if count < limiter_local:
                continue
            else:
                count = 0

            combination = list(combination)

            #print(combination)

            # Test each combination with each ranging device individually
            # (and later with all of them to see if the same combinations work best)
            all_ranging_devices = []
            for ranging_device_id, ranging_locations in data['distances'].items():

                ranging_device_results = []

                # Prepare ranging locations to be sent to model
                test_ranging_locations = {}
                for elm in combination:
                    if ranging_locations_translator[elm] in data['distances'][ranging_device_id]:
                        test_ranging_locations[ranging_locations_translator[elm]] = data['distances'][ranging_device_id][ranging_locations_translator[elm]]

                # Get model data
                model_data = instance.compute_data(test_ranging_locations)
                if model_data is False:
                    continue

                # Use this model to compare computed distance to each ranging location to read distance and take average
                for id, ranging_location in data['distances'][ranging_device_id].items():
                    distance = instance.get_distance(ranging_device_id, ranging_location['signal'], client_id, model_data)
                    diff = abs(distance - ranging_location['distance'])

                    stats[model][selection].append(diff)

                # Only use locations not used for regression
                combination_real_ids = list(map(lambda x: ranging_locations_translator[x], combination))
                for id, ranging_location in data['distances'][ranging_device_id].items():
                    if id not in combination_real_ids:
                        distance = instance.get_distance(ranging_device_id, ranging_location['signal'], client_id, model_data)
                        diff = abs(distance - ranging_location['distance'])
                        additional_stats.append(diff)


        elapsed_time = time.time() - start_time
        print("-- Time: " + str(elapsed_time))

plot()
