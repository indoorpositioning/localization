import importlib
import itertools
from random import shuffle
import time

import matplotlib.pyplot as plt
import numpy as np

from settings import ranging_settings

location_id = 8
client_id = 3
limiter = 300
location_amount_start = 38
location_amount_end = 40
n_bins = 3000
model_colors = {
    'linear': 'b',
    'exponential': 'g',
    'power': 'r',
    'poly2': 'c',
    'poly3': 'm',
    'leastsquaressoftl1': 'y',
    'poly2leastsquares': 'k'
}



def plot():

    # By model
    for model, selections in stats.items():

        fig, ax = plt.subplots(figsize=(8, 8))

        for selection, errors in selections.items():
            n, bins, patches = ax.hist(errors, n_bins, normed=1, histtype='step', cumulative=True, label="# locations: " + str(selection))

        # Tidy up
        plt.xlim(0, 1000)
        ax.grid(True)
        ax.legend(loc='lower right')
        ax.set_title('CDF Ranging Error: ' + model)
        ax.set_xlabel('Error (cm)')
        ax.set_ylabel('CDF (%)')

        #plt.show()
        fig.savefig("algorithms/ranging/statistics/plots/cdn_" + model + ".pdf", format='pdf')

    # By # location
    reversed_data = {}
    for model, selections in stats.items():
        for selection, errors in selections.items():
            if selection not in reversed_data:
                reversed_data[selection] = {}
            reversed_data[selection][model] = errors

    for selection, models in reversed_data.items():
        fig, ax = plt.subplots(figsize=(8, 8))
        for model, errors in models.items():
            ax.hist(errors, n_bins, normed=1, histtype='step', cumulative=True, label=model)

        # Tidy up
        plt.xlim(0, 1000)
        ax.grid(True)
        ax.legend(loc='lower right')
        ax.set_title('CDF Ranging Error: ' + str(selection))
        ax.set_xlabel('Error (cm)')
        ax.set_ylabel('CDF (%)')

        #plt.show()
        fig.savefig("algorithms/ranging/statistics/plots/cdn_" + str(selection) + ".pdf", format='pdf')


        # Different approach 1
        fig, ax = plt.subplots(figsize=(8, 8))
        for model, errors in models.items():
            values, base = np.histogram(errors, bins=100)
            cumulative = np.cumsum(values)
            ax.plot(base[:-1], cumulative, model_colors[model] + '-', label=model)
        plt.xlim(0, 1000)
        ax.grid(True)
        ax.legend(loc='lower right')
        ax.set_title('CDF Ranging Error: ' + str(selection))
        ax.set_xlabel('Error (cm)')
        ax.set_ylabel('CDF (%)')

        #plt.show()
        fig.savefig("algorithms/ranging/statistics/plots/cdn_1_" + str(selection) + ".pdf", format='pdf')


        # Different approach 1 full
        fig, ax = plt.subplots(figsize=(8, 8))
        for model, errors in models.items():
            values, base = np.histogram(errors, bins=100)
            cumulative = np.cumsum(values)
            ax.plot(base[:-1], cumulative, model_colors[model] + '-', label=model)
        ax.grid(True)
        ax.legend(loc='lower right')
        ax.set_title('CDF Ranging Error: ' + str(selection))
        ax.set_xlabel('Error (cm)')
        ax.set_ylabel('CDF (%)')

        #plt.show()
        fig.savefig("algorithms/ranging/statistics/plots/cdn_1_full_" + str(selection) + ".pdf", format='pdf')


        # Different approach 2
        fig, ax = plt.subplots(figsize=(8, 8))
        for model, errors in models.items():
            sorted = np.sort(errors)
            ax.step(sorted, np.arange(sorted.size), label=model)
        plt.xlim(0, 1000)
        ax.grid(True)
        ax.legend(loc='lower right')
        ax.set_title('CDF Ranging Error: ' + str(selection))
        ax.set_xlabel('Error (cm)')
        ax.set_ylabel('CDF (%)')

        #plt.show()
        fig.savefig("algorithms/ranging/statistics/plots/cdn_2_" + str(selection) + ".pdf", format='pdf')


        # Free width
        fig, ax = plt.subplots(figsize=(3.5, 3.5))
        for model, errors in models.items():
            ax.hist(errors, n_bins, normed=1, histtype='step', cumulative=True, label=model)

        ax.grid(True)
        ax.legend(loc='lower right')
        ax.set_title('CDF Ranging Error: ' + str(selection))
        ax.set_xlabel('Error (cm)')
        ax.set_ylabel('CDF (%)')

        #plt.show()
        fig.savefig("algorithms/ranging/statistics/plots/cdn_free_" + str(selection) + ".pdf", format='pdf')


stats = {}

# We want to get data for every model available
for model in ranging_settings['active_models']:
    print("Model: " + model)
    stats[model] = {

    }
    module_ = importlib.import_module('algorithms.ranging.models.' + model)
    class_ = getattr(module_, model[0].upper() + model[1:])
    instance = class_(location_id=location_id)
    instance.client_id = client_id
    data = instance.get_data()

    first_ranging_device_key = (list(data['distances']))[0]
    num_locations = len(data['distances'][first_ranging_device_key])

    ranging_locations_translator = []
    for id, ranging_location in data['distances'][first_ranging_device_key].items():
        ranging_locations_translator.append(id)

    # Find the best combination on ranging locations possible => test them all
    for selection in range(location_amount_start, min(num_locations+1, location_amount_end+1)):

        linmin = 9999
        linmax = 0

        print("Selection: " + str(selection))
        start_time = time.time()

        # Build stats
        stats[model][selection] = []

        # Get number of combinations (for limiter)
        # combinations = itertools.combinations(range(num_locations), selection)
        # count = 0
        # for combination in combinations:
        #     count += 1
        # limiter_local = int(count / limiter)
        # #print("(" + str(count) + "/" + str(limiter_local) + ")")
        #
        # combinations = itertools.combinations(range(num_locations), selection)
        #
        # count = 0

        locations = list(range(num_locations))
        combinations = []
        for i in range(limiter):
            shuffle(locations)
            combinations.append(locations[0:selection])

        for combination in combinations:
            # count += 1
            # if count < limiter_local:
            #     continue
            # else:
            #     count = 0
            #
            # combination = list(combination)

            #print(combination)

            # Test each combination with each ranging device individually
            # (and later with all of them to see if the same combinations work best)
            all_ranging_devices = []
            for ranging_device_id, ranging_locations in data['distances'].items():

                ranging_device_results = []

                # Prepare ranging locations to be sent to model
                test_ranging_locations = {}
                for elm in combination:
                    test_ranging_locations[ranging_locations_translator[elm]] = data['distances'][ranging_device_id][ranging_locations_translator[elm]]

                # Get model data
                model_data = instance.compute_data(test_ranging_locations)
                if model_data is False:
                    continue

                # Use this model to compare computed distance to each ranging location to read distance and take average
                for id, ranging_location in data['distances'][ranging_device_id].items():
                    distance = instance.get_distance(ranging_device_id, ranging_location['signal'], client_id, model_data)
                    diff = abs(distance - ranging_location['distance'])

                    #ranging_device_results.append(diff)
                    #print(diff)
                    #all_ranging_devices.append(diff)
                    stats[model][selection].append(diff)

                #avg = sum(ranging_device_results) / len(ranging_device_results)
                #all_ranging_devices.append(avg)

            #stats[model][selection].append(sum(all_ranging_devices)/len(all_ranging_devices))

                #stats[model][selection].append(avg)

                # if model == 'linear':
                #     if avg < linmin:
                #         linmin = avg
                #         print("Min: " + str(avg))
                #         print(combination)
                #     if avg > linmax:
                #         linmax = avg
                #         print("Max: " + str(avg))
                #         print(combination)


        elapsed_time = time.time() - start_time
        print("-- Time: " + str(elapsed_time))

plot()
