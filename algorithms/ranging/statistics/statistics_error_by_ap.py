from collections import namedtuple
from typing import Dict, List, Tuple

import numpy as np
from matplotlib import pyplot

from algorithms.ranging.models.poly3 import Poly3
from data.models import Location, RangingData, RangingLocation, RangingDevice

DistanceMuSigma = namedtuple('DistanceMuSigma', 'distance mu sigma')


class ErrorByDevices(object):
    ranging_locations: Dict[int, RangingLocation]
    ranging_devices: Dict[int, RangingDevice]
    location_device_distance_dict: Dict[int, Dict[int, Dict[int, float]]]
    location_device_data_dict: Dict[int, np.array]

    def __init__(self, location_id: int, client_id: int) -> None:
        client_id = client_id
        location_id = location_id
        location: Location = Location.get(Location.id == location_id)
        ranging_locations = RangingLocation.select().where(RangingLocation.location == location_id)
        ranging_datas: List[RangingData] = RangingData \
            .select(RangingData, RangingDevice, RangingLocation) \
            .join(RangingLocation) \
            .switch(RangingData) \
            .join(RangingDevice) \
            .where(RangingLocation.location == location_id) \
            # .group_by(RangingData.ranging_location, RangingData.ranging_device)
        # DataError = namedtuple('DataError', 'ranging_data error')
        data: Dict[RangingDevice, Dict[RangingLocation, List[float]]] = {}
        ranging: Poly3 = Poly3(location_id=location_id)
        for ranging_data in ranging_datas:
            device: RangingDevice = ranging_data.ranging_device
            location: RangingLocation = ranging_data.ranging_location
            if device not in data:
                data[device] = {}
            if location not in data[device]:
                data[device][location] = []
            ranging_results = ranging.get_distance(device.id, int(ranging_data.value), 3)
            distance = abs(location - device)
            data[device][location].append(abs(ranging_results - distance))
        # Sort the stuff by distance, calculate sigma and mu
        data: Dict[RangingDevice, List[Tuple[RangingLocation, List[float]]]]
        for ranging_device in data.keys():
            data[ranging_device] = sorted(data[ranging_device].items(), key=lambda loc: abs(ranging_device - loc[0]))
            data: Dict[RangingDevice, List[DistanceMuSigma]]
            data[ranging_device] = [DistanceMuSigma(abs(ranging_device - d[0]), np.std(d[1]), np.mean(d[1])) for d in
                                    data[ranging_device]]
        self.data: Dict[RangingDevice, List[DistanceMuSigma]]
        self.data = data

    def get_ranging_locations(self):
        return self.ranging_locations

    def plot(self):
        fig, ax = pyplot.subplots(figsize=(8, 8))

        for ranging_device, tuples in self.data.items():
            tuple: DistanceMuSigma
            x = [tuple.distance for tuple in tuples]
            mus = [tuple.mu for tuple in tuples]
            sigmas = [tuple.sigma for tuple in tuples]
            pyplot.plot(x, mus, 'o', label=str(ranging_device.id) + ' (mean: ' + f'{np.average(mus):.2f}' + ' / dev: '
                                           + f'{np.average(sigmas):.2f}' + ')')
            pyplot.plot(x, sigmas, 'x')

            # Tidy up
        # pyplot.xlim(0, 1000)
        ax.grid(True)
        ax.legend(loc='lower right', prop={'size': 18})
        ax.set_title('Measuring error by distance')
        ax.set_xlabel('Distance to device')
        ax.set_ylabel('offset cm')

        pyplot.show()
        fig.savefig("algorithms/ranging/statistics/plots/measuring_error_by_distance.png", format='png')


if __name__ == '__main__':
    statistic = ErrorByDevices(8, 3)
    statistic.plot()
    pass
