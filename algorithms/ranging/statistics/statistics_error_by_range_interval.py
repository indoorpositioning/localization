import itertools
import importlib
import time
import numpy as np
from scipy import stats as stat
import matplotlib.pyplot as plt
from settings import ranging_settings
import pprint
import math

location_id = 5
client_id = 3
limiter = 1000
location_amount_start = 26
location_amount_end = 26
model_colors = {
    'linear': 'b',
    'exponential': 'g',
    'power': 'r',
    'poly2': 'c',
    'poly3': 'm',
    'leastsquaressoftl1': 'y',
    'poly2leastsquares': 'k'
}


def plot():

    pp = pprint.PrettyPrinter(indent=4)
    #pp.pprint(stats['linear'])

    # By model
    for model, selections in stats.items():
        for selection, buckets in selections.items():

            averages = []
            sems = []
            sds = []
            buckets_s = []

            for bucket, errors in buckets.items():

                buckets_s.append(bucket)
                if len(errors):
                    averages.append(sum(errors) / len(errors))
                sems.append(stat.sem(errors))
                sds.append(np.std(errors))

            plt.errorbar(buckets_s, averages, sems, fmt='-o')
            plt.title('Mean Error: ' + model)
            plt.xlabel('Distance from Access Point')
            plt.ylabel('Error')
            plt.ylim(0, 500)
            plt.savefig("algorithms/ranging/statistics/plots/error_by_range_" + model + '_' + str(selection) + ".pdf", format='pdf')
            #plt.show()

            plt.errorbar(buckets_s, averages, sds, fmt='-o')
            plt.title('Standard Deviation: ' + model)
            plt.xlabel('Distance from Access Point')
            plt.ylabel('Deviation')
            plt.ylim(0, 500)
            plt.savefig("algorithms/ranging/statistics/plots/deviation_by_range_" + model + '_' + str(selection) + ".pdf", format='pdf')
            plt.show()


stats = {}

# We want to get data for every model available
for model in ranging_settings['active_models']:
    print("Model: " + model)
    stats[model] = {}
    module_ = importlib.import_module('algorithms.ranging.models.' + model)
    class_ = getattr(module_, model[0].upper() + model[1:])
    instance = class_(location_id=location_id)
    instance.client_id = client_id
    data = instance.get_data()

    first_ranging_device_key = (list(data['distances']))[0]
    num_locations = len(data['distances'][first_ranging_device_key])

    ranging_locations_translator = []
    for id, ranging_location in data['distances'][first_ranging_device_key].items():
        ranging_locations_translator.append(id)

    # Find the best combination on ranging locations possible => test them all
    for selection in range(location_amount_start, min(num_locations+1, location_amount_end+1)):

        print("Selection: " + str(selection))
        start_time = time.time()

        # Build stats
        stats[model][selection] = {
            '0-2': [],
            '2-4': [],
            '4-6': [],
            '6-8': [],
            '8-10': [],
            '10-12': [],
            '12-14': [],
            '14-16': []
        }

        # Get number of combinations (for limiter)
        combinations = itertools.combinations(range(num_locations), selection)
        count = 0
        for combination in combinations:
            count += 1
        limiter_local = int(count / limiter)
        #print("(" + str(count) + "/" + str(limiter_local) + ")")


        # if model == 'linear':
        #     print("Selection: " + str(selection))
        #     print("     Count: " + str(count))
        #     print("     Limiter: " + str(limiter))
        #     print("     Limiter Local: " + str(limiter_local))


        combinations = itertools.combinations(range(num_locations), selection)

        count = 0
        for combination in combinations:
            count += 1
            if count < limiter_local:
                continue
            else:
                count = 0

            combination = list(combination)

            #print(combination)

            # Test each combination with each ranging device individually
            # (and later with all of them to see if the same combinations work best)
            for ranging_device_id, ranging_locations in data['distances'].items():

                ranging_device_results = []

                # Prepare ranging locations to be sent to model
                test_ranging_locations = {}
                for elm in combination:
                    test_ranging_locations[ranging_locations_translator[elm]] = data['distances'][ranging_device_id][ranging_locations_translator[elm]]

                # Get model data
                model_data = instance.compute_data(test_ranging_locations)
                if model_data is False:
                    continue

                # Use this model to compare computed distance to each ranging location to read distance and take average
                for id, ranging_location in data['distances'][ranging_device_id].items():
                    distance = instance.get_distance(ranging_device_id, ranging_location['signal'], client_id, model_data)
                    diff = abs(distance - ranging_location['distance'])

                    if ranging_location['distance'] < 200:
                        stats[model][selection]['0-2'].append(diff)
                    elif 200 <= ranging_location['distance'] < 400:
                        stats[model][selection]['2-4'].append(diff)
                    elif 400 <= ranging_location['distance'] < 600:
                        stats[model][selection]['4-6'].append(diff)
                    elif 600 <= ranging_location['distance'] < 800:
                        stats[model][selection]['6-8'].append(diff)
                    elif 800 <= ranging_location['distance'] < 1000:
                        stats[model][selection]['8-10'].append(diff)
                    elif 1000 <= ranging_location['distance'] < 1200:
                        stats[model][selection]['10-12'].append(diff)
                    elif 1200 <= ranging_location['distance'] < 1400:
                        stats[model][selection]['12-14'].append(diff)
                    elif 1400 <= ranging_location['distance'] < 1600:
                        stats[model][selection]['14-16'].append(diff)
                    #ranging_device_results.append(diff)
                    #print(diff)

                #avg = sum(ranging_device_results) / len(ranging_device_results)


        elapsed_time = time.time() - start_time
        print("-- Time: " + str(elapsed_time))

plot()
