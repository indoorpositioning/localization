import importlib
import pprint
import time

import matplotlib.pyplot as plt
import numpy as np
from scipy import stats as stat

location_id = 8
client_id = 3
limiter = 1000
location_amount_start = 4
location_amount_end = 48
model_colors = {
    # 'linear': 'b',
    # 'exponential': 'g',
    # 'power': 'r',
    # 'poly2': 'c',
    'poly3': 'm',
    # 'leastsquaressoftl1': 'y',
    # 'poly2leastsquares': 'k'
}


def plot():

    pp = pprint.PrettyPrinter(indent=4)
    #pp.pprint(stats['linear'])

    # By model
    for model, selections in stats.items():

        averages = []
        sems = []
        sds = []

        for selection, errors in selections.items():

            averages.append(sum(errors) / len(errors))
            sems.append(stat.sem(errors))
            sds.append(np.std(errors))

        plt.errorbar(list(selections), averages, sems, fmt='-o')
        plt.title('Mean Error: ' + model)
        plt.xlabel('# Locations')
        plt.ylabel('Standard Error of the Mean')
        plt.ylim(0, 400)
        plt.savefig("algorithms/ranging/statistics/plots/error_" + model + ".pdf", format='pdf')
        plt.show()

        print(list(selections))
        print(averages)
        print(sds)
        plt.errorbar(list(selections), averages, sds, fmt='-o')
        plt.title('Standard deviation: ' + model)
        plt.xlabel('# Locations')
        plt.ylabel('Standard Deviation')
        plt.ylim(0, 400)
        plt.savefig("algorithms/ranging/statistics/plots/deviation_" + model + ".pdf", format='pdf')
        plt.show()


stats = {}

# We want to get data for every model available
for model in ['poly3']: #ranging_settings['active_models']:
    print("Model: " + model)
    stats[model] = {

    }
    module_ = importlib.import_module('algorithms.ranging.models.' + model)
    class_ = getattr(module_, model[0].upper() + model[1:])
    instance = class_(location_id=location_id)
    instance.client_id = client_id
    data = instance.get_data()

    first_ranging_device_key = (list(data['distances']))[0]
    num_locations = len(data['distances'][first_ranging_device_key])

    ranging_locations_translator = []
    for id, ranging_location in data['distances'][first_ranging_device_key].items():
        ranging_locations_translator.append(id)

    # Find the best combination on ranging locations possible => test them all
    # for selection in range(location_amount_start, min(num_locations+1, location_amount_end+1)):
    for selection in [49]:

        print("Selection: " + str(selection))
        start_time = time.time()

        # Build stats
        stats[model][selection] = []

        locations = list(range(num_locations))
        combinations = []
        # for i in range(limiter):
        #     shuffle(locations)
        #     combinations.append(locations[0:selection])
        combinations.append(locations)
        for combination in combinations:


            #print(combination)

            # Test each combination with each ranging device individually
            # (and later with all of them to see if the same combinations work best)
            all_ranging_devices = []
            for ranging_device_id, ranging_locations in data['distances'].items():

                ranging_device_results = []

                # Prepare ranging locations to be sent to model
                test_ranging_locations = {}
                for elm in combination:
                    test_ranging_locations[ranging_locations_translator[elm]] = data['distances'][ranging_device_id][ranging_locations_translator[elm]]

                # Get model data
                model_data = instance.compute_data(test_ranging_locations)
                if model_data is False:
                    continue

                # Use this model to compare computed distance to each ranging location to read distance and take average
                for id, ranging_location in data['distances'][ranging_device_id].items():
                    distance = instance.get_distance(ranging_device_id, ranging_location['signal'], client_id, model_data)
                    diff = abs(distance - ranging_location['distance'])

                    #stats[model][selection].append(diff)
                    #ranging_device_results.append(diff)
                    #print(diff)
                    all_ranging_devices.append(diff)

                #all_ranging_devices.append(sum(ranging_device_results) / len(ranging_device_results))
            print("Std: " + str(np.std(all_ranging_devices)))
            print("Mean: " + str(np.average(all_ranging_devices)))
            stats[model][selection].append(sum(all_ranging_devices)/len(all_ranging_devices))

        elapsed_time = time.time() - start_time
        print("-- Time: " + str(elapsed_time))

plot()
