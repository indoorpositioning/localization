import itertools
import importlib
import time
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from data.models import RangingLocation, Location
from PIL import Image, ImageDraw
from settings import ranging_settings
import pprint
import statistics
from random import shuffle

limiter = 500
location_id = 8
client_id = 3
min_selection = 4
max_selection = 35
model_colors = {
    'linear': 'b',
    'exponential': 'g',
    'power': 'r',
    'poly2': 'c',
    'poly3': 'm',
    'leastsquaressoftl1': 'y',
    'poly2leastsquares': 'k'
}


def plot():

    fig, ax = plt.subplots()
    for axis in [ax.xaxis, ax.yaxis]:
        axis.set_major_locator(ticker.MaxNLocator(integer=True))

    for model, data in stats.items():

        x = list(data.keys())
        mean = []
        median = []
        for d in data.values():
            mean.append(d['mean'])
            median.append(d['median'])

        c = model_colors[model] + 'o-'
        plt.plot(x, mean, c, label=model)

        c = model_colors[model] + 'x-'
        plt.plot(x, median, c, label=model)

    plt.title('Mean vs. Median Error')
    plt.ylabel('Error (cm)')
    plt.xlabel('# Locations')
    plt.legend()

    plt.ylim(100, 400)
    fig.savefig("algorithms/ranging/statistics/plots/mean_vs_median_" + model + ".pdf", format='pdf')
    plt.show()


def get_mean_median(results):
    # return {
    #     'mean': f'{statistics.mean(results):.2f}',
    #     'median':  f'{statistics.median(results):.2f}'
    # }
    return {
        'mean': statistics.mean(results),
        'median':  statistics.median(results)
    }



stats = {}

# We want to get data for every model available
for model in ranging_settings['active_models']:
    print("Model: " + model)
    stats[model] = {}
    module_ = importlib.import_module('algorithms.ranging.models.' + model)
    class_ = getattr(module_, model[0].upper() + model[1:])
    instance = class_(location_id=location_id)
    instance.client_id = client_id
    data = instance.get_data()

    first_ranging_device_key = (list(data['distances']))[0]
    num_locations = len(data['distances'][first_ranging_device_key])

    ranging_locations_translator = []
    for id, ranging_location in data['distances'][first_ranging_device_key].items():
        ranging_locations_translator.append(id)

    # Find the best combination on ranging locations possible => test them all
    for selection in range(min_selection, max_selection):

        print("Selection: " + str(selection))
        start_time = time.time()

        # Build stats
        stats[model][selection] = {}

        stats_all_storage = []

        locations = list(range(num_locations))
        combinations = []
        for i in range(limiter):
            shuffle(locations)
            combinations.append(locations[0:selection])

        temp_results = []

        for combination in combinations:

            # Test each combination with each ranging device individually
            # (and later with all of them to see if the same combinations work best)
            for ranging_device_id, ranging_locations in data['distances'].items():

                temp_results_ranging_device = []

                # Prepare ranging locations to be sent to model
                test_ranging_locations = {}
                for elm in combination:
                    try:
                        test_ranging_locations[ranging_locations_translator[elm]] = data['distances'][ranging_device_id][ranging_locations_translator[elm]]
                    except KeyError as e:
                        pass
                        #print(e)

                # Get model data
                model_data = instance.compute_data(test_ranging_locations)
                if model_data is False:
                    continue

                # Use this model to compare computed distance to each ranging location to read distance and take average
                for id, ranging_location in data['distances'][ranging_device_id].items():
                    if id not in test_ranging_locations.keys():
                        distance = instance.get_distance(ranging_device_id, ranging_location['signal'], client_id, model_data)
                        diff = abs(distance - ranging_location['distance'])

                        temp_results.append(diff)

        stats[model][selection] = get_mean_median(temp_results)
        del temp_results[:]

        elapsed_time = time.time() - start_time
        print("-- Time: " + str(elapsed_time))

plot()
#evaluate_positions()
#evaluate_combinations()
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(stats)
