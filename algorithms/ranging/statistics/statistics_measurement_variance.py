from data.models import RangingData
import numpy as np
import pprint


data = {}
for d in RangingData.select().where(RangingData.ranging_device >= 32).order_by(RangingData.ranging_device_id, RangingData.id):
    id = str(d.ranging_device_id) + '-' + str(d.ranging_location_id)
    if id not in data:
        data[id] = []
    data[id].append(float(d.value))

deviations = []
for key, values in data.items():
    deviations.append(np.std(values[0:15]))
    deviations.append(np.std(values[15:30]))
    deviations.append(np.std(values[30:45]))
    deviations.append(np.std(values[45:60]))

print(sum(deviations) / len(deviations))

#pp = pprint.PrettyPrinter(indent=4)
#pp.pprint(data)

'''
counter = {}
for d in RangingData.select().where(RangingData.ranging_device >= 32):
    if d.ranging_location_id not in counter:
        counter[d.ranging_location_id] = 0
    counter[d.ranging_location_id] += 1

for id in counter.keys():
    if counter[id] != 360:
        print(id)
        print(counter[id])
'''