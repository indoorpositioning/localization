import importlib
import itertools
import statistics
import time
import pprint
import matplotlib.pyplot as plt
import numpy as np
import random

location_id = 5
client_id = 3
limiter = 1000
location_amount = 15
n_bins = 100
model = 'poly3'
model_colors = {
    'linear': 'b',
    'exponential': 'g',
    'power': 'r',
    'poly2': 'c',
    'poly3': 'm',
    'leastsquaressoftl1': 'y',
    'poly2leastsquares': 'k'
}

def get_location_groups(data, amount):
    first_key = (list(data['distances']))[0]
    locations = list(data['distances'][first_key].keys())
    random.shuffle(locations)
    return locations[0:amount], locations[amount:]


module_ = importlib.import_module('algorithms.ranging.models.' + model)
class_ = getattr(module_, model[0].upper() + model[1:])
instance = class_(location_id=location_id)
instance.client_id = client_id
data = instance.get_data()

model_locations, test_locations = get_location_groups(data, 15)

for ranging_device_id, ranging_locations in data['distances'].items():

    # Get Model Data
    locations = {}
    for location in model_locations:
        locations['location'] = data['distances'][ranging_device_id][location]

    model_data = instance.compute_data(locations)






#pp = pprint.PrettyPrinter(indent=4)
#pp.pprint(data)


#distance = instance.get_distance(ranging_device_id, ranging_location['signal'], client_id, model_data)