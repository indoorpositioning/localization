import itertools
import importlib
import time
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from data.models import RangingLocation, Location
from PIL import Image, ImageDraw
from settings import ranging_settings
import pprint
from random import shuffle

limiter = 2000
location_id = 9
client_id = 3
min_selection = 5
max_selection = 40
model_colors = {
    'linear': 'b',
    'exponential': 'g',
    'power': 'r',
    'poly2': 'c',
    'poly3': 'm',
    'leastsquaressoftl1': 'y',
    'poly2leastsquares': 'k'
}


# We want to get data for every model available
model = 'linear'
module_ = importlib.import_module('algorithms.ranging.models.' + model)
class_ = getattr(module_, model[0].upper() + model[1:])
instance = class_(location_id=location_id)
instance.client_id = client_id
data = instance.get_data()

combination = range(46)
test_ranging_locations = {}
counter = 0

first_ranging_device_key = (list(data['distances']))[0]
num_locations = len(data['distances'][first_ranging_device_key])

ranging_locations_translator = []
for id, ranging_location in data['distances'][first_ranging_device_key].items():
    ranging_locations_translator.append(id)


for ranging_device_id, ranging_locations in data['distances'].items():

    ranging_device_results = []

    # Prepare ranging locations to be sent to model
    test_ranging_locations = {}
    for elm in combination:
        try:
            test_ranging_locations[ranging_locations_translator[elm]] = data['distances'][ranging_device_id][ranging_locations_translator[elm]]
        except KeyError as e:
            pass
            #print(e)
        counter += 1


    # Get model data
    model_data = instance.compute_data(test_ranging_locations)

    distance = instance.get_distance(ranging_device_id, -55.43, client_id, model_data)
    print(distance)
    distance = instance.get_distance(ranging_device_id, -54.16, client_id, model_data)
    print(distance)
