import matplotlib.pyplot as plt
import numpy as np
import importlib
import math
from data.models import RangingDevice
from settings import ranging_settings


client_id = 3
location_id = 11
model = 'poly3'
plot_width = 3

# Get Ranging Devices
ranging_devices = {}
for ranging_device in RangingDevice.select().where(RangingDevice.location_id == location_id):
    ranging_devices[ranging_device.id] = ranging_device.identifier

#for model in ranging_settings['active_models']:

# Instantiate model class
module_ = importlib.import_module('algorithms.ranging.models.' + model)
class_ = getattr(module_, model[0].upper() + model[1:])
instance = class_(location_id=location_id)
instance.create_models(client_id=client_id)
instance.device_id = client_id
data = instance.get_data()

# Initialize plot
plot_len = len(data['distances'].items())
plot_height = math.ceil(plot_len / plot_width)
plt.figure(figsize=(2*plot_width, 4*plot_height))

r_squareds = []
plot_index = 0

for ranging_device_id, ranging_data in data['distances'].items():
    signal = []
    distance = []

    for elm in ranging_data.values():
        signal.append(elm['signal'])
        distance.append(elm['distance'])

    plot_index += 1
    plt.subplot(plot_width, plot_height, plot_index)
    plt.plot(signal, distance, 'bo')

    r_squared = instance.get_r_squared(ranging_device_id, client_id, ranging_data)
    r_squareds.append(r_squared)

    sig = np.arange(min(signal), max(signal), 0.01)
    distance = instance.get_distance(ranging_device_id, sig, client_id)
    plt.plot(sig, distance)
    plt.ylim(0, 2000)

    plt.xlabel(ranging_devices[ranging_device_id] + " -- " + str(round(r_squared, 2)))
    plt.ylabel('Distance')

plt.suptitle('Regression Analysis: ' + model + ' -- ' + str(round(sum(r_squareds)/len(r_squareds), 2)), y=1.0, fontsize="x-large")
#plt.subplots_adjust(top=4, wspace=1, hspace=3)
plt.tight_layout()
plt.savefig("algorithms/ranging/statistics/plots/regression_analysis_" + model + ".pdf", format='pdf')
plt.show()




