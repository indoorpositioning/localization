import itertools
import importlib
import time
import matplotlib.pyplot as plt
import scipy.stats as ss
import numpy as np
from settings import ranging_settings
import pprint


n_bins = 10

numbers = [1,1,1,1,1,1,1,1,1,1,1,6]

fig, roll = plt.subplots(figsize=(8, 8))
roll.hist(numbers, n_bins, normed=1, histtype='step', cumulative=True)

# Tidy up
roll.grid(True)
roll.set_title('CDF Ranging Error')
roll.set_xlabel('Error (cm)')
roll.set_ylabel('CDF (%)')

plt.show()

