import math
from collections import namedtuple
from typing import Dict, Tuple

import numpy as np
from matplotlib import pyplot

from algorithms.particlefilter.vector import Vector
from data.models import Location, Client, RangingData, RangingLocation, RangingDevice

LocationClientTuple = namedtuple('LocationClientTuple', 'location client')  # type: Tuple[Location, Client]


class Signal2DistanceStatistic(object):
    ranging_locations: Dict[int, RangingLocation]
    ranging_devices: Dict[int, RangingDevice]
    location_device_distance_dict: Dict[int, Dict[int, Dict[int, float]]]
    location_device_data_dict: Dict[int, np.array]

    def __init__(self, client_location_tuples: [LocationClientTuple]) -> None:
        self.location_device_distance_dict = {}
        self.ranging_locations = {}
        self.ranging_devices = {}
        self.location_device_data_dict = {}
        for client_location_tuple in client_location_tuples:  # type: LocationClientTuple
            # Build a matrix with the distances of each ranging point to device
            client_id = client_location_tuple.client
            location_id = client_location_tuple.location
            location: Location = Location.get(Location.id == location_id)
            ranging_locations = RangingLocation.select().where(RangingLocation.location == location_id)
            ranging_devices = RangingDevice.select().where(RangingDevice.location == location_id)
            locations_position = np.array([Vector(loc.position_x, loc.position_y) for loc in ranging_locations])
            devices_position = np.array([Vector(device.position_x, device.position_y) for device in ranging_devices])
            location_device_vector_matrix = locations_position[:, np.newaxis] - devices_position[np.newaxis, :]
            location_device_distance_matrix = np.abs(location_device_vector_matrix)
            location_device_distance_matrix /= location.get_pixels_per_cm()
            location_device_distance_dict = {}
            # Convert it back to a dictionary so we know the keys
            for (x, ranging_location) in enumerate(ranging_locations):
                location_device_distance_dict[ranging_location] = {}
                for (y, ranging_device) in enumerate(ranging_devices):
                    location_device_distance_dict[ranging_location][ranging_device] = \
                        location_device_distance_matrix[x, y]
            self.location_device_distance_dict[location_id] = location_device_distance_dict
            self.ranging_locations[location_id] = ranging_locations
            self.ranging_devices[location_id] = ranging_devices
            # Build a matrix with lists of measurements between devices and locations
            ranging_datas: [RangingData] = RangingData.select().join(RangingLocation) \
                .where((RangingData.client == client_id) & (RangingLocation.location == location_id))
            # location_device_data_matrix = np.ndarray((len(ranging_locations), len(ranging_devices)))
            location_device_data_dict = {}
            for ranging_data in ranging_datas:  # type: RangingData
                ranging_location = ranging_data.ranging_location
                ranging_device = ranging_data.ranging_device
                device_dict = location_device_data_dict.get(ranging_location, {})
                data_list = device_dict.get(ranging_device, [])
                data_list.append(ranging_data)
                device_dict[ranging_device] = data_list
                location_device_data_dict[ranging_location] = device_dict
            self.location_device_data_dict[location_id] = location_device_data_dict

    def get_ranging_locations(self):
        return self.ranging_locations


class SignalSum2DistanceStatistic(Signal2DistanceStatistic):

    def __init__(self, client_location_tuples: [LocationClientTuple]) -> None:
        super().__init__(client_location_tuples)

    def plot(self):
        fig, ax = pyplot.subplots(figsize=(8, 8))

        for (location_id, _) in self.location_device_distance_dict.items():
            location_device_data_dict = self.location_device_data_dict[location_id]
            location_device_distance_dict = self.location_device_distance_dict[location_id]

            x = []
            y = []
            for (ranging_location, devices_dict) in location_device_data_dict.items():
                for (ranging_device, data_list) in devices_dict.items():
                    x.append(location_device_distance_dict[ranging_location][ranging_device])
                    y.append(sum([ranging_data.value for ranging_data in data_list]) / len(data_list))
            pyplot.plot(x, y, 'o')

            # Tidy up
        # pyplot.xlim(0, 1000)
        ax.grid(True)
        ax.legend(loc='lower right', prop={'size': 18})
        ax.set_title('Signal strength by distance')
        ax.set_xlabel('Distance to device')
        ax.set_ylabel('RSSI')

        pyplot.show()
        fig.savefig("algorithms/ranging/statistics/plots/signal_avg_by_distance.png", format='png')


class SignalDeviation2DistanceStatistic(Signal2DistanceStatistic):

    def __init__(self, client_location_tuples: [LocationClientTuple]) -> None:
        super().__init__(client_location_tuples)

    def plot(self):
        fig, ax = pyplot.subplots(figsize=(8, 8))

        for (location_id, _) in self.location_device_distance_dict.items():
            location_device_data_dict = self.location_device_data_dict[location_id]
            location_device_distance_dict = self.location_device_distance_dict[location_id]

            x = []
            y = []
            for (ranging_location, devices_dict) in location_device_data_dict.items():
                for (ranging_device, data_list) in devices_dict.items():
                    x.append(location_device_distance_dict[ranging_location][ranging_device])
                    values = [ranging_data.value for ranging_data in data_list]
                    avg = sum(values) / len(values)
                    summed = sum([(value - avg) **2 for value in values])
                    squared = math.sqrt(summed/len(values))
                    y.append(squared)
            pyplot.plot(x, y, 'o')

            # Tidy up
        # pyplot.xlim(0, 1000)
        ax.grid(True)
        ax.legend(loc='lower right', prop={'size': 18})
        ax.set_title('Signal strength standard deviation by distance')
        ax.set_xlabel('Distance to device')
        ax.set_ylabel('RSSI Standard Deviation')

        pyplot.show()
        fig.savefig("algorithms/ranging/statistics/plots/signal_deviation_by_distance.png", format='png')


class SignalNumberDatapoints2DistanceStatistic(Signal2DistanceStatistic):

    def __init__(self, client_location_tuples: [LocationClientTuple]) -> None:
        super().__init__(client_location_tuples)

    def plot(self):
        fig, ax = pyplot.subplots(figsize=(8, 8))

        for (location_id, _) in self.location_device_distance_dict.items():
            location_device_data_dict = self.location_device_data_dict[location_id]
            location_device_distance_dict = self.location_device_distance_dict[location_id]

            x = []
            y = []
            for (ranging_location, devices_dict) in location_device_data_dict.items():
                for (ranging_device, data_list) in devices_dict.items():
                    x.append(location_device_distance_dict[ranging_location][ranging_device])
                    y.append(len([item for item in data_list if item.value != 0]))
            pyplot.plot(x, y, 'o')

            # Tidy up
        # pyplot.xlim(0, 1000)
        ax.grid(True)
        ax.legend(loc='lower right', prop={'size': 18})
        ax.set_title('Captured data by distance')
        ax.set_xlabel('Distance to device')
        ax.set_ylabel('Number of datapoints per measurement')

        pyplot.show()
        fig.savefig("algorithms/ranging/statistics/plots/signal_number_by_distance.png", format='png')


if __name__ == '__main__':
    # statistic = SignalSum2DistanceStatistic([LocationClientTuple(7, 2), LocationClientTuple(5, 3)])
    # statistic.plot()
    # statistic = SignalDeviation2DistanceStatistic([LocationClientTuple(7, 2), LocationClientTuple(5, 3)])
    # statistic.plot()
    statistic = SignalNumberDatapoints2DistanceStatistic([LocationClientTuple(7, 2), LocationClientTuple(5, 3)])
    statistic.plot()
    pass
