from data.models import RangingData
import statistics

client_id = 3

ranging_data = {}
for data in RangingData.select().where(RangingData.client_id == client_id and RangingData.ranging_device_id >= 13):
    key = f'{data.ranging_device_id:02d}' + '-' + f'{data.ranging_location_id:02d}';
    if key not in ranging_data:
        ranging_data[key] = []
    ranging_data[key].append(data.value)


stdev_all = []

for key, data in ranging_data.items():
    stdev = statistics.stdev(data)
    stdev_all.append(stdev)
    print(key)
    print('=====')
    print('Standard Deviation: ' + f'{stdev:.2f}')
    for d in data:
        print(d, end=', ')
    print('')
    print('')

print('Average Standard Deviation')
print('==========================')
print(sum(stdev_all)/len(stdev_all))