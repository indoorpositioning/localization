from scipy.optimize import minimize
import math
from data.models import RangingLocation, RangingData, RangingDevice, fn, Location
from random import shuffle
import importlib
from settings import ranging_settings
from random import shuffle
import matplotlib.pyplot as plt
from algorithms.ranging.enhanced.final.room import get_trilateration_ranging_devices, Room


limiter = 400
location_id = 11
current_ranging_device_id = 45
client_id = 3
min_selection = 5
max_selection = 11
model_colors = {
    'linear': 'b',
    'exponential': 'g',
    'power': 'r',
    'poly2': 'c',
    'poly3': 'm',
    'leastsquaressoftl1': 'y',
    'poly2leastsquares': 'k'
}



def trilateration(anchors):

    def mean_squared_error(x, distances):
        mse = 0.0
        for ap in distances:
            dist2 = math.sqrt(math.pow(x[0] - float(ap['x']), 2) + math.pow(x[1] - float(ap['y']), 2))
            mse += ap['weight'] * math.pow(ap['distance'] - dist2, 2)
        return mse / len(distances)

    # Get starting position (average of anchors)
    average_position = [0, 0]
    for anchor in anchors:
        average_position[0] += anchor['x']
        average_position[1] += anchor['y']
    average_position[0] /= len(anchors)
    average_position[1] /= len(anchors)

    result = minimize(
        mean_squared_error,
        average_position,
        args=anchors,
        method='L-BFGS-B',
        options={
            'ftol': 1e-5,
            'maxiter': 1e+7
        })

    if result.success:
        return result.x
    else:
        return False


def get_conversion_ratio(location_id):
    location = Location.get(Location.id == location_id)
    x_dist = location.map_scale_end_x - location.map_scale_start_x
    y_dist = location.map_scale_end_y - location.map_scale_start_y
    distance_px = math.sqrt(pow(x_dist, 2) + pow(y_dist, 2))
    ratio = float(location.map_width) / distance_px
    return ratio

def cm2px(value, location_id):
    ratio = get_conversion_ratio(location_id)
    px = value / ratio
    return px

def px2cm(value, location_id):
    ratio = get_conversion_ratio(location_id)
    cm = value * ratio
    return cm






ranging_locations_translator = []
for ranging_location in RangingLocation.select().where(RangingLocation.location_id == location_id):
    ranging_locations_translator.append(ranging_location.id)
num_locations = len(ranging_locations_translator)

training_locations = list(range(num_locations))
shuffle(training_locations)
training_locations = training_locations[0:max_selection]

stats = {}
accesspoint_min = {}
for model in ranging_settings['active_models']:
    print("Model: " + model)
    stats[model] = {}

    module_ = importlib.import_module('algorithms.ranging.models.' + model)
    class_ = getattr(module_, model[0].upper() + model[1:])
    instance = class_(location_id=location_id)
    instance.client_id = client_id
    data = instance.get_data()

    for selection in range(min_selection, max_selection):

        print(selection)

        # Prepare stats object
        if selection not in stats[model]:
            stats[model][selection] = {
                'overall': {
                    'values': []
                },
                'accesspoints': {}
            }

        # Get combinations
        #locations = list(range(num_locations))
        locations = training_locations
        combinations = []
        for i in range(limiter):
            shuffle(locations)
            combinations.append(locations[0:selection])

        # For each combination...
        for combination in combinations:

            location_results = []

            for ranging_device_id, ranging_locations in data['distances'].items():

                if ranging_device_id not in accesspoint_min:
                    accesspoint_min[ranging_device_id] = {
                        'min': 9999,
                        'model_data': None,
                        'model': None
                    }

                if ranging_device_id not in stats[model][selection]['accesspoints']:
                    stats[model][selection]['accesspoints'][ranging_device_id] = {
                        'values': []
                    }

                ranging_device_results = []

                # Prepare ranging locations to be sent to model
                test_ranging_locations = {}
                for elm in combination:
                    try:
                        test_ranging_locations[ranging_locations_translator[elm]] = data['distances'][ranging_device_id][ranging_locations_translator[elm]]
                    except KeyError as e:
                        pass

                # Get model data
                model_data = instance.compute_data(test_ranging_locations)
                if model_data is False:
                    continue

                # Use this model to compare computed distance to each ranging location to read distance and take average
                for id, ranging_location in data['distances'][ranging_device_id].items():
                    distance = instance.get_distance(ranging_device_id, ranging_location['signal'], client_id, model_data)
                    diff = abs(distance - ranging_location['distance'])

                    ranging_device_results.append(diff)
                    location_results.append(diff)

                avg = sum(ranging_device_results) / len(ranging_device_results)
                del ranging_device_results[:]

                if avg < accesspoint_min[ranging_device_id]['min']:
                    accesspoint_min[ranging_device_id]['min'] = avg
                    accesspoint_min[ranging_device_id]['model_data'] = model_data
                    accesspoint_min[ranging_device_id]['model'] = model


total = 0
for rd in accesspoint_min.values():
    total += rd['min']
print(total / len(accesspoint_min))

# Get all models
models = {}
for model in ranging_settings['active_models']:
    module_ = importlib.import_module('algorithms.ranging.models.' + model)
    class_ = getattr(module_, model[0].upper() + model[1:])
    models[model] = class_(location_id=location_id)
    models[model].client_id = client_id

# Get locations etc.
data = {}
for d in RangingData \
        .select(RangingData.ranging_device_id, RangingData.ranging_location_id,
                fn.Avg(RangingData.value).alias('avg')) \
        .join(RangingDevice) \
        .where((RangingData.client_id == client_id) & (RangingDevice.location_id == location_id)) \
        .group_by(RangingData.ranging_device_id, RangingData.ranging_location_id):
    if d.ranging_location_id not in data:
        data[d.ranging_location_id] = {}
    data[d.ranging_location_id][d.ranging_device_id] = {'signal': float(d.avg)}


# Get ranging devices
ranging_devices = {}
for rd in RangingDevice.select().where(RangingDevice.location_id == location_id):
    ranging_devices[rd.id] = {
        'x': float(rd.position_x),
        'y': float(rd.position_y),
        'id': rd.id,
        'power_loss': rd.power_loss,
        'path_loss_efficient': rd.path_loss_efficient,
        'location_id': rd.location_id
    }

ranging_locations = {}
for rl in RangingLocation.select().where(RangingLocation.location_id == location_id):
    ranging_locations[rl.id] = {
        'x': float(rl.position_x),
        'y': float(rl.position_y)
    }








room = Room()
average_final_error = []
average_los_error = []
#trilateration_ranging_devices = get_trilateration_ranging_devices(current_ranging_device_id, ranging_devices)

trilateration_ranging_devices = []
for d in ranging_devices.values():
    if d['id'] != current_ranging_device_id:
        trilateration_ranging_devices.append(d['id'])

for loc_id, loc in data.items():
    print(" ")
    print(loc_id)
    print("====")
    signal = data[loc_id][current_ranging_device_id]['signal']
    los_distance = room.get_los_distance(ranging_devices[current_ranging_device_id], signal)
    anchors = [{
        'distance': los_distance,
        'x': ranging_devices[current_ranging_device_id]['x'],
        'y': ranging_devices[current_ranging_device_id]['y'],
        'weight': 5
    }]
    real_distance = math.sqrt(math.pow(ranging_devices[current_ranging_device_id]['x']-ranging_locations[loc_id]['x'], 2) + math.pow(ranging_devices[current_ranging_device_id]['y']-ranging_locations[loc_id]['y'], 2))
    los_error = px2cm(abs(los_distance - real_distance), location_id)
    average_los_error.append(los_error)
    print("LOS error: " + str(los_error))

    for ranging_device_id in trilateration_ranging_devices:
        signal = data[loc_id][ranging_device_id]['signal']
        distance = models[accesspoint_min[ranging_device_id]['model']].get_distance(ranging_device_id, signal, client_id)
        anchors.append({
            'distance': distance,
            'x': ranging_devices[ranging_device_id]['x'],
            'y': ranging_devices[ranging_device_id]['y'],
            'weight': 1
        })

    pos = trilateration(anchors)
    distance_px = math.sqrt(math.pow(pos[0]-ranging_locations[loc_id]['x'], 2) + math.pow(pos[1]-ranging_locations[loc_id]['y'], 2))
    distance_cm = px2cm(distance_px, location_id)
    print("Trilateration error: " + str(distance_cm))
    average_final_error.append(distance_cm)

    fig, ax = plt.subplots()
    ax.set_xlim((-1500, 4000))
    ax.set_ylim((-1500, 3000))
    for a in anchors:
        circle = plt.Circle((a['x'], a['y']), a['distance'], color=('b' if a['weight'] == 1 else 'r'), fill=False)
        ax.add_artist(circle)
    ax.add_artist(plt.Circle((pos[0], pos[1]), 50, color='g'))
    ax.add_artist(plt.Circle((ranging_locations[loc_id]['x'], ranging_locations[loc_id]['y']), 50, color='y'))
    ax.text(2600, 2700, "LOS error: " + str(f'{los_error:.2f}') + " cm")
    ax.text(2600, 2500, "Final error: " + str(f'{distance_cm:.2f}') + " cm")
    fig.show()

print("Average Final Error: " + str(sum(average_final_error) / len(average_final_error)))
print("Average LOS Error " + str(sum(average_los_error) / len(average_los_error)))

''' - NON LOS Tests
print(data)
for loc_id, loc in data.items():
    print(loc_id)
    print("====")

    ranging_location_ids = list(loc.keys())
    for amount in range(3, 7):
        attempt_average = []
        for attempt in range(0, 10):
            ids = ranging_location_ids
            shuffle(ids)
            ids = ids[0:amount]

            anchors = []
            for rang_id in ids:
                signal = data[loc_id][rang_id]
                anchors.append({
                    'distance': models[accesspoint_min[rang_id]['model']].get_distance(rang_id, signal['signal'], client_id, accesspoint_min[rang_id]['model_data']),
                    'x': ranging_devices[rang_id]['x'],
                    'y': ranging_devices[rang_id]['y'],
                    'weight': 1
                })

            pos = trilateration(anchors)
            distance_px = math.sqrt(math.pow(pos[0]-ranging_locations[loc_id]['x'], 2) + math.pow(pos[1]-ranging_locations[loc_id]['y'], 2))
            distance_cm = px2cm(distance_px, location_id)

            if distance_cm > 700:
                fig, ax = plt.subplots()
                ax.set_xlim((-1500,5000))
                ax.set_ylim((-1500,3000))
                for a in anchors:
                    circle = plt.Circle((a['x'], a['y']), a['distance'], color='b', fill=False)
                    ax.add_artist(circle)
                ax.add_artist(plt.Circle((pos[0], pos[1]), 50, color='r'))
                ax.add_artist(plt.Circle((ranging_locations[loc_id]['x'], ranging_locations[loc_id]['y']), 50, color='g'))
                fig.show()

            attempt_average.append(distance_cm)

        average = sum(attempt_average) / len(attempt_average)
        print(str(amount) + ": " + str(average))
        #print(attempt_average)
'''