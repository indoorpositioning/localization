from math import *

from data.models import Location, RangingDevice
from misc.error import DataError
from settings import ranging_settings


class Venue(object):

    def get_location(self, data):
        # TODO: Do not return locations that haven't yet set all required attributes. To do this: Identify essential attributes (map scale, at least one ranging device, etc.) for computation. Possibly store it in a boolean on location.
        if 'location' not in data:
            return False
        else:
            if data['location']['type'] == 'gps':
                latitude = data['location']['latitude']
                longitude = data['location']['longitude']
                for location in Location.select(Location.id, Location.gps_latitude, Location.gps_longitude):
                    if location.gps_latitude is not None and location.gps_longitude is not None:
                        distance = self.haversine(location.gps_longitude, location.gps_latitude, longitude, latitude)
                        if distance < ranging_settings['gps_detection_range']:
                            return location.id

                raise DataError('bad_gps_location', 'GPS location too far from next location!')

            else:
                devices = data['location']['devices']
                location_count = {}

                for device in RangingDevice.select():
                    if device.identifier in devices:
                        if device.location_id not in location_count:
                            location_count[device.location_id] = 0
                        location_count[device.location_id] += 1

                v = list(location_count.values())
                k = list(location_count.keys())

                if len(v) > 0 and max(v) > 0:
                    return k[v.index(max(v))]
                else:
                    raise DataError('bad_wifi_location', 'No matching WiFi hotspots found!')

    def get_ranging_devices(self, location_id, capabilities):

        ranging_devices = {}
        bluetooth = 'bluetooth'
        wifi = 'wifi'
        for ranging_device in RangingDevice.select().where(RangingDevice.location_id == location_id):
            if ranging_device.type == wifi and wifi in capabilities:
                ranging_devices.setdefault(wifi, []).append(ranging_device.identifier)
        if bluetooth in capabilities:
            proximity_uuid = Location.get(Location.id == location_id).proximityUUID
            if proximity_uuid:
                ranging_devices[bluetooth] = proximity_uuid

        return ranging_devices

    def haversine(self, lon1, lat1, lon2, lat2):
        """
        Calculate the great circle distance between two points
        on the earth (specified in decimal degrees)
        """
        # convert decimal degrees to radians
        lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

        # haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * asin(sqrt(a))
        r = 6371  # Radius of earth in kilometers. Use 3956 for miles
        return c * r
