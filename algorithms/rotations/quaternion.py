import numpy as np
import numbers
from math import sqrt, atan2, asin, degrees, radians



class Quaternion:
    """
    A class that implements quaternions
    """

    def __init__(self, w_or_q, x=None, y=None, z=None):
        """
        Initializes a Quaternion object
        :param w: A scalar representing the real part of the quaternion
        :param x: The first imaginary part if w is a scalar
        :param y: The second imaginary part if w is a scalar
        :param z: The third imaginary part if w is a scalar
        """
        self._q = np.array([1, 0, 0, 0])

        if x is not None and y is not None and z is not None:
            w = w_or_q
            q = np.array([w, x, y, z])
        elif isinstance(w_or_q, Quaternion):
            q = np.array(w_or_q.q)
        else:
            q = np.array(w_or_q)
            if len(q) != 4:
                raise ValueError("Expecting a 4-element array or w x y z as parameters")

        self._set_q(q)

    def conjugation(self):
        """
        Returns the conjugate of the quaternion
        """
        return Quaternion(self._q[0], -self._q[1], -self._q[2], -self._q[3])

    @staticmethod
    def from_angle_axis(rad, x, y, z):
        """
        We use this methode when we want to represent a rotation by a quaternion by getting the axis r = (x,y,z) and the angle rad
        Args:
            rad: The rotation angle
            x: component of the axis r
            y: component of the axis r
            z: component of the axis r

        Returns: The quaternion that realises the given rotation

        """
        c = np.cos(rad / 2)
        s = np.sin(rad / 2)
        return Quaternion(c, x * s, y * s, z * s)


    def quaternion_to_euler_angle(self):
        """
        roll around x-axis
        pitch around y-axis
        yaw around z-axis
        Returns: the three angles roll, pitch, yaw

        """
        w = self[0]
        x = self[1]
        y = self[2]
        z = self[3]
        #ysquared = y * y

        # #first try
        # t0 = 2.0 * (w * x + y * z)
        # t1 = 1.0 - 2.0 * (x * x + ysquared)
        # roll = np.math.degrees(np.math.atan2(t0, t1))
        #
        # t2 = 2.0 * (w * y - z * x)
        # t2 = +1.0 if t2 >= +1.0 else t2
        # t2 = -1.0 if t2 < -1.0 else t2
        # pitch = np.math.degrees(np.math.asin(t2))
        #
        # t3 = 2.0 * (w * z + x * y)
        # t4 = 1.0 - 2.0 * (ysquared + z * z)
        # yaw = np.math.degrees(np.math.atan2(t3, t4))

        roll = np.arctan2(2 * (w * x + y * z), w * w - (x * x) - (y * y) + z * z)
        pitchTemp = 2 * (x * z - (w * y))
        pitchTemp = +1.0 if pitchTemp >= +1.0 else pitchTemp
        pitchTemp = -1.0 if pitchTemp < -1.0 else pitchTemp
        pitch = - np.arcsin(pitchTemp)
        yaw = np.arctan2(2 * (x * y + w * z), w * w + x * x - (y * y) - (z * z))

        roll = roll * 180 / np.pi
        pitch = pitch * 180 / np.pi
        yaw = yaw * 180 / np.pi
        yaw = yaw + 2.07 # Magnetic declination in Bern, Switzerland is +2.07°

        return roll, pitch, yaw


    def to_euler_angles(self):
        """
        This method calculates the 3 euler rotation angles roll, pitch and yaw around the x, y and z axes
        Returns: roll, pitch and yaw

        """
        pitch = np.arcsin(2 * self[1] * self[2] + 2 * self[0] * self[3])
        if np.abs(self[1] * self[2] + self[3] * self[0] - 0.5) < 1e-8:
            roll = 0
            yaw = 2 * np.arctan2(self[1], self[0])
        elif np.abs(self[1] * self[2] + self[3] * self[0] + 0.5) < 1e-8:
            roll = -2 * np.arctan2(self[1], self[0])
            yaw = 0
        else:
            roll = np.arctan2(2 * self[0] * self[1] - 2 * self[2] * self[3], 1 - 2 * self[1] ** 2 - 2 * self[3] ** 2)
            yaw = np.arctan2(2 * self[0] * self[2] - 2 * self[1] * self[3], 1 - 2 * self[2] ** 2 - 2 * self[3] ** 2)
        return roll, pitch, yaw

    def to_euler_angles_in_degrees(self):
        anglesInRad = self.to_euler_angles()
        rollDeg = anglesInRad[0] * 180 / np.pi
        pitchDeg = anglesInRad[1] * 180 / np.pi
        yawDeg = anglesInRad[2] * 180 / np.pi
        return rollDeg, pitchDeg, yawDeg

    def to_euler123(self):
        roll = np.arctan2(-2*(self[2]*self[3] - self[0]*self[1]), self[0]**2 - self[1]**2 - self[2]**2 + self[3]**2)
        pitch = np.arcsin(2*(self[1]*self[3] + self[0]*self[1]))
        yaw = np.arctan2(-2*(self[1]*self[2] - self[0]*self[3]), self[0]**2 + self[1]**2 - self[2]**2 - self[3]**2)
        return roll, pitch, yaw

    def to_euler_new_try(self):
        yaw = np.degrees(atan2(2.0 * (self[1] * self[2] + self[0] * self[3]), self[0] * self[0] + self[1] * self[1] - self[2] * self[2] - self[3] * self[3]))
        pitch = degrees(-asin(2.0 * (self[1] * self[3] - self[0] * self[2])))
        roll = degrees(atan2(2.0 * (self[0] * self[1] + self[2] * self[3]), self[0] * self[0] - self[1] * self[1] - self[2] * self[2] + self[3] * self[3]))
        return  yaw, pitch, roll

    def calculate_rotation_matrix(self, pitch, yaw, roll):
        """
        This method creates the rotation matrix using the three rotation angles
        Args:
            pitch: rotation angle around the z axis
            yaw: rotation angle around the y axis
            roll: rotation angle around the x axis

        Returns: rotationMatrix

        """
        rollMatrix = np.matrix([[1, 0, 0],
                                [0, np.cos(roll), -np.sin(roll)],
                                [0, np.sin(roll), np.cos(roll)]])
        yawMatrix = np.matrix([[np.cos(yaw), 0, np.sin(yaw)],
                               [0, 1, 0],
                               [-np.sin(yaw),0, np.cos(yaw)]])
        pitchMatrix = np.matrix([[np.cos(pitch), -np.sin(pitch), 0],
                                 [np.sin(pitch), np.cos(pitch), 0],
                                 [0, 0, 1]])
        rotationMatrix = rollMatrix.dot(yawMatrix).dot(pitchMatrix)
        return rotationMatrix

    def get_rotation_matrix(self):
        angles = self.to_euler_angles()
        roll = angles[0]
        pitch = angles[1]
        yaw = angles[2]

        rotationMatrix = self.calculate_rotation_matrix(pitch, yaw, roll)

        return rotationMatrix

    def __mul__(self, other):
        """
        multiply the given quaternion with another quaternion or a scalar

        Definition:
        q = (x0,x1,x2,x3) p = (y0, y1, y2, y3)
        q*p =   (x0y0 - x1y1 - x2y2 - x3y3,
                 x0y1 + x1y0 + x2y3 - x3y2,
                 x0y2 - x1y3 + x2y0 + x3y1,
                 x0y3 + x1y2 - x2y1 + x3y0)

        :param other: a Quaternion object or a number
        :return:
        """
        if isinstance(other, Quaternion):
            w = self._q[0] * other._q[0] - self._q[1] * other._q[1] - self._q[2] * other._q[2] - self._q[3] * other._q[
                3]
            x = self._q[0] * other._q[1] + self._q[1] * other._q[0] + self._q[2] * other._q[3] - self._q[3] * other._q[
                2]
            y = self._q[0] * other._q[2] - self._q[1] * other._q[3] + self._q[2] * other._q[0] + self._q[3] * other._q[
                1]
            z = self._q[0] * other._q[3] + self._q[1] * other._q[2] - self._q[2] * other._q[1] + self._q[3] * other._q[
                0]

            return Quaternion(w, x, y, z)
        elif isinstance(other, numbers.Number):
            q = self._q * other
            return Quaternion(q)

    def __truediv__(self, other):

        q0 = self.q[0] / other
        q1 = self.q[1] / other
        q2 = self.q[2] / other
        q3 = self.q[3] / other

        return Quaternion(q0, q1, q2, q3)

    def __add__(self, other):
        """
        add two quaternions element-wise or add a scalar to each element of the quaternion
        :param other:
        :return:
        """
        if not isinstance(other, Quaternion):
            if len(other) != 4:
                raise TypeError("Quaternions must be added to other quaternions or a 4-element array")
            q = self.q + other
        else:
            q = self.q + other.q

        return Quaternion(q)

    def __sub__(self, other):
        """
        substracts two quaternions element-wise or add a scalar to each element of the quaternion
        :param other:
        :return:
        """
        if not isinstance(other, Quaternion):
            if len(other) != 4:
                raise TypeError("Quaternions must be added to other quaternions or a 4-element array")
            q = self.q - other
        else:
            q = self.q - other.q

        return Quaternion(q)

    def norm(self):
        q0square = self.q[0] * self.q[0]
        q1square = self.q[1] * self.q[1]
        q2square = self.q[2] * self.q[2]
        q3square = self.q[3] * self.q[3]

        qlength = np.sqrt(q0square + q1square + q2square + q3square)

        return qlength

    def _set_q(self, q):
        self._q = q

    def _get_q(self):
        return self._q

    def __getitem__(self, item):
        return self._q[item]

    q = property(_get_q, _set_q)
