import json
from datetime import datetime
from math import sqrt, atan2, asin, degrees

import numpy as np

from algorithms.rotations.quaternion import Quaternion
from algorithms.rotations.stepDetection import StepDetection
from algorithms.rotations.vectorCreator import VectorCreator

try:
    import utime as time
except ImportError:
    import time

"""
General
-------
Rotations calculates the heading of a device using a implementation of the Madgwick Algorithm, which needs
the sensor data of a Accelerometer, a Gyroscope and a Magnetometer. If a step is detected Rotations informs
the controller.

Heading
-------
The Madgwick Algorithm uses Quaterions to calculate the devices change of possition in space in real time.
Using a Quaternion we can calculate the Heading of a device which is a Value between -180° and 180°.
(0° when pointing North, - 90° when pointing East, 180° when pointing South and 90° when pointing West)

Step Detection
--------------
The Rotations class uses the StepDetection to trigger a movement. The Step Detection only needs the Acceleromenter
data to recognize steps.

Vector Creator
--------------
The VectorCreator class is used to create a two dimensional vector x pointing East and y pointing North.
As inputs we need the calculated heading and the length of a average step (AVERAGE_STEP_LENGTH).
To get the length of x and y we use then the inputs and the relation between Sinus and Cosinus.

Observer pattern
----------------
To "trigger" the Particle Filter when a step is detected I am using the observer pattern.

Visualization
-------------
The Visualization can be used to get an impression of the calculations done by the Madgwick Algorithm.
"""



class Rotations:
    quaternion = Quaternion(1, 0, 0, 0)
    gyroMeasError = np.pi * (40 / 180) # = 0.69813 rad = 40 degrees
    beta = 1.5 #np.sqrt(3 / 4) * gyroMeasError # = 0.60460 = 34,641 degrees
    samplePeriod = 1/100  # 1 / 256
    #samplePeriodForTheArray = 1/100
    doWeNeedToCalibrate = False
    theFirst1000Measurements = 1000 # The first 1000 measurements I use for the calibration of the magnetometer.
    magnetoMax = [0, 0, 0]
    magnetoMin = [0, 0, 0]
    magnetoBias = [0, 0, 0]
    magnetoSoftBias = [0, 0, 0]
    roll = 0
    pitch = 0
    yaw = 0
    heading = 0
    timestampLastSample = 0
    timestampCurrent = 0
    lastTimestampeOfLastJSONMessage = time.time() # This specification is only for the first run of the algorithm
    # visualization = Visualization()
    stepDetection = StepDetection(20, 20)
    vectorCreator = VectorCreator()
    AVERAGE_STEP_LENGTH = 65.0

    #csvOutputFile = 'C:/Users/lucien.madl/devFolder/AndroidClient/theResultFile.csv'

    def __init__(self, quaternion=None, beta=None):
        if quaternion is not None:
            self.quaternion = quaternion
        if beta is not None:
            self.beta = beta

        self.subscribers = set()

        self.stepDetection.register(self)

    def publish(self, numberOfSteps, timeStampStep):
        print("Rotation got informed about the step number: " + str(numberOfSteps) + " with timestampe: " + str(timeStampStep))
        print("The Heading at this time was: " + str(Rotations.heading))

        fd = open('resultFile.csv', 'a') # writes a file to the project folder
        fd.write("ts: "+ str(timeStampStep) + " number of Steps: " + str(numberOfSteps) + "Heading: " + str(self.heading) +
                "Quaternion" + str(self.quaternion) + "\n")
        fd.close()

        xAndy = self.vectorCreator.vectorsRelativtoTheInstitutBuilding(self.heading, self.AVERAGE_STEP_LENGTH)
        x = xAndy[0]
        y = xAndy[1]

        self.informTheController(x, y) # should be x and y not numberOfSteps and heading

    def register(self, who):
        self.subscribers.add(who)

    def unregister(self, who):
        self.subscribers.discard(who)

    def informTheController(self, x, y):
        for subscriber in self.subscribers:
            subscriber.observeRotation(x, y)

    def update(self, json_data):
        """Some documentation here.
        """
        jsonDataDumps = json.dumps(json_data) #wenn mit JSON Arrays [{},{},{},{}}} getested wird, können die JSON daten direkt in einem per for loop weitergegeben werden.
        jsonDataLoaded = json.loads(jsonDataDumps) # dumps und loads fallen dann weg und können auskommentiert werden...

        accelerStack = []  # The stacks are used if the json data (acceler, gyro and magneto) doesn't come in sync
        gyroStack = []
        magnetoStack = []

        timestampStack = []
        #for i in range(0, len(jsonDataLoaded)):
        # for oneJSON in jsonDataLoaded: # um mit JSON Arrays zu testen... Einfach jsonDataLoaded unten durch oneJSON ersetzen...

        starttime = (datetime.now()).microsecond

        # second try -------------------
        # try:
        #     self.firstTimestamp = jsonDataLoaded['d'][0]['t']
        #     self.lastTimestamp = jsonDataLoaded['d'][len(jsonDataLoaded['d'])-1]['t']
        #     passedtimeSeconds = (self.lastTimestamp - self.firstTimestamp) / 1000
        #     frequency = int(len(jsonDataLoaded['d']) / passedtimeSeconds)
        #     if(frequency != 0):
        #         self.samplePeriod = 1 / frequency
        # except:
        #     pass
        # second try -------------------

        try:
            indexOfLastElement = len(jsonDataLoaded['d'])
            timestampLastSample = jsonDataLoaded['d'][0]['t'] - 20 #epoch_millis_from_timestamp(jsonDataLoaded['d'][0]['t']) - 20
        except:
            pass

        for i in range(0, indexOfLastElement):
            try:
                timestampCurrent = jsonDataLoaded['d'][0]['t'] - 20 #epoch_millis_from_timestamp(jsonDataLoaded['d'][i]['t'])
                if(i == 0):
                    timestampLastSample = self.lastTimestampeOfLastJSONMessage
                    self.lastTimestampeOfLastJSONMessage = jsonDataLoaded['d'][indexOfLastElement]['t']
                else:
                    timestampLastSample = jsonDataLoaded['d'][i-1]['t']
                passedTimeInSeconds = 1000 / abs(timestampCurrent - timestampLastSample)
                self.samplePeriod = 1 / passedTimeInSeconds

                # --------------------older try
                # If it's the first object of a JSON array we need to set another frequency
                # if (i == 0):
                #     difference = (timestampCurrent - self.lastTimestampeOfLastJSONMessage) / 1000
                #     firstFrequency = 2 / difference
                #     self.samplePeriod = 1 / firstFrequency
                #
                # # If it's the last object of a JSON array I want to save the timestamp for the next incoming JSON Array
                # elif (i == len(jsonDataLoaded['d'])):
                #     self.lastTimestampeOfLastJSONMessage = timestampCurrent

                # timeBetweenInMilliSeconds = abs(timestampCurrent - self.timestampLastSample)
                # if(timeBetweenInMilliSeconds != 0):
                #     samplesPerSecond = 1000 / timeBetweenInMilliSeconds
                # else:
                #     samplesPerSecond = 1
                # self.timestampLastSample = timestampCurrent
                # self.samplePeriod = 1 / samplesPerSecond
                #--------------------------------------
            except:
                pass

            try:
                acceler = jsonDataLoaded['d'][i]['a']
                tempAcceler = acceler
                acceler[0] = tempAcceler[1]  # Convert the accelerator data from xEast, yNorth and zUp to xNorth, yEast, zDown
                acceler[1] = tempAcceler[0]
                acceler[2] = - tempAcceler[2]
                accelerStack.append(acceler)
            except:
                pass
            try:
                gyro = jsonDataLoaded['d'][i]['g']
                tempGyro = gyro
                gyro[0] = tempGyro[1] #* np.pi / 180  # Convert the gyroscope data from xEast, yNorth and zUp to xNorth, yEast, zDown
                gyro[1] = tempGyro[0] #* np.pi / 180
                gyro[2] = - tempGyro[2] #* np.pi / 180
                gyroStack.append(gyro)
            except:
                pass
            try:
                magneto = jsonDataLoaded['d'][i]['m']
                tempMagneto = magneto
                magneto[0] = tempMagneto[1]  # Convert the magneto data from xEast, yNorth and zUp to xNorth, yEast, zDown
                magneto[1] = tempMagneto[0]
                magneto[2] = - tempMagneto[2]
                magnetoStack.append(magneto)
            except:
                pass

            # If there is a value in every stack we will update the orientation
            #vector = None
            if accelerStack != [] and gyroStack != [] and magnetoStack != []:
                latestAcceler = accelerStack.pop()
                latestGyro = gyroStack.pop()
                latestMagneto = magnetoStack.pop()
                if self.doWeNeedToCalibrate:
                    self.calculateMagnetoMaxNMin(latestMagneto)
                    self.theFirst1000Measurements -= 1
                    if self.theFirst1000Measurements == 0:
                        self.doWeNeedToCalibrate = False
                        self.calibrateHard()
                        #self.calibrateSoft()
                        print("Magneto Max: " + str(self.magnetoMax))
                        print("Magneto Min: " + str(self.magnetoMin))
                        print("Magneto Bias: " + str(self.magnetoBias))
                        print("Magneto Soft Bias: " + str(self.magnetoSoftBias))
                else:
                    self.calculateChange(self.timestampCurrent, latestAcceler, latestGyro, latestMagneto)
                    self.stepDetection.detectStep(latestAcceler, timestampCurrent)
                    #if self.stepDetection.detectStep(latestAcceler, self.timestampCurrent):
                        # vector = self.vectorCreator.vectorsHeSaidSoGodDammitPythagorasStopWatchingAndGiveMeSomeVectors(
                        #     self.heading, self.AVERAGE_STEP_LENGTH)
                       #vector = Vector(vector[0], vector[1])
                print("Elapsed Time " + str((datetime.now()).microsecond - starttime))
            #return vector


    # Hard iron biases are typically the largest and the easiest errors to correct for. The simplest way to correct
    # for them is to record a bunch of magnetometer data as the sensor is moved slowly in a figure eight pattern
    # and keep track of the minimum and maximum field measured in each of the six principal directions;
    # +/- Mx, +/- My, +/- Mz. Once the min/max values along the three axes are known, the average can be subtracted
    # from the subsequent data which amounts to re-centering the response surface on the origin.


    def calculateMagnetoMaxNMin(self, magnetoData):
        if (self.magnetoMax[0] < magnetoData[0]):
            self.magnetoMax[0] = magnetoData[0]
        if (self.magnetoMax[1] < magnetoData[1]):
            self.magnetoMax[1] = magnetoData[1]
        if (self.magnetoMax[2] < magnetoData[2]):
            self.magnetoMax[2] = magnetoData[2]
        if (self.magnetoMin[0] > magnetoData[0]):
            self.magnetoMin[0] = magnetoData[0]
        if (self.magnetoMin[1] > magnetoData[1]):
            self.magnetoMin[1] = magnetoData[1]
        if (self.magnetoMin[2] > magnetoData[2]):
            self.magnetoMin[2] = magnetoData[2]


    def calibrateHard(self):
        # Get hard iron correction
        self.magnetoBias[0] = (self.magnetoMax[0] + self.magnetoMin[0]) / 2
        self.magnetoBias[1] = (self.magnetoMax[1] + self.magnetoMin[1]) / 2
        self.magnetoBias[2] = (self.magnetoMax[2] + self.magnetoMin[2]) / 2

    def calibrateSoft(self):
        # Get soft iron correction
        magnetoScale = [0, 0, 0]
        magnetoScale[0] = (self.magnetoMax[0] - self.magnetoMin[0]) / 2
        magnetoScale[1] = (self.magnetoMax[1] - self.magnetoMin[1]) / 2
        magnetoScale[2] = (self.magnetoMax[2] - self.magnetoMin[2]) / 2
        magnetoScaleTotal = magnetoScale[0] + magnetoScale[1] + magnetoScale[2]
        avgScale = magnetoScaleTotal / 3
        self.magnetoSoftBias[0] = avgScale / magnetoScale[0]
        self.magnetoSoftBias[1] = avgScale / magnetoScale[1]
        self.magnetoSoftBias[2] = avgScale / magnetoScale[2]

    def calculateChange(self, timestamp, accel, gyro, magneto):  # 3-tuples (x, y, z) for accel, gyro and mag data
        if (self.magnetoSoftBias != [0,0,0]):
            mx = self.magnetoSoftBias[0] * np.array(magneto, dtype=float).flatten()[0] - self.magnetoBias[0]
            my = self.magnetoSoftBias[1] * np.array(magneto, dtype=float).flatten()[1] - self.magnetoBias[1]
            mz = self.magnetoSoftBias[2] * np.array(magneto, dtype=float).flatten()[2] - self.magnetoBias[2]
        else:
            mx = np.array(magneto, dtype=float).flatten()[0] - self.magnetoBias[0]
            my = np.array(magneto, dtype=float).flatten()[1] - self.magnetoBias[1]
            mz = np.array(magneto, dtype=float).flatten()[2] - self.magnetoBias[2]

        ax = np.array(accel, dtype=float).flatten()[0]
        ay = np.array(accel, dtype=float).flatten()[1]
        az = np.array(accel, dtype=float).flatten()[2] # accel  # Units irrelevant (normalised)
        gx = np.array(gyro, dtype=float).flatten()[0]
        gy = np.array(gyro, dtype=float).flatten()[1]
        gz = np.array(gyro, dtype=float).flatten()[2] #(radians(x) for x in gyro)  # Units deg/s
        q1, q2, q3, q4 = (self.quaternion[x] for x in range(4))  # short name local variable for readability
        # Auxiliary variables to avoid repeated arithmetic
        _2q1 = 2 * q1
        _2q2 = 2 * q2
        _2q3 = 2 * q3
        _2q4 = 2 * q4
        _2q1q3 = 2 * q1 * q3
        _2q3q4 = 2 * q3 * q4
        q1q1 = q1 * q1
        q1q2 = q1 * q2
        q1q3 = q1 * q3
        q1q4 = q1 * q4
        q2q2 = q2 * q2
        q2q3 = q2 * q3
        q2q4 = q2 * q4
        q3q3 = q3 * q3
        q3q4 = q3 * q4
        q4q4 = q4 * q4

        # Normalise accelerometer measurement
        norm = sqrt(ax * ax + ay * ay + az * az)
        if norm == 0:
            return  # handle NaN
        norm = 1 / norm  # use reciprocal for division
        ax *= norm
        ay *= norm
        az *= norm

        # Normalise magnetometer measurement
        norm = sqrt(mx * mx + my * my + mz * mz)
        if norm == 0:
            return  # handle NaN
        norm = 1 / norm  # use reciprocal for division
        mx *= norm
        my *= norm
        mz *= norm

        # Reference direction of Earth's magnetic field
        _2q1mx = 2 * q1 * mx
        _2q1my = 2 * q1 * my
        _2q1mz = 2 * q1 * mz
        _2q2mx = 2 * q2 * mx
        hx = mx * q1q1 - _2q1my * q4 + _2q1mz * q3 + mx * q2q2 + _2q2 * my * q3 + _2q2 * mz * q4 - mx * q3q3 - mx * q4q4
        hy = _2q1mx * q4 + my * q1q1 - _2q1mz * q2 + _2q2mx * q3 - my * q2q2 + my * q3q3 + _2q3 * mz * q4 - my * q4q4
        _2bx = sqrt(hx * hx + hy * hy)
        _2bz = -_2q1mx * q3 + _2q1my * q2 + mz * q1q1 + _2q2mx * q4 - mz * q2q2 + _2q3 * my * q4 - mz * q3q3 + mz * q4q4
        _4bx = 2 * _2bx
        _4bz = 2 * _2bz

        # Gradient descent algorithm corrective step
        s1 = (-_2q3 * (2 * q2q4 - _2q1q3 - ax) + _2q2 * (2 * q1q2 + _2q3q4 - ay) - _2bz * q3 * (
                    _2bx * (0.5 - q3q3 - q4q4)
                    + _2bz * (q2q4 - q1q3) - mx) + (-_2bx * q4 + _2bz * q2) * (
                          _2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my)
              + _2bx * q3 * (_2bx * (q1q3 + q2q4) + _2bz * (0.5 - q2q2 - q3q3) - mz))

        s2 = (_2q4 * (2 * q2q4 - _2q1q3 - ax) + _2q1 * (2 * q1q2 + _2q3q4 - ay) - 4 * q2 * (
                    1 - 2 * q2q2 - 2 * q3q3 - az)
              + _2bz * q4 * (_2bx * (0.5 - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx) + (_2bx * q3 + _2bz * q1) * (
                          _2bx * (q2q3 - q1q4)
                          + _2bz * (q1q2 + q3q4) - my) + (_2bx * q4 - _4bz * q2) * (
                          _2bx * (q1q3 + q2q4) + _2bz * (0.5 - q2q2 - q3q3) - mz))

        s3 = (-_2q1 * (2 * q2q4 - _2q1q3 - ax) + _2q4 * (2 * q1q2 + _2q3q4 - ay) - 4 * q3 * (
                    1 - 2 * q2q2 - 2 * q3q3 - az)
              + (-_4bx * q3 - _2bz * q1) * (_2bx * (0.5 - q3q3 - q4q4) + _2bz * (q2q4 - q1q3) - mx)
              + (_2bx * q2 + _2bz * q4) * (_2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my)
              + (_2bx * q1 - _4bz * q3) * (_2bx * (q1q3 + q2q4) + _2bz * (0.5 - q2q2 - q3q3) - mz))

        s4 = (_2q2 * (2 * q2q4 - _2q1q3 - ax) + _2q3 * (2 * q1q2 + _2q3q4 - ay) + (-_4bx * q4 + _2bz * q2) * (
                    _2bx * (0.5 - q3q3 - q4q4)
                    + _2bz * (q2q4 - q1q3) - mx) + (-_2bx * q1 + _2bz * q3) * (
                          _2bx * (q2q3 - q1q4) + _2bz * (q1q2 + q3q4) - my)
              + _2bx * q2 * (_2bx * (q1q3 + q2q4) + _2bz * (0.5 - q2q2 - q3q3) - mz))

        norm = 1 / sqrt(s1 * s1 + s2 * s2 + s3 * s3 + s4 * s4)  # normalise step magnitude
        s1 *= norm
        s2 *= norm
        s3 *= norm
        s4 *= norm

        # Compute rate of change of quaternion
        qDot1 = 0.5 * (-q2 * gx - q3 * gy - q4 * gz) - self.beta * s1
        qDot2 = 0.5 * (q1 * gx + q3 * gz - q4 * gy) - self.beta * s2
        qDot3 = 0.5 * (q1 * gy - q2 * gz + q4 * gx) - self.beta * s3
        qDot4 = 0.5 * (q1 * gz + q2 * gy - q3 * gx) - self.beta * s4

        # Integrate to yield quaternion
        q1 += qDot1 * self.samplePeriod
        q2 += qDot2 * self.samplePeriod
        q3 += qDot3 * self.samplePeriod
        q4 += qDot4 * self.samplePeriod
        norm = 1 / sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4)  # normalise quaternion

        self.quaternion = q1 * norm, q2 * norm, q3 * norm, q4 * norm
        self.heading = degrees(atan2(2.0 * (self.quaternion[1] * self.quaternion[2] + self.quaternion[0] * self.quaternion[3]),
                                                        self.quaternion[0] * self.quaternion[0] + self.quaternion[1] * self.quaternion[1] - self.quaternion[2] *
                                                        self.quaternion[2] - self.quaternion[3] * self.quaternion[3]))
        self.pitch = degrees(-asin(2.0 * (self.quaternion[1] * self.quaternion[3] - self.quaternion[0] * self.quaternion[2])))
        self.roll = degrees(atan2(2.0 * (self.quaternion[0] * self.quaternion[1] + self.quaternion[2] * self.quaternion[3]),
                                  self.quaternion[0] * self.quaternion[0] - self.quaternion[1] * self.quaternion[1] - self.quaternion[2] * self.quaternion[2] + self.quaternion[3] *
                                  self.quaternion[3]))

        #Rotations.heading = self.heading
        #Rotations.quaternion = self.quaternion

        # print("timestamp: " + str(timestamp))
        # print("sampleperiode: " + str(self.samplePeriod))
        # print("Quaterion: " + str(self.quaternion))
        # print("roll: " + str(self.roll))
        # print("pitch: " + str(self.pitch))
        # print("heading: " + str(self.heading) + "\n")

        self.visualization.setTheEulerAngles(self.roll, self.pitch, self.heading)



#
# jsonTestDataArray = [{"t":"d","c":"86","d":[{"t":"1525639327734","a":[-1.6664152145385742,3.754222869873047,8.607418060302734],"g":[-0.0012217304902151227,-0.012217304669320583,-0.01710422709584236],"m":[0.9000000357627869,-26.30000114440918,2.6000001430511475]}]},
# {"t":"d","c":"26","d":[{"t":"1525639327886","a":[-1.8986599445343018,4.2330780029296875,7.8580098152160645],"g":[0.6401867866516113,-0.013439035043120384,-0.019547687843441963],"m":[2,-26.80000114440918,4.900000095367432]}]},
# {"t":"d","c":"90","d":[{"t":"1525639328065","a":[-1.9082369804382324,4.024775981903076,8.071100234985352],"g":[0.11239920556545258,-0.010995574295520782,-0.006108652334660292],"m":[3.9000000953674316,-27,7.200000286102295]}]},
# {"t":"d","c":"22","d":[{"t":"1525639328246","a":[-2.14048171043396,3.993650436401367,8.913885116577148],"g":[-0.012217304669320583,0.04153883829712868,0.004886921960860491],"m":[3.9000000953674316,-26.700000762939453,7.099999904632568]}]},
# {"t":"d","c":"31","d":[{"t":"1525639328426","a":[-1.874717116355896,3.8547823429107666,8.813325881958008],"g":[0.01710422709584236,-0.05864306539297104,-0.013439035043120384],"m":[4,-26.700000762939453,7.300000190734863]}]},
# {"t":"d","c":"56","d":[{"t":"1525639328605","a":[-1.874717116355896,3.8835136890411377,8.846845626831055],"g":[0.02321287989616394,-0.00855211354792118,-0.01710422709584236],"m":[3.9000000953674316,-26.899999618530273,6.800000190734863]}]},
# {"t":"d","c":"57","d":[{"t":"1525639328785","a":[-2.1620302200317383,4.034353256225586,8.724737167358398],"g":[-0.025656340643763542,-0.01466076634824276,-0.00733038317412138],"m":[3.9000000953674316,-26.80000114440918,6.700000286102295]}]},
# {"t":"d","c":"69","d":[{"t":"1525639328965","a":[-2.104567527770996,4.003227710723877,8.980924606323242],"g":[0.01710422709584236,-0.013439035043120384,-0.021991148591041565],"m":[3.799999952316284,-26.700000762939453,6.800000190734863]}]},
# {"t":"d","c":"85","d":[{"t":"1525639329146","a":[-2.1261160373687744,4.058295726776123,8.99289608001709],"g":[-0.02321287989616394,0.004886921960860491,-0.0354301854968071],"m":[3.6000001430511475,-26.30000114440918,6.700000286102295]}]},
# {"t":"d","c":"36","d":[{"t":"1525639329326","a":[-2.0255565643310547,3.9577362537384033,8.877970695495605],"g":[-0.019547687843441963,-0.019547687843441963,-0.015882495790719986],"m":[3.700000047683716,-26,6.700000286102295]}]},
# {"t":"d","c":"4","d":[{"t":"1525639329506","a":[-1.939362645149231,3.9290049076080322,8.846845626831055],"g":[-0.021991148591041565,0.02687807008624077,-0.025656340643763542],"m":[3.799999952316284,-25.899999618530273,6.800000190734863]}]},
# {"t":"d","c":"51","d":[{"t":"1525639329687","a":[-2.073441982269287,3.9864675998687744,8.820508003234863],"g":[0.009773843921720982,-0.004886921960860491,0.0024434609804302454],"m":[3.6000001430511475,-25.5,6.400000095367432]}]},
# {"t":"d","c":"99","d":[{"t":"1525639329866","a":[-1.9728823900222778,3.9792847633361816,8.985713005065918],"g":[0.03787364438176155,0.015882495790719986,0.01466076634824276],"m":[4,-25.30000114440918,6.900000095367432]}]},
# {"t":"d","c":"79","d":[{"t":"1525639330046","a":[-2.4948344230651855,4.314483165740967,8.344047546386719],"g":[-0.013439035043120384,0.2028072625398636,-0.1771509200334549],"m":[4.700000286102295,-25.600000381469727,7.900000095367432]}]},
# {"t":"d","c":"60","d":[{"t":"1525639330226","a":[-2.6791934967041016,4.273780345916748,8.523618698120117],"g":[-0.1417207419872284,-0.7257078886032104,-0.43127086758613586],"m":[6.300000190734863,-26,9.600000381469727]}]},
# {"t":"d","c":"74","d":[{"t":"1525639330406","a":[-1.9321798086166382,3.3040993213653564,9.577098846435547],"g":[-0.051312681287527084,-0.12461651116609573,0.006108652334660292],"m":[6.900000095367432,-25.100000381469727,9.100000381469727]}]},
# {"t":"d","c":"75","d":[{"t":"1525639330586","a":[-1.831620216369629,3.2083282470703125,9.225140571594238],"g":[-0.10262536257505417,-0.0012217304902151227,0.1319468915462494],"m":[6,-23.80000114440918,7.099999904632568]}]},
# {"t":"d","c":"1","d":[{"t":"1525639330766","a":[-1.7262721061706543,3.5243725776672363,10.204399108886719],"g":[0.6572909951210022,0.3933972120285034,-0.1319468915462494],"m":[5.599999904632568,-23.399999618530273,6.099999904632568]}]},
# {"t":"d","c":"99","d":[{"t":"1525639330946","a":[-4.0128045082092285,5.073468208312988,6.0623040199279785],"g":[1.1740829944610596,1.017701506614685,-0.10384709388017654],"m":[12.300000190734863,-28.100000381469727,14.90000057220459]}]},
# {"t":"d","c":"16","d":[{"t":"1525639331127","a":[-3.131711483001709,5.032765865325928,6.838048934936523],"g":[-1.1582005023956299,-1.0702359676361084,0.1966986060142517],"m":[13.699999809265137,-29.5,17]}]},
# {"t":"d","c":"2","d":[{"t":"1525639331306","a":[-1.7190892696380615,3.598595142364502,9.842864036560059],"g":[-0.46792277693748474,-0.2932153046131134,0.4092797040939331],"m":[8.90000057220459,-26.899999618530273,9.800000190734863]}]},
# {"t":"d","c":"42","d":[{"t":"1525639331486","a":[-1.1085492372512817,3.751828670501709,9.895537376403809],"g":[1.1313223838806152,1.1398745775222778,-0.0403171069920063],"m":[7.400000095367432,-26,7.700000286102295]}]},
# {"t":"d","c":"12","d":[{"t":"1525639331669","a":[-4.051113128662109,5.609786033630371,7.520417213439941],"g":[-0.9297369122505188,-0.7061602473258972,-0.10751228034496307],"m":[14.100000381469727,-29.899999618530273,15]}]},
# {"t":"d","c":"51","d":[{"t":"1525639331847","a":[-2.6480679512023926,3.849993944168091,9.024022102355957],"g":[-0.18570303916931152,-0.7269296646118164,0.28099802136421204],"m":[11.199999809265137,-27.200000762939453,10.800000190734863]}]},
# {"t":"d","c":"88","d":[{"t":"1525639332027","a":[-0.8547561168670654,3.972101926803589,8.796566009521484],"g":[0.3323107063770294,0.16859880089759827,0.44837507605552673],"m":[8.40000057220459,-26.399999618530273,7.900000095367432]}]},
# {"t":"d","c":"7","d":[{"t":"1525639332209","a":[-2.516382932662964,4.304905891418457,8.775016784667969],"g":[-0.16982053220272064,1.340238332748413,-0.11850785464048386],"m":[7.5,-27.30000114440918,7.400000095367432]}]},
# {"t":"d","c":"11","d":[{"t":"1525639332386","a":[-3.4908525943756104,4.235472202301025,10.194822311401367],"g":[1.2705997228622437,-0.02932153269648552,-0.08674286305904388],"m":[10,-27.600000381469727,7.800000190734863]}]},
# {"t":"d","c":"15","d":[{"t":"1525639332566","a":[-3.44057297706604,6.177228927612305,2.2841382026672363],"g":[-0.9773843884468079,-1.2791517972946167,0.028099801391363144],"m":[12.40000057220459,-30.5,16.80000114440918]}]},
# {"t":"d","c":"97","d":[{"t":"1525639332746","a":[-1.6759923696517944,3.280156373977661,10.635368347167969],"g":[-0.685390830039978,0.44715335965156555,0.43127086758613586],"m":[8.100000381469727,-25.700000762939453,8]}]},
# {"t":"d","c":"35","d":[{"t":"1525639332926","a":[-1.7909175157546997,3.227482557296753,10.221158981323242],"g":[1.4526375532150269,0.4337143301963806,-0.10995574295520782],"m":[7.800000190734863,-24.80000114440918,6]}]},
# {"t":"d","c":"71","d":[{"t":"1525639333105","a":[-2.8635525703430176,6.39271354675293,3.2394537925720215],"g":[-0.13316862285137177,-0.11484266817569733,0.09896016865968704],"m":[11.100000381469727,-32,17.80000114440918]}]},
# {"t":"d","c":"22","d":[{"t":"1525639333286","a":[-1.0989720821380615,4.16124963760376,9.292180061340332],"g":[-1.5027284622192383,-0.4141666293144226,0.3237585723400116],"m":[7.800000190734863,-28.899999618530273,11.199999809265137]}]},
# {"t":"d","c":"60","d":[{"t":"1525639333466","a":[0.2657645046710968,3.1077687740325928,11.107040405273438],"g":[0.9590584635734558,-0.02076941914856434,-0.2431243658065796],"m":[3.799999952316284,-25.100000381469727,7.200000286102295]}]},
# {"t":"d","c":"20","d":[{"t":"1525639333646","a":[-2.169213056564331,5.585843086242676,5.399089813232422],"g":[-0.10995574295520782,-0.11362093687057495,-1.23394775390625],"m":[8.699999809265137,-30.200000762939453,17.100000381469727]}]},
# {"t":"d","c":"46","d":[{"t":"1525639333828","a":[-2.3344178199768066,5.142902374267578,8.696005821228027],"g":[-1.235169529914856,-0.901637077331543,0.3029891550540924],"m":[9.5,-29.700000762939453,16.600000381469727]}]},
# {"t":"d","c":"82","d":[{"t":"1525639334005","a":[-1.8938714265823364,4.27138614654541,9.823709487915039],"g":[0.15760323405265808,-0.16249015927314758,-0.4593706727027893],"m":[6.900000095367432,-24,10.800000190734863]}]},
# {"t":"d","c":"46","d":[{"t":"1525639334185","a":[-1.0726351737976074,5.344021320343018,8.549955368041992],"g":[1.6920967102050781,-0.6401867866516113,0.12461651116609573],"m":[5.599999904632568,-23.80000114440918,11.199999809265137]}]},
# {"t":"d","c":"80","d":[{"t":"1525639334365","a":[-0.22027328610420227,7.326480865478516,5.956955909729004],"g":[0.8283332586288452,0.32620203495025635,0.1612684279680252],"m":[5.400000095367432,-29.80000114440918,19.200000762939453]}]},
# {"t":"d","c":"42","d":[{"t":"1525639334546","a":[-0.16759923100471497,3.7733771800994873,8.44460678100586],"g":[-3.1984903812408447,0.22479841113090515,-0.1111774742603302],"m":[5.5,-30.100000381469727,18.100000381469727]}]},
# {"t":"d","c":"61","d":[{"t":"1525639334726","a":[-1.5419130325317383,1.0271438360214233,11.162109375],"g":[-0.598647952079773,0.2125810980796814,-0.030543262138962746],"m":[2.799999952316284,-14.5,4.200000286102295]}]},
# {"t":"d","c":"94","d":[{"t":"1525639334906","a":[-0.26337021589279175,3.074248790740967,8.257853507995605],"g":[3.927863597869873,0.3689626157283783,-0.08918632566928864],"m":[4.200000286102295,-18.100000381469727,6.599999904632568]}]},
# {"t":"d","c":"17","d":[{"t":"1525639335086","a":[-1.9130256175994873,5.3200788497924805,4.881926536560059],"g":[-2.2492058277130127,-0.9297369122505188,-0.4960225820541382],"m":[12.40000057220459,-28.5,18.100000381469727]}]},
# {"t":"d","c":"56","d":[{"t":"1525639335266","a":[-3.0239691734313965,0.6177229285240173,14.55479621887207],"g":[0.20036379992961884,0.2382374405860901,0.2687807083129883],"m":[4.700000286102295,-13.100000381469727,3.700000047683716]}]},
# {"t":"d","c":"17","d":[{"t":"1525639335445","a":[0.4788549542427063,4.446168422698975,5.2841644287109375],"g":[0.04764748737215996,-0.12095131725072861,-0.2272418737411499],"m":[7.300000190734863,-24.30000114440918,12.40000057220459]}]},
# {"t":"d","c":"99","d":[{"t":"1525639335625","a":[-1.6400781869888306,2.669616460800171,11.959403038024902],"g":[-1.4123204946517944,0.006108652334660292,-0.5522221922874451],"m":[6.599999904632568,-23,11.100000381469727]}]},
# {"t":"d","c":"83","d":[{"t":"1525639335809","a":[-0.29928433895111084,3.7901370525360107,6.098217964172363],"g":[3.0054569244384766,1.0824532508850098,-0.039095375686883926],"m":[8.100000381469727,-23.80000114440918,12]}]},
# {"t":"d","c":"15","d":[{"t":"1525639335986","a":[-3.596200704574585,4.455745220184326,5.83245325088501],"g":[-2.833193063735962,-1.8069393634796143,-1.0152580738067627],"m":[16.5,-27.899999618530273,20.700000762939453]}]},
# {"t":"d","c":"57","d":[{"t":"1525639336169","a":[-2.7366561889648438,2.9210152626037598,13.0440092086792],"g":[0.9297369122505188,-0.6951646208763123,0.10384709388017654],"m":[11.100000381469727,-21.5,9.90000057220459]}]},
# {"t":"d","c":"69","d":[{"t":"1525639336346","a":[-3.50042986869812,7.269018173217773,5.906675815582275],"g":[1.5833626985549927,2.090380907058716,0.5900958180427551],"m":[13.600000381469727,-29.100000381469727,18.5]}]},
# {"t":"d","c":"76","d":[{"t":"1525639336529","a":[1.3575538396835327,1.4150164127349854,8.889942169189453],"g":[-4.718323230743408,0.3237585723400116,0.9065240025520325],"m":[13,-31.700000762939453,14.600000381469727]}]},
# {"t":"d","c":"46","d":[{"t":"1525639336708","a":[-0.48124924302101135,-0.6488484740257263,17.310606002807617],"g":[2.1844542026519775,-0.17104226350784302,-0.3628539443016052],"m":[7,-10.199999809265137,-6.300000190734863]}]},
# {"t":"d","c":"8","d":[{"t":"1525639336889","a":[-1.953728199005127,6.3089141845703125,1.6089526414871216],"g":[0.788016140460968,0.16493362188339233,0.504574716091156],"m":[15.100000381469727,-29.200000762939453,15]}]},
# {"t":"d","c":"59","d":[{"t":"1525639337069","a":[-2.3631491661071777,2.509200096130371,10.496500968933105],"g":[0.2565633952617645,-0.8686503767967224,-0.4141666293144226],"m":[13.5,-26.600000381469727,8.800000190734863]}]},
# {"t":"d","c":"49","d":[{"t":"1525639337248","a":[-2.4062461853027344,4.058295726776123,9.656110763549805],"g":[-0.1417207419872284,-0.22968533635139465,-0.1417207419872284],"m":[11.40000057220459,-25.80000114440918,8.699999809265137]}]},
# {"t":"d","c":"68","d":[{"t":"1525639337424","a":[-2.444554567337036,4.8172807693481445,6.833260536193848],"g":[0.4911356568336487,0.22357667982578278,0.1466076523065567],"m":[10.100000381469727,-25.100000381469727,8.800000190734863]}]},
# {"t":"d","c":"59","d":[{"t":"1525639337617","a":[-3.0479118824005127,4.798126697540283,9.759063720703125],"g":[0.6194173693656921,0.49235737323760986,0.0757472887635231],"m":[9.90000057220459,-25.200000762939453,9.800000190734863]}]},
# {"t":"d","c":"19","d":[{"t":"1525639337783","a":[-2.044710636138916,5.396695613861084,2.727078914642334],"g":[-1.606575608253479,0.3933972120285034,0.5644394755363464],"m":[12.40000057220459,-29.600000381469727,13.90000057220459]}]},
# {"t":"d","c":"26","d":[{"t":"1525639337963","a":[-2.391880512237549,2.20033860206604,15.874041557312012],"g":[0.05619960278272629,-2.120924234390259,-0.6511823534965515],"m":[13.800000190734863,-23.600000381469727,5.5]}]},
# {"t":"d","c":"10","d":[{"t":"1525639338143","a":[-1.7981003522872925,6.035966873168945,5.650488376617432],"g":[2.4055874347686768,-0.4300491213798523,0.030543262138962746],"m":[9.699999809265137,-30.30000114440918,12.40000057220459]}]},
# {"t":"d","c":"35","d":[{"t":"1525639338324","a":[-0.4980091452598572,4.865166187286377,2.1524529457092285],"g":[-3.4941492080688477,0.2528982162475586,0.0403171069920063],"m":[11.100000381469727,-33.60000228881836,18.399999618530273]}]},
# {"t":"d","c":"8","d":[{"t":"1525639338506","a":[-0.7398309111595154,0.3663240373134613,19.161380767822266],"g":[0.4129449129104614,-0.44715335965156555,-0.29565876722335815],"m":[9.300000190734863,-16.5,-2.5]}]},
# {"t":"d","c":"75","d":[{"t":"1525639338684","a":[-0.5171633362770081,5.988081455230713,2.9545350074768066],"g":[1.882686734199524,-0.1514945775270462,0.0012217304902151227],"m":[7.400000095367432,-29.200000762939453,9.40000057220459]}]},
# {"t":"d","c":"23","d":[{"t":"1525639338865","a":[-2.545114040374756,5.27219295501709,4.807703971862793],"g":[-1.693318486213684,1.4978415966033936,0.7635815739631653],"m":[6.599999904632568,-30.100000381469727,11.600000381469727]}]},
# {"t":"d","c":"90","d":[{"t":"1525639339044","a":[-1.0486923456192017,1.082212209701538,14.696059226989746],"g":[2.0146336555480957,1.224173903465271,-0.3640756905078888],"m":[12.100000381469727,-22.80000114440918,1.8000000715255737]}]},
# {"t":"d","c":"86","d":[{"t":"1525639339225","a":[-3.6440863609313965,7.419857501983643,1.2641770839691162],"g":[0.6255260109901428,-0.3445279896259308,-0.02687807008624077],"m":[15.600000381469727,-33.10000228881836,21.30000114440918]}]},
# {"t":"d","c":"50","d":[{"t":"1525639339406","a":[-3.351984739303589,2.8994667530059814,4.78136682510376],"g":[-3.930306911468506,0.14905111491680145,0.10995574295520782],"m":[14.300000190734863,-30.399999618530273,11.100000381469727]}]},
# {"t":"d","c":"50","d":[{"t":"1525639339585","a":[-1.6089526414871216,-2.0614705085754395,19.328981399536133],"g":[1.3329079151153564,0.1918116807937622,-0.11972958594560623],"m":[24.399999618530273,-4.800000190734863,-3.5]}]},
# {"t":"d","c":"75","d":[{"t":"1525639339766","a":[-2.7462332248687744,4.1660380363464355,1.3431881666183472],"g":[4.543615818023682,0.10262536257505417,0.394618958234787],"m":[15.199999809265137,-32.70000076293945,16.700000762939453]}]},
# {"t":"d","c":"64","d":[{"t":"1525639339944","a":[-2.6839821338653564,7.841249942779541,2.269772529602051],"g":[-1.9046778678894043,0.0012217304902151227,-0.14905111491680145],"m":[13.199999809265137,-35.900001525878906,25]}]},
# {"t":"d","c":"35","d":[{"t":"1525639340124","a":[-1.8052831888198853,-0.5554717779159546,20.341758728027344],"g":[-2.824640989303589,-0.3139847218990326,0.7452555894851685],"m":[10.100000381469727,-13.100000381469727,-7.200000286102295]}]},
# {"t":"d","c":"51","d":[{"t":"1525639340305","a":[-1.3096683025360107,-0.2849186956882477,14.43508243560791],"g":[2.1502456665039062,-2.671924591064453,-1.2669345140457153],"m":[7.900000095367432,-13.800000190734863,-6.099999904632568]}]},
# {"t":"d","c":"26","d":[{"t":"1525639340484","a":[0.4381522834300995,6.749460697174072,2.858764171600342],"g":[2.7427849769592285,1.351233959197998,0.5827654600143433],"m":[6.5,-35.60000228881836,19.600000381469727]}]},
# {"t":"d","c":"79","d":[{"t":"1525639340664","a":[1.3048797845840454,3.50042986869812,4.300117492675781],"g":[-2.956587791442871,0.7476990818977356,-0.26022860407829285],"m":[7.900000095367432,-36.400001525878906,18]}]},
# {"t":"d","c":"97","d":[{"t":"1525639340845","a":[-4.235472202301025,-0.42857518792152405,22.008174896240234],"g":[-1.8594738245010376,0.8258898258209229,-0.4043927788734436],"m":[21.100000381469727,-9,-4.800000190734863]}]},
# {"t":"d","c":"8","d":[{"t":"1525639341025","a":[-2.102173328399658,2.0949904918670654,5.219519138336182],"g":[4.3395867347717285,-1.36955988407135,0.5375614166259766],"m":[14,-27.399999618530273,7.099999904632568]}]},
# {"t":"d","c":"42","d":[{"t":"1525639341206","a":[0.6225114464759827,7.134938716888428,0.2729473412036896],"g":[-1.938886284828186,0.2064724564552307,0.17959438264369965],"m":[10.199999809265137,-38,24.200000762939453]}]},
# {"t":"d","c":"8","d":[{"t":"1525639341384","a":[-5.102199554443359,-0.6943396925926208,14.207626342773438],"g":[-4.170988082885742,1.8863518238067627,-0.051312681287527084],"m":[17,-20.700000762939453,0.5]}]},
# {"t":"d","c":"74","d":[{"t":"1525639341563","a":[-4.553910732269287,0.5123748183250427,10.474952697753906],"g":[4.296826362609863,-1.4550809860229492,-0.12095131725072861],"m":[19,-21.100000381469727,0.4000000059604645]}]},
# {"t":"d","c":"14","d":[{"t":"1525639341746","a":[-0.8044763207435608,6.660872459411621,0.9385557174682617],"g":[-2.03540301322937,-0.1966986060142517,0.05253441259264946],"m":[12.699999809265137,-34.70000076293945,17.399999618530273]}]},
# {"t":"d","c":"17","d":[{"t":"1525639341924","a":[-4.115758419036865,-1.0008069276809692,17.166950225830078],"g":[-3.7983601093292236,0.9810495972633362,0.5009095072746277],"m":[27.5,-15,-2.5]}]},
# {"t":"d","c":"32","d":[{"t":"1525639342105","a":[-2.2099156379699707,1.1396747827529907,10.61382007598877],"g":[1.49906325340271,-0.6279694437980652,-0.47769662737846375],"m":[23.200000762939453,-14.5,-2.200000047683716]}]},
# {"t":"d","c":"60","d":[{"t":"1525639342285","a":[-6.213143348693848,6.004841327667236,4.21152925491333],"g":[2.094046115875244,-0.35185837745666504,-0.29565876722335815],"m":[17.200000762939453,-27.5,10.699999809265137]}]},
# {"t":"d","c":"22","d":[{"t":"1525639342465","a":[-2.566662549972534,4.544333457946777,4.312088966369629],"g":[-1.7067575454711914,1.0543533563613892,1.321912407875061],"m":[14.199999809265137,-28,8.699999809265137]}]},
# {"t":"d","c":"4","d":[{"t":"1525639342644","a":[-1.9585168361663818,-1.582615613937378,20.74399757385254],"g":[0.4239404797554016,0.6511823534965515,-0.3225368559360504],"m":[24.700000762939453,-10.90000057220459,-0.5]}]},
# {"t":"d","c":"49","d":[{"t":"1525639342825","a":[-5.7199225425720215,4.752635478973389,5.8109049797058105],"g":[1.607797384262085,-0.6597344875335693,-0.5094616413116455],"m":[20.200000762939453,-21.200000762939453,2.1000001430511475]}]},
# {"t":"d","c":"70","d":[{"t":"1525639343003","a":[-3.155654191970825,4.376734256744385,6.634535312652588],"g":[-1.0018190145492554,0.46670106053352356,1.3548991680145264],"m":[15.90000057220459,-25.100000381469727,4.900000095367432]}]},
# {"t":"d","c":"54","d":[{"t":"1525639343184","a":[-3.7278859615325928,0.6703969240188599,12.739935874938965],"g":[3.1105258464813232,-1.794722080230713,-1.6994271278381348],"m":[16.80000114440918,-24.5,4.200000286102295]}]},
# {"t":"d","c":"70","d":[{"t":"1525639343364","a":[-1.2354458570480347,5.803721904754639,5.121353626251221],"g":[-2.4801127910614014,0.07208210229873657,0.8967501521110535],"m":[12.699999809265137,-31.80000114440918,11.600000381469727]}]},
# {"t":"d","c":"21","d":[{"t":"1525639343543","a":[-0.3663240373134613,-0.41181525588035583,19.206872940063477],"g":[-1.3646728992462158,-0.3176499307155609,-0.5155702829360962],"m":[20.80000114440918,-18.200000762939453,0]}]},
# {"t":"d","c":"44","d":[{"t":"1525639343724","a":[-5.104593753814697,5.255433082580566,4.977697372436523],"g":[3.2009339332580566,-1.3158037662506104,-0.03298672288656235],"m":[16.30000114440918,-30,11]}]},
# {"t":"d","c":"1","d":[{"t":"1525639343873","a":[-0.7996878027915955,7.474925994873047,-0.20351335406303406],"g":[3.2009339332580566,-1.3158037662506104,-0.03298672288656235],"m":[16.30000114440918,-30,11]}]},
# {"t":"d","c":"31","d":[{"t":"1525639343928","a":[-0.9194015264511108,6.160469055175781,2.794118642807007],"g":[-2.381152629852295,0.4434881806373596,0.37751471996307373],"m":[12.90000057220459,-35.5,21.700000762939453]}]},
# {"t":"d","c":"28","d":[{"t":"1525639343933","a":[-1.2019259929656982,5.348809719085693,2.789330244064331],"g":[-2.381152629852295,0.4434881806373596,0.37751471996307373],"m":[12.90000057220459,-35.5,21.700000762939453]}]},
# {"t":"d","c":"11","d":[{"t":"1525639343951","a":[-1.5107873678207397,4.364762783050537,3.282550811767578],"g":[-2.381152629852295,0.4434881806373596,0.37751471996307373],"m":[12.90000057220459,-35.5,21.700000762939453]}]},
# {"t":"d","c":"10","d":[{"t":"1525639343972","a":[-2.1285102367401123,3.3232533931732178,4.003227710723877],"g":[-2.381152629852295,0.4434881806373596,0.37751471996307373],"m":[12.90000057220459,-35.5,21.700000762939453]}]},
# {"t":"d","c":"74","d":[{"t":"1525639343993","a":[-3.510006904602051,2.229069948196411,5.365570068359375],"g":[-2.381152629852295,0.4434881806373596,0.37751471996307373],"m":[12.90000057220459,-35.5,21.700000762939453]}]},
# {"t":"d","c":"79","d":[{"t":"1525639344011","a":[-4.0487189292907715,1.3958622217178345,8.233911514282227],"g":[-2.381152629852295,0.4434881806373596,0.37751471996307373],"m":[12.90000057220459,-35.5,21.700000762939453]}]},
# {"t":"d","c":"40","d":[{"t":"1525639344033","a":[-2.911438226699829,0.14605076611042023,10.889162063598633],"g":[-2.381152629852295,0.4434881806373596,0.37751471996307373],"m":[12.90000057220459,-35.5,21.700000762939453]}]},
# {"t":"d","c":"1","d":[{"t":"1525639344052","a":[-3.009603500366211,-0.09816526621580124,13.252310752868652],"g":[-2.381152629852295,0.4434881806373596,0.37751471996307373],"m":[12.90000057220459,-35.5,21.700000762939453]}]},
# {"t":"d","c":"54","d":[{"t":"1525639344071","a":[-4.017592906951904,0.06225114315748215,16.60190200805664],"g":[-2.381152629852295,0.4434881806373596,0.37751471996307373],"m":[12.90000057220459,-35.5,21.700000762939453]}]},
# {"t":"d","c":"72","d":[{"t":"1525639344083","a":[-4.017592906951904,0.06225114315748215,16.60190200805664],"g":[-2.872288465499878,0.9541715383529663,0.2223549485206604],"m":[21.700000762939453,-16.600000381469727,0]}]},
# {"t":"d","c":"51","d":[{"t":"1525639344094","a":[-4.213923454284668,-0.3256213665008545,18.574783325195312],"g":[-2.872288465499878,0.9541715383529663,0.2223549485206604],"m":[21.700000762939453,-16.600000381469727,0]}]},
# {"t":"d","c":"23","d":[{"t":"1525639344111","a":[-3.5147953033447266,-0.6847625970840454,19.37686538696289],"g":[-2.872288465499878,0.9541715383529663,0.2223549485206604],"m":[21.700000762939453,-16.600000381469727,0]}]},
# {"t":"d","c":"79","d":[{"t":"1525639344131","a":[-3.1724140644073486,-0.39266106486320496,19.003358840942383],"g":[-2.872288465499878,0.9541715383529663,0.2223549485206604],"m":[21.700000762939453,-16.600000381469727,0]}]},
# {"t":"d","c":"94","d":[{"t":"1525639344151","a":[-2.8060901165008545,-0.12929083406925201,17.31778907775879],"g":[-2.872288465499878,0.9541715383529663,0.2223549485206604],"m":[21.700000762939453,-16.600000381469727,0]}]},
# {"t":"d","c":"73","d":[{"t":"1525639344171","a":[-2.8922839164733887,-0.112530916929245,17.111881256103516],"g":[-2.872288465499878,0.9541715383529663,0.2223549485206604],"m":[21.700000762939453,-16.600000381469727,0]}]},
# {"t":"d","c":"75","d":[{"t":"1525639344191","a":[-2.7701759338378906,-0.7948992252349854,15.986573219299316],"g":[-2.872288465499878,0.9541715383529663,0.2223549485206604],"m":[21.700000762939453,-16.600000381469727,0]}]},
# {"t":"d","c":"19","d":[{"t":"1525639344211","a":[-3.155654191970825,-0.45730647444725037,16.297828674316406],"g":[-2.872288465499878,0.9541715383529663,0.2223549485206604],"m":[21.700000762939453,-16.600000381469727,0]}]},
# {"t":"d","c":"95","d":[{"t":"1525639344231","a":[-3.7135202884674072,0.0071828244253993034,15.000131607055664],"g":[-2.872288465499878,0.9541715383529663,0.2223549485206604],"m":[21.700000762939453,-16.600000381469727,0]}]},
# {"t":"d","c":"71","d":[{"t":"1525639344251","a":[-4.589824676513672,0.4980091452598572,13.819754600524902],"g":[-2.872288465499878,0.9541715383529663,0.2223549485206604],"m":[21.700000762939453,-16.600000381469727,0]}]},
# {"t":"d","c":"67","d":[{"t":"1525639344262","a":[-4.589824676513672,0.4980091452598572,13.819754600524902],"g":[4.549724578857422,-2.291966438293457,-0.4434881806373596],"m":[19.100000381469727,-21.5,2.6000001430511475]}]},
# {"t":"d","c":"35","d":[{"t":"1525639344271","a":[-5.403878211975098,0.7781392931938171,8.75346851348877],"g":[4.549724578857422,-2.291966438293457,-0.4434881806373596],"m":[19.100000381469727,-21.5,2.6000001430511475]}]},
# {"t":"d","c":"72","d":[{"t":"1525639344291","a":[-4.604190349578857,2.1309046745300293,5.83245325088501],"g":[4.549724578857422,-2.291966438293457,-0.4434881806373596],"m":[19.100000381469727,-21.5,2.6000001430511475]}]},
# {"t":"d","c":"35","d":[{"t":"1525639344311","a":[-2.762993097305298,3.2131168842315674,3.569863796234131],"g":[4.549724578857422,-2.291966438293457,-0.4434881806373596],"m":[19.100000381469727,-21.5,2.6000001430511475]}]},
# {"t":"d","c":"15","d":[{"t":"1525639344332","a":[-0.3663240373134613,3.9553420543670654,0.5387118458747864],"g":[4.549724578857422,-2.291966438293457,-0.4434881806373596],"m":[19.100000381469727,-21.5,2.6000001430511475]}]},
# {"t":"d","c":"18","d":[{"t":"1525639344352","a":[-0.5482889413833618,5.554717540740967,-1.5251530408859253],"g":[4.549724578857422,-2.291966438293457,-0.4434881806373596],"m":[19.100000381469727,-21.5,2.6000001430511475]}]},
# {"t":"d","c":"75","d":[{"t":"1525639344372","a":[-0.8667274713516235,7.1445159912109375,-2.4924399852752686],"g":[4.549724578857422,-2.291966438293457,-0.4434881806373596],"m":[19.100000381469727,-21.5,2.6000001430511475]}]},
# {"t":"d","c":"32","d":[{"t":"1525639344392","a":[-0.8786988258361816,7.869981288909912,-2.542719841003418],"g":[4.549724578857422,-2.291966438293457,-0.4434881806373596],"m":[19.100000381469727,-21.5,2.6000001430511475]}]},
# {"t":"d","c":"85","d":[{"t":"1525639344412","a":[-0.05267404764890671,7.92265510559082,-2.195549964904785],"g":[4.549724578857422,-2.291966438293457,-0.4434881806373596],"m":[19.100000381469727,-21.5,2.6000001430511475]}]},
# {"t":"d","c":"38","d":[{"t":"1525639344432","a":[0.17238777875900269,7.776604652404785,-1.2498114109039307],"g":[4.549724578857422,-2.291966438293457,-0.4434881806373596],"m":[19.100000381469727,-21.5,2.6000001430511475]}]},
# {"t":"d","c":"3","d":[{"t":"1525639344444","a":[0.17238777875900269,7.776604652404785,-1.2498114109039307],"g":[-1.3854423761367798,-0.2382374405860901,0.06719517707824707],"m":[11.199999809265137,-38.20000076293945,24]}]},
# {"t":"d","c":"14","d":[{"t":"1525639344453","a":[-0.004788549616932869,7.553936958312988,0.931372880935669],"g":[-1.3854423761367798,-0.2382374405860901,0.06719517707824707],"m":[11.199999809265137,-38.20000076293945,24]}]},
# {"t":"d","c":"18","d":[{"t":"1525639344473","a":[-0.6680026650428772,7.043956279754639,2.482862949371338],"g":[-1.3854423761367798,-0.2382374405860901,0.06719517707824707],"m":[11.199999809265137,-38.20000076293945,24]}]},
# {"t":"d","c":"91","d":[{"t":"1525639344493","a":[-1.0295381546020508,6.605803966522217,3.7063374519348145],"g":[-1.3854423761367798,-0.2382374405860901,0.06719517707824707],"m":[11.199999809265137,-38.20000076293945,24]}]},
# {"t":"d","c":"30","d":[{"t":"1525639344513","a":[-0.8834874033927917,5.9042816162109375,4.328848838806152],"g":[-1.3854423761367798,-0.2382374405860901,0.06719517707824707],"m":[11.199999809265137,-38.20000076293945,24]}]},
# {"t":"d","c":"84","d":[{"t":"1525639344532","a":[-1.3240339756011963,5.382329940795898,3.9840731620788574],"g":[-1.3854423761367798,-0.2382374405860901,0.06719517707824707],"m":[11.199999809265137,-38.20000076293945,24]}]},
# {"t":"d","c":"98","d":[{"t":"1525639344553","a":[-1.4365649223327637,4.5706706047058105,3.5866236686706543],"g":[-1.3854423761367798,-0.2382374405860901,0.06719517707824707],"m":[11.199999809265137,-38.20000076293945,24]}]},
# {"t":"d","c":"9","d":[{"t":"1525639344572","a":[-2.0375277996063232,3.5124011039733887,4.0439300537109375],"g":[-1.3854423761367798,-0.2382374405860901,0.06719517707824707],"m":[11.199999809265137,-38.20000076293945,24]}]},
# {"t":"d","c":"61","d":[{"t":"1525639344592","a":[-2.7725701332092285,2.2410411834716797,4.826858043670654],"g":[-1.3854423761367798,-0.2382374405860901,0.06719517707824707],"m":[11.199999809265137,-38.20000076293945,24]}]},
# {"t":"d","c":"81","d":[{"t":"1525639344612","a":[-3.375927448272705,0.9146129488945007,6.222720146179199],"g":[-1.3854423761367798,-0.2382374405860901,0.06719517707824707],"m":[11.199999809265137,-38.20000076293945,24]}]},
# {"t":"d","c":"12","d":[{"t":"1525639344623","a":[-3.375927448272705,0.9146129488945007,6.222720146179199],"g":[-4.257730960845947,1.9352210760116577,-0.08307767659425735],"m":[15,-26.100000381469727,5.599999904632568]}]},
# {"t":"d","c":"43","d":[{"t":"1525639344632","a":[-4.065478801727295,0.05506832152605057,8.875576972961426],"g":[-4.257730960845947,1.9352210760116577,-0.08307767659425735],"m":[15,-26.100000381469727,5.599999904632568]}]},
# {"t":"d","c":"76","d":[{"t":"1525639344653","a":[-4.8484063148498535,-0.46927785873413086,12.603462219238281],"g":[-4.257730960845947,1.9352210760116577,-0.08307767659425735],"m":[15,-26.100000381469727,5.599999904632568]}]},
# {"t":"d","c":"22","d":[{"t":"1525639344672","a":[-4.738269805908203,-0.8523618578910828,17.40398406982422],"g":[-4.257730960845947,1.9352210760116577,-0.08307767659425735],"m":[15,-26.100000381469727,5.599999904632568]}]},
# {"t":"d","c":"12","d":[{"t":"1525639344692","a":[-3.560286521911621,-1.1540404558181763,21.99620246887207],"g":[-4.257730960845947,1.9352210760116577,-0.08307767659425735],"m":[15,-26.100000381469727,5.599999904632568]}]},
# {"t":"d","c":"31","d":[{"t":"1525639344713","a":[-2.456526041030884,-1.3431881666183472,23.396852493286133],"g":[-4.257730960845947,1.9352210760116577,-0.08307767659425735],"m":[15,-26.100000381469727,5.599999904632568]}]},
# {"t":"d","c":"28","d":[{"t":"1525639344733","a":[-2.288926601409912,-1.2162915468215942,22.47745132446289],"g":[-4.257730960845947,1.9352210760116577,-0.08307767659425735],"m":[15,-26.100000381469727,5.599999904632568]}]},
# {"t":"d","c":"29","d":[{"t":"1525639344757","a":[-2.8013014793395996,-1.6233183145523071,18.093534469604492],"g":[-4.257730960845947,1.9352210760116577,-0.08307767659425735],"m":[15,-26.100000381469727,5.599999904632568]}]},
# {"t":"d","c":"40","d":[{"t":"1525639344773","a":[-3.9481592178344727,-0.8499675393104553,14.336917877197266],"g":[-4.257730960845947,1.9352210760116577,-0.08307767659425735],"m":[15,-26.100000381469727,5.599999904632568]}]},
# {"t":"d","c":"6","d":[{"t":"1525639344792","a":[-4.098998546600342,-0.33998700976371765,12.615433692932129],"g":[-4.257730960845947,1.9352210760116577,-0.08307767659425735],"m":[15,-26.100000381469727,5.599999904632568]}]},
# {"t":"d","c":"13","d":[{"t":"1525639344804","a":[-4.098998546600342,-0.33998700976371765,12.615433692932129],"g":[3.597996234893799,-0.22113321721553802,-0.37629300355911255],"m":[23.899999618530273,-15.300000190734863,-0.30000001192092896]}]},
# {"t":"d","c":"30","d":[{"t":"1525639344813","a":[-3.447755813598633,-0.009577099233865738,10.893950462341309],"g":[3.597996234893799,-0.22113321721553802,-0.37629300355911255],"m":[23.899999618530273,-15.300000190734863,-0.30000001192092896]}]},
# {"t":"d","c":"24","d":[{"t":"1525639344834","a":[-3.8595709800720215,0.84517902135849,10.319324493408203],"g":[3.597996234893799,-0.22113321721553802,-0.37629300355911255],"m":[23.899999618530273,-15.300000190734863,-0.30000001192092896]}]},
# {"t":"d","c":"66","d":[{"t":"1525639344852","a":[-3.8835136890411377,1.7047237157821655,8.241093635559082],"g":[3.597996234893799,-0.22113321721553802,-0.37629300355911255],"m":[23.899999618530273,-15.300000190734863,-0.30000001192092896]}]},
# {"t":"d","c":"46","d":[{"t":"1525639344872","a":[-4.2282891273498535,3.0167863368988037,5.923435688018799],"g":[3.597996234893799,-0.22113321721553802,-0.37629300355911255],"m":[23.899999618530273,-15.300000190734863,-0.30000001192092896]}]},
# {"t":"d","c":"85","d":[{"t":"1525639344892","a":[-4.297723293304443,4.209135055541992,4.453351020812988],"g":[3.597996234893799,-0.22113321721553802,-0.37629300355911255],"m":[23.899999618530273,-15.300000190734863,-0.30000001192092896]}]},
# {"t":"d","c":"18","d":[{"t":"1525639344912","a":[-2.853975534439087,4.628133296966553,2.822849988937378],"g":[3.597996234893799,-0.22113321721553802,-0.37629300355911255],"m":[23.899999618530273,-15.300000190734863,-0.30000001192092896]}]},
# {"t":"d","c":"10","d":[{"t":"1525639344932","a":[-1.7861289978027344,5.20036506652832,1.3599480390548706],"g":[3.597996234893799,-0.22113321721553802,-0.37629300355911255],"m":[23.899999618530273,-15.300000190734863,-0.30000001192092896]}]},
# {"t":"d","c":"40","d":[{"t":"1525639344952","a":[-1.92020845413208,6.256239891052246,0.6727912425994873],"g":[3.597996234893799,-0.22113321721553802,-0.37629300355911255],"m":[23.899999618530273,-15.300000190734863,-0.30000001192092896]}]},
# {"t":"d","c":"55","d":[{"t":"1525639344972","a":[-1.9010541439056396,6.852414608001709,0.2897072434425354],"g":[3.597996234893799,-0.22113321721553802,-0.37629300355911255],"m":[23.899999618530273,-15.300000190734863,-0.30000001192092896]}]},
# {"t":"d","c":"30","d":[{"t":"1525639344983","a":[-1.9010541439056396,6.852414608001709,0.2897072434425354],"g":[-0.2577851414680481,-0.5729916095733643,0.2687807083129883],"m":[14.600000381469727,-33.20000076293945,15.40000057220459]}]},
# {"t":"d","c":"40","d":[{"t":"1525639344992","a":[-1.3407938480377197,6.787769317626953,0.41181525588035583],"g":[-0.2577851414680481,-0.5729916095733643,0.2687807083129883],"m":[14.600000381469727,-33.20000076293945,15.40000057220459]}]},
# {"t":"d","c":"42","d":[{"t":"1525639345012","a":[-1.2354458570480347,6.529187202453613,1.3886793851852417],"g":[-0.2577851414680481,-0.5729916095733643,0.2687807083129883],"m":[14.600000381469727,-33.20000076293945,15.40000057220459]}]},
# {"t":"d","c":"54","d":[{"t":"1525639345033","a":[-1.6879637241363525,6.296942710876465,2.973689317703247],"g":[-0.2577851414680481,-0.5729916095733643,0.2687807083129883],"m":[14.600000381469727,-33.20000076293945,15.40000057220459]}]},
# {"t":"d","c":"78","d":[{"t":"1525639345053","a":[-2.0662591457366943,5.645699977874756,4.283357620239258],"g":[-0.2577851414680481,-0.5729916095733643,0.2687807083129883],"m":[14.600000381469727,-33.20000076293945,15.40000057220459]}]},
# {"t":"d","c":"27","d":[{"t":"1525639345073","a":[-2.669616460800171,5.1309309005737305,4.802915096282959],"g":[-0.2577851414680481,-0.5729916095733643,0.2687807083129883],"m":[14.600000381469727,-33.20000076293945,15.40000057220459]}]},
# {"t":"d","c":"74","d":[{"t":"1525639345093","a":[-2.7175018787384033,4.180403709411621,5.813299179077148],"g":[-0.2577851414680481,-0.5729916095733643,0.2687807083129883],"m":[14.600000381469727,-33.20000076293945,15.40000057220459]}]},
# {"t":"d","c":"90","d":[{"t":"1525639345113","a":[-2.4062461853027344,3.2418479919433594,6.426233768463135],"g":[-0.2577851414680481,-0.5729916095733643,0.2687807083129883],"m":[14.600000381469727,-33.20000076293945,15.40000057220459]}]},
# {"t":"d","c":"93","d":[{"t":"1525639345133","a":[-2.3080809116363525,2.3128695487976074,7.120573043823242],"g":[-0.2577851414680481,-0.5729916095733643,0.2687807083129883],"m":[14.600000381469727,-33.20000076293945,15.40000057220459]}]},
# {"t":"d","c":"83","d":[{"t":"1525639345153","a":[-2.6480679512023926,1.953728199005127,9.3017578125],"g":[-0.2577851414680481,-0.5729916095733643,0.2687807083129883],"m":[14.600000381469727,-33.20000076293945,15.40000057220459]}]},
# {"t":"d","c":"84","d":[{"t":"1525639345165","a":[-2.6480679512023926,1.953728199005127,9.3017578125],"g":[-2.956587791442871,1.3781119585037231,0.31642818450927734],"m":[16.100000381469727,-22.5,0.6000000238418579]}]},
# {"t":"d","c":"79","d":[{"t":"1525639345173","a":[-3.1173458099365234,1.3264282941818237,13.771868705749512],"g":[-2.956587791442871,1.3781119585037231,0.31642818450927734],"m":[16.100000381469727,-22.5,0.6000000238418579]}]},
# {"t":"d","c":"61","d":[{"t":"1525639345193","a":[-2.3751206398010254,0.337592750787735,16.925128936767578],"g":[-2.956587791442871,1.3781119585037231,0.31642818450927734],"m":[16.100000381469727,-22.5,0.6000000238418579]}]},
# {"t":"d","c":"54","d":[{"t":"1525639345212","a":[-2.6839821338653564,0.39984390139579773,17.84213638305664],"g":[-2.956587791442871,1.3781119585037231,0.31642818450927734],"m":[16.100000381469727,-22.5,0.6000000238418579]}]},
# {"t":"d","c":"21","d":[{"t":"1525639345232","a":[-2.849187135696411,0.7446194887161255,19.0440616607666],"g":[-2.956587791442871,1.3781119585037231,0.31642818450927734],"m":[16.100000381469727,-22.5,0.6000000238418579]}]},
# {"t":"d","c":"81","d":[{"t":"1525639345252","a":[-2.3200523853302,0.3136500120162964,17.976215362548828],"g":[-2.956587791442871,1.3781119585037231,0.31642818450927734],"m":[16.100000381469727,-22.5,0.6000000238418579]}]},
# {"t":"d","c":"17","d":[{"t":"1525639345272","a":[-2.6432793140411377,0.4022381603717804,15.632220268249512],"g":[-2.956587791442871,1.3781119585037231,0.31642818450927734],"m":[16.100000381469727,-22.5,0.6000000238418579]}]},
# {"t":"d","c":"88","d":[{"t":"1525639345292","a":[-2.423006057739258,0.596174418926239,13.446247100830078],"g":[-2.956587791442871,1.3781119585037231,0.31642818450927734],"m":[16.100000381469727,-22.5,0.6000000238418579]}]},
# {"t":"d","c":"44","d":[{"t":"1525639345312","a":[-1.7597919702529907,0.44054657220840454,12.16530990600586],"g":[-2.956587791442871,1.3781119585037231,0.31642818450927734],"m":[16.100000381469727,-22.5,0.6000000238418579]}]},
# {"t":"d","c":"27","d":[{"t":"1525639345332","a":[-3.225088119506836,1.1995316743850708,10.807756423950195],"g":[-2.956587791442871,1.3781119585037231,0.31642818450927734],"m":[16.100000381469727,-22.5,0.6000000238418579]}]},
# {"t":"d","c":"60","d":[{"t":"1525639345344","a":[-3.225088119506836,1.1995316743850708,10.807756423950195],"g":[2.9529225826263428,-1.0836749076843262,-0.7489207983016968],"m":[19.30000114440918,-21.700000762939453,1.8000000715255737]}]},
# {"t":"d","c":"23","d":[{"t":"1525639345352","a":[-5.399089813232422,2.4062461853027344,8.61220645904541],"g":[2.9529225826263428,-1.0836749076843262,-0.7489207983016968],"m":[19.30000114440918,-21.700000762939453,1.8000000715255737]}]},
# {"t":"d","c":"0","d":[{"t":"1525639345372","a":[-5.667248249053955,3.873936653137207,6.466936111450195],"g":[2.9529225826263428,-1.0836749076843262,-0.7489207983016968],"m":[19.30000114440918,-21.700000762939453,1.8000000715255737]}]},
# {"t":"d","c":"76","d":[{"t":"1525639345392","a":[-3.94097638130188,4.563488006591797,4.855589389801025],"g":[2.9529225826263428,-1.0836749076843262,-0.7489207983016968],"m":[19.30000114440918,-21.700000762939453,1.8000000715255737]}]},
# {"t":"d","c":"50","d":[{"t":"1525639345412","a":[-2.942563772201538,5.2530388832092285,3.3304362297058105],"g":[2.9529225826263428,-1.0836749076843262,-0.7489207983016968],"m":[19.30000114440918,-21.700000762939453,1.8000000715255737]}]},
# {"t":"d","c":"70","d":[{"t":"1525639345433","a":[-3.155654191970825,6.1556806564331055,2.1931557655334473],"g":[2.9529225826263428,-1.0836749076843262,-0.7489207983016968],"m":[19.30000114440918,-21.700000762939453,1.8000000715255737]}]},
# {"t":"d","c":"12","d":[{"t":"1525639345453","a":[-3.471698522567749,6.888328552246094,1.7789461612701416],"g":[2.9529225826263428,-1.0836749076843262,-0.7489207983016968],"m":[19.30000114440918,-21.700000762939453,1.8000000715255737]}]},
# {"t":"d","c":"38","d":[{"t":"1525639345473","a":[-2.0830190181732178,6.687209606170654,1.58022141456604],"g":[2.9529225826263428,-1.0836749076843262,-0.7489207983016968],"m":[19.30000114440918,-21.700000762939453,1.8000000715255737]}]},
# {"t":"d","c":"69","d":[{"t":"1525639345493","a":[-1.2067145109176636,6.984099388122559,3.3687446117401123],"g":[2.9529225826263428,-1.0836749076843262,-0.7489207983016968],"m":[19.30000114440918,-21.700000762939453,1.8000000715255737]}]},
# {"t":"d","c":"90","d":[{"t":"1525639345513","a":[-1.5921927690505981,6.421444892883301,4.142095565795898],"g":[2.9529225826263428,-1.0836749076843262,-0.7489207983016968],"m":[19.30000114440918,-21.700000762939453,1.8000000715255737]}]},
# {"t":"d","c":"13","d":[{"t":"1525639345524","a":[-1.5921927690505981,6.421444892883301,4.142095565795898],"g":[-1.0604621171951294,-0.43249258399009705,0.5143485069274902],"m":[12.300000190734863,-29.80000114440918,11.600000381469727]}]},
# {"t":"d","c":"46","d":[{"t":"1525639345534","a":[-1.4916331768035889,6.162863254547119,3.694365978240967],"g":[-1.0604621171951294,-0.43249258399009705,0.5143485069274902],"m":[12.300000190734863,-29.80000114440918,11.600000381469727]}]},
# {"t":"d","c":"82","d":[{"t":"1525639345551","a":[-1.5299415588378906,5.7510480880737305,3.7302801609039307],"g":[-1.0604621171951294,-0.43249258399009705,0.5143485069274902],"m":[12.300000190734863,-29.80000114440918,11.600000381469727]}]},
# {"t":"d","c":"58","d":[{"t":"1525639345572","a":[-1.551490068435669,5.008822917938232,2.9210152626037598],"g":[-1.0604621171951294,-0.43249258399009705,0.5143485069274902],"m":[12.300000190734863,-29.80000114440918,11.600000381469727]}]},
# {"t":"d","c":"19","d":[{"t":"1525639345592","a":[-2.3416006565093994,4.225894927978516,2.5522968769073486],"g":[-1.0604621171951294,-0.43249258399009705,0.5143485069274902],"m":[12.300000190734863,-29.80000114440918,11.600000381469727]}]},
# {"t":"d","c":"90","d":[{"t":"1525639345613","a":[-1.6305011510849,2.7677817344665527,3.7973198890686035],"g":[-1.0604621171951294,-0.43249258399009705,0.5143485069274902],"m":[12.300000190734863,-29.80000114440918,11.600000381469727]}]},
# {"t":"d","c":"68","d":[{"t":"1525639345632","a":[-1.2258687019348145,1.5275473594665527,5.470917701721191],"g":[-1.0604621171951294,-0.43249258399009705,0.5143485069274902],"m":[12.300000190734863,-29.80000114440918,11.600000381469727]}]},
# {"t":"d","c":"76","d":[{"t":"1525639345652","a":[-2.0973846912384033,0.8427847623825073,7.249864101409912],"g":[-1.0604621171951294,-0.43249258399009705,0.5143485069274902],"m":[12.300000190734863,-29.80000114440918,11.600000381469727]}]},
# {"t":"d","c":"24","d":[{"t":"1525639345672","a":[-3.076643228530884,0.06703969836235046,9.799766540527344],"g":[-1.0604621171951294,-0.43249258399009705,0.5143485069274902],"m":[12.300000190734863,-29.80000114440918,11.600000381469727]}]},
# {"t":"d","c":"18","d":[{"t":"1525639345692","a":[-3.292127847671509,-0.6608198285102844,13.616240501403809],"g":[-1.0604621171951294,-0.43249258399009705,0.5143485069274902],"m":[12.300000190734863,-29.80000114440918,11.600000381469727]}]},
# {"t":"d","c":"10","d":[{"t":"1525639345704","a":[-3.292127847671509,-0.6608198285102844,13.616240501403809],"g":[-3.539353132247925,0.9370672702789307,0.33597588539123535],"m":[20,-16.399999618530273,-1.3000000715255737]}]},
# {"t":"d","c":"64","d":[{"t":"1525639345712","a":[-2.9880549907684326,-1.2330515384674072,17.336944580078125],"g":[-3.539353132247925,0.9370672702789307,0.33597588539123535],"m":[20,-16.399999618530273,-1.3000000715255737]}]},
# {"t":"d","c":"98","d":[{"t":"1525639345732","a":[-2.8611583709716797,-1.1516461372375488,20.243593215942383],"g":[-3.539353132247925,0.9370672702789307,0.33597588539123535],"m":[20,-16.399999618530273,-1.3000000715255737]}]},
# {"t":"d","c":"52","d":[{"t":"1525639345753","a":[-2.796513080596924,-1.3958622217178345,19.89163589477539],"g":[-3.539353132247925,0.9370672702789307,0.33597588539123535],"m":[20,-16.399999618530273,-1.3000000715255737]}]},
# {"t":"d","c":"50","d":[{"t":"1525639345772","a":[-2.604970932006836,-1.0846065282821655,17.358491897583008],"g":[-3.539353132247925,0.9370672702789307,0.33597588539123535],"m":[20,-16.399999618530273,-1.3000000715255737]}]},
# {"t":"d","c":"11","d":[{"t":"1525639345793","a":[-3.3232533931732178,-0.742225170135498,14.64577865600586],"g":[-3.539353132247925,0.9370672702789307,0.33597588539123535],"m":[20,-16.399999618530273,-1.3000000715255737]}]},
# {"t":"d","c":"96","d":[{"t":"1525639345813","a":[-3.5171897411346436,-0.5171633362770081,13.702434539794922],"g":[-3.539353132247925,0.9370672702789307,0.33597588539123535],"m":[20,-16.399999618530273,-1.3000000715255737]}]},
# {"t":"d","c":"56","d":[{"t":"1525639345833","a":[-3.378321647644043,-0.4142095446586609,14.040027618408203],"g":[-3.539353132247925,0.9370672702789307,0.33597588539123535],"m":[20,-16.399999618530273,-1.3000000715255737]}]},
# {"t":"d","c":"5","d":[{"t":"1525639345853","a":[-2.6432793140411377,-0.40463244915008545,14.923515319824219],"g":[-3.539353132247925,0.9370672702789307,0.33597588539123535],"m":[20,-16.399999618530273,-1.3000000715255737]}]},
# {"t":"d","c":"57","d":[{"t":"1525639345873","a":[-3.067065954208374,0.20830190181732178,14.74394416809082],"g":[-3.539353132247925,0.9370672702789307,0.33597588539123535],"m":[20,-16.399999618530273,-1.3000000715255737]}]},
# {"t":"d","c":"79","d":[{"t":"1525639345885","a":[-3.067065954208374,0.20830190181732178,14.74394416809082],"g":[3.484375476837158,-1.5723671913146973,-0.7538077235221863],"m":[21.600000381469727,-17.200000762939453,-0.5]}]},
# {"t":"d","c":"60","d":[{"t":"1525639345894","a":[-2.877918243408203,0.31604427099227905,13.781445503234863],"g":[3.484375476837158,-1.5723671913146973,-0.7538077235221863],"m":[21.600000381469727,-17.200000762939453,-0.5]}]},
# {"t":"d","c":"46","d":[{"t":"1525639345953","a":[-4.443774223327637,1.8842942714691162,6.335251331329346],"g":[3.484375476837158,-1.5723671913146973,-0.7538077235221863],"m":[21.600000381469727,-17.200000762939453,-0.5]}]},
# {"t":"d","c":"93","d":[{"t":"1525639345972","a":[-2.9928436279296875,2.8372156620025635,2.968900680541992],"g":[3.484375476837158,-1.5723671913146973,-0.7538077235221863],"m":[21.600000381469727,-17.200000762939453,-0.5]}]},
# {"t":"d","c":"29","d":[{"t":"1525639345992","a":[-1.1779831647872925,4.484476566314697,1.1851660013198853],"g":[3.484375476837158,-1.5723671913146973,-0.7538077235221863],"m":[21.600000381469727,-17.200000762939453,-0.5]}]},
# {"t":"d","c":"40","d":[{"t":"1525639346012","a":[-0.9696813225746155,5.684008598327637,-0.5770202279090881],"g":[3.484375476837158,-1.5723671913146973,-0.7538077235221863],"m":[21.600000381469727,-17.200000762939453,-0.5]}]},
# {"t":"d","c":"8","d":[{"t":"1525639346031","a":[-1.228263020515442,6.9290313720703125,-1.891477108001709],"g":[3.484375476837158,-1.5723671913146973,-0.7538077235221863],"m":[21.600000381469727,-17.200000762939453,-0.5]}]},
# {"t":"d","c":"87","d":[{"t":"1525639346052","a":[-1.7238779067993164,8.035185813903809,-1.3743137121200562],"g":[3.484375476837158,-1.5723671913146973,-0.7538077235221863],"m":[21.600000381469727,-17.200000762939453,-0.5]}]},
# {"t":"d","c":"10","d":[{"t":"1525639346064","a":[-1.7238779067993164,8.035185813903809,-1.3743137121200562],"g":[0.36529740691185,-0.6096435189247131,0.06352998316287994],"m":[12.100000381469727,-37,21.899999618530273]}]},
# {"t":"d","c":"78","d":[{"t":"1525639346075","a":[-0.7518022656440735,8.107014656066895,-0.8667274713516235],"g":[0.36529740691185,-0.6096435189247131,0.06352998316287994],"m":[12.100000381469727,-37,21.899999618530273]}]},
# {"t":"d","c":"17","d":[{"t":"1525639346092","a":[-0.4860377907752991,8.298556327819824,0.9457385540008545],"g":[0.36529740691185,-0.6096435189247131,0.06352998316287994],"m":[12.100000381469727,-37,21.899999618530273]}]},
# {"t":"d","c":"99","d":[{"t":"1525639346113","a":[-0.6344828009605408,7.731113433837891,2.8348214626312256],"g":[0.36529740691185,-0.6096435189247131,0.06352998316287994],"m":[12.100000381469727,-37,21.899999618530273]}]},
# {"t":"d","c":"25","d":[{"t":"1525639346133","a":[-0.9553156495094299,7.324086666107178,3.8667538166046143],"g":[0.36529740691185,-0.6096435189247131,0.06352998316287994],"m":[12.100000381469727,-37,21.899999618530273]}]},
# {"t":"d","c":"75","d":[{"t":"1525639346153","a":[-1.9896423816680908,7.324086666107178,5.109382629394531],"g":[0.36529740691185,-0.6096435189247131,0.06352998316287994],"m":[12.100000381469727,-37,21.899999618530273]}]},
# {"t":"d","c":"16","d":[{"t":"1525639346172","a":[-2.236252784729004,7.075081825256348,5.039948463439941],"g":[0.36529740691185,-0.6096435189247131,0.06352998316287994],"m":[12.100000381469727,-37,21.899999618530273]}]},
# {"t":"d","c":"22","d":[{"t":"1525639346193","a":[-2.2218871116638184,6.696786403656006,3.4501500129699707],"g":[0.36529740691185,-0.6096435189247131,0.06352998316287994],"m":[12.100000381469727,-37,21.899999618530273]}]},
# {"t":"d","c":"70","d":[{"t":"1525639346213","a":[-1.7597919702529907,5.8731560707092285,2.4684972763061523],"g":[0.36529740691185,-0.6096435189247131,0.06352998316287994],"m":[12.100000381469727,-37,21.899999618530273]}]},
# {"t":"d","c":"88","d":[{"t":"1525639346233","a":[-2.3128695487976074,4.814886569976807,2.3990633487701416],"g":[0.36529740691185,-0.6096435189247131,0.06352998316287994],"m":[12.100000381469727,-37,21.899999618530273]}]},
# {"t":"d","c":"69","d":[{"t":"1525639346246","a":[-2.3128695487976074,4.814886569976807,2.3990633487701416],"g":[-3.310889720916748,1.5149458646774292,0.8014551997184753],"m":[10.300000190734863,-33.5,13.90000057220459]}]},
# {"t":"d","c":"97","d":[{"t":"1525639346252","a":[-2.6408851146698,3.2346651554107666,3.6800003051757812],"g":[-3.310889720916748,1.5149458646774292,0.8014551997184753],"m":[10.300000190734863,-33.5,13.90000057220459]}]},
# {"t":"d","c":"4","d":[{"t":"1525639346273","a":[-2.516382932662964,2.262589693069458,6.160469055175781],"g":[-3.310889720916748,1.5149458646774292,0.8014551997184753],"m":[10.300000190734863,-33.5,13.90000057220459]}]},
# {"t":"d","c":"8","d":[{"t":"1525639346293","a":[-2.451737403869629,1.1396747827529907,9.646533012390137],"g":[-3.310889720916748,1.5149458646774292,0.8014551997184753],"m":[10.300000190734863,-33.5,13.90000057220459]}]},
# {"t":"d","c":"5","d":[{"t":"1525639346312","a":[-2.4684972763061523,0.4429408311843872,12.514874458312988],"g":[-3.310889720916748,1.5149458646774292,0.8014551997184753],"m":[10.300000190734863,-33.5,13.90000057220459]}]},
# {"t":"d","c":"6","d":[{"t":"1525639346332","a":[-2.171607255935669,0.22027328610420227,16.613872528076172],"g":[-3.310889720916748,1.5149458646774292,0.8014551997184753],"m":[10.300000190734863,-33.5,13.90000057220459]}]},
# {"t":"d","c":"70","d":[{"t":"1525639346353","a":[-2.573845386505127,0.20351335406303406,18.17494010925293],"g":[-3.310889720916748,1.5149458646774292,0.8014551997184753],"m":[10.300000190734863,-33.5,13.90000057220459]}]},
# {"t":"d","c":"41","d":[{"t":"1525639346373","a":[-1.4269877672195435,0.05985686928033829,19.400808334350586],"g":[-3.310889720916748,1.5149458646774292,0.8014551997184753],"m":[10.300000190734863,-33.5,13.90000057220459]}]},
# {"t":"d","c":"79","d":[{"t":"1525639346392","a":[-0.7302538156509399,-0.023942748084664345,19.80544090270996],"g":[-3.310889720916748,1.5149458646774292,0.8014551997184753],"m":[10.300000190734863,-33.5,13.90000057220459]}]},
# {"t":"d","c":"5","d":[{"t":"1525639346412","a":[-0.5291347503662109,-0.07182824611663818,18.440704345703125],"g":[-3.310889720916748,1.5149458646774292,0.8014551997184753],"m":[10.300000190734863,-33.5,13.90000057220459]}]},
# {"t":"d","c":"57","d":[{"t":"1525639346424","a":[-0.5291347503662109,-0.07182824611663818,18.440704345703125],"g":[1.7727309465408325,0.01710422709584236,-0.01710422709584236],"m":[19.30000114440918,-15.90000057220459,-1.5]}]},
# {"t":"d","c":"62","d":[{"t":"1525639346432","a":[-0.3830839693546295,-0.6105400919914246,16.98259162902832],"g":[1.7727309465408325,0.01710422709584236,-0.01710422709584236],"m":[19.30000114440918,-15.90000057220459,-1.5]}]},
# {"t":"d","c":"26","d":[{"t":"1525639346452","a":[-0.46927785873413086,-0.7158881425857544,14.267483711242676],"g":[1.7727309465408325,0.01710422709584236,-0.01710422709584236],"m":[19.30000114440918,-15.90000057220459,-1.5]}]},
# {"t":"d","c":"58","d":[{"t":"1525639346473","a":[-1.9465453624725342,-0.1245022863149643,12.823736190795898],"g":[1.7727309465408325,0.01710422709584236,-0.01710422709584236],"m":[19.30000114440918,-15.90000057220459,-1.5]}]},
# {"t":"d","c":"31","d":[{"t":"1525639346493","a":[-2.5906052589416504,0.73504239320755,11.339285850524902],"g":[1.7727309465408325,0.01710422709584236,-0.01710422709584236],"m":[19.30000114440918,-15.90000057220459,-1.5]}]},
# {"t":"d","c":"4","d":[{"t":"1525639346513","a":[-2.662433624267578,1.4150164127349854,8.968953132629395],"g":[1.7727309465408325,0.01710422709584236,-0.01710422709584236],"m":[19.30000114440918,-15.90000057220459,-1.5]}]},
# {"t":"d","c":"6","d":[{"t":"1525639346534","a":[-2.7677817344665527,2.545114040374756,6.526793003082275],"g":[1.7727309465408325,0.01710422709584236,-0.01710422709584236],"m":[19.30000114440918,-15.90000057220459,-1.5]}]},
# {"t":"d","c":"75","d":[{"t":"1525639346553","a":[-2.4661030769348145,3.8021082878112793,4.984879970550537],"g":[1.7727309465408325,0.01710422709584236,-0.01710422709584236],"m":[19.30000114440918,-15.90000057220459,-1.5]}]},
# {"t":"d","c":"11","d":[{"t":"1525639346571","a":[-1.9944308996200562,4.613767623901367,3.507612705230713],"g":[1.7727309465408325,0.01710422709584236,-0.01710422709584236],"m":[19.30000114440918,-15.90000057220459,-1.5]}]},
# {"t":"d","c":"71","d":[{"t":"1525639346592","a":[-1.6185297966003418,5.430215358734131,2.111750364303589],"g":[1.7727309465408325,0.01710422709584236,-0.01710422709584236],"m":[19.30000114440918,-15.90000057220459,-1.5]}]},
# {"t":"d","c":"26","d":[{"t":"1525639346603","a":[-1.6185297966003418,5.430215358734131,2.111750364303589],"g":[1.2767083644866943,-0.759916365146637,-0.22479841113090515],"m":[13.5,-31.600000381469727,11.5]}]},
# {"t":"d","c":"92","d":[{"t":"1525639346612","a":[-1.764580488204956,6.5243988037109375,1.3551595211029053],"g":[1.2767083644866943,-0.759916365146637,-0.22479841113090515],"m":[13.5,-31.600000381469727,11.5]}]},
# {"t":"d","c":"79","d":[{"t":"1525639346632","a":[-1.3671308755874634,7.024802207946777,0.7015225291252136],"g":[1.2767083644866943,-0.759916365146637,-0.22479841113090515],"m":[13.5,-31.600000381469727,11.5]}]},
# {"t":"d","c":"19","d":[{"t":"1525639346652","a":[-1.5778270959854126,7.290566921234131,1.1540404558181763],"g":[1.2767083644866943,-0.759916365146637,-0.22479841113090515],"m":[13.5,-31.600000381469727,11.5]}]},
# {"t":"d","c":"57","d":[{"t":"1525639346672","a":[-2.1548473834991455,7.273807048797607,1.9968252182006836],"g":[1.2767083644866943,-0.759916365146637,-0.22479841113090515],"m":[13.5,-31.600000381469727,11.5]}]},
# {"t":"d","c":"83","d":[{"t":"1525639346693","a":[-2.2194926738739014,6.823683261871338,3.3256475925445557],"g":[1.2767083644866943,-0.759916365146637,-0.22479841113090515],"m":[13.5,-31.600000381469727,11.5]}]},
# {"t":"d","c":"39","d":[{"t":"1525639346713","a":[-2.9329867362976074,6.694392204284668,4.09181547164917],"g":[1.2767083644866943,-0.759916365146637,-0.22479841113090515],"m":[13.5,-31.600000381469727,11.5]}]},
# {"t":"d","c":"6","d":[{"t":"1525639346733","a":[-2.4661030769348145,5.973715782165527,4.525179386138916],"g":[1.2767083644866943,-0.759916365146637,-0.22479841113090515],"m":[13.5,-31.600000381469727,11.5]}]},
# {"t":"d","c":"29","d":[{"t":"1525639346753","a":[-1.848380208015442,4.927417755126953,4.616161823272705],"g":[1.2767083644866943,-0.759916365146637,-0.22479841113090515],"m":[13.5,-31.600000381469727,11.5]}]},
# {"t":"d","c":"86","d":[{"t":"1525639346772","a":[-1.874717116355896,4.089421272277832,5.106987953186035],"g":[1.2767083644866943,-0.759916365146637,-0.22479841113090515],"m":[13.5,-31.600000381469727,11.5]}]},
# {"t":"d","c":"4","d":[{"t":"1525639346788","a":[-1.874717116355896,4.089421272277832,5.106987953186035],"g":[-2.3652701377868652,2.105041742324829,0.2223549485206604],"m":[13.40000057220459,-29.100000381469727,6.599999904632568]}]},
# {"t":"d","c":"91","d":[{"t":"1525639346792","a":[-2.4637088775634766,2.942563772201538,5.449369430541992],"g":[-2.3652701377868652,2.105041742324829,0.2223549485206604],"m":[13.40000057220459,-29.100000381469727,6.599999904632568]}]},
# {"t":"d","c":"45","d":[{"t":"1525639346812","a":[-3.4597270488739014,1.9872480630874634,6.541158676147461],"g":[-2.3652701377868652,2.105041742324829,0.2223549485206604],"m":[13.40000057220459,-29.100000381469727,6.599999904632568]}]},
# {"t":"d","c":"67","d":[{"t":"1525639346831","a":[-4.122941017150879,1.6520496606826782,10.18285083770752],"g":[-2.3652701377868652,2.105041742324829,0.2223549485206604],"m":[13.40000057220459,-29.100000381469727,6.599999904632568]}]},
# {"t":"d","c":"80","d":[{"t":"1525639346851","a":[-4.098998546600342,0.6177229285240173,14.636201858520508],"g":[-2.3652701377868652,2.105041742324829,0.2223549485206604],"m":[13.40000057220459,-29.100000381469727,6.599999904632568]}]},
# {"t":"d","c":"43","d":[{"t":"1525639346871","a":[-3.775771379470825,0.483643501996994,18.569995880126953],"g":[-2.3652701377868652,2.105041742324829,0.2223549485206604],"m":[13.40000057220459,-29.100000381469727,6.599999904632568]}]},
# {"t":"d","c":"53","d":[{"t":"1525639346891","a":[-3.5746521949768066,0.2178790122270584,20.245986938476562],"g":[-2.3652701377868652,2.105041742324829,0.2223549485206604],"m":[13.40000057220459,-29.100000381469727,6.599999904632568]}]},
# {"t":"d","c":"5","d":[{"t":"1525639346912","a":[-3.7039430141448975,0.17238777875900269,18.201276779174805],"g":[-2.3652701377868652,2.105041742324829,0.2223549485206604],"m":[13.40000057220459,-29.100000381469727,6.599999904632568]}]},
# {"t":"d","c":"33","d":[{"t":"1525639346932","a":[-4.134912490844727,0.4980091452598572,14.578739166259766],"g":[-2.3652701377868652,2.105041742324829,0.2223549485206604],"m":[13.40000057220459,-29.100000381469727,6.599999904632568]}]},
# {"t":"d","c":"26","d":[{"t":"1525639346952","a":[-4.0415358543396,0.5363175868988037,11.286611557006836],"g":[-2.3652701377868652,2.105041742324829,0.2223549485206604],"m":[13.40000057220459,-29.100000381469727,6.599999904632568]}]},
# {"t":"d","c":"93","d":[{"t":"1525639346964","a":[-4.0415358543396,0.5363175868988037,11.286611557006836],"g":[1.665218710899353,0.17470745742321014,0.09407325088977814],"m":[23.899999618530273,-17.100000381469727,-0.4000000059604645]}]},
# {"t":"d","c":"37","d":[{"t":"1525639346972","a":[-3.3352248668670654,0.7302538156509399,9.555550575256348],"g":[1.665218710899353,0.17470745742321014,0.09407325088977814],"m":[23.899999618530273,-17.100000381469727,-0.4000000059604645]}]},
# {"t":"d","c":"74","d":[{"t":"1525639346992","a":[-3.3902931213378906,1.702329397201538,11.566741943359375],"g":[1.665218710899353,0.17470745742321014,0.09407325088977814],"m":[23.899999618530273,-17.100000381469727,-0.4000000059604645]}]},
# {"t":"d","c":"91","d":[{"t":"1525639347012","a":[-1.8459858894348145,1.1372805833816528,13.740742683410645],"g":[1.665218710899353,0.17470745742321014,0.09407325088977814],"m":[23.899999618530273,-17.100000381469727,-0.4000000059604645]}]},
# {"t":"d","c":"15","d":[{"t":"1525639347033","a":[-1.8818999528884888,1.5347301959991455,14.152558326721191],"g":[1.665218710899353,0.17470745742321014,0.09407325088977814],"m":[23.899999618530273,-17.100000381469727,-0.4000000059604645]}]},
# {"t":"d","c":"89","d":[{"t":"1525639347053","a":[-3.2993106842041016,2.6504621505737305,15.349696159362793],"g":[1.665218710899353,0.17470745742321014,0.09407325088977814],"m":[23.899999618530273,-17.100000381469727,-0.4000000059604645]}]},
# {"t":"d","c":"31","d":[{"t":"1525639347073","a":[-2.205127000808716,1.6640210151672363,13.180482864379883],"g":[1.665218710899353,0.17470745742321014,0.09407325088977814],"m":[23.899999618530273,-17.100000381469727,-0.4000000059604645]}]},
# {"t":"d","c":"70","d":[{"t":"1525639347093","a":[-2.293715238571167,1.2857255935668945,10.587483406066895],"g":[1.665218710899353,0.17470745742321014,0.09407325088977814],"m":[23.899999618530273,-17.100000381469727,-0.4000000059604645]}]},
# {"t":"d","c":"27","d":[{"t":"1525639347113","a":[-4.666441440582275,1.7981003522872925,7.350423812866211],"g":[1.665218710899353,0.17470745742321014,0.09407325088977814],"m":[23.899999618530273,-17.100000381469727,-0.4000000059604645]}]},
# {"t":"d","c":"51","d":[{"t":"1525639347133","a":[-6.584255695343018,3.7949256896972656,2.856369733810425],"g":[1.665218710899353,0.17470745742321014,0.09407325088977814],"m":[23.899999618530273,-17.100000381469727,-0.4000000059604645]}]},
# {"t":"d","c":"49","d":[{"t":"1525639347145","a":[-6.584255695343018,3.7949256896972656,2.856369733810425],"g":[3.72994327545166,-1.0286970138549805,-0.23945917189121246],"m":[15,-29.600000381469727,9.800000190734863]}]},
# {"t":"d","c":"19","d":[{"t":"1525639347153","a":[-3.3040993213653564,5.528380393981934,0.5985686779022217],"g":[3.72994327545166,-1.0286970138549805,-0.23945917189121246],"m":[15,-29.600000381469727,9.800000190734863]}]},
# {"t":"d","c":"71","d":[{"t":"1525639347172","a":[-0.24661029875278473,6.050332546234131,-1.2905141115188599],"g":[3.72994327545166,-1.0286970138549805,-0.23945917189121246],"m":[15,-29.600000381469727,9.800000190734863]}]},
# {"t":"d","c":"63","d":[{"t":"1525639347192","a":[-0.6488484740257263,7.276201248168945,-2.8036959171295166],"g":[3.72994327545166,-1.0286970138549805,-0.23945917189121246],"m":[15,-29.600000381469727,9.800000190734863]}]},
# {"t":"d","c":"78","d":[{"t":"1525639347212","a":[-1.9656996726989746,8.468549728393555,-3.44057297706604],"g":[3.72994327545166,-1.0286970138549805,-0.23945917189121246],"m":[15,-29.600000381469727,9.800000190734863]}]},
# {"t":"d","c":"61","d":[{"t":"1525639347232","a":[-1.7909175157546997,7.927443981170654,-3.2035396099090576],"g":[3.72994327545166,-1.0286970138549805,-0.23945917189121246],"m":[15,-29.600000381469727,9.800000190734863]}]},
# {"t":"d","c":"1","d":[{"t":"1525639347253","a":[-1.5131816864013672,7.008042335510254,-0.6584255695343018],"g":[3.72994327545166,-1.0286970138549805,-0.23945917189121246],"m":[15,-29.600000381469727,9.800000190734863]}]},
# {"t":"d","c":"53","d":[{"t":"1525639347273","a":[-1.6376839876174927,6.217931747436523,1.5610672235488892],"g":[3.72994327545166,-1.0286970138549805,-0.23945917189121246],"m":[15,-29.600000381469727,9.800000190734863]}]},
# {"t":"d","c":"53","d":[{"t":"1525639347293","a":[-1.9944308996200562,5.576265811920166,4.005621910095215],"g":[3.72994327545166,-1.0286970138549805,-0.23945917189121246],"m":[15,-29.600000381469727,9.800000190734863]}]},
# {"t":"d","c":"7","d":[{"t":"1525639347312","a":[-2.317657947540283,4.72150993347168,5.686402797698975],"g":[3.72994327545166,-1.0286970138549805,-0.23945917189121246],"m":[15,-29.600000381469727,9.800000190734863]}]},
# {"t":"d","c":"67","d":[{"t":"1525639347324","a":[-2.317657947540283,4.72150993347168,5.686402797698975],"g":[-3.4245104789733887,0.8246681094169617,0.5082398653030396],"m":[11.5,-30.5,9.100000381469727]}]},
# {"t":"d","c":"48","d":[{"t":"1525639347331","a":[-2.2482240200042725,3.8523881435394287,5.868367671966553],"g":[-3.4245104789733887,0.8246681094169617,0.5082398653030396],"m":[11.5,-30.5,9.100000381469727]}]},
# {"t":"d","c":"24","d":[{"t":"1525639347352","a":[-1.7957061529159546,2.6145482063293457,6.330462455749512],"g":[-3.4245104789733887,0.8246681094169617,0.5082398653030396],"m":[11.5,-30.5,9.100000381469727]}]},
# {"t":"d","c":"7","d":[{"t":"1525639347371","a":[-2.2458298206329346,1.6544438600540161,7.697593688964844],"g":[-3.4245104789733887,0.8246681094169617,0.5082398653030396],"m":[11.5,-30.5,9.100000381469727]}]},
# {"t":"d","c":"9","d":[{"t":"1525639347391","a":[-3.064671754837036,0.9217957854270935,9.948211669921875],"g":[-3.4245104789733887,0.8246681094169617,0.5082398653030396],"m":[11.5,-30.5,9.100000381469727]}]},
# {"t":"d","c":"39","d":[{"t":"1525639347413","a":[-3.7422516345977783,0.6416656374931335,14.782252311706543],"g":[-3.4245104789733887,0.8246681094169617,0.5082398653030396],"m":[11.5,-30.5,9.100000381469727]}]},
# {"t":"d","c":"68","d":[{"t":"1525639347433","a":[-2.882706880569458,-0.07901106774806976,20.200496673583984],"g":[-3.4245104789733887,0.8246681094169617,0.5082398653030396],"m":[11.5,-30.5,9.100000381469727]}]},
# {"t":"d","c":"4","d":[{"t":"1525639347453","a":[-1.472478985786438,-0.39266106486320496,24.37611198425293],"g":[-3.4245104789733887,0.8246681094169617,0.5082398653030396],"m":[11.5,-30.5,9.100000381469727]}]},
# {"t":"d","c":"9","d":[{"t":"1525639347472","a":[-1.6017698049545288,-0.0885881707072258,23.880496978759766],"g":[-3.4245104789733887,0.8246681094169617,0.5082398653030396],"m":[11.5,-30.5,9.100000381469727]}]},
# {"t":"d","c":"52","d":[{"t":"1525639347493","a":[-2.8707354068756104,-0.2657645046710968,20.84455680847168],"g":[-3.4245104789733887,0.8246681094169617,0.5082398653030396],"m":[11.5,-30.5,9.100000381469727]}]},
# {"t":"d","c":"17","d":[{"t":"1525639347504","a":[-2.8707354068756104,-0.2657645046710968,20.84455680847168],"g":[3.3121113777160645,-0.4239404797554016,-0.5681046843528748],"m":[20.5,-16,-0.6000000238418579]}]},
# {"t":"d","c":"93","d":[{"t":"1525639347512","a":[-3.098191499710083,-0.6895511150360107,17.600313186645508],"g":[3.3121113777160645,-0.4239404797554016,-0.5681046843528748],"m":[20.5,-16,-0.6000000238418579]}]},
# {"t":"d","c":"25","d":[{"t":"1525639347531","a":[-2.672010660171509,-0.8092648983001709,13.065557479858398],"g":[3.3121113777160645,-0.4239404797554016,-0.5681046843528748],"m":[20.5,-16,-0.6000000238418579]}]},
# {"t":"d","c":"53","d":[{"t":"1525639347554","a":[-3.6081721782684326,0.30886146426200867,9.768641471862793],"g":[3.3121113777160645,-0.4239404797554016,-0.5681046843528748],"m":[20.5,-16,-0.6000000238418579]}]},
# {"t":"d","c":"72","d":[{"t":"1525639347572","a":[-4.968120098114014,1.6472610235214233,6.859597206115723],"g":[3.3121113777160645,-0.4239404797554016,-0.5681046843528748],"m":[20.5,-16,-0.6000000238418579]}]},
# {"t":"d","c":"53","d":[{"t":"1525639347591","a":[-4.628133296966553,3.3950817584991455,4.0199875831604],"g":[3.3121113777160645,-0.4239404797554016,-0.5681046843528748],"m":[20.5,-16,-0.6000000238418579]}]},
# {"t":"d","c":"9","d":[{"t":"1525639347611","a":[-2.449343204498291,4.412648677825928,1.5658557415008545],"g":[3.3121113777160645,-0.4239404797554016,-0.5681046843528748],"m":[20.5,-16,-0.6000000238418579]}]},
# {"t":"d","c":"41","d":[{"t":"1525639347631","a":[-1.2019259929656982,5.698374271392822,0.2011190801858902],"g":[3.3121113777160645,-0.4239404797554016,-0.5681046843528748],"m":[20.5,-16,-0.6000000238418579]}]},
# {"t":"d","c":"56","d":[{"t":"1525639347651","a":[-1.8795057535171509,6.754249095916748,-1.1995316743850708],"g":[3.3121113777160645,-0.4239404797554016,-0.5681046843528748],"m":[20.5,-16,-0.6000000238418579]}]},
# {"t":"d","c":"72","d":[{"t":"1525639347671","a":[-1.4988160133361816,7.415069103240967,-1.489238977432251],"g":[3.3121113777160645,-0.4239404797554016,-0.5681046843528748],"m":[20.5,-16,-0.6000000238418579]}]},
# {"t":"d","c":"40","d":[{"t":"1525639347683","a":[-1.4988160133361816,7.415069103240967,-1.489238977432251],"g":[-0.6328563690185547,-0.5913175344467163,0.3188716471195221],"m":[12.300000190734863,-33.20000076293945,13.699999809265137]}]},
# {"t":"d","c":"82","d":[{"t":"1525639347691","a":[-0.9074301719665527,6.907482624053955,-0.8356019258499146],"g":[-0.6328563690185547,-0.5913175344467163,0.3188716471195221],"m":[12.300000190734863,-33.20000076293945,13.699999809265137]}]},
# {"t":"d","c":"77","d":[{"t":"1525639347711","a":[-1.5299415588378906,6.988888263702393,0.9385557174682617],"g":[-0.6328563690185547,-0.5913175344467163,0.3188716471195221],"m":[12.300000190734863,-33.20000076293945,13.699999809265137]}]},
# {"t":"d","c":"65","d":[{"t":"1525639347731","a":[-2.0064022541046143,6.469330310821533,2.1213274002075195],"g":[-0.6328563690185547,-0.5913175344467163,0.3188716471195221],"m":[12.300000190734863,-33.20000076293945,13.699999809265137]}]},
# {"t":"d","c":"83","d":[{"t":"1525639347752","a":[-1.8675343990325928,5.645699977874756,3.155654191970825],"g":[-0.6328563690185547,-0.5913175344467163,0.3188716471195221],"m":[12.300000190734863,-33.20000076293945,13.699999809265137]}]},
# {"t":"d","c":"78","d":[{"t":"1525639347772","a":[-2.142875909805298,4.846012115478516,3.905062198638916],"g":[-0.6328563690185547,-0.5913175344467163,0.3188716471195221],"m":[12.300000190734863,-33.20000076293945,13.699999809265137]}]},
# {"t":"d","c":"44","d":[{"t":"1525639347793","a":[-2.423006057739258,3.778165578842163,4.776578426361084],"g":[-0.6328563690185547,-0.5913175344467163,0.3188716471195221],"m":[12.300000190734863,-33.20000076293945,13.699999809265137]}]},
# {"t":"d","c":"28","d":[{"t":"1525639347813","a":[-2.8707354068756104,2.5906052589416504,5.645699977874756],"g":[-0.6328563690185547,-0.5913175344467163,0.3188716471195221],"m":[12.300000190734863,-33.20000076293945,13.699999809265137]}]},
# {"t":"d","c":"43","d":[{"t":"1525639347833","a":[-2.885101079940796,1.2689656019210815,7.525205612182617],"g":[-0.6328563690185547,-0.5913175344467163,0.3188716471195221],"m":[12.300000190734863,-33.20000076293945,13.699999809265137]}]},
# {"t":"d","c":"55","d":[{"t":"1525639347853","a":[-2.6791934967041016,0.36871832609176636,10.28101634979248],"g":[-0.6328563690185547,-0.5913175344467163,0.3188716471195221],"m":[12.300000190734863,-33.20000076293945,13.699999809265137]}]},
# {"t":"d","c":"7","d":[{"t":"1525639347865","a":[-2.6791934967041016,0.36871832609176636,10.28101634979248],"g":[-3.5503487586975098,1.635897159576416,0.39706242084503174],"m":[16.899999618530273,-19.399999618530273,0.30000001192092896]}]},
# {"t":"d","c":"95","d":[{"t":"1525639347873","a":[-2.755810260772705,-0.2130904644727707,14.116643905639648],"g":[-3.5503487586975098,1.635897159576416,0.39706242084503174],"m":[16.899999618530273,-19.399999618530273,0.30000001192092896]}]},
# {"t":"d","c":"73","d":[{"t":"1525639347892","a":[-3.002420663833618,-0.41181525588035583,18.479013442993164],"g":[-3.5503487586975098,1.635897159576416,0.39706242084503174],"m":[16.899999618530273,-19.399999618530273,0.30000001192092896]}]},
# {"t":"d","c":"46","d":[{"t":"1525639347912","a":[-2.9569294452667236,-0.699128270149231,20.451894760131836],"g":[-3.5503487586975098,1.635897159576416,0.39706242084503174],"m":[16.899999618530273,-19.399999618530273,0.30000001192092896]}]},
# {"t":"d","c":"13","d":[{"t":"1525639347951","a":[-3.126922845840454,-0.4189980924129486,18.921953201293945],"g":[-3.5503487586975098,1.635897159576416,0.39706242084503174],"m":[16.899999618530273,-19.399999618530273,0.30000001192092896]}]},
# {"t":"d","c":"52","d":[{"t":"1525639347980","a":[-3.3615617752075195,-0.42378664016723633,16.724010467529297],"g":[-3.5503487586975098,1.635897159576416,0.39706242084503174],"m":[16.899999618530273,-19.399999618530273,0.30000001192092896]}]},
# {"t":"d","c":"27","d":[{"t":"1525639347991","a":[-3.383110284805298,-0.22266755998134613,14.727184295654297],"g":[-3.5503487586975098,1.635897159576416,0.39706242084503174],"m":[16.899999618530273,-19.399999618530273,0.30000001192092896]}]},
# {"t":"d","c":"86","d":[{"t":"1525639348012","a":[-3.0934031009674072,-0.22027328610420227,13.6210298538208],"g":[-3.5503487586975098,1.635897159576416,0.39706242084503174],"m":[16.899999618530273,-19.399999618530273,0.30000001192092896]}]},
# {"t":"d","c":"27","d":[{"t":"1525639348031","a":[-3.2633965015411377,0.5818088054656982,12.85486125946045],"g":[-3.5503487586975098,1.635897159576416,0.39706242084503174],"m":[16.899999618530273,-19.399999618530273,0.30000001192092896]}]},
# {"t":"d","c":"28","d":[{"t":"1525639348043","a":[-3.2633965015411377,0.5818088054656982,12.85486125946045],"g":[3.489262342453003,-1.4110987186431885,-0.375071257352829],"m":[19.399999618530273,-20.5,0.30000001192092896]}]},
# {"t":"d","c":"35","d":[{"t":"1525639348052","a":[-3.198751211166382,0.8092648983001709,11.162109375],"g":[3.489262342453003,-1.4110987186431885,-0.375071257352829],"m":[19.399999618530273,-20.5,0.30000001192092896]}]},
# {"t":"d","c":"28","d":[{"t":"1525639348071","a":[-4.563488006591797,1.6017698049545288,9.368797302246094],"g":[3.489262342453003,-1.4110987186431885,-0.375071257352829],"m":[19.399999618530273,-20.5,0.30000001192092896]}]},
# {"t":"d","c":"33","d":[{"t":"1525639348091","a":[-4.666441440582275,2.7462332248687744,7.254652500152588],"g":[3.489262342453003,-1.4110987186431885,-0.375071257352829],"m":[19.399999618530273,-20.5,0.30000001192092896]}]},
# {"t":"d","c":"12","d":[{"t":"1525639348134","a":[-3.694365978240967,4.7574238777160645,3.4262073040008545],"g":[3.489262342453003,-1.4110987186431885,-0.375071257352829],"m":[19.399999618530273,-20.5,0.30000001192092896]}]},
# {"t":"d","c":"33","d":[{"t":"1525639348151","a":[-3.0215747356414795,5.76062536239624,0.8260248303413391],"g":[3.489262342453003,-1.4110987186431885,-0.375071257352829],"m":[19.399999618530273,-20.5,0.30000001192092896]}]},
# {"t":"d","c":"20","d":[{"t":"1525639348171","a":[-3.967313289642334,7.417463302612305,-1.4389591217041016],"g":[3.489262342453003,-1.4110987186431885,-0.375071257352829],"m":[19.399999618530273,-20.5,0.30000001192092896]}]},
# {"t":"d","c":"13","d":[{"t":"1525639348191","a":[-2.3990633487701416,9.40231704711914,-0.7518022656440735],"g":[3.489262342453003,-1.4110987186431885,-0.375071257352829],"m":[19.399999618530273,-20.5,0.30000001192092896]}]},
# {"t":"d","c":"67","d":[{"t":"1525639348211","a":[0.8044763207435608,7.369577884674072,-0.4740664064884186],"g":[3.489262342453003,-1.4110987186431885,-0.375071257352829],"m":[19.399999618530273,-20.5,0.30000001192092896]}]},
# {"t":"d","c":"60","d":[{"t":"1525639348223","a":[0.8044763207435608,7.369577884674072,-0.4740664064884186],"g":[-0.0757472887635231,0.03176499158143997,1.0910053253173828],"m":[12.699999809265137,-37.10000228881836,21.5]}]},
# {"t":"d","c":"58","d":[{"t":"1525639348231","a":[-0.6320885419845581,8.205180168151855,0.35195839405059814],"g":[-0.0757472887635231,0.03176499158143997,1.0910053253173828],"m":[12.699999809265137,-37.10000228881836,21.5]}]},
# {"t":"d","c":"35","d":[{"t":"1525639348251","a":[-2.418217658996582,8.705583572387695,2.205127000808716],"g":[-0.0757472887635231,0.03176499158143997,1.0910053253173828],"m":[12.699999809265137,-37.10000228881836,21.5]}]},
# {"t":"d","c":"69","d":[{"t":"1525639348271","a":[-1.082212209701538,7.824490070343018,3.715914487838745],"g":[-0.0757472887635231,0.03176499158143997,1.0910053253173828],"m":[12.699999809265137,-37.10000228881836,21.5]}]},
# {"t":"d","c":"68","d":[{"t":"1525639348291","a":[-0.3663240373134613,7.575485706329346,4.843617916107178],"g":[-0.0757472887635231,0.03176499158143997,1.0910053253173828],"m":[12.699999809265137,-37.10000228881836,21.5]}]},
# {"t":"d","c":"75","d":[{"t":"1525639348311","a":[-0.32801565527915955,7.688016414642334,7.034379482269287],"g":[-0.0757472887635231,0.03176499158143997,1.0910053253173828],"m":[12.699999809265137,-37.10000228881836,21.5]}]},
# {"t":"d","c":"61","d":[{"t":"1525639348331","a":[0.40942099690437317,5.9042816162109375,6.301731109619141],"g":[-0.0757472887635231,0.03176499158143997,1.0910053253173828],"m":[12.699999809265137,-37.10000228881836,21.5]}]},
# {"t":"d","c":"95","d":[{"t":"1525639348351","a":[0.3304099142551422,6.227508544921875,6.967339515686035],"g":[-0.0757472887635231,0.03176499158143997,1.0910053253173828],"m":[12.699999809265137,-37.10000228881836,21.5]}]},
# {"t":"d","c":"3","d":[{"t":"1525639348372","a":[0.18675343692302704,5.77259635925293,7.3025383949279785],"g":[-0.0757472887635231,0.03176499158143997,1.0910053253173828],"m":[12.699999809265137,-37.10000228881836,21.5]}]},
# {"t":"d","c":"83","d":[{"t":"1525639348392","a":[-0.20830190181732178,5.7510480880737305,7.2666239738464355],"g":[-0.0757472887635231,0.03176499158143997,1.0910053253173828],"m":[12.699999809265137,-37.10000228881836,21.5]}]},
# {"t":"d","c":"95","d":[{"t":"1525639348403","a":[-0.20830190181732178,5.7510480880737305,7.2666239738464355],"g":[-0.5253441333770752,2.3151793479919434,0.2981022298336029],"m":[9.699999809265137,-35.60000228881836,18.30000114440918]}]},
# {"t":"d","c":"13","d":[{"t":"1525639348412","a":[-0.2729473412036896,5.588237285614014,8.815719604492188],"g":[-0.5253441333770752,2.3151793479919434,0.2981022298336029],"m":[9.699999809265137,-35.60000228881836,18.30000114440918]}]},
# {"t":"d","c":"92","d":[{"t":"1525639348431","a":[-0.8284190893173218,5.296135902404785,8.2817964553833],"g":[-0.5253441333770752,2.3151793479919434,0.2981022298336029],"m":[9.699999809265137,-35.60000228881836,18.30000114440918]}]},
# {"t":"d","c":"61","d":[{"t":"1525639348452","a":[-0.7374366521835327,4.9753031730651855,8.138139724731445],"g":[-0.5253441333770752,2.3151793479919434,0.2981022298336029],"m":[9.699999809265137,-35.60000228881836,18.30000114440918]}]},
# {"t":"d","c":"2","d":[{"t":"1525639348472","a":[-0.06464541703462601,4.517996788024902,7.3336639404296875],"g":[-0.5253441333770752,2.3151793479919434,0.2981022298336029],"m":[9.699999809265137,-35.60000228881836,18.30000114440918]}]},
# {"t":"d","c":"83","d":[{"t":"1525639348492","a":[-2.535537004470825,5.013611316680908,6.69199800491333],"g":[-0.5253441333770752,2.3151793479919434,0.2981022298336029],"m":[9.699999809265137,-35.60000228881836,18.30000114440918]}]},
# {"t":"d","c":"83","d":[{"t":"1525639348512","a":[-3.318464994430542,5.016005516052246,7.367183685302734],"g":[-0.5253441333770752,2.3151793479919434,0.2981022298336029],"m":[9.699999809265137,-35.60000228881836,18.30000114440918]}]},
# {"t":"d","c":"22","d":[{"t":"1525639348532","a":[-3.5746521949768066,4.642498970031738,8.425453186035156],"g":[-0.5253441333770752,2.3151793479919434,0.2981022298336029],"m":[9.699999809265137,-35.60000228881836,18.30000114440918]}]},
# {"t":"d","c":"17","d":[{"t":"1525639348552","a":[-3.2083282470703125,3.811685562133789,8.44221305847168],"g":[-0.5253441333770752,2.3151793479919434,0.2981022298336029],"m":[9.699999809265137,-35.60000228881836,18.30000114440918]}]},
# {"t":"d","c":"20","d":[{"t":"1525639348573","a":[-3.253819465637207,3.217905282974243,8.480521202087402],"g":[-0.5253441333770752,2.3151793479919434,0.2981022298336029],"m":[9.699999809265137,-35.60000228881836,18.30000114440918]}]},
# {"t":"d","c":"69","d":[{"t":"1525639348584","a":[-3.253819465637207,3.217905282974243,8.480521202087402],"g":[-1.6786576509475708,2.361604928970337,-0.4862487316131592],"m":[17.899999618530273,-32,14.300000190734863]}]},
# {"t":"d","c":"82","d":[{"t":"1525639348595","a":[-2.937775135040283,2.569056987762451,8.755863189697266],"g":[-1.6786576509475708,2.361604928970337,-0.4862487316131592],"m":[17.899999618530273,-32,14.300000190734863]}]},
# {"t":"d","c":"54","d":[{"t":"1525639348612","a":[-2.9281980991363525,2.7510218620300293,8.964164733886719],"g":[-1.6786576509475708,2.361604928970337,-0.4862487316131592],"m":[17.899999618530273,-32,14.300000190734863]}]},
# {"t":"d","c":"15","d":[{"t":"1525639348632","a":[-4.374340057373047,3.227482557296753,8.257853507995605],"g":[-1.6786576509475708,2.361604928970337,-0.4862487316131592],"m":[17.899999618530273,-32,14.300000190734863]}]},
# {"t":"d","c":"78","d":[{"t":"1525639348652","a":[-5.408666610717773,3.3855044841766357,6.718335151672363],"g":[-1.6786576509475708,2.361604928970337,-0.4862487316131592],"m":[17.899999618530273,-32,14.300000190734863]}]},
# {"t":"d","c":"19","d":[{"t":"1525639348671","a":[-7.283383846282959,3.996044635772705,5.45176362991333],"g":[-1.6786576509475708,2.361604928970337,-0.4862487316131592],"m":[17.899999618530273,-32,14.300000190734863]}]},
# {"t":"d","c":"3","d":[{"t":"1525639348691","a":[-9.06233024597168,4.477293968200684,4.525179386138916],"g":[-1.6786576509475708,2.361604928970337,-0.4862487316131592],"m":[17.899999618530273,-32,14.300000190734863]}]},
# {"t":"d","c":"33","d":[{"t":"1525639348711","a":[-7.527599811553955,3.60098934173584,5.767807960510254],"g":[-1.6786576509475708,2.361604928970337,-0.4862487316131592],"m":[17.899999618530273,-32,14.300000190734863]}]},
# {"t":"d","c":"11","d":[{"t":"1525639348732","a":[-6.883540153503418,3.3639562129974365,6.938608169555664],"g":[-1.6786576509475708,2.361604928970337,-0.4862487316131592],"m":[17.899999618530273,-32,14.300000190734863]}]},
# {"t":"d","c":"78","d":[{"t":"1525639348753","a":[-6.967339515686035,3.2633965015411377,6.8356547355651855],"g":[-1.6786576509475708,2.361604928970337,-0.4862487316131592],"m":[17.899999618530273,-32,14.300000190734863]}]},
# {"t":"d","c":"26","d":[{"t":"1525639348764","a":[-6.967339515686035,3.2633965015411377,6.8356547355651855],"g":[-0.0757472887635231,0.30054569244384766,-1.4966198205947876],"m":[32.10000228881836,-22.5,12.5]}]},
# {"t":"d","c":"6","d":[{"t":"1525639348772","a":[-8.197997093200684,3.5147953033447266,5.774991035461426],"g":[-0.0757472887635231,0.30054569244384766,-1.4966198205947876],"m":[32.10000228881836,-22.5,12.5]}]},
# {"t":"d","c":"31","d":[{"t":"1525639348792","a":[-8.734314918518066,3.342407703399658,5.02079439163208],"g":[-0.0757472887635231,0.30054569244384766,-1.4966198205947876],"m":[32.10000228881836,-22.5,12.5]}]},
# {"t":"d","c":"40","d":[{"t":"1525639348812","a":[-8.221940040588379,2.415823221206665,6.447782039642334],"g":[-0.0757472887635231,0.30054569244384766,-1.4966198205947876],"m":[32.10000228881836,-22.5,12.5]}]},
# {"t":"d","c":"67","d":[{"t":"1525639348832","a":[-7.326480865478516,1.6233183145523071,7.465348720550537],"g":[-0.0757472887635231,0.30054569244384766,-1.4966198205947876],"m":[32.10000228881836,-22.5,12.5]}]},
# {"t":"d","c":"93","d":[{"t":"1525639348852","a":[-7.393520832061768,2.078230619430542,7.62097692489624],"g":[-0.0757472887635231,0.30054569244384766,-1.4966198205947876],"m":[32.10000228881836,-22.5,12.5]}]},
# {"t":"d","c":"94","d":[{"t":"1525639348873","a":[-7.130150318145752,1.6951465606689453,7.063110828399658],"g":[-0.0757472887635231,0.30054569244384766,-1.4966198205947876],"m":[32.10000228881836,-22.5,12.5]}]},
# {"t":"d","c":"78","d":[{"t":"1525639348893","a":[-8.935433387756348,2.207521438598633,6.5220046043396],"g":[-0.0757472887635231,0.30054569244384766,-1.4966198205947876],"m":[32.10000228881836,-22.5,12.5]}]},
# {"t":"d","c":"63","d":[{"t":"1525639348913","a":[-8.995290756225586,1.6017698049545288,7.951386451721191],"g":[-0.0757472887635231,0.30054569244384766,-1.4966198205947876],"m":[32.10000228881836,-22.5,12.5]}]},
# {"t":"d","c":"65","d":[{"t":"1525639348932","a":[-7.8268842697143555,0.2011190801858902,9.687235832214355],"g":[-0.0757472887635231,0.30054569244384766,-1.4966198205947876],"m":[32.10000228881836,-22.5,12.5]}]},
# {"t":"d","c":"28","d":[{"t":"1525639348944","a":[-7.8268842697143555,0.2011190801858902,9.687235832214355],"g":[-0.19303341209888458,-3.936415672302246,-1.871691107749939],"m":[32.60000228881836,-9.90000057220459,3.299999952316284]}]},
# {"t":"d","c":"69","d":[{"t":"1525639348952","a":[-6.404685020446777,-0.5387118458747864,11.66251277923584],"g":[-0.19303341209888458,-3.936415672302246,-1.871691107749939],"m":[32.60000228881836,-9.90000057220459,3.299999952316284]}]},
# {"t":"d","c":"37","d":[{"t":"1525639348972","a":[-6.5004563331604,0.17957061529159546,10.568328857421875],"g":[-0.19303341209888458,-3.936415672302246,-1.871691107749939],"m":[32.60000228881836,-9.90000057220459,3.299999952316284]}]},
# {"t":"d","c":"57","d":[{"t":"1525639348992","a":[-7.070293426513672,1.599375605583191,11.99292278289795],"g":[-0.19303341209888458,-3.936415672302246,-1.871691107749939],"m":[32.60000228881836,-9.90000057220459,3.299999952316284]}]},
# {"t":"d","c":"14","d":[{"t":"1525639349013","a":[-9.067118644714355,1.6400781869888306,10.09426212310791],"g":[-0.19303341209888458,-3.936415672302246,-1.871691107749939],"m":[32.60000228881836,-9.90000057220459,3.299999952316284]}]},
# {"t":"d","c":"84","d":[{"t":"1525639349032","a":[-7.647313594818115,0.90024733543396,10.35284423828125],"g":[-0.19303341209888458,-3.936415672302246,-1.871691107749939],"m":[32.60000228881836,-9.90000057220459,3.299999952316284]}]},
# {"t":"d","c":"3","d":[{"t":"1525639349051","a":[-5.2219133377075195,-0.10295381397008896,10.927470207214355],"g":[-0.19303341209888458,-3.936415672302246,-1.871691107749939],"m":[32.60000228881836,-9.90000057220459,3.299999952316284]}]},
# {"t":"d","c":"56","d":[{"t":"1525639349071","a":[-4.77178955078125,0.05746259540319443,11.327314376831055],"g":[-0.19303341209888458,-3.936415672302246,-1.871691107749939],"m":[32.60000228881836,-9.90000057220459,3.299999952316284]}]},
# {"t":"d","c":"61","d":[{"t":"1525639349091","a":[-5.614574432373047,0.8834874033927917,10.678465843200684],"g":[-0.19303341209888458,-3.936415672302246,-1.871691107749939],"m":[32.60000228881836,-9.90000057220459,3.299999952316284]}]},
# {"t":"d","c":"10","d":[{"t":"1525639349111","a":[-6.229903221130371,1.501210331916809,9.433443069458008],"g":[-0.19303341209888458,-3.936415672302246,-1.871691107749939],"m":[32.60000228881836,-9.90000057220459,3.299999952316284]}]},
# {"t":"d","c":"61","d":[{"t":"1525639349123","a":[-6.229903221130371,1.501210331916809,9.433443069458008],"g":[1.9071213006973267,-1.557706356048584,0.8429940342903137],"m":[20.80000114440918,-14.600000381469727,-1.100000023841858]}]},
# {"t":"d","c":"67","d":[{"t":"1525639349131","a":[-6.4621477127075195,2.104567527770996,10.40073013305664],"g":[1.9071213006973267,-1.557706356048584,0.8429940342903137],"m":[20.80000114440918,-14.600000381469727,-1.100000023841858]}]},
# {"t":"d","c":"31","d":[{"t":"1525639349152","a":[-5.528380393981934,2.4301888942718506,9.763853073120117],"g":[1.9071213006973267,-1.557706356048584,0.8429940342903137],"m":[20.80000114440918,-14.600000381469727,-1.100000023841858]}]},
# {"t":"d","c":"47","d":[{"t":"1525639349171","a":[-4.252232074737549,2.5331428050994873,8.590658187866211],"g":[1.9071213006973267,-1.557706356048584,0.8429940342903137],"m":[20.80000114440918,-14.600000381469727,-1.100000023841858]}]},
# {"t":"d","c":"92","d":[{"t":"1525639349191","a":[-1.5706442594528198,2.82045578956604,6.955368518829346],"g":[1.9071213006973267,-1.557706356048584,0.8429940342903137],"m":[20.80000114440918,-14.600000381469727,-1.100000023841858]}]},
# {"t":"d","c":"40","d":[{"t":"1525639349211","a":[-1.489238977432251,3.5124011039733887,6.179623126983643],"g":[1.9071213006973267,-1.557706356048584,0.8429940342903137],"m":[20.80000114440918,-14.600000381469727,-1.100000023841858]}]},
# {"t":"d","c":"70","d":[{"t":"1525639349231","a":[-2.942563772201538,4.84122371673584,5.798933506011963],"g":[1.9071213006973267,-1.557706356048584,0.8429940342903137],"m":[20.80000114440918,-14.600000381469727,-1.100000023841858]}]},
# {"t":"d","c":"46","d":[{"t":"1525639349251","a":[-1.6352896690368652,5.138113975524902,5.34162712097168],"g":[1.9071213006973267,-1.557706356048584,0.8429940342903137],"m":[20.80000114440918,-14.600000381469727,-1.100000023841858]}]},
# {"t":"d","c":"14","d":[{"t":"1525639349271","a":[-0.5746259689331055,5.205153465270996,5.8803391456604],"g":[1.9071213006973267,-1.557706356048584,0.8429940342903137],"m":[20.80000114440918,-14.600000381469727,-1.100000023841858]}]},
# {"t":"d","c":"62","d":[{"t":"1525639349291","a":[-0.25139886140823364,5.394300937652588,6.763826370239258],"g":[1.9071213006973267,-1.557706356048584,0.8429940342903137],"m":[20.80000114440918,-14.600000381469727,-1.100000023841858]}]},
# {"t":"d","c":"15","d":[{"t":"1525639349304","a":[-0.25139886140823364,5.394300937652588,6.763826370239258],"g":[0.9822713136672974,0.6878342628479004,1.6872098445892334],"m":[12.5,-31.80000114440918,11]}]},
# {"t":"d","c":"75","d":[{"t":"1525639349313","a":[-0.16759923100471497,5.207547664642334,6.9290313720703125],"g":[0.9822713136672974,0.6878342628479004,1.6872098445892334],"m":[12.5,-31.80000114440918,11]}]},
# {"t":"d","c":"65","d":[{"t":"1525639349332","a":[-0.019154198467731476,5.293741703033447,7.055927753448486],"g":[0.9822713136672974,0.6878342628479004,1.6872098445892334],"m":[12.5,-31.80000114440918,11]}]},
# {"t":"d","c":"57","d":[{"t":"1525639349352","a":[0.50519198179245,5.1309309005737305,6.366376876831055],"g":[0.9822713136672974,0.6878342628479004,1.6872098445892334],"m":[12.5,-31.80000114440918,11]}]},
# {"t":"d","c":"4","d":[{"t":"1525639349373","a":[0.10295381397008896,5.633728504180908,5.918647289276123],"g":[0.9822713136672974,0.6878342628479004,1.6872098445892334],"m":[12.5,-31.80000114440918,11]}]},
# {"t":"d","c":"78","d":[{"t":"1525639349392","a":[-0.4668835997581482,6.538764476776123,6.31130838394165],"g":[0.9822713136672974,0.6878342628479004,1.6872098445892334],"m":[12.5,-31.80000114440918,11]}]},
# {"t":"d","c":"72","d":[{"t":"1525639349411","a":[-0.15802213549613953,6.1820173263549805,6.210748672485352],"g":[0.9822713136672974,0.6878342628479004,1.6872098445892334],"m":[12.5,-31.80000114440918,11]}]},
# {"t":"d","c":"5","d":[{"t":"1525639349432","a":[0.4740664064884186,5.875550270080566,6.0886406898498535],"g":[0.9822713136672974,0.6878342628479004,1.6872098445892334],"m":[12.5,-31.80000114440918,11]}]},
# {"t":"d","c":"31","d":[{"t":"1525639349451","a":[1.0870007276535034,6.498061656951904,6.129343509674072],"g":[0.9822713136672974,0.6878342628479004,1.6872098445892334],"m":[12.5,-31.80000114440918,11]}]},
# {"t":"d","c":"19","d":[{"t":"1525639349471","a":[0.8356019258499146,6.593832969665527,5.7199225425720215],"g":[0.9822713136672974,0.6878342628479004,1.6872098445892334],"m":[12.5,-31.80000114440918,11]}]},
# {"t":"d","c":"94","d":[{"t":"1525639349483","a":[0.8356019258499146,6.593832969665527,5.7199225425720215],"g":[0.6792821288108826,0.6096435189247131,0.23945917189121246],"m":[8.699999809265137,-36.10000228881836,17.100000381469727]}]},
# {"t":"d","c":"65","d":[{"t":"1525639349491","a":[0.5147690773010254,6.3089141845703125,5.214730739593506],"g":[0.6792821288108826,0.6096435189247131,0.23945917189121246],"m":[8.699999809265137,-36.10000228881836,17.100000381469727]}]},
# {"t":"d","c":"79","d":[{"t":"1525639349512","a":[0.09098244458436966,6.505244731903076,4.551516532897949],"g":[0.6792821288108826,0.6096435189247131,0.23945917189121246],"m":[8.699999809265137,-36.10000228881836,17.100000381469727]}]},
# {"t":"d","c":"29","d":[{"t":"1525639349532","a":[-0.2657645046710968,7.010436534881592,4.676018714904785],"g":[0.6792821288108826,0.6096435189247131,0.23945917189121246],"m":[8.699999809265137,-36.10000228881836,17.100000381469727]}]},
# {"t":"d","c":"22","d":[{"t":"1525639349552","a":[0.4022381603717804,7.079870700836182,4.3551859855651855],"g":[0.6792821288108826,0.6096435189247131,0.23945917189121246],"m":[8.699999809265137,-36.10000228881836,17.100000381469727]}]},
# {"t":"d","c":"15","d":[{"t":"1525639349573","a":[-0.8188419938087463,7.218738555908203,4.4796881675720215],"g":[0.6792821288108826,0.6096435189247131,0.23945917189121246],"m":[8.699999809265137,-36.10000228881836,17.100000381469727]}]},
# {"t":"d","c":"39","d":[{"t":"1525639349593","a":[-0.13407939672470093,6.7135467529296875,4.316877365112305],"g":[0.6792821288108826,0.6096435189247131,0.23945917189121246],"m":[8.699999809265137,-36.10000228881836,17.100000381469727]}]},
# {"t":"d","c":"97","d":[{"t":"1525639349613","a":[0.19393625855445862,5.767807960510254,3.7566170692443848],"g":[0.6792821288108826,0.6096435189247131,0.23945917189121246],"m":[8.699999809265137,-36.10000228881836,17.100000381469727]}]},
# {"t":"d","c":"5","d":[{"t":"1525639349633","a":[-0.6895511150360107,5.379935264587402,3.4645156860351562],"g":[0.6792821288108826,0.6096435189247131,0.23945917189121246],"m":[8.699999809265137,-36.10000228881836,17.100000381469727]}]},
# {"t":"d","c":"99","d":[{"t":"1525639349653","a":[-1.9992194175720215,5.157268047332764,3.720703125],"g":[0.6792821288108826,0.6096435189247131,0.23945917189121246],"m":[8.699999809265137,-36.10000228881836,17.100000381469727]}]},
# {"t":"d","c":"99","d":[{"t":"1525639349665","a":[-1.9992194175720215,5.157268047332764,3.720703125],"g":[-2.2992968559265137,1.5063936710357666,-1.7763961553573608],"m":[9.800000190734863,-36.900001525878906,18.899999618530273]}]},
# {"t":"d","c":"0","d":[{"t":"1525639349673","a":[-3.222693920135498,4.857983589172363,4.144489765167236],"g":[-2.2992968559265137,1.5063936710357666,-1.7763961553573608],"m":[9.800000190734863,-36.900001525878906,18.899999618530273]}]},
# {"t":"d","c":"76","d":[{"t":"1525639349693","a":[-3.9840731620788574,3.9577362537384033,4.814886569976807],"g":[-2.2992968559265137,1.5063936710357666,-1.7763961553573608],"m":[9.800000190734863,-36.900001525878906,18.899999618530273]}]},
# {"t":"d","c":"27","d":[{"t":"1525639349712","a":[-5.612180233001709,2.9329867362976074,6.608198642730713],"g":[-2.2992968559265137,1.5063936710357666,-1.7763961553573608],"m":[9.800000190734863,-36.900001525878906,18.899999618530273]}]},
# {"t":"d","c":"18","d":[{"t":"1525639349732","a":[-5.2817702293396,1.874717116355896,8.42066478729248],"g":[-2.2992968559265137,1.5063936710357666,-1.7763961553573608],"m":[9.800000190734863,-36.900001525878906,18.899999618530273]}]},
# {"t":"d","c":"42","d":[{"t":"1525639349752","a":[-5.980898380279541,1.7382434606552124,10.312141418457031],"g":[-2.2992968559265137,1.5063936710357666,-1.7763961553573608],"m":[9.800000190734863,-36.900001525878906,18.899999618530273]}]},
# {"t":"d","c":"73","d":[{"t":"1525639349772","a":[-7.462954521179199,2.1213274002075195,11.437450408935547],"g":[-2.2992968559265137,1.5063936710357666,-1.7763961553573608],"m":[9.800000190734863,-36.900001525878906,18.899999618530273]}]},
# {"t":"d","c":"43","d":[{"t":"1525639349792","a":[-7.187613010406494,1.283331274986267,11.626598358154297],"g":[-2.2992968559265137,1.5063936710357666,-1.7763961553573608],"m":[9.800000190734863,-36.900001525878906,18.899999618530273]}]},
# {"t":"d","c":"86","d":[{"t":"1525639349812","a":[-7.285778045654297,0.6680026650428772,11.356045722961426],"g":[-2.2992968559265137,1.5063936710357666,-1.7763961553573608],"m":[9.800000190734863,-36.900001525878906,18.899999618530273]}]},
# {"t":"d","c":"40","d":[{"t":"1525639349831","a":[-6.905088424682617,-0.019154198467731476,11.02084732055664],"g":[-2.2992968559265137,1.5063936710357666,-1.7763961553573608],"m":[9.800000190734863,-36.900001525878906,18.899999618530273]}]},
# {"t":"d","c":"46","d":[{"t":"1525639349843","a":[-6.905088424682617,-0.019154198467731476,11.02084732055664],"g":[-0.9101892113685608,0.35552358627319336,-0.27122417092323303],"m":[29.80000114440918,-17.600000381469727,3.6000001430511475]}]},
# {"t":"d","c":"89","d":[{"t":"1525639349851","a":[-5.988081455230713,-0.18196488916873932,11.470970153808594],"g":[-0.9101892113685608,0.35552358627319336,-0.27122417092323303],"m":[29.80000114440918,-17.600000381469727,3.6000001430511475]}]},
# {"t":"d","c":"99","d":[{"t":"1525639349871","a":[-5.763019561767578,-0.011971374042332172,11.399142265319824],"g":[-0.9101892113685608,0.35552358627319336,-0.27122417092323303],"m":[29.80000114440918,-17.600000381469727,3.6000001430511475]}]},
# {"t":"d","c":"61","d":[{"t":"1525639349891","a":[-6.514821529388428,0.40463244915008545,10.3217191696167],"g":[-0.9101892113685608,0.35552358627319336,-0.27122417092323303],"m":[29.80000114440918,-17.600000381469727,3.6000001430511475]}]},
# {"t":"d","c":"87","d":[{"t":"1525639349911","a":[-6.2155375480651855,0.07182824611663818,8.935433387756348],"g":[-0.9101892113685608,0.35552358627319336,-0.27122417092323303],"m":[29.80000114440918,-17.600000381469727,3.6000001430511475]}]},
# {"t":"d","c":"97","d":[{"t":"1525639349931","a":[-4.678412914276123,0.21069619059562683,8.710371971130371],"g":[-0.9101892113685608,0.35552358627319336,-0.27122417092323303],"m":[29.80000114440918,-17.600000381469727,3.6000001430511475]}]},
# {"t":"d","c":"35","d":[{"t":"1525639349951","a":[-4.767001152038574,0.4429408311843872,9.694418907165527],"g":[-0.9101892113685608,0.35552358627319336,-0.27122417092323303],"m":[29.80000114440918,-17.600000381469727,3.6000001430511475]}]},
# {"t":"d","c":"58","d":[{"t":"1525639349971","a":[-4.89150333404541,0.3543526828289032,10.21397590637207],"g":[-0.9101892113685608,0.35552358627319336,-0.27122417092323303],"m":[29.80000114440918,-17.600000381469727,3.6000001430511475]}]},
# {"t":"d","c":"19","d":[{"t":"1525639349997","a":[-4.95136022567749,0.6775797605514526,12.608251571655273],"g":[-0.9101892113685608,0.35552358627319336,-0.27122417092323303],"m":[29.80000114440918,-17.600000381469727,3.6000001430511475]}]},
# {"t":"d","c":"65","d":[{"t":"1525639350011","a":[-5.480494976043701,0.20590762794017792,13.357659339904785],"g":[-0.9101892113685608,0.35552358627319336,-0.27122417092323303],"m":[29.80000114440918,-17.600000381469727,3.6000001430511475]}]},
# {"t":"d","c":"36","d":[{"t":"1525639350022","a":[-5.480494976043701,0.20590762794017792,13.357659339904785],"g":[1.1093312501907349,-1.186300277709961,-0.9749409556388855],"m":[30.100000381469727,-10.699999809265137,1.600000023841858]}]},
# {"t":"d","c":"15","d":[{"t":"1525639350031","a":[-6.526793003082275,0.14605076611042023,10.877190589904785],"g":[1.1093312501907349,-1.186300277709961,-0.9749409556388855],"m":[30.100000381469727,-10.699999809265137,1.600000023841858]}]},
# {"t":"d","c":"94","d":[{"t":"1525639350051","a":[-6.799740314483643,0.8499675393104553,9.545973777770996],"g":[1.1093312501907349,-1.186300277709961,-0.9749409556388855],"m":[30.100000381469727,-10.699999809265137,1.600000023841858]}]},
# {"t":"d","c":"47","d":[{"t":"1525639350071","a":[-6.095823764801025,1.0941835641860962,10.597060203552246],"g":[1.1093312501907349,-1.186300277709961,-0.9749409556388855],"m":[30.100000381469727,-10.699999809265137,1.600000023841858]}]},
# {"t":"d","c":"51","d":[{"t":"1525639350091","a":[-5.183605194091797,0.7709565162658691,11.815746307373047],"g":[1.1093312501907349,-1.186300277709961,-0.9749409556388855],"m":[30.100000381469727,-10.699999809265137,1.600000023841858]}]},
# {"t":"d","c":"82","d":[{"t":"1525639350112","a":[-3.811685562133789,0.31604427099227905,11.569135665893555],"g":[1.1093312501907349,-1.186300277709961,-0.9749409556388855],"m":[30.100000381469727,-10.699999809265137,1.600000023841858]}]},
# {"t":"d","c":"38","d":[{"t":"1525639350132","a":[-4.108575344085693,0.8379961848258972,10.242707252502441],"g":[1.1093312501907349,-1.186300277709961,-0.9749409556388855],"m":[30.100000381469727,-10.699999809265137,1.600000023841858]}]},
# {"t":"d","c":"44","d":[{"t":"1525639350151","a":[-6.047938346862793,2.176395893096924,10.324112892150879],"g":[1.1093312501907349,-1.186300277709961,-0.9749409556388855],"m":[30.100000381469727,-10.699999809265137,1.600000023841858]}]},
# {"t":"d","c":"22","d":[{"t":"1525639350171","a":[-5.753442287445068,2.4349775314331055,10.767053604125977],"g":[1.1093312501907349,-1.186300277709961,-0.9749409556388855],"m":[30.100000381469727,-10.699999809265137,1.600000023841858]}]},
# {"t":"d","c":"76","d":[{"t":"1525639350191","a":[-3.227482557296753,2.1189332008361816,9.47175121307373],"g":[1.1093312501907349,-1.186300277709961,-0.9749409556388855],"m":[30.100000381469727,-10.699999809265137,1.600000023841858]}]},
# {"t":"d","c":"44","d":[{"t":"1525639350203","a":[-3.227482557296753,2.1189332008361816,9.47175121307373],"g":[2.9126055240631104,-1.2681562900543213,0.32498031854629517],"m":[16.5,-20.399999618530273,1]}]},
# {"t":"d","c":"3","d":[{"t":"1525639350211","a":[-2.5594797134399414,2.7797529697418213,8.705583572387695],"g":[2.9126055240631104,-1.2681562900543213,0.32498031854629517],"m":[16.5,-20.399999618530273,1]}]},
# {"t":"d","c":"55","d":[{"t":"1525639350231","a":[-2.4900457859039307,3.5147953033447266,8.73192024230957],"g":[2.9126055240631104,-1.2681562900543213,0.32498031854629517],"m":[16.5,-20.399999618530273,1]}]},
# {"t":"d","c":"73","d":[{"t":"1525639350252","a":[-1.0750293731689453,3.414235830307007,6.234691619873047],"g":[2.9126055240631104,-1.2681562900543213,0.32498031854629517],"m":[16.5,-20.399999618530273,1]}]},
# {"t":"d","c":"56","d":[{"t":"1525639350272","a":[-1.922602653503418,4.606584548950195,5.243461608886719],"g":[2.9126055240631104,-1.2681562900543213,0.32498031854629517],"m":[16.5,-20.399999618530273,1]}]},
# {"t":"d","c":"7","d":[{"t":"1525639350292","a":[-1.848380208015442,6.079063892364502,4.58024787902832],"g":[2.9126055240631104,-1.2681562900543213,0.32498031854629517],"m":[16.5,-20.399999618530273,1]}]},
# {"t":"d","c":"56","d":[{"t":"1525639350312","a":[0.4620950520038605,6.787769317626953,3.1125571727752686],"g":[2.9126055240631104,-1.2681562900543213,0.32498031854629517],"m":[16.5,-20.399999618530273,1]}]},
# {"t":"d","c":"0","d":[{"t":"1525639350331","a":[1.0846065282821655,6.723123550415039,1.4198049306869507],"g":[2.9126055240631104,-1.2681562900543213,0.32498031854629517],"m":[16.5,-20.399999618530273,1]}]},
# {"t":"d","c":"32","d":[{"t":"1525639350352","a":[0.16281068325042725,7.292961120605469,2.015979290008545],"g":[2.9126055240631104,-1.2681562900543213,0.32498031854629517],"m":[16.5,-20.399999618530273,1]}]},
# {"t":"d","c":"49","d":[{"t":"1525639350372","a":[0.12689656019210815,7.69280481338501,2.2194926738739014],"g":[2.9126055240631104,-1.2681562900543213,0.32498031854629517],"m":[16.5,-20.399999618530273,1]}]},
# {"t":"d","c":"47","d":[{"t":"1525639350383","a":[0.12689656019210815,7.69280481338501,2.2194926738739014],"g":[0.18570303916931152,0.32131510972976685,0.1014036312699318],"m":[11.800000190734863,-35.29999923706055,19.80000114440918]}]},
# {"t":"d","c":"57","d":[{"t":"1525639350392","a":[-0.20830190181732178,7.4557719230651855,2.449343204498291],"g":[0.18570303916931152,0.32131510972976685,0.1014036312699318],"m":[11.800000190734863,-35.29999923706055,19.80000114440918]}]},
# {"t":"d","c":"96","d":[{"t":"1525639350412","a":[-0.3184385597705841,7.285778045654297,3.38071608543396],"g":[0.18570303916931152,0.32131510972976685,0.1014036312699318],"m":[11.800000190734863,-35.29999923706055,19.80000114440918]}]},
# {"t":"d","c":"93","d":[{"t":"1525639350432","a":[-0.6871568560600281,6.976916790008545,4.21152925491333],"g":[0.18570303916931152,0.32131510972976685,0.1014036312699318],"m":[11.800000190734863,-35.29999923706055,19.80000114440918]}]},
# {"t":"d","c":"12","d":[{"t":"1525639350452","a":[-0.28731298446655273,6.531581878662109,4.599401950836182],"g":[0.18570303916931152,0.32131510972976685,0.1014036312699318],"m":[11.800000190734863,-35.29999923706055,19.80000114440918]}]},
# {"t":"d","c":"74","d":[{"t":"1525639350472","a":[-0.5171633362770081,6.562707424163818,4.367157459259033],"g":[0.18570303916931152,0.32131510972976685,0.1014036312699318],"m":[11.800000190734863,-35.29999923706055,19.80000114440918]}]},
# {"t":"d","c":"88","d":[{"t":"1525639350492","a":[-0.4860377907752991,6.239480018615723,3.7925312519073486],"g":[0.18570303916931152,0.32131510972976685,0.1014036312699318],"m":[11.800000190734863,-35.29999923706055,19.80000114440918]}]},
# {"t":"d","c":"91","d":[{"t":"1525639350512","a":[-1.0462981462478638,6.222720146179199,3.416630268096924],"g":[0.18570303916931152,0.32131510972976685,0.1014036312699318],"m":[11.800000190734863,-35.29999923706055,19.80000114440918]}]},
# {"t":"d","c":"80","d":[{"t":"1525639350532","a":[-0.8739103078842163,5.025582790374756,3.2993106842041016],"g":[0.18570303916931152,0.32131510972976685,0.1014036312699318],"m":[11.800000190734863,-35.29999923706055,19.80000114440918]}]},
# {"t":"d","c":"4","d":[{"t":"1525639350552","a":[-1.5059988498687744,3.931399345397949,3.5866236686706543],"g":[0.18570303916931152,0.32131510972976685,0.1014036312699318],"m":[11.800000190734863,-35.29999923706055,19.80000114440918]}]},
# {"t":"d","c":"51","d":[{"t":"1525639350564","a":[-1.5059988498687744,3.931399345397949,3.5866236686706543],"g":[-3.1642820835113525,1.6811012029647827,-0.3995058834552765],"m":[12.90000057220459,-33.400001525878906,14.600000381469727]}]},
# {"t":"d","c":"18","d":[{"t":"1525639350572","a":[-2.0279507637023926,2.669616460800171,3.696760416030884],"g":[-3.1642820835113525,1.6811012029647827,-0.3995058834552765],"m":[12.90000057220459,-33.400001525878906,14.600000381469727]}]},
# {"t":"d","c":"25","d":[{"t":"1525639350592","a":[-2.8755240440368652,1.5586729049682617,4.704750061035156],"g":[-3.1642820835113525,1.6811012029647827,-0.3995058834552765],"m":[12.90000057220459,-33.400001525878906,14.600000381469727]}]},
# {"t":"d","c":"25","d":[{"t":"1525639350611","a":[-4.008016109466553,0.2849186956882477,5.655277252197266],"g":[-3.1642820835113525,1.6811012029647827,-0.3995058834552765],"m":[12.90000057220459,-33.400001525878906,14.600000381469727]}]},
# {"t":"d","c":"89","d":[{"t":"1525639350631","a":[-4.824463844299316,-0.4716721475124359,8.068706512451172],"g":[-3.1642820835113525,1.6811012029647827,-0.3995058834552765],"m":[12.90000057220459,-33.400001525878906,14.600000381469727]}]},
# {"t":"d","c":"18","d":[{"t":"1525639350656","a":[-5.446975231170654,-1.3695251941680908,11.286611557006836],"g":[-3.1642820835113525,1.6811012029647827,-0.3995058834552765],"m":[12.90000057220459,-33.400001525878906,14.600000381469727]}]},
# {"t":"d","c":"73","d":[{"t":"1525639350671","a":[-5.746259689331055,-2.3344178199768066,14.669721603393555],"g":[-3.1642820835113525,1.6811012029647827,-0.3995058834552765],"m":[12.90000057220459,-33.400001525878906,14.600000381469727]}]},
# {"t":"d","c":"14","d":[{"t":"1525639350691","a":[-5.633728504180908,-3.560286521911621,18.18691062927246],"g":[-3.1642820835113525,1.6811012029647827,-0.3995058834552765],"m":[12.90000057220459,-33.400001525878906,14.600000381469727]}]},
# {"t":"d","c":"26","d":[{"t":"1525639350710","a":[-4.245048999786377,-4.333637237548828,19.546859741210938],"g":[-3.1642820835113525,1.6811012029647827,-0.3995058834552765],"m":[12.90000057220459,-33.400001525878906,14.600000381469727]}]},
# {"t":"d","c":"86","d":[{"t":"1525639350730","a":[-3.687183141708374,-4.453351020812988,18.112688064575195],"g":[-3.1642820835113525,1.6811012029647827,-0.3995058834552765],"m":[12.90000057220459,-33.400001525878906,14.600000381469727]}]},
# {"t":"d","c":"20","d":[{"t":"1525639350741","a":[-3.687183141708374,-4.453351020812988,18.112688064575195],"g":[-0.375071257352829,-1.5259413719177246,1.1520918607711792],"m":[23,-0.5,-3]}]},
# {"t":"d","c":"37","d":[{"t":"1525639350750","a":[-3.8595709800720215,-5.061496734619141,16.305011749267578],"g":[-0.375071257352829,-1.5259413719177246,1.1520918607711792],"m":[23,-0.5,-3]}]},
# {"t":"d","c":"48","d":[{"t":"1525639350770","a":[-1.6017698049545288,-2.794118642807007,13.494132995605469],"g":[-0.375071257352829,-1.5259413719177246,1.1520918607711792],"m":[23,-0.5,-3]}]},
# {"t":"d","c":"30","d":[{"t":"1525639350791","a":[5.660065650939941,0.5554717779159546,7.898712635040283],"g":[-0.375071257352829,-1.5259413719177246,1.1520918607711792],"m":[23,-0.5,-3]}]},
# {"t":"d","c":"4","d":[{"t":"1525639350811","a":[-2.511594295501709,-1.0582695007324219,10.278621673583984],"g":[-0.375071257352829,-1.5259413719177246,1.1520918607711792],"m":[23,-0.5,-3]}]},
# {"t":"d","c":"25","d":[{"t":"1525639350831","a":[-1.8771114349365234,-1.52036452293396,-1.2737542390823364],"g":[-0.375071257352829,-1.5259413719177246,1.1520918607711792],"m":[23,-0.5,-3]}]},
# {"t":"d","c":"85","d":[{"t":"1525639350851","a":[3.7614057064056396,-1.4221992492675781,12.19164752960205],"g":[-0.375071257352829,-1.5259413719177246,1.1520918607711792],"m":[23,-0.5,-3]}]},
# {"t":"d","c":"1","d":[{"t":"1525639350872","a":[1.7143007516860962,-3.043123245239258,8.39193344116211],"g":[-0.375071257352829,-1.5259413719177246,1.1520918607711792],"m":[23,-0.5,-3]}]},
# {"t":"d","c":"54","d":[{"t":"1525639350891","a":[-1.6376839876174927,-3.629720687866211,14.063970565795898],"g":[-0.375071257352829,-1.5259413719177246,1.1520918607711792],"m":[23,-0.5,-3]}]},
# {"t":"d","c":"42","d":[{"t":"1525639350912","a":[3.5866236686706543,-0.5865973234176636,11.243514060974121],"g":[-0.375071257352829,-1.5259413719177246,1.1520918607711792],"m":[23,-0.5,-3]}]},
# {"t":"d","c":"84","d":[{"t":"1525639350921","a":[3.5866236686706543,-0.5865973234176636,11.243514060974121],"g":[2.596177339553833,-1.2669345140457153,1.186300277709961],"m":[3.299999952316284,-4.599999904632568,-6.599999904632568]}]},
# {"t":"d","c":"53","d":[{"t":"1525639350930","a":[2.537931203842163,-0.3782954216003418,9.696812629699707],"g":[2.596177339553833,-1.2669345140457153,1.186300277709961],"m":[3.299999952316284,-4.599999904632568,-6.599999904632568]}]},
# {"t":"d","c":"37","d":[{"t":"1525639350950","a":[0.8619389533996582,0.7589851021766663,7.055927753448486],"g":[2.596177339553833,-1.2669345140457153,1.186300277709961],"m":[3.299999952316284,-4.599999904632568,-6.599999904632568]}]},
# {"t":"d","c":"79","d":[{"t":"1525639350970","a":[2.353572130203247,0.2011190801858902,6.284971237182617],"g":[2.596177339553833,-1.2669345140457153,1.186300277709961],"m":[3.299999952316284,-4.599999904632568,-6.599999904632568]}]},
# {"t":"d","c":"43","d":[{"t":"1525639350990","a":[3.7685885429382324,-0.4357580244541168,9.138946533203125],"g":[2.596177339553833,-1.2669345140457153,1.186300277709961],"m":[3.299999952316284,-4.599999904632568,-6.599999904632568]}]},
# {"t":"d","c":"16","d":[{"t":"1525639351010","a":[2.815667152404785,-0.39984390139579773,9.486116409301758],"g":[2.596177339553833,-1.2669345140457153,1.186300277709961],"m":[3.299999952316284,-4.599999904632568,-6.599999904632568]}]},
# {"t":"d","c":"70","d":[{"t":"1525639351030","a":[1.8292258977890015,-0.27534160017967224,9.086273193359375],"g":[2.596177339553833,-1.2669345140457153,1.186300277709961],"m":[3.299999952316284,-4.599999904632568,-6.599999904632568]}]},
# {"t":"d","c":"16","d":[{"t":"1525639351051","a":[0.5339232683181763,-0.8356019258499146,9.28260326385498],"g":[2.596177339553833,-1.2669345140457153,1.186300277709961],"m":[3.299999952316284,-4.599999904632568,-6.599999904632568]}]},
# {"t":"d","c":"14","d":[{"t":"1525639351070","a":[1.3551595211029053,-0.8379961848258972,10.774236679077148],"g":[2.596177339553833,-1.2669345140457153,1.186300277709961],"m":[3.299999952316284,-4.599999904632568,-6.599999904632568]}]},
# {"t":"d","c":"82","d":[{"t":"1525639351090","a":[1.5419130325317383,-0.5482889413833618,10.92268180847168],"g":[2.596177339553833,-1.2669345140457153,1.186300277709961],"m":[3.299999952316284,-4.599999904632568,-6.599999904632568]}]},
# {"t":"d","c":"61","d":[{"t":"1525639351102","a":[1.5419130325317383,-0.5482889413833618,10.92268180847168],"g":[0.836885392665863,-0.2223549485206604,-0.07330382615327835],"m":[2.4000000953674316,-13.699999809265137,-4.300000190734863]}]},
# {"t":"d","c":"37","d":[{"t":"1525639351110","a":[2.008796453475952,-0.13168510794639587,10.395940780639648],"g":[0.836885392665863,-0.2223549485206604,-0.07330382615327835],"m":[2.4000000953674316,-13.699999809265137,-4.300000190734863]}]},
# {"t":"d","c":"77","d":[{"t":"1525639351130","a":[2.0925962924957275,0.2370332032442093,10.079896926879883],"g":[0.836885392665863,-0.2223549485206604,-0.07330382615327835],"m":[2.4000000953674316,-13.699999809265137,-4.300000190734863]}]},
# {"t":"d","c":"41","d":[{"t":"1525639351150","a":[1.9776710271835327,0.7230709791183472,9.759063720703125],"g":[0.836885392665863,-0.2223549485206604,-0.07330382615327835],"m":[2.4000000953674316,-13.699999809265137,-4.300000190734863]}]},
# {"t":"d","c":"53","d":[{"t":"1525639351171","a":[1.551490068435669,1.1731946468353271,10.024828910827637],"g":[0.836885392665863,-0.2223549485206604,-0.07330382615327835],"m":[2.4000000953674316,-13.699999809265137,-4.300000190734863]}]},
# {"t":"d","c":"14","d":[{"t":"1525639351190","a":[0.5435003638267517,1.161223292350769,11.133378028869629],"g":[0.836885392665863,-0.2223549485206604,-0.07330382615327835],"m":[2.4000000953674316,-13.699999809265137,-4.300000190734863]}]},
# {"t":"d","c":"27","d":[{"t":"1525639351210","a":[0.6799740195274353,1.302485466003418,10.326507568359375],"g":[0.836885392665863,-0.2223549485206604,-0.07330382615327835],"m":[2.4000000953674316,-13.699999809265137,-4.300000190734863]}]},
# {"t":"d","c":"21","d":[{"t":"1525639351230","a":[1.0606637001037598,2.1285102367401123,9.320911407470703],"g":[0.836885392665863,-0.2223549485206604,-0.07330382615327835],"m":[2.4000000953674316,-13.699999809265137,-4.300000190734863]}]},
# {"t":"d","c":"48","d":[{"t":"1525639351250","a":[0.7541965842247009,2.853975534439087,8.980924606323242],"g":[0.836885392665863,-0.2223549485206604,-0.07330382615327835],"m":[2.4000000953674316,-13.699999809265137,-4.300000190734863]}]},
# {"t":"d","c":"0","d":[{"t":"1525639351270","a":[0.31125572323799133,3.3855044841766357,7.8867411613464355],"g":[0.836885392665863,-0.2223549485206604,-0.07330382615327835],"m":[2.4000000953674316,-13.699999809265137,-4.300000190734863]}]},
# {"t":"d","c":"18","d":[{"t":"1525639351281","a":[0.31125572323799133,3.3855044841766357,7.8867411613464355],"g":[0.8845328688621521,-0.10262536257505417,-0.9529497623443604],"m":[3.4000000953674316,-18.899999618530273,-1.399999976158142]}]},
# {"t":"d","c":"15","d":[{"t":"1525639351290","a":[0.23463892936706543,3.6464805603027344,7.403097629547119],"g":[0.8845328688621521,-0.10262536257505417,-0.9529497623443604],"m":[3.4000000953674316,-18.899999618530273,-1.399999976158142]}]},
# {"t":"d","c":"32","d":[{"t":"1525639351311","a":[-0.22027328610420227,3.847599506378174,8.308133125305176],"g":[0.8845328688621521,-0.10262536257505417,-0.9529497623443604],"m":[3.4000000953674316,-18.899999618530273,-1.399999976158142]}]},
# {"t":"d","c":"61","d":[{"t":"1525639351330","a":[0.27534160017967224,3.6776061058044434,8.698400497436523],"g":[0.8845328688621521,-0.10262536257505417,-0.9529497623443604],"m":[3.4000000953674316,-18.899999618530273,-1.399999976158142]}]},
# {"t":"d","c":"12","d":[{"t":"1525639351350","a":[0.7781392931938171,3.5459210872650146,8.868393898010254],"g":[0.8845328688621521,-0.10262536257505417,-0.9529497623443604],"m":[3.4000000953674316,-18.899999618530273,-1.399999976158142]}]},
# {"t":"d","c":"86","d":[{"t":"1525639351370","a":[1.2809370756149292,3.2418479919433594,8.940221786499023],"g":[0.8845328688621521,-0.10262536257505417,-0.9529497623443604],"m":[3.4000000953674316,-18.899999618530273,-1.399999976158142]}]},
# {"t":"d","c":"84","d":[{"t":"1525639351390","a":[1.3096683025360107,3.033546209335327,8.763046264648438],"g":[0.8845328688621521,-0.10262536257505417,-0.9529497623443604],"m":[3.4000000953674316,-18.899999618530273,-1.399999976158142]}]},
# {"t":"d","c":"88","d":[{"t":"1525639351410","a":[0.19633053243160248,3.134105682373047,9.75188159942627],"g":[0.8845328688621521,-0.10262536257505417,-0.9529497623443604],"m":[3.4000000953674316,-18.899999618530273,-1.399999976158142]}]},
# {"t":"d","c":"59","d":[{"t":"1525639351430","a":[-0.30407288670539856,3.0598831176757812,10.644946098327637],"g":[0.8845328688621521,-0.10262536257505417,-0.9529497623443604],"m":[3.4000000953674316,-18.899999618530273,-1.399999976158142]}]},
# {"t":"d","c":"45","d":[{"t":"1525639351450","a":[0.6488484740257263,2.7342617511749268,10.477346420288086],"g":[0.8845328688621521,-0.10262536257505417,-0.9529497623443604],"m":[3.4000000953674316,-18.899999618530273,-1.399999976158142]}]},
# {"t":"d","c":"59","d":[{"t":"1525639351462","a":[0.6488484740257263,2.7342617511749268,10.477346420288086],"g":[-0.2321287989616394,-0.34574973583221436,-0.1514945775270462],"m":[3.799999952316284,-21.100000381469727,-1]}]},
# {"t":"d","c":"87","d":[{"t":"1525639351470","a":[1.302485466003418,2.6265194416046143,9.986519813537598],"g":[-0.2321287989616394,-0.34574973583221436,-0.1514945775270462],"m":[3.799999952316284,-21.100000381469727,-1]}]},
# {"t":"d","c":"58","d":[{"t":"1525639351490","a":[1.3743137121200562,2.6217308044433594,9.696812629699707],"g":[-0.2321287989616394,-0.34574973583221436,-0.1514945775270462],"m":[3.799999952316284,-21.100000381469727,-1]}]},
# {"t":"d","c":"93","d":[{"t":"1525639351510","a":[1.07024085521698,2.6504621505737305,9.488511085510254],"g":[-0.2321287989616394,-0.34574973583221436,-0.1514945775270462],"m":[3.799999952316284,-21.100000381469727,-1]}]},
# {"t":"d","c":"62","d":[{"t":"1525639351530","a":[0.7996878027915955,2.6744048595428467,9.150918006896973],"g":[-0.2321287989616394,-0.34574973583221436,-0.1514945775270462],"m":[3.799999952316284,-21.100000381469727,-1]}]},
# {"t":"d","c":"93","d":[{"t":"1525639351550","a":[0.48843204975128174,2.729473352432251,9.399923324584961],"g":[-0.2321287989616394,-0.34574973583221436,-0.1514945775270462],"m":[3.799999952316284,-21.100000381469727,-1]}]},
# {"t":"d","c":"34","d":[{"t":"1525639351571","a":[0.42378664016723633,2.7151076793670654,9.675264358520508],"g":[-0.2321287989616394,-0.34574973583221436,-0.1514945775270462],"m":[3.799999952316284,-21.100000381469727,-1]}]},
# {"t":"d","c":"71","d":[{"t":"1525639351592","a":[0.8020820617675781,2.6815876960754395,9.747093200683594],"g":[-0.2321287989616394,-0.34574973583221436,-0.1514945775270462],"m":[3.799999952316284,-21.100000381469727,-1]}]},
# {"t":"d","c":"51","d":[{"t":"1525639351612","a":[1.0151724815368652,2.695953369140625,9.629773139953613],"g":[-0.2321287989616394,-0.34574973583221436,-0.1514945775270462],"m":[3.799999952316284,-21.100000381469727,-1]}]},
# {"t":"d","c":"25","d":[{"t":"1525639351632","a":[1.161223292350769,2.722290515899658,9.689630508422852],"g":[-0.2321287989616394,-0.34574973583221436,-0.1514945775270462],"m":[3.799999952316284,-21.100000381469727,-1]}]},
# {"t":"d","c":"81","d":[{"t":"1525639351643","a":[1.161223292350769,2.722290515899658,9.689630508422852],"g":[0.039095375686883926,0.02321287989616394,-0.16004669666290283],"m":[4.300000190734863,-20.600000381469727,-1.5]}]},
# {"t":"d","c":"10","d":[{"t":"1525639351652","a":[1.039115309715271,2.604970932006836,9.440625190734863],"g":[0.039095375686883926,0.02321287989616394,-0.16004669666290283],"m":[4.300000190734863,-20.600000381469727,-1.5]}]},
# {"t":"d","c":"12","d":[{"t":"1525639351671","a":[0.8284190893173218,2.6744048595428467,9.869200706481934],"g":[0.039095375686883926,0.02321287989616394,-0.16004669666290283],"m":[4.300000190734863,-20.600000381469727,-1.5]}]},
# {"t":"d","c":"81","d":[{"t":"1525639351691","a":[0.541106104850769,2.6744048595428467,10.11341667175293],"g":[0.039095375686883926,0.02321287989616394,-0.16004669666290283],"m":[4.300000190734863,-20.600000381469727,-1.5]}]},
# {"t":"d","c":"19","d":[{"t":"1525639351711","a":[0.6464542150497437,2.6265194416046143,9.75427532196045],"g":[0.039095375686883926,0.02321287989616394,-0.16004669666290283],"m":[4.300000190734863,-20.600000381469727,-1.5]}]},
# {"t":"d","c":"1","d":[{"t":"1525639351730","a":[0.9529213905334473,2.7749645709991455,9.510059356689453],"g":[0.039095375686883926,0.02321287989616394,-0.16004669666290283],"m":[4.300000190734863,-20.600000381469727,-1.5]}]},
# {"t":"d","c":"46","d":[{"t":"1525639351750","a":[1.0103839635849,2.8707354068756104,9.462174415588379],"g":[0.039095375686883926,0.02321287989616394,-0.16004669666290283],"m":[4.300000190734863,-20.600000381469727,-1.5]}]},
# {"t":"d","c":"82","d":[{"t":"1525639351770","a":[0.8379961848258972,2.889889717102051,9.411893844604492],"g":[0.039095375686883926,0.02321287989616394,-0.16004669666290283],"m":[4.300000190734863,-20.600000381469727,-1.5]}]},
# {"t":"d","c":"59","d":[{"t":"1525639351790","a":[0.9122186899185181,3.038334846496582,9.261054992675781],"g":[0.039095375686883926,0.02321287989616394,-0.16004669666290283],"m":[4.300000190734863,-20.600000381469727,-1.5]}]},
# {"t":"d","c":"1","d":[{"t":"1525639351810","a":[0.5506832003593445,3.3088877201080322,9.28260326385498],"g":[0.039095375686883926,0.02321287989616394,-0.16004669666290283],"m":[4.300000190734863,-20.600000381469727,-1.5]}]},
# {"t":"d","c":"55","d":[{"t":"1525639351822","a":[0.5506832003593445,3.3088877201080322,9.28260326385498],"g":[0.13316862285137177,-0.05497787147760391,-0.22357667982578278],"m":[5.200000286102295,-20.600000381469727,-1.8000000715255737]}]},
# {"t":"d","c":"66","d":[{"t":"1525639351830","a":[0.45012366771698,3.3040993213653564,9.086273193359375],"g":[0.13316862285137177,-0.05497787147760391,-0.22357667982578278],"m":[5.200000286102295,-20.600000381469727,-1.8000000715255737]}]},
# {"t":"d","c":"16","d":[{"t":"1525639351850","a":[0.3256213665008545,3.409447431564331,9.411893844604492],"g":[0.13316862285137177,-0.05497787147760391,-0.22357667982578278],"m":[5.200000286102295,-20.600000381469727,-1.8000000715255737]}]},
# {"t":"d","c":"53","d":[{"t":"1525639351870","a":[0.5004034638404846,3.316070556640625,9.395133972167969],"g":[0.13316862285137177,-0.05497787147760391,-0.22357667982578278],"m":[5.200000286102295,-20.600000381469727,-1.8000000715255737]}]},
# {"t":"d","c":"51","d":[{"t":"1525639351891","a":[0.5267404317855835,3.3902931213378906,9.577098846435547],"g":[0.13316862285137177,-0.05497787147760391,-0.22357667982578278],"m":[5.200000286102295,-20.600000381469727,-1.8000000715255737]}]},
# {"t":"d","c":"74","d":[{"t":"1525639351911","a":[0.5554717779159546,3.3352248668670654,9.49329948425293],"g":[0.13316862285137177,-0.05497787147760391,-0.22357667982578278],"m":[5.200000286102295,-20.600000381469727,-1.8000000715255737]}]},
# {"t":"d","c":"43","d":[{"t":"1525639351932","a":[0.7709565162658691,3.3040993213653564,9.313729286193848],"g":[0.13316862285137177,-0.05497787147760391,-0.22357667982578278],"m":[5.200000286102295,-20.600000381469727,-1.8000000715255737]}]},
# {"t":"d","c":"29","d":[{"t":"1525639351952","a":[0.8834874033927917,3.2657909393310547,9.579493522644043],"g":[0.13316862285137177,-0.05497787147760391,-0.22357667982578278],"m":[5.200000286102295,-20.600000381469727,-1.8000000715255737]}]},
# {"t":"d","c":"84","d":[{"t":"1525639351996","a":[1.0941835641860962,2.971295118331909,9.273026466369629],"g":[0.13316862285137177,-0.05497787147760391,-0.22357667982578278],"m":[5.200000286102295,-20.600000381469727,-1.8000000715255737]}]},
# {"t":"d","c":"98","d":[{"t":"1525639352020","a":[0.6895511150360107,2.8994667530059814,9.505270957946777],"g":[-0.05864306539297104,-0.14294247329235077,-0.1771509200334549],"m":[5,-20.80000114440918,-1.7000000476837158]}]},
# {"t":"d","c":"95","d":[{"t":"1525639352032","a":[1.0989720821380615,2.7725701332092285,9.124581336975098],"g":[-0.05864306539297104,-0.14294247329235077,-0.1771509200334549],"m":[5,-20.80000114440918,-1.7000000476837158]}]},
# {"t":"d","c":"6","d":[{"t":"1525639352052","a":[1.4198049306869507,2.7366561889648438,9.107821464538574],"g":[-0.05864306539297104,-0.14294247329235077,-0.1771509200334549],"m":[5,-20.80000114440918,-1.7000000476837158]}]},
# {"t":"d","c":"44","d":[{"t":"1525639352071","a":[1.398256540298462,2.5618741512298584,9.138946533203125],"g":[-0.05864306539297104,-0.14294247329235077,-0.1771509200334549],"m":[5,-20.80000114440918,-1.7000000476837158]}]},
# {"t":"d","c":"64","d":[{"t":"1525639352091","a":[0.6177229285240173,2.758204460144043,9.866806030273438],"g":[-0.05864306539297104,-0.14294247329235077,-0.1771509200334549],"m":[5,-20.80000114440918,-1.7000000476837158]}]},
# {"t":"d","c":"43","d":[{"t":"1525639352111","a":[0.8260248303413391,2.695953369140625,9.87159538269043],"g":[-0.05864306539297104,-0.14294247329235077,-0.1771509200334549],"m":[5,-20.80000114440918,-1.7000000476837158]}]},
# {"t":"d","c":"59","d":[{"t":"1525639352131","a":[1.6352896690368652,2.413429021835327,9.577098846435547],"g":[-0.05864306539297104,-0.14294247329235077,-0.1771509200334549],"m":[5,-20.80000114440918,-1.7000000476837158]}]},
# {"t":"d","c":"58","d":[{"t":"1525639352151","a":[1.9872480630874634,2.384697675704956,9.574705123901367],"g":[-0.05864306539297104,-0.14294247329235077,-0.1771509200334549],"m":[5,-20.80000114440918,-1.7000000476837158]}]},
# {"t":"d","c":"90","d":[{"t":"1525639352171","a":[1.8028889894485474,2.2841382026672363,9.677659034729004],"g":[-0.05864306539297104,-0.14294247329235077,-0.1771509200334549],"m":[5,-20.80000114440918,-1.7000000476837158]}]},
# {"t":"d","c":"32","d":[{"t":"1525639352183","a":[1.8028889894485474,2.2841382026672363,9.677659034729004],"g":[-0.4703662395477295,-0.35552358627319336,-0.6524040699005127],"m":[4.300000190734863,-20.200000762939453,-1.8000000715255737]}]},
# {"t":"d","c":"70","d":[{"t":"1525639352191","a":[1.381496548652649,2.3966691493988037,9.706390380859375],"g":[-0.4703662395477295,-0.35552358627319336,-0.6524040699005127],"m":[4.300000190734863,-20.200000762939453,-1.8000000715255737]}]},
# {"t":"d","c":"64","d":[{"t":"1525639352212","a":[1.1684061288833618,2.3368122577667236,9.452596664428711],"g":[-0.4703662395477295,-0.35552358627319336,-0.6524040699005127],"m":[4.300000190734863,-20.200000762939453,-1.8000000715255737]}]},
# {"t":"d","c":"63","d":[{"t":"1525639352231","a":[0.991229772567749,2.358360767364502,9.596253395080566],"g":[-0.4703662395477295,-0.35552358627319336,-0.6524040699005127],"m":[4.300000190734863,-20.200000762939453,-1.8000000715255737]}]},
# {"t":"d","c":"46","d":[{"t":"1525639352252","a":[1.2378400564193726,2.202732801437378,9.761458396911621],"g":[-0.4703662395477295,-0.35552358627319336,-0.6524040699005127],"m":[4.300000190734863,-20.200000762939453,-1.8000000715255737]}]},
# {"t":"d","c":"70","d":[{"t":"1525639352271","a":[1.6209239959716797,2.0542876720428467,9.744698524475098],"g":[-0.4703662395477295,-0.35552358627319336,-0.6524040699005127],"m":[4.300000190734863,-20.200000762939453,-1.8000000715255737]}]},
# {"t":"d","c":"1","d":[{"t":"1525639352291","a":[1.6233183145523071,2.0590763092041016,9.75188159942627],"g":[-0.4703662395477295,-0.35552358627319336,-0.6524040699005127],"m":[4.300000190734863,-20.200000762939453,-1.8000000715255737]}]},
# {"t":"d","c":"14","d":[{"t":"1525639352312","a":[1.2809370756149292,2.176395893096924,9.756669998168945],"g":[-0.4703662395477295,-0.35552358627319336,-0.6524040699005127],"m":[4.300000190734863,-20.200000762939453,-1.8000000715255737]}]},
# {"t":"d","c":"26","d":[{"t":"1525639352332","a":[1.0654523372650146,2.2147042751312256,9.557945251464844],"g":[-0.4703662395477295,-0.35552358627319336,-0.6524040699005127],"m":[4.300000190734863,-20.200000762939453,-1.8000000715255737]}]},
# {"t":"d","c":"64","d":[{"t":"1525639352352","a":[1.0941835641860962,2.293715238571167,9.75427532196045],"g":[-0.4703662395477295,-0.35552358627319336,-0.6524040699005127],"m":[4.300000190734863,-20.200000762939453,-1.8000000715255737]}]},
# {"t":"d","c":"6","d":[{"t":"1525639352364","a":[1.0941835641860962,2.293715238571167,9.75427532196045],"g":[-0.2321287989616394,-0.23457226157188416,-0.41661009192466736],"m":[3.700000047683716,-17.5,-3.4000000953674316]}]},
# {"t":"d","c":"72","d":[{"t":"1525639352373","a":[1.252205729484558,2.102173328399658,9.785401344299316],"g":[-0.2321287989616394,-0.23457226157188416,-0.41661009192466736],"m":[3.700000047683716,-17.5,-3.4000000953674316]}]},
# {"t":"d","c":"0","d":[{"t":"1525639352393","a":[1.5610672235488892,2.0351336002349854,9.756669998168945],"g":[-0.2321287989616394,-0.23457226157188416,-0.41661009192466736],"m":[3.700000047683716,-17.5,-3.4000000953674316]}]},
# {"t":"d","c":"60","d":[{"t":"1525639352413","a":[1.6831752061843872,2.1165390014648438,9.706390380859375],"g":[-0.2321287989616394,-0.23457226157188416,-0.41661009192466736],"m":[3.700000047683716,-17.5,-3.4000000953674316]}]},
# {"t":"d","c":"68","d":[{"t":"1525639352432","a":[1.5562785863876343,2.111750364303589,9.675264358520508],"g":[-0.2321287989616394,-0.23457226157188416,-0.41661009192466736],"m":[3.700000047683716,-17.5,-3.4000000953674316]}]},
# {"t":"d","c":"24","d":[{"t":"1525639352452","a":[1.472478985786438,2.0830190181732178,9.634561538696289],"g":[-0.2321287989616394,-0.23457226157188416,-0.41661009192466736],"m":[3.700000047683716,-17.5,-3.4000000953674316]}]},
# {"t":"d","c":"45","d":[{"t":"1525639352472","a":[1.3431881666183472,2.0255565643310547,9.759063720703125],"g":[-0.2321287989616394,-0.23457226157188416,-0.41661009192466736],"m":[3.700000047683716,-17.5,-3.4000000953674316]}]}]
#
#To get a feeling of the algorithm, needs some small changes described in the code above...
#rot = Rotations(None, None)
#rot.update(jsonTestDataArray)





# A small stack of test data to run Rotations if no device is connected:
#
# jsonWithMultipleSensorMessurements = {"t":"d","c":"86","d":[{"t":"1525639327734","a":[-1.6664152145385742,3.754222869873047,8.607418060302734],"g":[-0.0012217304902151227,-0.012217304669320583,-0.01710422709584236],"m":[0.9000000357627869,-26.30000114440918,2.6000001430511475]}, {"t":"1525639327886","a":[-1.8986599445343018,4.2330780029296875,7.8580098152160645],"g":[0.6401867866516113,-0.013439035043120384,-0.019547687843441963],"m":[2,-26.80000114440918,4.900000095367432]},{"t":"1525639328065","a":[-1.9082369804382324,4.024775981903076,8.071100234985352],"g":[0.11239920556545258,-0.010995574295520782,-0.006108652334660292],"m":[3.9000000953674316,-27,7.200000286102295]},{"t":"1525639328246","a":[-2.14048171043396,3.993650436401367,8.913885116577148],"g":[-0.012217304669320583,0.04153883829712868,0.004886921960860491],"m":[3.9000000953674316,-26.700000762939453,7.099999904632568]}]}
# jsonOnlyOne2 = {"t":"d","c":"69","d":[{"t":"1525639328965","a":[-2.104567527770996,4.003227710723877,8.980924606323242],"g":[0.01710422709584236,-0.013439035043120384,-0.021991148591041565],"m":[3.799999952316284,-26.700000762939453,6.800000190734863]}]}
# jsonOnlyOne3 = {"t":"d","c":"85","d":[{"t":"1525639329146","a":[-2.1261160373687744,4.058295726776123,8.99289608001709],"g":[-0.02321287989616394,0.004886921960860491,-0.0354301854968071],"m":[3.6000001430511475,-26.30000114440918,6.700000286102295]}]}
# jsonOnlyOne4 = {"t":"d","c":"36","d":[{"t":"1525639329326","a":[-2.0255565643310547,3.9577362537384033,8.877970695495605],"g":[-0.019547687843441963,-0.019547687843441963,-0.015882495790719986],"m":[3.700000047683716,-26,6.700000286102295]}]}
# jsonOnlyOne5 = {"t":"d","c":"4","d":[{"t":"1525639329506","a":[-1.939362645149231,3.9290049076080322,8.846845626831055],"g":[-0.021991148591041565,0.02687807008624077,-0.025656340643763542],"m":[3.799999952316284,-25.899999618530273,6.800000190734863]}]}
# jsonOnlyOne6 = {"t":"d","c":"51","d":[{"t":"1525639329687","a":[-2.073441982269287,3.9864675998687744,8.820508003234863],"g":[0.009773843921720982,-0.004886921960860491,0.0024434609804302454],"m":[3.6000001430511475,-25.5,6.400000095367432]}]}
# jsonOnlyOne7 = {"t":"d","c":"99","d":[{"t":"1525639329866","a":[-1.9728823900222778,3.9792847633361816,8.985713005065918],"g":[0.03787364438176155,0.015882495790719986,0.01466076634824276],"m":[4,-25.30000114440918,6.900000095367432]}]}
# jsonOnlyOne8 = {"t":"d","c":"79","d":[{"t":"1525639330046","a":[-2.4948344230651855,4.314483165740967,8.344047546386719],"g":[-0.013439035043120384,0.2028072625398636,-0.1771509200334549],"m":[4.700000286102295,-25.600000381469727,7.900000095367432]}]}
# jsonOnlyOne9 = {"t":"d","c":"60","d":[{"t":"1525639330226","a":[-2.6791934967041016,4.273780345916748,8.523618698120117],"g":[-0.1417207419872284,-0.7257078886032104,-0.43127086758613586],"m":[6.300000190734863,-26,9.600000381469727]}]}
# jsonOnlyOne10 = {"t":"d","c":"74","d":[{"t":"1525639330406","a":[-1.9321798086166382,3.3040993213653564,9.577098846435547],"g":[-0.051312681287527084,-0.12461651116609573,0.006108652334660292],"m":[6.900000095367432,-25.100000381469727,9.100000381469727]}]}
# jsonOnlyOne11 = {"t":"d","c":"75","d":[{"t":"1525639330586","a":[-1.831620216369629,3.2083282470703125,9.225140571594238],"g":[-0.10262536257505417,-0.0012217304902151227,0.1319468915462494],"m":[6,-23.80000114440918,7.099999904632568]}]}
# jsonOnlyOne12 = {"t":"d","c":"1","d":[{"t":"1525639330766","a":[-1.7262721061706543,3.5243725776672363,10.204399108886719],"g":[0.6572909951210022,0.3933972120285034,-0.1319468915462494],"m":[5.599999904632568,-23.399999618530273,6.099999904632568]}]}
# jsonOnlyOne13 = {"t":"d","c":"99","d":[{"t":"1525639330946","a":[-4.0128045082092285,5.073468208312988,6.0623040199279785],"g":[1.1740829944610596,1.017701506614685,-0.10384709388017654],"m":[12.300000190734863,-28.100000381469727,14.90000057220459]}]}
# jsonOnlyOne14 = {"t":"d","c":"16","d":[{"t":"1525639331127","a":[-3.131711483001709,5.032765865325928,6.838048934936523],"g":[-1.1582005023956299,-1.0702359676361084,0.1966986060142517],"m":[13.699999809265137,-29.5,17]}]}
# jsonOnlyOne15 = {"t":"d","c":"2","d":[{"t":"1525639331306","a":[-1.7190892696380615,3.598595142364502,9.842864036560059],"g":[-0.46792277693748474,-0.2932153046131134,0.4092797040939331],"m":[8.90000057220459,-26.899999618530273,9.800000190734863]}]}
# jsonOnlyOne16 = {"t":"d","c":"42","d":[{"t":"1525639331486","a":[-1.1085492372512817,3.751828670501709,9.895537376403809],"g":[1.1313223838806152,1.1398745775222778,-0.0403171069920063],"m":[7.400000095367432,-26,7.700000286102295]}]}
# jsonOnlyOne17 = {"t":"d","c":"12","d":[{"t":"1525639331669","a":[-4.051113128662109,5.609786033630371,7.520417213439941],"g":[-0.9297369122505188,-0.7061602473258972,-0.10751228034496307],"m":[14.100000381469727,-29.899999618530273,15]}]}
# jsonOnlyOne18 = {"t":"d","c":"51","d":[{"t":"1525639331847","a":[-2.6480679512023926,3.849993944168091,9.024022102355957],"g":[-0.18570303916931152,-0.7269296646118164,0.28099802136421204],"m":[11.199999809265137,-27.200000762939453,10.800000190734863]}]}
# jsonOnlyOne19 = {"t":"d","c":"88","d":[{"t":"1525639332027","a":[-0.8547561168670654,3.972101926803589,8.796566009521484],"g":[0.3323107063770294,0.16859880089759827,0.44837507605552673],"m":[8.40000057220459,-26.399999618530273,7.900000095367432]}]}
# jsonOnlyOne20 = {"t":"d","c":"7","d":[{"t":"1525639332209","a":[-2.516382932662964,4.304905891418457,8.775016784667969],"g":[-0.16982053220272064,1.340238332748413,-0.11850785464048386],"m":[7.5,-27.30000114440918,7.400000095367432]}]}
# jsonOnlyOne21 = {"t":"d","c":"11","d":[{"t":"1525639332386","a":[-3.4908525943756104,4.235472202301025,10.194822311401367],"g":[1.2705997228622437,-0.02932153269648552,-0.08674286305904388],"m":[10,-27.600000381469727,7.800000190734863]}]}
# jsonOnlyOne22 = {"t":"d","c":"15","d":[{"t":"1525639332566","a":[-3.44057297706604,6.177228927612305,2.2841382026672363],"g":[-0.9773843884468079,-1.2791517972946167,0.028099801391363144],"m":[12.40000057220459,-30.5,16.80000114440918]}]}
#
# #To get a feeling about the algorithm...
# rot = Rotations(None, None)
# rot.update(jsonWithMultipleSensorMessurements)
# rot.update(jsonOnlyOne2)
# rot.update(jsonOnlyOne3)
# rot.update(jsonOnlyOne4)
# rot.update(jsonOnlyOne5)
# rot.update(jsonOnlyOne6)
# rot.update(jsonOnlyOne7)
# rot.update(jsonOnlyOne8)
# rot.update(jsonOnlyOne9)
# rot.update(jsonOnlyOne10)
# rot.update(jsonOnlyOne11)
# rot.update(jsonOnlyOne12)
# rot.update(jsonOnlyOne13)
# rot.update(jsonOnlyOne14)
# rot.update(jsonOnlyOne15)
# rot.update(jsonOnlyOne16)
# rot.update(jsonOnlyOne17)
# rot.update(jsonOnlyOne18)
# rot.update(jsonOnlyOne19)
# rot.update(jsonOnlyOne20)
# rot.update(jsonOnlyOne21)
# rot.update(jsonOnlyOne22)