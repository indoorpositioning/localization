import collections

import numpy as np

# 0.4 seconds
PACE = 400  #milliseconds
PACE_BUFFER_MAX = 300

# Average step jerk
# 2.5 m/s**3
JERK = 2.5
JERK_BUFFER_MAX = 300

class StepDetection:
    accelerometer = [0,0,0]

    # Filter requirements.
    order = 3
    sampleRate = 50.0  # sample rate, Hz
    cutoff = 3.667  # desired cutoff frequency of the filter, Hz
    scalarAccelArray = []
    timestamps = []
    numberOfSteps = 0
    stepDetection = None
    lastPeak = None
    potentialPeak = None
    lastLow = None
    potentialLow = None
    lastValue = None
    slope = None
    lastSlope = None
    paceBuffer = None
    jerkBuffer = None

    def __init__(self, pace_buffer_max, jerk_buffer_max):
        self.jerkBuffer = collections.deque(maxlen=jerk_buffer_max)
        self.jerkBuffer.append(JERK)

        self.paceBuffer = collections.deque(maxlen=pace_buffer_max)
        self.paceBuffer.append(PACE)

        self.subscribers = set()


        # Graphing Purpose Array
        # 0 - timestamp
        # 1 - Jerk Average
        # 2 - Pace Duration
        self.avgs = []

    def register(self, who):
        self.subscribers.add(who)

    def unregister(self, who):
        self.subscribers.discard(who)

    def dispatch(self, numberOfSteps, timeStampStep):
        for subscriber in self.subscribers:
            subscriber.publish(numberOfSteps, timeStampStep)

    def detectStep(self, acceler, timestamp):
        scalar = self.calculateScalarProd(acceler)
        # print("ScalerProd: "+str(scalar))
        self.scalarAccelArray.append(scalar)

        #filteredAccel = self.lowPassFilter(np.array(scalar), self.cutoff, self.sampleRate, self.order)
        #print("filtered ScallarProd: " + str(filteredAccel))

        return self.extractPeakLowAvg(timestamp, scalar)

        # self.timestamps.append(int(timestamp) - 1525639327734)
        #
        # if (int(timestamp) == 1525639332566):
        #     plt.subplot(2, 1, 1)
        #     plt.plot(np.array(self.timestamps), np.array(self.scalarAccelArray), 'r-', label='scalar')
        #     plt.title("Scalar and Timestamp")
        #     plt.xlabel('Frequency [Hz]')
        #     plt.grid()
        #     plt.show()

    def calculateScalarProd(self, accelerometer):
        scalarProdAccel = np.sqrt(accelerometer[0] * accelerometer[0] +
                             accelerometer[1] * accelerometer[1] +
                             accelerometer[2] * accelerometer[2])
        return scalarProdAccel

    def getScalarProdAndTimestamp(self, accelerometer, timestamp):
        scalarProdAccel = self.calculateScalarProd(accelerometer)
        return scalarProdAccel, timestamp

    def extractPeakLowAvg(self, timestamp, accelScalar):

        peaks = []
        lows = []

        isStep = False

        currentValue = accelScalar

        if self.lastValue:
            if currentValue > self.lastValue:
                self.slope = 'rising'
            elif currentValue < self.lastValue:
                self.slope = 'falling'

            if self.lastSlope and self.lastSlope is not self.slope:

                if self.slope is 'falling':
                    # Maximum
                    self.potentialPeak = {
                        "ts": timestamp,
                        "val": currentValue,
                        "min_max": "max"
                    }
                    print(self.potentialPeak)

                    if self.lastLow:
                        # print('trough?')
                        if self.decide(self.potentialPeak, self.lastLow):
                            # print('trough added')
                            lows.append(self.lastLow)
                            self.numberOfSteps += 1
                            isStep = True
                            self.dispatch(self.numberOfSteps, self.potentialPeak['ts'])
                            # self.dispatch(self.numberOfSteps, self.potentialPeak['ts'])
                            # last_peak = potential_peak
                    # 	elif last_peak is None or  potential_peak['val'] > last_peak['val']:
                    # 		last_peak = potential_peak
                    # else:
                    self.lastPeak = self.potentialPeak

                if self.slope is 'rising':
                    # Minimum
                    self.potentialLow = {
                        "ts": timestamp,
                        "val": currentValue,
                        "min_max": "min"
                    }
                    print(self.potentialLow)

                    if self.lastPeak:
                        # print('peak?')
                        if self.decide(self.lastPeak, self.potentialLow):
                            # print('peak added')
                            peaks.append(self.lastPeak)
                            # last_trough = potential_trough
                    # 	elif last_trough is None or potential_trough['val'] < last_trough['val']:
                    # 		last_trough = potential_trough
                    # else:
                    self.lastLow = self.potentialLow

            self.lastSlope = self.slope
        self.lastValue = currentValue
        # print(i)

        print("A SMALL STEP FOR ME... number of steps = " + str(self.numberOfSteps))

        return isStep
        #return np.array(peaks), np.array(lows), np.array(stepDecider.avgs)

    def decide(self, peak, low):
        # Given a peak and a low, determine if the jerk spike
        # and pace spacing is a step

        jerkAvg = sum(self.jerkBuffer) / len(self.jerkBuffer)
        paceAvg = sum(self.paceBuffer) / len(self.paceBuffer)

        jerk = abs(peak['val'] - low['val'])
        pace = abs(int(peak['ts']) - int(low['ts']))

        if self.lastPeak and self.lastLow:
            peakPace = int(peak['ts']) - int(self.lastPeak['ts'])
            troughPace = int(low['ts']) - int(self.lastLow['ts'])
            # print(peak_pace, trough_pace)

            # print('peak', self.last_peak['ts'], peak['ts'], peakPace)
            # print('trough', self.last_trough['ts'], trough['ts'], troughPace)

            pace = max(peakPace, troughPace)
        else:
            pace = paceAvg

        self.lastPeak = peak
        self.lastLow = low

        self.avgs.append([
            max(peak['ts'], low['ts']),
            jerkAvg,
            float(paceAvg) / 10 ** 8,
        ])

        #print('jerk', jerk, jerkAvg, jerk > jerkAvg * .5)
        if jerk >= jerkAvg * .5 or jerk >= JERK * 2:
            #print('pace', float(pace)/10**8, float(paceAvg)/10**8, pace >= paceAvg * .5, pace <= paceAvg * 2, pace >= paceAvg * .5 and pace <= paceAvg * 2)
            if paceAvg * .5 <= pace <= paceAvg * 2:
                self.jerkBuffer.append(jerk)
                self.paceBuffer.append(pace)

                return True
            else:
                return False
        else:
            return False




