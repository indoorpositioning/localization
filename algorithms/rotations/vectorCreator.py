import numpy as np



class VectorCreator:

    """
    Returs the Vector (x,y) where x is facing East and y is facing North.
    """

    x = 0
    y = 0

    def __init__(self):
        pass



    def vectorsRelativtoTheInstitutBuilding(self, heading, averageStepLenght):
        """
        Returs the Vector (x,y) where x and y are relative to the institute for computer science building,
        as shown in the png file of this class.
        """
        w = heading
        r = averageStepLenght

        # Change the heading form beeing -180° to 180° to 0° to 360° degrees
        if w < 0:
            w = 360 + w

        # Rotait form x pointing East to x pointing to 53° (relativ to north)
        if w > 37:
            w = w - 37
        else:
            w = 360 - (37 - w)

        w = np.deg2rad(w)

        if 0 < w < np.pi/2:
            self.x = r * np.cos(w)
            self.y = -(r * np.sin(w))
            return self.x, self.y

        if np.pi/2 < w < np.pi:
            self.x = -(r * np.cos(np.pi - w))
            self.y = -(r * np.sin(np.pi - w))
            return self.x, self.y

        if np.pi < w < 3*np.pi/2:
            self.x = -(r * np.cos(w - np.pi))
            self.y = r * np.sin(w - np.pi)
            return self.x, self.y

        if 3*np.pi/2 < w < 2*np.pi:
            self.x = r * np.cos(2*np.pi - w)
            self.y = r * np.sin(2*np.pi - w)
            return self.x, self.y

        if w == 0:
            self.x = r
            self.y = 0.0
            return self.x, self.y

        if w == (np.pi/2):
            self.x = 0.0
            self.y = -r
            return self.x, self.y

        if w == np.pi:
            self.x = -r
            self.y = 0.0
            return self.x, self.y

        if w == -(3*np.pi/2):
            self.x = 0.0
            self.y = r
            return self.x, self.y

        if w == 2*np.pi:
            self.x = r
            self.y = 0.0
            return self.x, self.y