from OpenGL.GL import *
from OpenGL.GLU import *
import pygame

from pygame.locals import *


yaw_mode = True
roll = pitch = yaw = 0.0


class Visualization:
    global roll, pitch, yaw
    global yaw_mode

    def __init__(self):
        video_flags = OPENGL | DOUBLEBUF
        pygame.init()
        screen = pygame.display.set_mode((640, 480), video_flags)
        pygame.display.set_caption("Press Esc to quit, z toggles yaw mode")
        self.resize(640, 480)
        self.init()

    def resize(self, width, height):
        if height == 0:
            height = 1
        glViewport(0, 0, width, height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45, 1.0 * width / height, 0.1, 100.0)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()


    def init(self):
        glShadeModel(GL_SMOOTH)
        glClearColor(0.0, 0.0, 0.0, 0.0)
        glClearDepth(1.0)
        glEnable(GL_DEPTH_TEST)
        glDepthFunc(GL_LEQUAL)
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)

    # Funtion to display in GUI
    def drawtext(self, position, textstring):
        font = pygame.font.SysFont("Courier", 18, True)
        textsurface = font.render(textstring, True, (255, 255, 255, 255), (0, 0, 0, 255))
        textData = pygame.image.tostring(textsurface, "RGBA", True)
        glRasterPos3d(*position)
        glDrawPixels(textsurface.get_width(), textsurface.get_height(), GL_RGBA, GL_UNSIGNED_BYTE, textData)


    # Function to display the block
    def draw(self):
        global roll, pitch, yaw
        global rquad
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        glLoadIdentity()
        glTranslatef(0, 0.0, -7.0)

        osd_text = "pitch: " + str("{0:.2f}".format(pitch)) + ", roll: " + str("{0:.2f}".format(roll))

        if yaw_mode:
            osd_line = osd_text + ", yaw: " + str("{0:.2f}".format(yaw))
        else:
            osd_line = osd_text

        self.drawtext((-2, -2, 2), osd_line)

        # the way I'm holding the IMU board, X and Y axis are switched,with respect to the OpenGL coordinate system

        if yaw_mode:
            yaw = yaw + 180  # Comment out if reading Euler Angle/Quaternion angles
            glRotatef(yaw, 0.0, 1.0, 0.0)  # Yaw, rotate around y-axis

        glRotatef(pitch, 1.0, 0.0, 0.0)  # Pitch, rotate around x-axis
        glRotatef(-1 * roll, 0.0, 0.0, 1.0)  # Roll, rotate around z-axis

        glBegin(GL_QUADS)
        glColor3f(1.0, 0.5, 0.0)
        glVertex3f(1.0, 0.2, -1.0)
        glVertex3f(-1.0, 0.2, -1.0)
        glVertex3f(-1.0, 0.2, 1.0)
        glVertex3f(1.0, 0.2, 1.0)

        glColor3f(1.0, 0.5, 0.0)
        glVertex3f(1.0, -0.2, 1.0)
        glVertex3f(-1.0, -0.2, 1.0)
        glVertex3f(-1.0, -0.2, -1.0)
        glVertex3f(1.0, -0.2, -1.0)

        glColor3f(1.0, 0.0, 0.0)
        glVertex3f(1.0, 0.2, 1.0)
        glVertex3f(-1.0, 0.2, 1.0)
        glVertex3f(-1.0, -0.2, 1.0)
        glVertex3f(1.0, -0.2, 1.0)

        glColor3f(1.0, 1.0, 0.0)
        glVertex3f(1.0, -0.2, -1.0)
        glVertex3f(-1.0, -0.2, -1.0)
        glVertex3f(-1.0, 0.2, -1.0)
        glVertex3f(1.0, 0.2, -1.0)

        glColor3f(0.0, 0.0, 1.0)
        glVertex3f(-1.0, 0.2, 1.0)
        glVertex3f(-1.0, 0.2, -1.0)
        glVertex3f(-1.0, -0.2, -1.0)
        glVertex3f(-1.0, -0.2, 1.0)

        glColor3f(1.0, 0.0, 1.0)
        glVertex3f(1.0, 0.2, -1.0)
        glVertex3f(1.0, 0.2, 1.0)
        glVertex3f(1.0, -0.2, 1.0)
        glVertex3f(1.0, -0.2, -1.0)
        glEnd()


    def setTheEulerAngles(self, r, p, y):
        global roll, pitch, yaw
        roll = r
        pitch = p
        yaw = y

        # while 1:
        #     event = pygame.event.poll()
        #     if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
        #         break
        #     if event.type == KEYDOWN and event.key == K_z:
        #         yaw_mode = not yaw_mode
        #
        pygame.display.flip()
        self.draw()