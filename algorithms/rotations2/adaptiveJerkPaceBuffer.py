from __future__ import print_function

import collections

# Average seconds per step
# 0.4 seconds
PACE = 400000000
PACE_BUFFER_MAX = 20

# Average step jerk
# 2.5 m/s**3
JERK = 2.5
JERK_BUFFER_MAX = 20


class StepDecider:
    def __init__(self, pace_buffer_max, jerk_buffer_max):
        self.jerk_buffer = collections.deque(maxlen=jerk_buffer_max)
        self.jerk_buffer.append(JERK)

        self.pace_buffer = collections.deque(maxlen=pace_buffer_max)
        self.pace_buffer.append(PACE)

        self.last_peak = None
        self.last_trough = None

        # Graphing Purpose Array
        # 0 - timestamp
        # 1 - Jerk Average
        # 2 - Pace Duration
        self.avgs = []

    def decide(self, peak, trough):
        # Given a peak and a trough, determine if the jerk spike
        # and pace spacing is a step

        jerk_avg = sum(self.jerk_buffer) / len(self.jerk_buffer)
        pace_avg = sum(self.pace_buffer) / len(self.pace_buffer)

        jerk = peak['val'] - trough['val']
        pace = abs(peak['ts'] - trough['ts'])

        if self.last_peak and self.last_trough:
            peak_pace = peak['ts'] - self.last_peak['ts']
            trough_pace = trough['ts'] - self.last_trough['ts']
            print(peak_pace, trough_pace)

            print('peak', self.last_peak['ts'], peak['ts'], peak_pace)
            print('trough', self.last_trough['ts'], trough['ts'], trough_pace)

            pace = max(peak_pace, trough_pace)
        else:
            pace = pace_avg

        self.last_peak = peak
        self.last_trough = trough

        self.avgs.append([
            max(peak['ts'], trough['ts']),
            jerk_avg,
            float(pace_avg) / 10 ** 8,
        ])

        # print('jerk', jerk, jerk_avg, jerk > jerk_avg * .5)
        if jerk >= jerk_avg * .5 or jerk >= JERK * 2:
            # print('pace', float(pace)/10**8, float(pace_avg)/10**8, pace >= pace_avg * .5, pace <= pace_avg * 2, pace >= pace_avg * .5 and pace <= pace_avg * 2)
            if pace_avg * .5 <= pace <= pace_avg * 2:
                self.jerk_buffer.append(jerk)
                self.pace_buffer.append(pace)

                return True
            else:
                return False
        else:
            return False

    def get_avgs(self):
        return self.avgs


class AdaptiveJerkPaceBuffer(object):

    def __init__(self) -> None:
        self.last_peak = None
        self.last_trough = None
        self.last_z = None
        self.last_slope = None
        self.peaks = []
        self.troughs = []
        self.i = 0
        self.sd = StepDecider(PACE_BUFFER_MAX, JERK_BUFFER_MAX)

    def adaptive_jerk_pace_buffer(self, z_value: float, timestamp):

        result = False
        if self.last_z:
            slope = False
            if z_value > self.last_z:
                slope = 'rising'
            elif z_value < self.last_z:
                slope = 'falling'

            if self.last_slope and self.last_slope is not slope:

                if slope is 'falling':
                    # Maximum
                    potential_peak = {
                        "ts": int(float(timestamp)),
                        "val": float(z_value),
                        "index": self.i,
                        "min_max": "max"
                    }

                    if self.last_trough:
                        # print('trough?')
                        if self.sd.decide(potential_peak, self.last_trough):
                            # print('trough added')
                            self.troughs.append(self.last_trough)
                            result = True
                            # last_peak = potential_peak
                    # 	elif last_peak is None or  potential_peak['val'] > last_peak['val']:
                    # 		last_peak = potential_peak
                    # else:
                    self.last_peak = potential_peak

                if slope is 'rising':
                    # Minimum
                    potential_trough = {
                        "ts": int(float(timestamp)),
                        "val": float(z_value),
                        "index": self.i,
                        "min_max": "min"
                    }

                    if self.last_peak:
                        # print('peak?')
                        if self.sd.decide(self.last_peak, potential_trough):
                            # print('peak added')
                            self.peaks.append(self.last_peak)
                            result = True
                            # last_trough = potential_trough
                    # 	elif last_trough is None or potential_trough['val'] < last_trough['val']:
                    # 		last_trough = potential_trough
                    # else:
                    self.last_trough = potential_trough

            self.last_slope = slope
        self.last_z = z_value
        self.i += 1
        # print(i)

        return result
