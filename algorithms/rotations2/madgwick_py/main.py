import pandas as pd

from algorithms.rotations2.madgwick_py.madgwickahrs import MadgwickAHRS


def main():
    df = pd.read_csv("sample.csv", header=None, sep=',')

    heading = MadgwickAHRS()
    for row in df.values:
        heading.update(row[0:3], row[3:6], row[6:9])
        # madgwick.filter_update(row[0:3], row[3:6], row[6:9])
        ahrs = heading.quaternion.to_euler_angles()
        roll = ahrs[0]
        pitch = ahrs[1]
        yaw = ahrs[2]
        print(str(yaw))


if __name__ == "__main__":
    main()
