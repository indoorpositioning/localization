from math import pi
from typing import List, Dict

import numpy

from algorithms.particlefilter.vector import Vector
from algorithms.rotations2.adaptiveJerkPaceBuffer import AdaptiveJerkPaceBuffer
from algorithms.rotations2.madgwick_py.madgwickahrs import MadgwickAHRS
from misc.measurement import Measurement, PDRData, Orientations


class Rotations(object):
    cumulated_rotations: float
    yaws: List[float]
    heading: MadgwickAHRS

    def __init__(self) -> None:
        self.heading = MadgwickAHRS()
        self.yaws = []
        self.cumulated_rotations = 0.0
        self.jerk_pace_buffer = AdaptiveJerkPaceBuffer()
        self.i = 0

    def update(self, measurement: Measurement):
        def xyz_dict_to_array(xyz_dict: Dict[Orientations, float]) -> List[float]:
            return [xyz_dict[Orientations.x], xyz_dict[Orientations.y], xyz_dict[Orientations.z]]

        gyrometer = [val * (pi / 180) for val in xyz_dict_to_array(measurement.pdr[PDRData.gyrometer])]
        accelerometer = xyz_dict_to_array(measurement.pdr[PDRData.accelerometer])
        # magnetometer = xyz_dict_to_array(measurement.pdr[PDRData.magnetometer])
        # self.heading.update(gyrometer, accelerometer, magnetometer)
        ahrs = self.heading.quaternion.to_euler_angles()
        # z_value = measurement.pdr[PDRData.accelerometer][Orientations.z]
        z_value = numpy.linalg.norm(accelerometer)
        yaw = ahrs[2]
        self.yaws.append(yaw)
        self.cumulated_rotations += measurement.pdr[PDRData.gyrometer][Orientations.z] * (pi / 180.0)
        # yaw = (yaw + pi + 143 / (pi * 2))
        # print("Pos " + str(measurement.last_waypoint) + " Yaw is " + str(yaw) + " mean " + str(mean(self.yaws)))
        # print(str(yaw * (180 /pi)), end=' ')
        # self.i += 1
        # if self.i % 20 == 0:
        if self.jerk_pace_buffer.adaptive_jerk_pace_buffer(z_value, measurement.timestamp.timestamp()):
            print("Was a step")
            vector = Vector(0, 30)
            vector.rotate(self.cumulated_rotations)
            # print("Pos " + str(measurement.last_waypoint) + " Vector is " + json.dumps(vector, cls=APIJSONEncoder))
            return vector
