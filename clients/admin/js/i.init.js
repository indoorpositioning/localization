I = {
    Init: (function() {

        var getMultiScripts = function(arr, path) {
            var _arr = $.map(arr, function(scr) {
                return $.getScript( (path||"") + scr );
            });

            _arr.push($.Deferred(function( deferred ){
                $( deferred.resolve );
            }));

            return $.when.apply($, _arr);
        };

        var prepare = function() {

            // Prepare navigation
            $('nav').find('li').each(function(i, elm) {
                $(elm).on('click', function() {
                    var page = $(elm).data('loc');
                    I.Templates.loadPage(page);
                });
            });

            // Logout
            $('#logout').on('click', function() {
                location.href = '/logout';
            });

            // Load initial page
            I.Templates.loadPage('locations');
        };

        return {
            getMultiScripts: getMultiScripts,
            prepare: prepare
        }
    })(),

    Pages: {}
};

var scripts = ['i.templates.js', 'i.pages.locations.js', 'i.pages.users.js', 'i.pages.tracks.js', 'i.pages.database.js'];
I.Init.getMultiScripts(scripts, 'js/').done(function() {
    I.Init.getMultiScripts(['i.pages.locations.ranging.js'], 'js/').done(function() {
        I.Init.prepare();
    });
});