I.Pages.Database = {
    init: function(templates) {
        $('button').on('click', function() {
            var method = $(this).data('call_method');
            var path = $(this).data('call_path');
            $.ajax({
                url: 'api/' + path,
                method: method,
                dataType: 'json'
            }).done(function(r) {
                console.log(r);
            });
        });
    }
}