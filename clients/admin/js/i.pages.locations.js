I.Pages.Locations = {
    init: function(templates) {

        I.Pages.Locations.Ranging.init(templates);
        I.Pages.Locations.loadLocations(templates);
        I.Pages.Locations.lastClicked = {'x':false,'y':false};
    },
    loadLocations: function(templates) {

        $.get('api/locations', function(locations) {

            // Show
            $('#locations_table').loadTemplate(templates['locations_table_entry'], locations, {append: true});

            // Delete
            $('#locationDeleteModal').on('show.bs.modal', function (event) {
                var id = $(event.relatedTarget).parents('tr').children('.id').text();
                var name = $(event.relatedTarget).parents('tr').children('.name').text();
                var modal = $(this);
                modal.find('.modal-body').text('Do you really want to delete \'' + name + '\' and all related information?');
                $('#locationDeleteConfirmed').on('click', function() {
                    $.ajax({
                        url: 'api/locations/' + id,
                        method: 'DELETE',
                        dataType: 'json'
                    }).done(function (r) {
                        if (r.success) {
                            modal.modal('hide');
                        }
                        else {
                            alert('Something did not work');
                        }
                    });
                });
            })
            $('#locationDeleteModal').on('hidden.bs.modal', function() {
                I.Templates.loadPage('locations');
            });

            // New
            $('#location_new_button').on('click', function() {
               var name = $('#location_new').val();
               if (name) {
                   $.ajax({
                       url: 'api/locations/',
                       method: 'POST',
                       dataType: 'json',
                       data: {
                           name: name
                       }
                   }).done(function(r) {
                       if (r.success) {
                           I.Templates.loadPage('locations');
                       }
                       else {
                           alert('Something did not work');
                       }
                   });
               }
            });

            // Edit
            $('.edit').on('click', function() {
                var id = $(this).parents('tr').children('.id').text();
                I.Pages.Locations.loadLocationDetails(templates, id);
            });
        }, 'json');
    },
    loadLocationDetails: function(templates, id) {

        var dialog;

        $.get('api/locations/' + id, function(r) {

            r.map = '<p style="padding: 20px">Please upload some maps.</p>';
            if (r.location.map_display) {
                r.location.map = '<img id="location_map_img" src="maps/' + id + '_display' + r.location.map_display + '" alt="" />';
            }

            $('#content').loadTemplate(templates['location_details'], r.location);

            // Enhanced Ranging
            I.Pages.Locations.Ranging.setupLocation($('.enhanced-ranging'), null, -1);
            $('#location_map_img').on('click', function(e) {
               I.Pages.Locations.lastClicked = {
                   x: e.originalEvent.offsetX,
                   y: e.originalEvent.offsetY
               };
               console.log(I.Pages.Locations.lastClicked);
            });

            // Autoupdate fields
            $('#locations_data input.autoupdate').on('change', function() {
                var value = $(this).val();
                var field = $(this).data('fieldname');
                $.ajax({
                    url: 'api/locations/' + id,
                    method: 'PATCH',
                    dataType: 'json',
                    data: {
                        field: field,
                        value: value
                    }
                }).done(function(r) {
                    if (r.success != 1) {
                        alert('Something did not work');
                    }
                });
            });

            // File upload
            $("#locations_fileuploader").uploadFile({
                url: '/api/locations/' + id,
                multiple: false,
                dragDrop: true,
                acceptFiles: 'image/*',
                showFileCounter: false,
                dynamicFormData: function() {
                    return { field: 'map', type: $('#locations_upload_type input:checked').val() }
                },
                fileName: "map",
                onSuccess:function(files,data,xhr,pd)
                {
                    I.Pages.Locations.loadLocationDetails(templates, id);
                    //$("#eventsmessage").html($("#eventsmessage").html()+"<br/>Success for: "+JSON.stringify(data));

                }
            });

            // Switch map
            $('#locations_upload_type input[type=radio]').on('change', function() {
                var type = $(this).val();
                if (r.location['map_' + type]) {
                    $('#location_map_img').attr('src', 'maps/' + id + '_' + type + r.location['map_' + type]);
                }
            });

            // Tools: Drag
            $('#locations_tools > .oi').draggable({
                scroll: true,
                appendTo: '#locations_map',
                containment: '#locations_map',
                helper: 'clone',
                start: function(e, ui) {
                    if ($(this).hasClass('oi-media-play') && $('#locations_map .oi-media-play').length >= 2) { // +1 because that's after it's appended
                        return false;
                    }
                    else if ($(this).hasClass('oi-plus') && $('#locations_map .oi-plus').length >= 3) {
                        return false;
                    }
                },
                stop: function(e, ui) {
                    var toolClass;
                    if ($(this).hasClass('oi-wifi')) { toolClass = 'wifi'; }
                    else if ($(this).hasClass('oi-bluetooth')) { toolClass = 'bluetooth'; }
                    else if ($(this).hasClass('oi-media-play')) { toolClass = 'media-play'; }
                    else if ($(this).hasClass('oi-plus')) { toolClass = 'plus'; }
                    else if ($(this).hasClass('oi-audio')) { toolClass = 'audio'; }
                    saveTool(toolClass, {left: ui.position.left, top: ui.position.top}, function(toolID) {
                        placeTool(toolClass, ui.position.left, ui.position.top, toolID);
                    });
                }
            });

            // Popup
            dialog = $("#locationDeviceModifyForm").dialog({
                autoOpen: false,
                height: 480,
                width: 380,
                modal: true,
                close: function(e, ui) {
                    $(this).find('input').off('keyup');
                }
            });

            // Tools: Place
            for(var n in r.ranging_devices) {
                var ranging_device = r.ranging_devices[n];
                placeTool(ranging_device.type, ranging_device.position_x, ranging_device.position_y, ranging_device.id, r.ranging_devices[n]);
            }
            for(var n in r.ranging_locations) {
                var location_device = r.ranging_locations[n];
                placeTool('audio', location_device.position_x, location_device.position_y, location_device.id);
            }
            if (r.location.map_start_x) {
                placeTool('media-play', r.location.map_start_x, r.location.map_start_y);
            }
            if (r.location.map_scale_start_x) {
                placeTool('plus', r.location.map_scale_start_x, r.location.map_scale_start_y, 'start');
            }
            if (r.location.map_scale_end_x) {
                placeTool('plus', r.location.map_scale_end_x, r.location.map_scale_end_y, 'end');
            }


        }, 'json');

        var placeTool = function(toolClass, left, top, toolID, data) {

            if (typeof data === 'undefined') {
                data = {
                    left: left,
                    top: top
                }
            }
            else {
                data.left = data.position_x;
                data.top = data.position_y;
            }

            var idAttr = '';
            if (toolID) {
                idAttr = 'data-id="' + toolID + '"';
            }

            top = Math.max(0, top);
            left = Math.max(0, left);
            var caption = '';
            if (toolClass === 'audio') {
                caption = toolID;
            }
            var element = $('<span ' + idAttr + ' class="oi oi-' + toolClass + '" style="top:' + top + 'px;left:' + left + 'px;" onclick="void(0)">' + caption + '</span>');
            $('#locations_map').append(element);

            // Make draggable
            (function(element, toolClass, toolID, data) {
                element.draggable({
                    scroll: true,
                    containment: '#locations_map',
                    stop: function (e, ui) {
                        var toolClass;
                        if ($(this).hasClass('oi-wifi')) {
                            toolClass = 'wifi';
                        }
                        else if ($(this).hasClass('oi-bluetooth')) {
                            toolClass = 'bluetooth';
                        }
                        else if ($(this).hasClass('oi-media-play')) {
                            toolClass = 'media-play';
                        }
                        else if ($(this).hasClass('oi-plus')) {
                            toolClass = 'plus';
                        }
                        else if ($(this).hasClass('oi-audio')) {
                            toolClass = 'audio';
                        }
                        saveTool(toolClass, {left: ui.position.left, top: ui.position.top}, null, toolID);
                        data.left = ui.position.left;
                        data.top = ui.position.top;
                    }
                });
            })(element, toolClass, toolID, data);

            // Dialog for RangingDevices
            if (element.hasClass('oi-wifi') || element.hasClass('oi-bluetooth')) {
                (function (element, data, toolClass, toolID) {

                    element.on("dblclick touchstart", function () {

                        $("#locationDeviceModifyForm").dialog('option', 'buttons', {
                            Delete: function() {
                                $.ajax({
                                    url: 'api/tools/' + toolID,
                                    method: 'DELETE',
                                    dataType: 'json'
                                }).done(function(r) {
                                    dialog.dialog('close');
                                    element.remove();
                                });
                            },
                            Close: function () {
                                dialog.dialog("close");
                            }
                        });
                        dialog.dialog("open");

                        // Values
                        dialog.find('input').val('');
                        for(var n in data) {
                            dialog.find('#loc' + n.charAt(0).toUpperCase() + n.slice(1)).val(data[n]);
                        }
                        dialog.find('input').on('keyup', function() {
                            var key = $(this).attr('name');
                            var value = $(this).val();
                            var sendData = {};
                            sendData[key] = value;
                            data[key] = value;
                            saveTool(toolClass, sendData, null, toolID);

                            // Move if needed
                            if (key === 'left') {
                                value = Math.max(0, value);
                                element.css('left', value + 'px');
                            }
                            else if (key === 'top') {
                                value = Math.max(0, value);
                                element.css('top', value + 'px');
                            }
                        });
                    });

                    element.on('click', function(e) {
                        if(e.shiftKey) {
                            $.post('api/enhancedranging/' + toolID);
                        }
                    });
                })(element, data, toolClass, toolID)
            }


            else if (element.hasClass('oi-audio')) {
                element.on("click", function () {
                    I.Pages.Locations.Ranging.setupLocation(element, data, toolID);
                });
            }

        }

        var saveTool = function(toolClass, data, callback, toolID) {
            var method = 'POST';
            var urlID = '';
            if (typeof toolID !== 'undefined' && toolID) {
                method = 'PATCH';
                urlID = '/' + toolID;
            }

            $.ajax({
               url: 'api/tools' + urlID,
               method: method,
               dataType: 'json',
               data: $.extend({}, data, {
                   locationID: id,
                   toolClass: toolClass
               })
            }).done(function(r) {
                if (r.success) {
                    if (typeof callback === 'function') {
                        callback(r.id);
                    }
                }
            });
        }
    }
}

