I.Pages.Locations.Ranging = (function() {

    var templates;
    var dialog;
    var rangingLocationID;
    var deviceID = -1;
    var measurementInterval;

    var init = function(tpl) {
        templates = tpl;
        $('#content').append(templates['locationsRangingLocationEdit'].html());

        dialog = $('#ranging_location_edit').dialog({
            autoOpen: false,
            height: 560,
            width: 760,
            modal: true,
            close: function(e, ui) {
                $(this).find('input').off('keyup');
                $(this).find('button').off('click');
                clearInterval(measurementInterval);
            }
        });
    }

    var setupLocation = function(element, data, id) {

        rangingLocationID = id;

        element.on("dblclick touchstart click", function (e) {

            if (e.type == 'click' && $(this).hasClass('oi-audio')) {
                return;
            }

            // Open Dialog
            $('#ranging_location_edit').dialog('option', 'buttons', {
                Delete: function() {
                    if (confirm('Do you really want to delete this Ranging Location and all associated data?')) {
                        $.ajax({
                            url: 'api/ranginglocation/' + id,
                            method: 'DELETE',
                            dataType: 'json'
                        }).done(function(r) {
                            if (r.success === 1) {
                                dialog.dialog('close');
                                $('.oi-audio[data-id="' + id + '"]').remove();
                            }
                        });
                    }
                },
                Close: function () {
                    dialog.dialog("close");
                }
            });
            dialog.dialog("open");

            // Load and Show Devices
            $.get('api/devices', function(devices) {

                // Show
                $('#ranging_location_form_devices .devices').html('');
                for (n in devices) {
                    if (devices[n].name != '') {
                        placeDevice(devices[n]);
                    }
                }

                showMeasurements($('#ranging_location_form_devices .devices > div:first-child').data('id'));

                // Add
                $('#ranging_location_form_devices .new_device input').on('keyup', function(e) {
                    if (e.keyCode === 13) { // Enter
                        var name = $(this).val();
                        addDevice(name);
                        $(this).val('');
                    }
                });

            }, 'json');

            // Start, End measuring
            $('#ranging_location_form_measurements .control button').on('click', function(e) {
                var action = $(this).text();

                $.get('api/ranging_measurements_control/' + rangingLocationID, {action: action, deviceID: deviceID, position: I.Pages.Locations.lastClicked.x + ',' + I.Pages.Locations.lastClicked.y});

                var status = 'Status: ' + (action === 'Start' ? 'Active' : 'Off');
                $('#ranging_location_form_measurements .control .status').text(status);
                e.stopImmediatePropagation();
            });

        });

    };

    var placeDevice = function(device) {
        var devices = $('#ranging_location_form_devices .devices');

        devices.loadTemplate(templates['locations_ranging_device'], device, {append: true});

        var newDevice = devices.find('div:last-child');

        // Delete
        newDevice.find('.delete').on('click', function() {
            var id = $(this).parent().data('id');
            var name = $(this).prev().text();
            deleteDevice(id, name);
        });

        // Edit
        newDevice.find('.name').on('dblclick', function(e) {
            this.contentEditable = true;
        });
        newDevice.find('.name').on('blur', function(e) {
            this.contentEditable = false;
        });
        newDevice.find('.name').on('input', function(e) {
            editDevice.call(this);
        });

        // Click - Show Measurements
        newDevice.find('.name').on('click', function(e) {
            var id = $(this).parent().data('id');
            showMeasurements(id);
        });
    }

    var deleteDevice = function(id, name) {
        if (confirm('Would you really like to delete device "' + name + '"?')) {
            $.ajax({
                url: 'api/devices/' + id,
                method: 'DELETE',
                dataType: 'json'
            }).done(function(r) {
                if (r.success === 1) {
                    $('#ranging_location_form_devices .devices').find('div[data-id="' + id + '"]').remove();
                }
            });
        }
    };

    var addDevice = function(name) {
        $.ajax({
            url: 'api/devices',
            method: 'POST',
            dataType: 'json',
            data: {name: name}
        }).done(function(r) {
            if (r.success === 1) {
                var id = r.id;
                placeDevice({id: id, name: name});
            }
        });
    }

    var editDevice = function() {
        var name = $(this).text();
        var id = $(this).parent().data('id');

        $.ajax({
            url: 'api/devices/' + id,
            method: 'PATCH',
            dataType: 'json',
            data: {name: name}
        }).done(function(r) {
            if (!r.success) {
                alert('Something went wrong!');
            }
        });
    }

    var showMeasurements = function(id) {
        deviceID = id;

        if (deviceID) {
            clearInterval(measurementInterval);
            measurementInterval = setInterval(function () {
                showNewMeasurements();
            }, 1000);

            showNewMeasurements();
        }
    };

    var showNewMeasurements = function() {
        $.get('api/rangingdata/' + rangingLocationID, {deviceID: deviceID}, function(rangingData) {
            $('#ranging_measurement_data tbody').html('');
            $('#ranging_measurement_data tbody').loadTemplate(templates['location_ranging_measurement_summary'], rangingData, {append: true});

            $('#ranging_measurement_data tbody .delete').on('click', function(e) {
                var rangingDeviceID = $(this).parents('tr').data('id');
                if (confirm('Would you really like to delete all these measurements?')) {
                    $.ajax({
                        url: 'api/rangingdata/' + rangingLocationID,
                        method: 'DELETE',
                        dataType: 'json',
                        data: {deviceID: deviceID, rangingDeviceID: rangingDeviceID}
                    }).done(function(r) {
                        if (r.success === 1) {
                            showNewMeasurements();
                        }
                    });
                }

            });
        }, 'json');
    }

    return {
        init: init,
        setupLocation: setupLocation
    }
})();
