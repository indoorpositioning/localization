I.Pages.Tracks = {
    init: function(templates) {

        // Configure Datepicker
        var dateFormat = "dd.mm.yy";
        var from = $('#track_date #from')
                .datepicker({
                    defaultDate: "-2m",
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: dateFormat,
                    numberOfMonths: 3
                })
                .on('change', function() {
                    to.datepicker('option', 'minDate', getDate(this));
                    updateData(from.datepicker('getDate'), to.datepicker('getDate'));
                });
        var to = $('#track_date #to').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: dateFormat,
                    numberOfMonths: 3
                })
                .on('change', function() {
                    from.datepicker('option', 'maxDate', getDate(this));
                    updateData(from.datepicker('getDate'), to.datepicker('getDate'));
                });

        from.datepicker('setDate', '-1m');
        to.datepicker('setDate', '0m');

        var getDate = function ( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        };


        var pad = function(num, size) {
            var s = num + "";
            while (s.length < size) s = "0" + s;
            return s;
        };

        var updateData = function(from, to) {
            $.get('api/tracks', {from: parseInt(from.getTime()/1000), to: parseInt(to.getTime()/1000)}, function(r) {

                if (r.length == 0) {
                    $('#track_data').html('<p>No matching tracks!</p>');
                }
                else {

                    for(var i in r) {
                        var date = new Date(r[i].timestamp * 1000);
                        r[i].time = pad(date.getDate(), 2) + '.' + pad((date.getMonth()+1), 2) + '.' + date.getFullYear() + ' ';
                        r[i].time += pad(date.getHours(), 2) + ':' + pad(date.getMinutes(), 2);
                    }

                    $('#track_data').loadTemplate(templates['tracks_table']);
                    $('#track_table').loadTemplate(templates['track_entry'], r, {append: true});
                }

                $('#track_data .oi-media-play').on('click', function() {
                    var id = $(this).parents('td').data('id');
                    I.Pages.Tracks.playTrack(id, templates);
                });

                $('#track_data .oi-map').on('click', function() {
                    var id = $(this).parents('td').data('id');
                    I.Pages.Tracks.loadTrackMap(id, templates);
                });


                $('#trackDeleteModal').on('show.bs.modal', function (event) {
                    var id = $(event.relatedTarget).parent().data('id');
                    var row = $(event.relatedTarget).parents('tr');
                    var modal = $(this);
                    $('#trackDeleteConfirmed').on('click', function() {
                        $.ajax({
                            url: 'api/tracks/' + id,
                            method: 'DELETE',
                            dataType: 'json'
                        }).done(function (r) {
                            if (r.success) {
                                row.remove();
                                modal.modal('hide');
                            }
                            else {
                                alert('Something went wrong!');
                            }
                        });
                    });
                });

            }, 'json');
        }


        updateData(from.datepicker('getDate'), to.datepicker('getDate'));
    },

    playTrack: function(trackID, templates) {


        var ws;
        var measurements;
        var run = false;

        var initWebsocket = function() {
            var websocket = 'ws://' + window.location.hostname + ':8000';
            ws = new WebSocket(websocket);
            ws.onopen = function(e) {
                initConnection();
            };
            ws.onmessage = function(e) {
                websocketInput(e.data);
            };
        };

        var websocketInput = function(data) {
            $('#ws_in').prepend('<p><pre>' + data + '</pre></p>');

            var dataJson = JSON.parse(data);
            if (dataJson.t === 'i') {
                sendMeasurements();
            }
        };

        var initConnection = function() {

            // Get devices
            var devices = {};
            for(var n in measurements) {
                if (typeof measurements[n].data.r !== 'undefined') {
                    var d = Object.keys(measurements[n].data.r);
                    for(var m in d) {
                        devices[d[m]] = true;
                    }
                }
            }
            devices = Object.keys(devices);

            // Create Init Object
            var init = {
                t: 'i',
                d: {
                    location: {
                        type: 'wifi',
                        devices: devices
                    },
                    upload_id: $('#track_client_identifier').val(),
                    type: 'upload',
                    capabilities: 'wifi',
                    log: false
                }
            };
            if ($('#simulate_rotations').is(":checked")) {
                init.r = $('#track_id').val()
            }
            wsSend(init);

        };

        var wrapData = function(data) {
            return {
                t: 'd',
                d: data
            }
        }

        var sendMeasurements = function() {
            var index = 0;

            var send = function() {
                wsSend(wrapData(measurements[index]['data']));
                if (typeof measurements[index+1] !== 'undefined' && run) {
                    index++;
                    var wait = measurements[index+1]['time'] - measurements[index]['time'];
                    //console.log(measurements[index+1]['time'] + ':' + measurements[index]['time'] + ': ' + wait + ' (' + measurements[index+1].data.t + ':' + measurements[index].data.t + ')');
                    if (wait > 5000) {
                        wait = 5000;
                    }
                    setTimeout(send, wait);
                }
            };

            send();
        };

        var wsSend = function(data) {
            if (typeof data !== 'string') {
                data = JSON.stringify(data);
            }
            ws.send(data);
            $('#ws_out').prepend('<p><pre>' + data + '</pre></p>');
        };

        $.get('api/tracks/' + trackID, function(r) {

            measurements = JSON.parse(r.measurements.replace(/(\d{17})/g, "\"$1\"").replace(/""/g, '"'));
            for(var n in measurements) {
                measurements[n] = {
                    time: parseInt(measurements[n].t.substr(8)),
                    data: measurements[n]
                };
            }

            $('#content').loadTemplate(templates['track_details'], r);

            $('#track_play').on('click', function() {
                if (!run) {
                    run = true;
                    $('#ws_in, #ws_out').html('');
                    $(this).html('Stop');

                    if ($('#track_client_identifier').val().length> 0) {
                        initWebsocket();
                    }
                    else {
                        alert('No client Identifier!');
                    }
                } else {
                    run = false;
                    $(this).html('Run');
                }
            });
        }, 'json');



    },

    loadTrackMap: function(trackID, templates) {

        var dialog;
        var currentWaypointNumber = 0;

        $.get('api/tracks/' + trackID, function(r) {

            r.map = '<p style="padding: 20px">Please upload some maps.</p>';
            if (r.location.map_display) {
                r.location.map = '<img id="location_map_img" src="maps/' + r.location.map_id + '_display' + r.location.map_display + '" alt="" />';
            }

            $('#content').loadTemplate(templates['track_positions'], r.location);

            // Tools: Drag
            $('#track_tools > .oi').draggable({
                scroll: true,
                appendTo: '#locations_map',
                containment: '#locations_map',
                helper: 'clone',
                stop: function(e, ui) {
                    currentWaypointNumber++;
                    saveWaypoint({waypointNumber: Number(currentWaypointNumber), left: ui.position.left, top: ui.position.top}, function(waypointId) {
                        placeWaypointTool(waypointId, ui.position.left, ui.position.top, currentWaypointNumber);
                    });
                }
            });

            // Tools: Place
            for(var i in r.waypoints) {
                var waypoint = r.waypoints[i];
                placeWaypointTool(waypoint.id, waypoint.position_x, waypoint.position_y, waypoint.waypointNumber);
            }
            currentWaypointNumber = parseInt(r.waypoints[r.waypoints.length - 1].waypointNumber)

        }, 'json');

        var placeWaypointTool = function (waypointId, left, top, waypointNumber) {

            var data = {
                left: left,
                top: top
            };

            var idAttr = 'data-id="waypoint' + waypointId + '"';

            top = Math.max(0, top);
            left = Math.max(0, left);
            var element = $('<span ' + idAttr + ' class="oi oi-flag" style="top:' + top + 'px;left:' + left + 'px;">' + waypointNumber + '</span>');
            $('#locations_map').append(element);

            // Make draggable
            (function (element, waypointId, data) {
                element.draggable({
                    scroll: true,
                    containment: '#locations_map',
                    stop: function (e, ui) {
                        var waypointNumber = $(this).innerText;
                        saveWaypoint({waypointNumber: Number(waypointNumber), left: ui.position.left, top: ui.position.top}, null, waypointId);
                        data.left = ui.position.left;
                        data.top = ui.position.top;
                    }
                });
            })(element, waypointId, data);

        };

        var saveWaypoint = function(data, callback, waypointId) {
            var method = 'POST';
            var urlID = '';
            if (typeof waypointId !== 'undefined' && waypointId) {
                method = 'PATCH';
                urlID = '/' + waypointId;
            }

            $.ajax({
               url: 'api/waypoints' + urlID,
               method: method,
               dataType: 'json',
               data: $.extend({}, data, {
                   trackId: trackID
               })
            }).done(function(r) {
                if (r.success) {
                    if (typeof callback === 'function') {
                        callback(r.id);
                    }
                }
            });
        }

    }
}