I.Pages.Users = {
    init: function(templates) {

        // Groups
        $.get('api/groups', function(groups) {
            $('#users_groups_accordion').loadTemplate(templates['users_groups_accordion_entry'], groups);
            $('#users_groups_accordion').accordion({
                collapsible: true
            });

            // Delete
            $('#groupDeleteModal').on('show.bs.modal', function (event) {
                var id = $(event.relatedTarget).parents('h3').data('id');
                var name = $(event.relatedTarget).parents('h3').text();
                var modal = $(this);
                modal.find('.modal-body').text('Do you really want to delete \'' + name + '\'? The associated users will be independent again.');
                $('#groupDeleteConfirmed').on('click', function() {
                    $.ajax({
                        url: 'api/groups/' + id,
                        method: 'DELETE',
                        dataType: 'json'
                    }).done(function (r) {
                        if (r.success) {
                            modal.modal('hide');
                        }
                        else {
                            alert('Something went wrong!');
                        }
                    });
                });
            });
            $('#groupDeleteModal').on('hidden.bs.modal', function() {
                I.Templates.loadPage('users');
            });

            // Droppable
            var drop = function(groupID, e, ui) {
                var groupID = groupID;
                var draggableID = $(ui.draggable).data('id');

                $(ui.draggable).css({
                    left: 'auto',
                    top: 'auto',
                    opacity: 1
                }).appendTo(this);

                $('#users_groups_accordion').accordion('refresh');

                $.ajax({
                    url: 'api/clients/' + draggableID,
                    method: 'PATCH',
                    dataType: 'json',
                    data: {
                        group: groupID
                    }
                }).done(function(r) {
                    if (!r.success) {
                        alert('Something went wrong!');
                    }
                });
            }

            $('#users_groups_accordion .users_drop').droppable({
                accept: '.users_client',
                drop: function(e, ui) {
                    drop.call(this, $(this).data('id'), e, ui);
                }
            });
            $('#users_clients').droppable({
                accept: '.users_client',
                drop: function(e, ui) {
                    drop.call(this, null, e, ui);
                }
            });

            // Clients
            $.get('api/clients', function(clients) {
                for(var n in clients) {
                    var client = clients[n];
                    var clientHTML = '<div class="users_client" data-id="' + client.id + '">' + client.identifier + '</div>';
                    if (client.group_id) {
                        $('#users_groups_accordion > div[data-id=' + client.group_id + ']').append(clientHTML);
                    }
                    else {
                        $('#users_clients').append(clientHTML);
                    }
                }

                $('#users_groups_accordion').accordion('refresh');

                $('.users_client').draggable({
                    revert: 'invalid',
                    appendTo: 'body',
                    helper: 'clone',
                    start: function(e, ui) {
                        $(this).css('opacity', 0.5);
                    }
                });

            }, 'json');

        }, 'json');

        $('#users_groups_add_button').on('click', function() {
            var value = $('#users_groups_add').val();
            $.post('api/groups', {name: value}, function(r) {
                if (r.success) {
                    I.Templates.loadPage('users');
                }
                else {
                    alert('Something did not work');
                }
            }, 'json');
        });

    }
}