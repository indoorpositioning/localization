I.Templates = (function() {

    var templates = {};

    var loadPage = function(page) {
        var callback = I.Pages[page.charAt(0).toUpperCase() + page.slice(1)].init;

        if (typeof templates[page] === 'undefined') {
            templates[page] = {};
            $.get('tpl/page_' + page + '.html', function(p) {
                $('<div>' + p + '</div>').children().each(function(id, template) {
                    var id = $(template).attr('id');
                    templates[page][id] = $(template);
                });
                $('#content').loadTemplate(templates[page]['page']);
                callback(templates[page]);
            });
        }
        else {
            $('#content').loadTemplate(templates[page]['page']);
            callback(templates[page]);
        }
    };

    $.addTemplateFormatter({
        DecimalFormatter: function(value, template) {
            if (typeof template === 'undefined' || !template) {
                template = 3;
            }
            return value.toFixed(template);
        }
    });

    return {
        loadPage: loadPage
    };
})();


/*
https://github.com/codepb/jquery-template
 */