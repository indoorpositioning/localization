// Displays and updates a map using download_id
import {fabric} from 'fabric'
import {
    Location,
    Particle,
    Position,
    PositionData,
    RangingDeviceDefinition,
    TrackingDeviceDefinition,
    TrackingDevicePosition,
    Vector
} from "./socket";
import {Path} from "./path";
import $ = require("jquery");


let IMAGE_PATH = '/img/';

type TrackingDeviceID = number;
type RangingDeviceId = number;

export class Floorplan {
    private canvas: fabric.Canvas;
    private group: fabric.Group;
    private trackingDevicePerId: Map<TrackingDeviceID, [TrackingDeviceDefinition, fabric.Image, fabric.Circle]>;
    private rangingDevices: Map<RangingDeviceId, [RangingDeviceDefinition, fabric.Circle]>;
    private rangingDeviceRanges: Map<RangingDeviceId, fabric.Circle>;
    private rangingDeviceColors: Map<RangingDeviceId, string>;
    private rangingDeviceIdentifierIdLookup: Map<string, RangingDeviceId>;
    private lastActualPosition: Map<TrackingDeviceID, Position>;
    private startPosition: Vector;
    private exactPath: Path;
    private walkPath: Path;
    private computedPath: Path;
    private trilateratedPath: Path;
    private particles: Array<fabric.Circle>;
    private pixels_per_cm: number;
    private connectionStatus: JQuery<HTMLElement>;
    private currentWaypoint: number;
    private allTrackingDevicePositions: Map<number, Array<Position>>;
    private readonly RADIAN_TO_DEGREE_FACTOR: number = (90 / (Math.PI / 2));

    constructor() {
        this.canvas = new fabric.Canvas('c', {
            selection: false
        });
        this.allTrackingDevicePositions = new Map();
        this.rangingDevices = new Map();
        this.trackingDevicePerId = new Map();
        this.rangingDeviceIdentifierIdLookup = new Map();
        this.rangingDeviceRanges = new Map();
        this.particles = [];
        this.currentWaypoint = -1;
        this.rangingDeviceColors = new Map();
    }

    loadMap(location: Location, callback: () => any): void {
        fabric.Object.prototype.originX = "left";
        fabric.Object.prototype.originY = "top";
        const xDistSquared = Math.pow(Math.abs(location.location.map_scale_end_x - location.location.map_scale_start_x), 2);
        const yDistSquared = Math.pow(Math.abs(location.location.map_scale_end_y - location.location.map_scale_start_y), 2);
        this.pixels_per_cm = Math.sqrt(xDistSquared + yDistSquared) / location.location.map_width;
        let self = this;

        function getRangingDeviceUrl(rangingDeviceDefinition: RangingDeviceDefinition): string {
            if (rangingDeviceDefinition.type == "wifi") {
                return IMAGE_PATH + 'ap.png'
            }
            return IMAGE_PATH + 'beacon' + rangingDeviceDefinition.type + 'Small.png'
        }

        function getTrackingDeviceUrl(icon: TrackingDeviceDefinition): string {
            // TODO(patrick) Make this dependent on icon instead of showing the same icon to all
            return IMAGE_PATH + 'avatar.png';
        }

        function preloadImages<T>(objectsToLoad: Array<T>, urlFunction: (objects: T) => string, finalCallback: (loadedImages: Array<[T, fabric.Image]>) => any, loadedImages: Array<[T, fabric.Image]> = []) {
            // Recursively loads all images from passed objects using the passed functions and calls the final callback at the end. Result is an array of tuples (object and its image)
            if (loadedImages == undefined) {
                loadedImages = [];
                objectsToLoad.reverse();
            } else if (objectsToLoad.length === 0) {
                finalCallback(loadedImages);
            } else {
                const lastIndex = objectsToLoad.length - 1;
                const currentObject = objectsToLoad[lastIndex];
                fabric.Image.fromURL(urlFunction(currentObject), function (img) {
                    loadedImages.push([currentObject, img]);
                    objectsToLoad.pop();
                    preloadImages<T>(objectsToLoad, urlFunction, finalCallback, loadedImages);
                });
            }
        }

        let c = this.canvas;
        this.group = new fabric.Group();
        this.group.hasControls = false;
        // this.group.hasBorders = false;
        let group: fabric.Group = this.group;
        // add the map and then the ranging Device fabric.Images using the function below
        let map = fabric.Image.fromURL(`../maps/${location.location.id}_display${location.location.map_display}`, function (img) {
            group.addWithUpdate(img);
            preloadImages<TrackingDeviceDefinition>(location.tracking_devices, getTrackingDeviceUrl, function (trackingDeviceDefinitionImages: Array<[TrackingDeviceDefinition, fabric.Image]>) {
                for (let trackingDeviceDefinitionImage of trackingDeviceDefinitionImages) {
                    const trackingDeviceDefinition = trackingDeviceDefinitionImage[0];
                    const trackingDeviceImage = trackingDeviceDefinitionImage[1];
                    // addObject2fabricGroup(trackingDeviceImage);
                    const trackingDeviceId = trackingDeviceDefinition.id;
                    const circle = new fabric.Circle({
                        'radius': 2,
                        'hasBorders': false,
                        'hasControls': false
                    });
                    self.addObject2fabricGroup(circle, trackingDeviceDefinition);
                    self.trackingDevicePerId.set(trackingDeviceId, [trackingDeviceDefinition, trackingDeviceImage, circle]);
                    self.allTrackingDevicePositions.set(trackingDeviceId, []);
                    self.walkPath = new Path(group, 'green');
                    // self.walkPath.addPosition(trackingDeviceDefinition);
                    self.computedPath = new Path(group, 'red');
                    self.trilateratedPath = new Path(group, 'blue');
                    self.exactPath = new Path(group, 'black');
                    // self.exactPath.addPosition(trackingDeviceDefinition);
                }
                preloadImages<RangingDeviceDefinition>(location.ranging_devices, getRangingDeviceUrl, function (rangingDeviceDefinitionImages: Array<[RangingDeviceDefinition, fabric.Image]>) {
                    for (let rangingDeviceDefinitionImage of rangingDeviceDefinitionImages) {
                        const devicefabricImage = rangingDeviceDefinitionImage[1];
                        const deviceDefinition: RangingDeviceDefinition = rangingDeviceDefinitionImage[0];
                        deviceDefinition.position_x /= self.pixels_per_cm;
                        deviceDefinition.position_y /= self.pixels_per_cm;
                        // self.addObject2fabricGroup(devicefabricImage, deviceDefinition);
                        self.rangingDevices[deviceDefinition.id] = rangingDeviceDefinitionImage;
                        self.rangingDeviceIdentifierIdLookup[deviceDefinition.identifier] = deviceDefinition.id;
                        let rangingDeviceColors = ['#f00', '#0f0', '#00f', '#ff0', '#0ff', '#f0f'];
                        self.rangingDeviceColors[deviceDefinition.id] = rangingDeviceColors[Object.keys(self.rangingDevices).length - 1];
                        const radius = 10;
                        const deviceSpot = new fabric.Circle({
                            top: deviceDefinition.position_y - radius,
                            left: deviceDefinition.position_x - radius,
                            radius: radius,
                            fill: self.rangingDeviceColors[deviceDefinition.id],
                            stroke: self.rangingDeviceColors[deviceDefinition.id],
                            strokeWidth: 1,
                            hasBorders: false,
                            hasControls: false,
                            selectable: false
                        });
                        self.group.addWithUpdate(deviceSpot);
                        self.canvas.renderAll();
                    }
                    callback();
                    self.canvas.renderAll();
                });
            });
        });
        c.add(group);
    }

    private addObject2fabricGroup(image: fabric.Image | fabric.Circle, position?: Position) {
        if (position) {
            image.left = position.position_x - (image.width!) / 2;
            image.top = position.position_y - (image.height!) / 2;
            // image.set('left', position.position_x - image.width / 2);
            // image.set('top', position.position_y - image.height / 2);
        } else {
            image.visible = false;
            // image.set('visible', false);
        }
        this.group.addWithUpdate(image);
    }

    positionUpdate: (position: PositionData) => void = position => {
        let self = this;
        // Update particles
        let particlePositions: Array<Position> = position.p ? position.p : [];
        let trackingDevicePositions: Array<TrackingDevicePosition> = position.tp;

        // // Ensure enough particles are available
        // if (particlePositions.length > this.particles.length) {
        //     this.initParticles(particlePositions.length)
        // }
        // // Render the particles
        // for (let particleIndex = 0; particleIndex < particlePositions.length; particleIndex++) {
        //     self.addObject2fabricGroup(this.particles[particleIndex], particlePositions[particleIndex]);
        //     this.particles[particleIndex].set('visible', true);
        // }
        // // Hide superfluous particles
        // for (let particleIndex = particlePositions.length; particleIndex < this.particles.length; particleIndex++) {
        //     this.particles[particleIndex].set('visible', false);
        // }

        // Render the tracking devices, hide those without data
        for (let trackingDevicePosition of trackingDevicePositions) {
            const trackingDeviceId = trackingDevicePosition.id;
            const devicefabricImage = <fabric.Image>(this.trackingDevicePerId.get(trackingDeviceId)![1]);
            self.exactPath.addPosition(trackingDevicePosition);
            self.addObject2fabricGroup(devicefabricImage, trackingDevicePosition);
            devicefabricImage.set('angle', trackingDevicePosition.angle);
            (<Array<Position>>this.allTrackingDevicePositions.get(trackingDeviceId)).push(trackingDevicePosition);
        }

        this.canvas.renderAll();
    };

    printInfo: (info: string) => void = info => {
        this.connectionStatus.text(info);
        this.canvas.renderAll();
    };

    public resize() {
        let canvasContainer = $('#canvas-container');
        this.canvas.setWidth(<number> canvasContainer.width())
        this.canvas.setHeight(<number> canvasContainer.height())
    }

    private initParticles(numberOfParticles: number) {
        this.particles = [];
        for (let i = 0; i < numberOfParticles; i++) {
            this.particles[i] = new fabric.Circle({
                'radius': 2,
                'fill': '#fc0',
                'strokeWidth': 0,
                'objectCaching': false,
                'visible': true
            });
            this.group.add(this.particles[i])
        }
    }

    public renderAPRanging(rangingDeviceIdentifier: string, radius: number) {
        let rangingDeviceId = this.rangingDeviceIdentifierIdLookup[rangingDeviceIdentifier];
        let rangingDevice = this.rangingDevices[rangingDeviceId];
        let group = this.group;
        const oldCircle = this.rangingDeviceRanges[rangingDeviceId];
        group.remove(oldCircle);
        this.canvas.renderAll();
        let circle = new fabric.Circle({
            // top: rangingDevice[0].position_y - radius,
            top: rangingDevice[0].position_y - (group.height!) / 2 - radius,
            // left: rangingDevice[0].position_x - radius,
            left: rangingDevice[0].position_x - (group.width!) / 2 - radius,
            radius: radius,
            fill: 'transparent',
            stroke: this.rangingDeviceColors[rangingDeviceId],
            strokeWidth: 1,
            opacity: 0.5,
            hasBorders: false,
            hasControls: false,
            selectable: false
        });
        this.rangingDeviceRanges[rangingDeviceId] = circle;
        this.group.add(circle);
        this.canvas.renderAll();
    }

    public setCurrentWaypoint(waypointNumber: number) {
        if (this.currentWaypoint != waypointNumber) {
            this.currentWaypoint = waypointNumber;
            let paths: Path[] = [this.exactPath, this.trilateratedPath, this.walkPath, this.computedPath];
            for (let path of paths) {
                let position: Position | null = path.getLastPosition();
                if (position) {
                    let color: string = path.getColor();
                    let circle = new fabric.Circle({
                        top: position.position_y - (this.group.height!) / 2 - 2,
                        left: position.position_x - (this.group.width!) / 2 - 2,
                        radius: 3,
                        fill: 'transparent',
                        stroke: color,
                        strokeWidth: 2,
                        hasBorders: false,
                        hasControls: false,
                        selectable: false
                    });
                    let text = new fabric.Text(waypointNumber.toString(), {
                        top: position.position_y - (this.group.height!) / 2 - 3,
                        left: position.position_x - (this.group.width!) / 2 - 20,
                        fontSize: 20,
                        stroke: color,
                        hasControls: false,
                        selectable: false
                    });
                    this.group.add(circle);
                    this.group.add(text);
                }
            }
            this.canvas.renderAll();
        }
    }

    public renderExactPath(vector: Vector) {
        this.exactPath.addVector(vector);
        this.canvas.renderAll();
    }

    public renderComputedPath(newPosition: Vector) {
        // if (this.computedPath.positions.length == 21) {
        //     window.open(this.canvas.toDataURL('png'));
        // }
        // this.cmToPixel(newPosition);
        this.computedPath.addPosition(newPosition);
    }

    public setStartPosiion(startPosition: Vector) {
        this.startPosition = startPosition;
        this.walkPath.addPosition(startPosition)
        this.exactPath.addPosition(startPosition)
    }

    public renderWalkPath(walkVector: Vector) {
        // this.cmToPixel(walkVector);
        this.walkPath.addVector(walkVector);
    }

    public renderTrilateratedPath(trilateratedPath: Vector) {
        // this.cmToPixel(trilateratedPath);
        this.trilateratedPath.addPosition(trilateratedPath);
    }

    private cmToPixel(newPosition: Vector) {
        newPosition.position_x /= this.pixels_per_cm;
        newPosition.position_y /= this.pixels_per_cm;
    }

    public renderAll() {
        this.canvas.renderAll();
    }

    public renderParticles(particles: Array<Particle>) {
        if (this.particles.length === 0) {
            this.initParticles(particles.length);
        }
        for (let index in particles) {
            const drawn_particle = this.particles[index];
            const particle = particles[index];
            // console.log("Render a particle at left " + position_x + ", " + position_y);
            const red = particle.weight * 255 * particles.length;
            // const red = 255 - index;
            // console.log('red: ' + red);
            // drawn_particle.set('fill', 'rgb(' + red + ', ' + red + ',0)');
            drawn_particle.set('fill', 'rgb(' + red + ',0,0)');
            drawn_particle.set('left', particle.position_x - (this.group.width!) / 2 - 2);
            drawn_particle.set('top', particle.position_y - (this.group.height!) / 2 - 2);
        }
        //    for testing
        // this.group.set('top', -850);
        this.canvas.renderAll();
        // window.open(this.canvas.toDataURL('png'));
    }

}

