import {Floorplan} from "./floorplan";
import {Socket} from "./socket";
import $ = require("jquery");


declare let upload_id, download_id: number | string;
declare let track_id: number;

let floorplan: Floorplan;

$(window).on('load', function () {
    floorplan = new Floorplan();
    let socket: Socket = new Socket(floorplan);
    if (upload_id) {
        socket.openAndSendUploadId(upload_id, download_id);
    } else if (track_id) {
        socket.setTrackIdAndLoadTrack(track_id);
    }
    floorplan.resize();
});

$(window).resize(function () {
    floorplan.resize();
});