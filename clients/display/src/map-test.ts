import {Floorplan} from "./floorplan"
import {Location, Position, PositionData} from "./socket";

describe("Building a Floorplan", () => {

    let floorplan = new Floorplan();
    (<any>window).floorplan = floorplan;

    it("can build a map", () => {
        floorplan.loadMap(<Location>{
            "location": {
                "id": 2,
                "map_display": ".jpg"
            },
            "ranging_devices": [
                {
                    "position_x": 100,
                    "position_y": 80,
                    "type": "Candy"
                },
                {
                    "position_x": 150,
                    "position_y": 250,
                    "type": "Black"
                }
            ],
            "tracking_devices": [
                {
                    "icon": "iphone",
                    "id": 12
                }
            ]
        }, function () {
        });
        expect(-1).toBe(-1)
    });

    it("can walk on the map", () => {
        let xposition = 200, yposition = 200;
        let interval = window.setInterval(function () {
            yposition += (Math.random() * 100) - 50;
            xposition += (Math.random() * 100) - 50;
            floorplan.positionUpdate(<PositionData>{
                "tp": [{
                    "id": 12,
                    "position_x": xposition,
                    "position_y": yposition
                }]
            });
        }, 1000);
        expect(-1).toBe(-1);
    });

    it("can display particles", () => {
        let interval = window.setInterval(function () {
            let positions: Array<Position> = [];
            for (let i = 0; i < 1000; i++) {
                let yposition = Math.random() * 1000;
                let xposition = Math.random() * 1000;
                positions[i] = <Position>{"position_x": xposition, "position_y": yposition};
            }
            floorplan.positionUpdate(<PositionData>{
                "tp": [{
                    "id": 12,
                    "position_x": Math.random() * 20,
                    "position_y": Math.random() * 10
                }],
                "p": positions
            });
        }, 2000);
        expect(-1).toBe(-1);
    });

});