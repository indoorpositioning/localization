import {fabric} from 'fabric'
import {Position, Vector} from "./socket";

export class Path {

    private path: Array<fabric.Line> = [];
    private positions: Array<Position> = [];
    private group: fabric.Group;
    private color: string;
    private circle: fabric.Circle;

    constructor(group: fabric.Group, color: string) {
        this.group = group;
        this.color = color;
    }

    addPosition(position: Position) {
        this.positions.push(position);
        if (this.positions.length > 1) {
            let start = this.positions[this.positions.length - 2];
            let end = this.positions[this.positions.length - 1];
            this.renderLine(start, end)
        }
    }

    addVector(vector: Vector) {
        let lastPosition = this.positions[this.positions.length - 1];
        vector.plus(lastPosition);
        this.addPosition(vector);
    }

    private renderLine(start, position: Position) {
        const line = new fabric.Line([start.position_x - (this.group.width!) / 2, start.position_y - (this.group.height!) / 2,
            position.position_x - (this.group.width!) / 2, position.position_y - (this.group.height!) / 2], {
            fill: this.color, stroke: this.color, strokeWidth: 2, selectable: false, originX: "center", originY: "center"
        });
        this.group.remove(this.circle);
        this.circle = new fabric.Circle({
            top: position.position_y - (this.group.height!) / 2 - 2,
            left: position.position_x - (this.group.width!) / 2 - 2,
            radius: 3,
            fill: 'transparent',
            stroke: this.color,
            strokeWidth: 2,
            hasBorders: false,
            hasControls: false,
            selectable: false
        });
        this.path.push(line);
        this.group.add(line);
        this.group.add(this.circle);
        for (let i = 0; i < this.path.length - 1; i++) {
            this.path[i].set('strokeWidth', 1);
            this.path[i].set('opacity', 0.2);
        }
    }

    public getLastPosition() {
        return this.positions.length > 1 ? this.positions[this.positions.length - 1] : null;
    }

    public getColor() {
        return this.color;
    }

}