import {Floorplan} from "./floorplan";
import {plainToClass} from "class-transformer";
import $ = require("jquery");

export class Socket {
    private connection: WebSocket;
    private downloadId: string = Math.random().toString(36).substring(7);
    private state: State = State.Init;
    private mode: Mode;
    private trackId: number;
    private connectionStatus: JQuery<HTMLElement>;
    private floorplan: Floorplan;

    private _initJSON = {
        "t": "i",
        "d": {
            "type": "download"
        }
    };

    constructor(floorplan: Floorplan) {
        this.floorplan = floorplan;
        this.connectionStatus = $('#status > p');
    }

    setTrackIdAndLoadTrack(trackId: number): void {
        this.trackId = trackId;
        this.mode = Mode.Track;
        let self = this;
        this.openWebSocket();
        this.connection.onopen = function () {
            let initJSON = JSON.parse(JSON.stringify(self._initJSON));
            initJSON["d"]["track_id"] = self.trackId;
            self.connection.send(JSON.stringify(initJSON));
            self.setState(State.Load)
        };
    }

    private setState(state: State): void {
        this.state = state;
        switch (this.state) {
            case State.Load:
                this.connectionStatus.text("Loading...");
                break;
            case State.Draw:
                this.connectionStatus.text("Drawing.");
                break;
        }
    }

    private static getWebsocketUrl(): string {
        return `ws://${window.location.hostname}:8000`;
    }

    private openWebSocket(): void {
        this.connection = new WebSocket(Socket.getWebsocketUrl());
        this.connection.onmessage = this.onMessage;
    }

    openAndSendUploadId(upload_id: number, download_id: number | string): void {
        this.openWebSocket();
        if (download_id == 'test') {
            this.mode = Mode.Test;
        } else {
            this.mode = Mode.Live;
        }
        let self = this;
        this.connection.onopen = function () {
            let initJSON = JSON.parse(JSON.stringify(self._initJSON));
            initJSON["d"]["upload_id"] = upload_id;
            initJSON["d"]["download_id"] = download_id;
            self.connection.send(JSON.stringify(initJSON));
            self.setState(State.Load)
        };
    }

    // onMessage = (message) => {
    private onMessage: (message: any) => void = (message) => {
        message = JSON.parse(message.data);
        switch (this.state) {
            case State.Load:
                if (message.location != undefined) {
                    // Received a map packet
                    this.floorplan.loadMap(<Location>message, this.startUpdate);
                    this.setState(State.Draw);
                } else if (message.tp != undefined) {
                    // Received a position schema
                    this.floorplan.positionUpdate(<PositionData>message);
                } else if (message.t == "e") {
                    this.floorplan.printInfo(message.d.msg);
                }
                break;
            case State.Draw:
                if (typeof message.message === "object") {
                    console.log(message.message);
                }
                if (typeof message.ranging === "object") {
                    for (let identifier in message.ranging) {
                        this.floorplan.renderAPRanging(identifier, message.ranging[identifier]['pixel'])
                    }
                }
                if (typeof message.positions_pixel === "object") {
                    let positions = message.positions_pixel;
                    if (typeof positions.start_position === "object") {
                        var vector = new Vector(<Position> positions.start_position);
                        this.floorplan.setStartPosiion(vector);
                    }
                    if (typeof positions.ground_truth === "object") {
                        var vector = new Vector(<Position> positions.ground_truth);
                        this.floorplan.renderExactPath(vector);
                    }
                    if (typeof positions.particle_filter === "object") {
                        var vector = new Vector(<Position> positions.particle_filter);
                        this.floorplan.renderComputedPath(vector);
                    }
                    if (typeof positions.rssi_trilaterated == "object") {
                        var vector = new Vector(<Position> positions.rssi_trilaterated);
                        this.floorplan.renderTrilateratedPath(vector);
                    }
                    if (typeof positions.particles === "object") {
                        var particles = plainToClass(Vector, positions.particles);
                        this.floorplan.renderParticles(<Array<Particle>> particles);
                    }
                    if (typeof positions.pdr === "object") {
                        var vector = new Vector(<Position> positions.pdr);
                        this.floorplan.renderWalkPath(vector);
                    }
                }
                if (typeof message.p === "number") {
                    this.floorplan.setCurrentWaypoint(message.p);
                }
                this.floorplan.renderAll();
                break;
        }
    };

    public startUpdate: () => void = () => {
        if (this.mode == Mode.Track) {
            let json = {"t": "i", "d": {"type": "download", "track_id": this.trackId, "start": true}};
            this.connection.send(JSON.stringify(json))
        }
    };

}

export class Vector implements Position {
    constructor(position: Position) {
        if (typeof position !== "undefined") {
            this.position_x = position.position_x;
            this.position_y = position.position_y;
        }
    }

    position_x: number;
    position_y: number;

    public plus(other: Vector | Position): void {
        this.position_x += other.position_x;
        this.position_y += other.position_y;
    }

    times(scalar: number) {
        this.position_x *= scalar;
        this.position_y *= scalar;
    }
}

export class Particle extends Vector {
    weight: number;
}

export interface PositionData {
    tp: Array<TrackingDevicePosition>
    // The particles
    p: Array<Position>
}

export interface TrackingDevicePosition extends Position {
    id: number
    angle: number
}

export interface Position {
    position_x: number
    position_y: number
}

export interface Location {
    location: Map
    ranging_devices: RangingDeviceDefinition[]
    tracking_devices: TrackingDeviceDefinition[]
}

export interface Map {
    id: number
    map_display: string
    map_scale_start_x: number
    map_scale_end_x: number
    map_scale_start_y: number
    map_scale_end_y: number
    map_width: number
}

export interface RangingDeviceDefinition extends Position {
    type: string
    id: number
    identifier: string
    position_x: number
    position_y: number
}

export interface TrackingDeviceDefinition extends Position {
    icon: string
    id: number
    position_x: number
    position_y: number
}

enum State {
    Disconnect = 0,
    Init = 1,
    Select = 2,
    Wait = 3,
    Load = 4,
    Draw = 5,
    DrawLive = 6
}

enum Mode {
    Test = 0,
    Track = 1,
    Live = 2
}