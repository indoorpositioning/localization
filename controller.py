import json

from algorithms.particlefilter.floorplan_pool import FloorplanPool
from algorithms.particlefilter.particle_filter import ParticleFilter
from algorithms.particlefilter.path_generator import PathGenerator
from algorithms.particlefilter.test_particle_filter import send_to_websocket
from algorithms.ranging.enhanced.final.controller import EnhancedRanging
from algorithms.ranging.ranging import Ranging
from algorithms.ranging.ranging_measurements import RangingMeasurements
from algorithms.ranging.venue import Venue
from algorithms.rotations2.rotations import Rotations
from data.database_query import DatabaseQuery
from data.datastore import DataStore
from data.models import Track, TrackWaypoint, GeneratedTrack, TrackGeneratorModel
from misc.error import InputError, DataError
from misc.json_encoder import APIJSONEncoder
from misc.track_replayer import TrackReplayer
from misc.utils import epoch_millis_from_timestamp
from servers.api_server import get_display_location


class Controller(object):

    def __init__(self, data_store):
        self.datastore = data_store  # type: DataStore
        self.venue = Venue()
        self.floorplan_pool = FloorplanPool()

    def update(self, websocket, data):

        # start = time.perf_counter()

        if data['t'] == 'i':
            self.init(websocket, data)
        elif data['t'] == 'd':
            self.compute(websocket, data)
        elif data['t'] == 'r':
            self.ranging(data)
        elif data['t'] == 'e':
            self.enhanced_ranging(data)

        # end = time.perf_counter()
        # elapsed = end - start
        # print("elapsed time = {:.12f} seconds".format(elapsed))

    def init(self, websocket, data):

        if 'd' not in data:
            return
        if 'type' not in data['d']:
            return

        # Upload
        if data['d']['type'] == 'upload':

            # Get location based on GPS or access point data
            try:
                # location_id = self.venue.get_location(data['d'])
                location_id = 6

                # Add client
                try:
                    # Return ranging devices of location
                    if 'capabilities' not in data['d'] or len(data['d']['capabilities']) == 0:
                        raise InputError('no_capabilities', 'No capabilities indicated for the device.')
                    ranging_devices = self.venue.get_ranging_devices(location_id, data['d']['capabilities'])
                    websocket.send_message({'t': 'i', 'd': ranging_devices})

                    # Write websocket specific information
                    websocket.upload_id = data['d']['upload_id']

                    # Never used...
                    # if 'device' in data['d']:
                    #     device_id = Device.get(Device.name == data['d']['device']).id
                    # else:
                    #     device_id = None
                    #
                    # if device_id is None:
                    #     device_id = Device.get(Device.name == data['d']['upload_id']).id
                    #
                    # websocket.device_id = device_id

                    # Log
                    if 'log' not in data['d']:
                        log = True
                    else:
                        log = data['d']['log']

                    # If a track is being replayed, we can simulate track_rotations
                    if 'r' not in data:
                        track_rotations = Rotations()
                    else:
                        track_rotations = PathGenerator(Track.get(Track.id == data['r']))

                    self.datastore.add_upload(data=data['d'],
                                              websocket=websocket,
                                              location_id=location_id,
                                              particle_filter=ParticleFilter(
                                                  self.floorplan_pool.get_floorplan(location_id), None, None, None,
                                                  None, None, None, ),
                                              rotations=track_rotations,
                                              ranging=Ranging(location_id=location_id),
                                              log=log)

                except InputError as e:
                    websocket.send_message(e.print())

            except (DataError, InputError) as e:
                websocket.send_message(e.print())

        # Download
        elif data['d']['type'] == 'download':
            try:
                if 'track_id' in data['d']:
                    #     -> We want to replay a track
                    OtherTrack = Track.alias()
                    OtherWaypoints = TrackWaypoint.alias()
                    # track: Track = Track.select(Track, TrackWaypoint).join(TrackWaypoint, join_type='LEFT OUTER') \
                    #     .where(Track.id == data['d']['track_id']).switch(Track).join(GeneratedTrack).join(TrackGeneratorModel) \
                    #     .join(OtherTrack).join(OtherWaypoints, join_type='LEFT OUTER').get()
                    track = DatabaseQuery.track_with_waypoints(data['d']['track_id'])
                    location = track.location
                    track_rotations = PathGenerator(track)
                    if ('start' not in data['d']) or not data['d']['start']:
                        location_dict = get_display_location(location.id, track.client.id)
                        if len(track.waypoints) > 0:
                            start_position = track.waypoints[0]
                        else:
                            start_position = location.get_start_position_cm()
                        location_dict["tracking_devices"][0] = start_position
                        websocket.send_message(json.dumps(location_dict, cls=APIJSONEncoder))
                    else:
                        location_id = location.id
                        generated_track_query = GeneratedTrack.select(GeneratedTrack).where(
                            GeneratedTrack.track == track)
                        generated_track = generated_track_query.get() if generated_track_query.exists() else None
                        if generated_track:
                            model: TrackGeneratorModel = TrackGeneratorModel.select(TrackGeneratorModel).where(
                                TrackGeneratorModel.id == generated_track.track_generator).get()
                            particle_filter = ParticleFilter(self.floorplan_pool.get_floorplan(location_id),
                                                             model.pdr_deviation_step_factor,
                                                             model.pdr_average_step_factor,
                                                             model.pdr_deviation_angle_offset,
                                                             model.pdr_average_angle_offset,
                                                             model.pdr_average_step_length,
                                                             model.pf_with_bresenham,
                                                             seed=generated_track.random_seed)
                        else:
                            particle_filter = ParticleFilter(self.floorplan_pool.get_floorplan(location_id), 0.2, 1,
                                                             1, 0, 70, True)
                        rotations = Rotations()
                        ranging = Ranging(location_id)
                        track_replayer = TrackReplayer(websocket, track, ranging, rotations, track_rotations,
                                                       particle_filter)
                        track_replayer.start()
                elif data['d']['upload_id'] == 'test':
                    send_to_websocket(websocket)
                else:
                    self.datastore.add_download(data['d']['download_id'], data['d']['upload_id'], websocket)
                    map = get_display_location(8, 3)
                    websocket.send_message(json.dumps(map, cls=APIJSONEncoder))
            except InputError as e:
                websocket.send_message(e.print())

    def compute(self, websocket, data):

        if websocket.upload_id:  # Make sure it was initialized

            data_d = data['d']
            if 't' in data_d:
                data_d = [data_d]

            self.datastore.add_measurements(websocket.upload_id, data_d)

            # Call algos
            algos = self.datastore.get_algos(websocket.upload_id)
            particle_filter = algos['particle_filter']
            data['d'][0]['t'] = epoch_millis_from_timestamp(data['d'][0]['t'])
            rotations = algos['rotations'].update(data)
            # ranges = algos['ranging'].update(data_d, websocket.upload_id)
            # position = particle_filter.update(ranges, rotations)
            position = False

            # Save position
            if position:
                self.datastore.add_positions(websocket.upload_id, position)

            # Return
            clients = self.datastore.get_download_clients(websocket.upload_id)
            # self.printToConsole(websocket.upload_id, data)
            ranging_result = {}
            # rssi_list: List[Dict[str, int]] = [element['r'] for element in data_d if 'r' in element]
            # rssi_dict: Dict = {k: v for d in rssi_list for k, v in d.items()}
            # if rssi_dict:
            #     for ap_identifier in rssi_dict.keys():
            #         ranging_result[ap_identifier] = {'RSSI': rssi_dict[ap_identifier][0],
            #                                          'cm': ranges[ap_identifier]}
            for client in clients:
                response = {}
                if position:
                    response['particleFilterPosition'] = position
                    response['particles'] = particle_filter.particles
                if rotations:
                    response['walkVector'] = position
                    response['particles'] = particle_filter.particles
                if ranging_result:
                    response['r'] = ranging_result
                client.send_message(json.dumps(response, cls=APIJSONEncoder))

        else:
            raise InputError('no_upload_id', 'No upload ID available!')

    def ranging(self, data):
        RangingMeasurements.gather_data(data['d'])

    def printToConsole(self, id, data):
        print(id + " " + json.dumps(data['d'][0]['r']))

    def enhanced_ranging(self, data):
        message = EnhancedRanging.update(data['d'])

    def observeRotation(self, x, y):
        print("The controller received x: " + str(x) + " and y: " + str(y))
