from functools import lru_cache
from typing import List, Union

from data.models import Location, RangingDevice, Track, TrackWaypoint, GeneratedTrack, TrackGeneratorModel


class DatabaseQuery:

    @staticmethod
    def get_location(location_id: int) -> Location:
        """Fetches a location with its ranging devices

        Args:
            location_id:    The id of the saved location

        Returns: List of locations including ranging devices

        """
        return Location.select(Location, RangingDevice).join(RangingDevice).where(Location.id == location_id).get()

    @lru_cache(maxsize=1000)
    def track_with_waypoints(track: Union[Track, int]):
        """Appends the trackwaypoints. If a track has a generated track and no waypoints, the waypoints are taken from
        the route track"""
        if not isinstance(track, Track):
            track = Track.get(Track.id == track)
        track_id = track.id
        if not track.waypoints_fetched:
            track.waypoints_fetched = TrackWaypoint.select().where(TrackWaypoint.track == track_id).order_by(
                TrackWaypoint.position_number)  # type: List[TrackWaypoint]
        if not track.waypoints_fetched and track.generatedtrack:
            OtherTrack = Track.alias()
            track.waypoints_fetched = TrackWaypoint.select().join(OtherTrack).join(TrackGeneratorModel) \
                .join(GeneratedTrack).join(Track).where(Track.id == track_id).order_by(TrackWaypoint.position_number)
        for waypoint in track.waypoints_fetched:
            waypoint.compute_vectors(track.location.get_pixels_per_cm())
        return track
