import json
import random
from datetime import datetime

import tornado.ioloop

from data.models import Client, Track
from misc.error import InputError
from settings import datastore_settings


def register_id(identifier):
    client_exists = Client.select().where(Client.identifier == identifier).count()
    if not client_exists:
        Client.create(identifier=identifier)


cleaning_interval = datastore_settings['cleaning_interval']


class DataStore:
    """Saves all currently used data.

    self.data keeps track of the relations between upload- and download-clients as well as their capabilities and
    uploaded data. In order for it to not grow too big, data should periodically be written to the database and then
    removed.
    """

    def __init__(self):
        self.data = {}

        tornado.ioloop.PeriodicCallback(self.clean_datastore, 1000 * cleaning_interval).start()

    def add_upload(self, **kwargs):
        """Saves a new upload connection in self.data.

        Args:
            kwargs: Stuff
        """

        d = kwargs['data']

        if 'upload_id' not in d:
            raise InputError('upload_id_missing', "No upload ID specified")

        if d['upload_id'] not in self.data:
            self.data[d['upload_id']] = {
                'clients': {
                    'upload': [],
                    'download': [],
                    'capabilities': d['capabilities']
                },
                'algos': {
                    'particle_filter': kwargs['particle_filter'],
                    'rotations': kwargs['rotations'],
                    'ranging': kwargs['ranging']
                },
                'location_id': kwargs['location_id'],
                'positions': [],
                'measurements': [],
                'last_update': datetime.now(),
                'log': kwargs['log']
            }

        self.data[d['upload_id']]['clients']['upload'].append(kwargs['websocket'])

        # Save all new IDs to database (User/Group Management)
        register_id(d['upload_id'])

    def add_download(self, download_id: int, upload_id: int, websocket):
        """Saves a new download connection ins elf.data.

        Several download clients are possible, hence the list.

        Args:
            download_id:    Id of the download client
            upload_id:      Id of the upload client
            websocket (Websocket): Websocket-client.
        """
        if upload_id not in self.data:
            raise InputError('upload_not_existing', 'The upload_id indicated does not exist.')

        self.data[upload_id]['clients']['download'].append(websocket)

        # Save all new IDs to database (User/Group Management)
        register_id(download_id)

    def generate_client_id(self) -> int:
        """Ensures uniqueness of a client_id

        Returns: A new client_id that is guaranteed to be unique

        """
        known_ids = [client.id for client in Client.select()]
        new_id = random.randint(0, 9999999)
        while new_id in known_ids:
            new_id = random.randint(0, 9999999)
        return new_id

    def add_measurements(self, upload_id, d):
        print(d)
        self.data[upload_id]['measurements'].extend(d)
        self.data[upload_id]['last_update'] = datetime.now()

    def add_positions(self, upload_id, positions):
        self.data[upload_id]['positions'].append(positions)

    def get_download_clients(self, upload_id):
        return self.data[upload_id]['clients']['download']

    def get_upload_clients(self, download_id):
        return [upload_id for (upload_id, client) in self.data if download_id in client['clients']['download']]

    def get_algos(self, upload_id):
        return self.data[upload_id]['algos']

    def clean_datastore(self):
        now = datetime.now()
        delete_entries = []
        for upload_id, data in self.data.items():
            delta = now - data['last_update']
            print(delta.total_seconds())
            if delta.total_seconds() > cleaning_interval:
                print(data['log'])
                if data['log'] is True:
                    print("in")
                    client = Client.select().where(Client.identifier == upload_id).get()
                    Track.create(timestamp=data['last_update'],
                                 measurements=json.dumps(data['measurements']),
                                 positions=data['positions'],
                                 client=client,
                                 location_id=data['location_id'])
                delete_entries += [upload_id]
            print("======")
        for upload_id in delete_entries:
            self.data.pop(upload_id, None)
