from datetime import datetime, timedelta

from data.models import Auth, Location, Track, Client, Group


def insert_dummy_data():
    Auth.create(username='admin', password='admin')
    Location.create(name='Location 1')
    Location.create(name='Location 2')
    Location.create(name='Location 3')

    Group.create(name='Group 1')
    Group.create(name='Group 2')
    Group.create(name='Group 3')

    Client.create(identifier=1234)

    client = Client.get(identifier=1234)
    location = Location.get(id=2)
    location.map_display = '.jpg'
    location.map_grid = '.gif'
    location.map_scale_start_x = 880
    location.map_scale_start_y = 250
    location.map_scale_end_x = 1320
    location.map_scale_end_y = 250
    location.map_width = 400
    location.save()

    location = Location.get(id=3)
    location.gps_longitude = 7.3991627
    location.gps_latitude = 46.9400988
    location.map_scale_start_x = 2810.1500244140625
    location.map_scale_start_y = 2776.50000
    location.map_scale_end_x = 1730.1499938964844
    location.map_scale_end_y = 2781.50000
    location.map_width = 456
    location.map_display = '.jpg'
    location.map_grid = '.jpg'
    location.save()

    Track.create(timestamp=datetime.now(),
                 measurements="[{'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}]",
                 positions="",
                 client=client,
                 location=location)

    Track.create(timestamp=datetime.now() - timedelta(days=2),
                 measurements="[{'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}]",
                 positions="",
                 client=client,
                 location=location)

    Track.create(timestamp=datetime.now() - timedelta(days=30),
                 measurements="[{'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}]",
                 positions="",
                 client=client,
                 location=location)

    Track.create(timestamp=datetime.now() - timedelta(days=180),
                 measurements="[{'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}]",
                 positions="",
                 client=client,
                 location=location)

    Track.create(timestamp=datetime.now() - timedelta(days=500),
                 measurements="[{'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}, {'t': 20171018205530567, 'a': [0.12, 1.23, 2.34], 'g': [3.45, 4.56, 5.67], 'm': [6.78, 7.89, 8.9], 'r': [['device1', 40], ['device2', 50]]}]",
                 positions="",
                 client=client,
                 location=location)
