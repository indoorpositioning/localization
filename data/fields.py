from datetime import timedelta

from peewee import Field


class DurationField(Field):
    field_type = 'duration'

    def db_value(self, value: timedelta):
        return timedelta.microseconds

    def python_value(self, value):
        return timedelta(microseconds=value)
