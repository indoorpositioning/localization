import os
from typing import List

import numpy
from peewee import *

from algorithms.particlefilter.vector import Vector
from settings import db_settings, MAPS_BASE_PATH

db = SqliteDatabase(db_settings['path'])


class Auth(Model):
    id = PrimaryKeyField()
    username = CharField()
    password = CharField()

    class Meta:
        database = db


class Location(Model):
    id = PrimaryKeyField()  # type: int
    name = CharField()
    floor = SmallIntegerField(null=True)
    gps_latitude = DecimalField(null=True)  # type: int
    gps_longitude = DecimalField(null=True)  # type: int
    map_display = CharField(null=True)
    map_grid = CharField(null=True)
    map_orientation = DecimalField(null=True)  # type: int
    map_width = DecimalField(null=True)  # type: int
    map_pixel_width = DecimalField(null=True)  # type: int
    map_pixel_height = DecimalField(null=True)  # type: int
    map_scale_start_x = DecimalField(null=True)  # type: int
    map_scale_start_y = DecimalField(null=True)  # type: int
    map_scale_end_x = DecimalField(null=True)  # type: int
    map_scale_end_y = DecimalField(null=True)  # type: int
    map_start_x = DecimalField(null=True)  # type: int
    map_start_y = DecimalField(null=True)  # type: int
    proximityUUID = CharField(null=True)  # type: str

    class Meta:
        database = db

    ranging_devices = None  # type: List[RangingDevice]

    def get_grid_image_path(self) -> str:
        """Returns the full image path of the grid map or false if the Location has none yet

        Returns: image path as a string or false

        """
        return self.__get_image_path(self._get_grid_map_name())

    def get_image_path_display(self) -> str:
        """Returns the full image path of the display map or false if the Location has none yet

        Returns: image path as a string or false

        """
        return self.__get_image_path(self._get_display_map_name())

    @staticmethod
    def __get_image_path(map_name):
        file_name = os.path.join(MAPS_BASE_PATH, map_name)
        return file_name if os.path.isfile(file_name) else False

    def can_create_image_array(self) -> bool:
        """ Returns true if everything needed to create an image array is avail

        Returns: true or false

        """
        start_exists = self.map_scale_start_x is not None and self.map_scale_start_y is not None
        end_exists = self.map_scale_start_x is not None and self.map_scale_end_y is not None
        map_width_set = bool(self.map_width)
        file_exists = bool(self.get_grid_image_path())
        return file_exists and start_exists and end_exists and map_width_set

    def get_pixels_per_cm(self) -> float:
        start = Vector(int(self.map_scale_start_x), int(self.map_scale_start_y))
        end = Vector(int(self.map_scale_end_x), int(self.map_scale_end_y))
        return abs(end - start) / float(self.map_width)

    def _get_grid_map_name(self) -> str:
        """Returns the name of the map of the grid

        Returns: Filename without extension

        """
        return str(self.id) + '_grid' + (self.map_grid if self.map_grid is not None else '')

    def _get_display_map_name(self) -> str:
        """Returns the name of the map of the grid

        Returns: Filename without extension

        """
        return str(self.id) + '_display' + self.map_display

    def get_dimensions_pixel(self):
        return Vector(float(self.map_pixel_width), float(self.map_pixel_height))

    def get_dimensions_cm(self):
        return self.get_dimensions_pixel() * float(self.get_pixels_per_cm())

    def get_start_position_cm(self):
        return Vector(int(self.map_start_x), int(self.map_start_y)) / self.get_pixels_per_cm()


class Group(Model):
    id = PrimaryKeyField()
    name = CharField()

    class Meta:
        database = db


class Client(Model):
    id = PrimaryKeyField()
    identifier = CharField()
    group = ForeignKeyField(Group, related_name='clients', null=True)

    class Meta:
        database = db


class Track(Model):
    waypoints_fetched: List['TrackWaypoint'] = None
    id = PrimaryKeyField()
    timestamp = TimestampField()
    measurements = TextField(null=True)
    positions = TextField(null=True)
    client = ForeignKeyField(Client, related_name='tracks')
    location = ForeignKeyField(Location, related_name='tracks', null=True)  # type: Location

    class Meta:
        database = db


class RangingDevice(Model, Vector):
    id = PrimaryKeyField()
    identifier = CharField(null=True)  # type: str
    group = CharField(null=True)
    signal_strength = DecimalField(null=True)
    position_x = DecimalField()  # type: float
    position_y = DecimalField()  # type: float
    alpha = DecimalField(null=True)
    beta = DecimalField(null=True)
    type = CharField()
    location = ForeignKeyField(Location, related_name='ranging_devices')
    power_loss = DecimalField(null=True)
    path_loss_efficient = DecimalField(null=True)

    class Meta:
        database = db

    @property
    def elements(self):
        if (self.position_x is not None) and (self.position_y is not None):
            return numpy.array([int(self.position_x), int(self.position_y)])


class RangingLocation(Model, Vector):
    id = PrimaryKeyField()
    position_x = DecimalField()  # type: float
    position_y = DecimalField()  # type: float
    location = ForeignKeyField(Location, related_name='ranging_locations')
    real_position_x = DecimalField()
    real_position_y = DecimalField()

    class Meta:
        database = db

    @property
    def elements(self):
        if (self.position_x is not None) and (self.position_y is not None):
            return numpy.array([int(self.position_x), int(self.position_y)])


class RangingData(Model):
    id = PrimaryKeyField()
    ranging_device = ForeignKeyField(RangingDevice, related_name='ranging_data')
    ranging_location = ForeignKeyField(RangingLocation, related_name='ranging_data')
    client = ForeignKeyField(Client, related_name='ranging_data', null=True)
    value = DecimalField()

    class Meta:
        database = db


class RangingModel(Model):
    id = PrimaryKeyField()
    name = CharField()
    client = ForeignKeyField(Client, related_name='ranging_model', null=True)
    ranging_device = ForeignKeyField(RangingDevice, related_name='ranging_model')
    data = TextField(null=True)

    class Meta:
        database = db


class TrackWaypoint(Model):
    position_pixel: Vector
    position_cm: Vector
    id = PrimaryKeyField()
    position_number = DecimalField()
    position_x = DecimalField()
    position_y = DecimalField()
    track = ForeignKeyField(Track, related_name='waypoints')

    class Meta:
        database = db

    def compute_vectors(self, pixels_per_cm: float):
        self.position_pixel = Vector(float(self.position_x), float(self.position_y))
        self.position_cm = self.position_pixel / pixels_per_cm


class TrackGeneratorModel(Model):
    id = PrimaryKeyField()
    location = ForeignKeyField(Location)  # type: Location
    route_track_model = ForeignKeyField(Track)  # type: Track
    walk_speed_meter_per_sec = DecimalField()  # type: float
    random_seed = DecimalField()  # type: int
    number_of_tracks = DecimalField()  # type: int
    pdr_average_angle_offset = DecimalField()  # type: float
    pdr_deviation_angle_offset = DecimalField()  # type: float
    pdr_average_step_length = DecimalField()  # type: float
    pdr_deviation_step_factor = DecimalField()  # type: float
    pdr_average_step_factor = DecimalField()  # type: float
    pf_alternative_resample_spread = DecimalField()  # type: int
    pf_number_of_particles = DecimalField()  # type: int
    pf_sigma = DecimalField()  # type: int
    pf_neff_threshold = DecimalField()  # type: float
    pf_known_start = BooleanField()  # type: bool
    pf_with_bresenham = BooleanField()  # type: bool

    class Meta:
        database = db


class GeneratedTrack(Model):
    id = PrimaryKeyField()
    track = ForeignKeyField(Track, related_name='generatedtrack', null=True)  # type: Track
    track_generator = ForeignKeyField(TrackGeneratorModel, related_name='generatedtracks')
    computation_time_seconds = DecimalField(null=True)  # type: float
    random_seed = DecimalField(null=True)  # type: int

    class Meta:
        database = db


class AccessPointSettingsGeneratedTrack(Model):
    id = PrimaryKeyField()
    std_deviation = DecimalField()  # type: float
    average = DecimalField()  # type: float
    track_generator_model = ForeignKeyField(TrackGeneratorModel, related_name='access_point_settings')  # type: Track
    ranging_device = ForeignKeyField(RangingDevice, related_name='access_point_generate_settings')

    # type: RangingDevice

    class Meta:
        database = db
