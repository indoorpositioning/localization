""" Indoor Localization Main App.

Initializes the servers and loads objects used by the entire app.
"""

import asyncio
import sys

import tornado.ioloop
from tornado.platform.asyncio import AnyThreadEventLoopPolicy

from controller import Controller
from data.datastore import DataStore
from servers.admin_server import AdminServer
from servers.api_server import APIServer
from servers.client_server import ClientServer
from servers.display_server import DisplayServer
from servers.login_handler import LoginHandler
from servers.logout_handler import LogoutHandler
from servers.websocket import WebSocket
from settings import MAPS_BASE_PATH, DISPLAY_CLIENT_PATH, NODE_MODULES_PATH

if __name__ == "__main__":

    asyncio.set_event_loop_policy(AnyThreadEventLoopPolicy())

    # Get port
    args = sys.argv[1:]
    if len(args) == 1:
        if 8000 < int(args[0]) < 8100:
            port = int(args[0])
        else:
            port = None
    else:
        port = None

    data_store = DataStore()
    controller = Controller(data_store)

    # ********************#
    # ** Define Routes ** #
    # ********************#

    # Websocket
    websocket_server = tornado.web.Application([
        (r"/", WebSocket, {'controller': controller})
    ])

    if not port:
        # Frontend client
        client_server = tornado.web.Application([
            (r"/maps/(.*)", ClientServer, {"path": MAPS_BASE_PATH}),
            (r"/(test)", DisplayServer, {'data_store': data_store}),
            (r"/(upload-id/[a-zA-Z0-9_]+)", DisplayServer, {'data_store': data_store}),
            (r"/(track-id/[0-9]+)", DisplayServer, {'data_store': data_store}),
            (r"/(\d+)", DisplayServer, {'data_store': data_store}),
            (r"/(?:upload-id/)?(.*)", ClientServer, {"path": DISPLAY_CLIENT_PATH}),
            (r"/node_modules/(.*)", ClientServer, {"path": NODE_MODULES_PATH})
        ])

        # Backend client
        settings = {
            "cookie_secret": "QFY9uLuCPhPFTLGYSoA8DUwwnevc0Y5H",
        }

        admin_server = tornado.web.Application([
            (r"/login", LoginHandler),
            (r"/logout", LogoutHandler),
            (r"/api/(.*)", APIServer),
            (r"/maps/(.*)", AdminServer, {"path": MAPS_BASE_PATH}),
            (r"/(.*)", AdminServer, {"path": "./clients/admin"})
        ], **settings)

    # Create Servers
    if not port:
        websocket_server.listen(8000)
        client_server.listen(8100)
        admin_server.listen(8101)  # PW: adminadmin
    else:
        websocket_server.listen(port)

    # Start IOLoop
    tornado.ioloop.IOLoop.current().start()
