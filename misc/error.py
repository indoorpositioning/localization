class Error(Exception):
    """Base class for exceptions in this module"""

    def print(self):
        print(self.message)
        return {'t': 'e', 'd': {'error': self.key, 'msg': self.message}}


class InputError(Error):
    """Exception raised for errors in the input."""

    def __init__(self, key, message):
        self.key = key
        self.message = message


class DataError(Error):
    """Exception raised for errors caused because the saved data didn't match expectations."""

    def __init__(self, key, message):
        self.key = key
        self.message = message
