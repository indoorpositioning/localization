from datetime import timedelta, datetime
from decimal import Decimal
from json import JSONEncoder
from typing import Union

from numpy import int32, int64, ndarray, float32, float64

from algorithms.particlefilter.particle import Particle
from algorithms.particlefilter.vector import Vector
from data.models import TrackWaypoint
from misc.measurement import Measurement


class APIJSONEncoder(JSONEncoder):

    def default(self, obj):
        if isinstance(obj, TrackWaypoint):
            return self._encode_track_waypoint(obj)
        if isinstance(obj, Particle):
            return self._encode_particle(obj)
        if isinstance(obj, Vector):
            return self._encode_vector(obj)
        if isinstance(obj, Decimal):
            return self._encode_decimal(obj)
        if isinstance(obj, timedelta):
            return self._encode_timedelta(obj)
        if isinstance(obj, int32) or isinstance(obj, int64):
            return self._encode_int(obj)
        if isinstance(obj, float32) or isinstance(obj, float64):
            return self._encode_float(obj)
        if isinstance(obj, Measurement):
            return obj.to_dict()
        if isinstance(obj, datetime):
            return int(obj.timestamp() * 1000)
        if isinstance(obj, ndarray):
            return obj.tolist()
        return JSONEncoder.default(self, obj)

    def _encode_vector(self, obj):
        return {'position_x': obj.x, 'position_y': obj.y}

    def _encode_particle(self, obj):
        particle = self._encode_vector(obj)
        particle['weight'] = obj.weight
        return particle

    def _encode_track_waypoint(self, obj: TrackWaypoint):
        return {"id": obj.id, "position_number": obj.position_number, "position_x": obj.position_x,
                "position_y": obj.position_y, "track_id": obj.track.id}

    def _encode_decimal(self, obj: Decimal):
        return int(obj)

    def _encode_timedelta(self, obj: timedelta):
        return str(obj.total_seconds())

    def _encode_int(self, obj: Union[int32, int64]):
        return int(obj)

    def _encode_float(self, obj: Union[float32, float64]):
        return int(obj)
