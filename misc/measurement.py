import json
from datetime import datetime, timedelta
from enum import Enum
from typing import Dict, List, Union, Iterable

from algorithms.particlefilter.vector import Vector
from data.models import RangingDevice, Track
from misc.utils import datetime_from_uploaded_timestamp


class PositionType(str, Enum):
    rssi_trilaterated = 'rssi_trilaterated'
    pdr = 'pdr'
    ground_truth = 'ground_truth'
    particle_filter = 'particle_filter'
    particles = 'particles'
    start_position = 'start_position'


class RangingData(str, Enum):
    rssi = 'r'
    measured_cm = 'cm'
    measured_pixel = 'pixel'


class Orientations(str, Enum):
    x = 'x'
    y = 'y'
    z = 'z'


class PDRData(str, Enum):
    accelerometer = 'a'
    gyrometer = 'g'
    magnetometer = 'm'


class Measurement(object):
    last_waypoint: int
    delta: timedelta
    timestamp: datetime
    pdr: Dict[PDRData, Dict[Orientations, float]]
    positions_cm: Dict[PositionType, Union[List[Vector], Vector]]
    positions_pixel: Dict[PositionType, Union[List[Vector], Vector]]
    ranging: Dict[RangingDevice, Dict[RangingData, float]]
    message: str
    _raw_data: \
        Dict[str, Union[datetime, int, Dict[PositionType, List[Vector]], dict, List[float], Dict[Orientations, float]]]

    def __init__(self, timestamp: datetime = None) -> None:
        self.delta = None
        self.timestamp = timestamp
        self.positions_cm = {}
        self.positions_pixel = {}
        self.ranging = {}
        self.pdr = {}
        self.message = None
        self.last_waypoint = None

    def to_dict(self):
        ranging = {ranging_device.identifier: position for ranging_device, position in self.ranging.items()}
        dict = {'t': self.timestamp}
        if self.message:
            dict['message'] = self.message
        if self.last_waypoint:
            dict['p'] = self.last_waypoint
        if self.positions_cm:
            dict['positions_cm'] = self.positions_cm
        if self.positions_pixel:
            dict['positions_pixel'] = self.positions_pixel
        if ranging:
            dict['ranging'] = ranging
            dict.update(self.get_ranging_json())

        def xyz_dict_to_array(xyz_dict: Dict[Orientations, float]) -> List[float]:
            return [xyz_dict[Orientations.x], xyz_dict[Orientations.y], xyz_dict[Orientations.z]]

        if self.pdr:
            dict['a'] = xyz_dict_to_array(self.pdr[PDRData.accelerometer])
            dict['g'] = xyz_dict_to_array(self.pdr[PDRData.gyrometer])
            dict['m'] = xyz_dict_to_array(self.pdr[PDRData.magnetometer])
            # if PDRData.vector in self.pdr:
            #     dict['pdr'] = self.pdr[PDRData.vector]

        return dict

    @staticmethod
    def from_dict(json_dict: Dict, location_id: int):
        obj = Measurement()
        obj.timestamp = datetime_from_uploaded_timestamp(int(json_dict['t']))
        positions_cm = dict.get(json_dict, 'positions_cm')
        positions_pixel = dict.get(json_dict, 'positions_pixel')

        def xyz_array_to_dict(xyz_array: List[float]) -> Dict[Orientations, float]:
            return {Orientations.x: xyz_array[0], Orientations.y: xyz_array[1], Orientations.z: xyz_array[2]}

        def load_positions(positions: dict):
            positions_loaded = {}
            for position_type, vector in positions.items():
                if isinstance(vector, list):
                    vector_s = [Vector.from_dict(v) for v in vector]
                else:
                    vector_s = Vector.from_dict(vector)
                positions_loaded[PositionType[position_type]] = vector_s
            return positions_loaded

        if 'a' in json_dict and 'g' in json_dict and 'm' in json_dict:
            obj.pdr = {
                PDRData.accelerometer: xyz_array_to_dict(json_dict['a']),
                PDRData.gyrometer: xyz_array_to_dict(json_dict['g']),
                PDRData.magnetometer: xyz_array_to_dict(json_dict['m'])
            }
        if 'pdr' in json_dict:
            obj.pdr[PDRData.vector] = json_dict['pdr']
        ranging = dict.get(json_dict, 'ranging', dict.get(json_dict, 'r', {}))

        obj.ranging = {}
        if ranging:
            for ranging_device_identifier, data in ranging.items():
                obj.ranging[Measurement._get_ranging_device(ranging_device_identifier, location_id)] = data
        if positions_pixel:
            obj.positions_pixel = load_positions(positions_pixel)
        if positions_cm:
            obj.positions_cm = load_positions(positions_cm)
        if 'p' in json_dict:
            obj.last_waypoint = json_dict['p']
        obj._raw_measurement_data = json_dict
        return obj

    def __contains__(self, item):
        return item in self.raw_measurement_data

    def __getitem__(self, item):
        return self.raw_measurement_data[item]

    @staticmethod
    def from_track(track: Track):
        if isinstance(track.measurements, List):
            return track.measurements
        measurement_objects = []
        for dict in json.loads(track.measurements):
            measurement_objects.append(Measurement.from_dict(dict, track.location))
        return measurement_objects

    known_ranging_devices: Dict[str, RangingDevice] = {}

    @staticmethod
    def _get_ranging_device(ranging_device_identifier: str, location_id: int):
        if ranging_device_identifier not in Measurement.known_ranging_devices:
            ranging_device = RangingDevice.select() \
                .where(RangingDevice.identifier == ranging_device_identifier,
                       RangingDevice.location == location_id).get()
            Measurement.known_ranging_devices[ranging_device_identifier] = ranging_device
        return Measurement.known_ranging_devices[ranging_device_identifier]

    def get_ranging_json(self):
        return {'r': {device.identifier: data[RangingData.rssi] for device, data in self.ranging.items()}}

    def compute_pixel_positions(self, pixels_per_cm: float):
        for key, positions in self.positions_cm.items():
            if isinstance(positions, Iterable):
                self.positions_pixel[key] = [(position * pixels_per_cm).to_int() for position in positions]
            else:
                self.positions_pixel[key] = (positions * pixels_per_cm).to_int()

    @property
    def raw_measurement_data(self):
        if not self._raw_measurement_data:
            self._raw_measurement_data = self.to_dict()
        return self._raw_measurement_data

    # def __mul__(self, other):
    #     """Creates a new measurement with all values except last_waypoint and last_position
    #     multiplied by a scalar"""
    #     if isinstance(other, float):
    #         new_measurement = Measurement()
    #         new_measurement.last_waypoint = self.last_waypoint
    #         new_measurement.start_position = self.start_position
    #         new_measurement.delta = other * self.delta
    #         new_measurement.pdr = {t: {o: vv * other for o, vv in v.items()} for t, v in self.pdr.items()}
    #         new_measurement.positions_cm = {p: [vec * other for vec in v] for p, v in self.positions_cm.items()}
    #         new_measurement.ranging = {d: {t: vv * other for t, vv in v.items()} for d, v in self.ranging.items()}
    #         return new_measurement
    #     return False
    #
    # def __add__(self, other):
    #     """Creates a new measurement with all values except last_waypoint and start_position
    #      added from another measurement. Only works if all available keys are the same. """
    #     if isinstance(other, Measurement):
    #         new_measurement = Measurement()
    #         new_measurement.last_waypoint = self.last_waypoint
    #         new_measurement.start_position = self.start_position
    #         new_measurement.delta = self.delta + other.delta
    #         new_measurement.pdr = {t: {o: vv + other[t][o] for o, vv in v.items()} for t, v in self.pdr.items()}
    #         new_measurement.positions_cm = {p: [vec + other[p][vec] for vec in v] for p, v in self.positions_cm.items()}
    #         new_measurement.ranging = {d: {t: m + other[d][m] for t, m in v.items()} for d, v in self.ranging.items()}
    #         return new_measurement
    #     return False
