# This adds position numbers to measurements when timestamps are separately recorded.
import datetime
import json
from typing import Dict, List

import numpy

from data.models import Track
from misc.utils import datetime_from_uploaded_timestamp

if __name__ == '__main__':
    track_id = 130
    track = Track.get(Track.id == track_id)
    measurements = json.loads(track.measurements)
    measurement: List[Dict[str, object]] = measurements[0]
    first = datetime_from_uploaded_timestamp(measurement['t']).timestamp()
    hunderdth_seconds_between = numpy.array([340, 306, 256, 444, 585,
                                             438, 336, 249, 510, 256,
                                             376, 526, 526, 666, 1034,
                                             293, 453, 242, 403, 477,
                                             510], dtype=float)
    millisec_should = numpy.cumsum(hunderdth_seconds_between / 100.0)
    millisec_should += first
    current_position = 0
    for measurement in measurements:
        millisec_recorded = measurement['t']
        if datetime_from_uploaded_timestamp(millisec_recorded) > datetime.datetime.fromtimestamp(
                millisec_should[current_position]):
            current_position += 1
        measurement['p'] = current_position
    track.measurements = json.dumps(measurements)
    track.save()
