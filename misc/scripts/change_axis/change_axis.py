"""Changes the axis of the IMU recordings of a track. Corrects the"""
import copy
import json
from typing import Callable, List, Dict

from data.models import Track
from misc.json_encoder import APIJSONEncoder
from misc.measurement import Measurement, Orientations, PDRData

if __name__ == '__main__':

    axis_switching_lambda: List[Callable[[Dict[Orientations, float]], Dict[Orientations, float]]] = [
        (lambda d: {'x': -d['y'], 'y': d['x'], 'z': d['z']}),
        (lambda d: {'x': d['y'], 'y': -d['x'], 'z': d['z']}),
        (lambda d: {'x': -d['x'], 'y': -d['y'], 'z': d['z']}),
    ]
    track_id = 135  # -> 136, 137, 138
    track = Track.get(Track.id == track_id)
    measurements = Measurement.from_track(track)
    for func in axis_switching_lambda:
        new_track: Track = Track()
        new_track.timestamp = track.timestamp
        new_track.positions = track.positions
        new_track.client = track.client
        new_track.location = track.location
        copied_measurements = copy.deepcopy(measurements)
        for measurement in copied_measurements:
            if measurement.pdr:
                measurement.pdr[PDRData.accelerometer] = func(measurement.pdr[PDRData.accelerometer])
                measurement.pdr[PDRData.magnetometer] = func(measurement.pdr[PDRData.magnetometer])
                measurement.pdr[PDRData.gyrometer] = func(measurement.pdr[PDRData.gyrometer])
        new_track.measurements = json.dumps(copied_measurements, cls=APIJSONEncoder)
        new_track.save()
        print("New track saved with id " + str(new_track.id))

# measurement.pdr

# print("Saved on id" + new_id)

# Outcomment the line if you really want it
# trackway_waypoint.save()
