"""Corrects the magnetometer orientation."""
import json

from data.models import Track
from misc.json_encoder import APIJSONEncoder
from misc.measurement import Measurement, Orientations, PDRData

if __name__ == '__main__':

    track_ids = [132, 133, 134, 135]
    tracks = Track.select().where(Track.id.in_(track_ids))
    for track in tracks:
        measurements = Measurement.from_track(track)
        for measurement in measurements:
            if measurement.pdr:
                new_magnetomer = {}
                magnetomer = measurement.pdr[PDRData.magnetometer]
                new_magnetomer[Orientations.x] = magnetomer[Orientations.y]
                new_magnetomer[Orientations.y] = magnetomer[Orientations.x]
                new_magnetomer[Orientations.z] = -magnetomer[Orientations.z]
                measurement.pdr[PDRData.magnetometer] = new_magnetomer
        track.measurements = json.dumps(measurements, cls=APIJSONEncoder)
        # track.save()
        print("Axis swapped on id " + str(track.id))
