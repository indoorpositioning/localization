# Converts the accelerometer values from g to m/s^2 and multiplies them by -1
import json
from typing import Dict, List

from data.models import Track

if __name__ == '__main__':
    track_ids = list(range(93, 107))
    track_ids.extend(list(range(109, 111)))
    waypoints = Track.select().where(Track.id.in_(track_ids))
    track: Track
    for track in waypoints:
        measurements: List[Dict[str, object]] = json.loads(track.measurements)
        for measurement in measurements:
            if 'a' in measurement:
                accelerometer_values: List[int] = measurement['a']
                if max(measurement['a']) > 9:
                    print("You made a mistake [" + str(track.id) + "]")
                    exit()
                measurement['a'] = [value * -9.81 for value in accelerometer_values]
        track.measurements = json.dumps(measurements)
        print("Converted " + str(track.id))
        track.save()
