# This copies Trackwaypoints from one track to another
from data.models import TrackWaypoint


def clone_and_save_waypoint(waypoint: TrackWaypoint, new_track_id: int):
    new_waypoint = TrackWaypoint()
    new_waypoint.position_x = waypoint.position_x
    new_waypoint.position_y = waypoint.position_y
    new_waypoint.position_number = waypoint.position_number
    new_waypoint.track = new_track_id
    # new_waypoint.save()


if __name__ == '__main__':
    # track_id = 133
    # new_track_ids = [136]
    # track_id = 134
    # new_track_ids = [137]
    # track_id = 133
    # new_track_ids = [139, 142, 145]
    # track_id = 134
    # new_track_ids = [140, 143, 146]
    track_id = [133, 134, 135]
    new_track_ids = [
        [148, 151],
        [149, 152],
        [150, 153],
    ]
    for old_track_id, new_track_id in zip(track_id, new_track_ids):
        waypoints = TrackWaypoint.select().where(TrackWaypoint.track == old_track_id)
        trackway_waypoint: TrackWaypoint
        for new_id in new_track_id:
            for trackway_waypoint in waypoints:
                clone_and_save_waypoint(trackway_waypoint, new_id)
