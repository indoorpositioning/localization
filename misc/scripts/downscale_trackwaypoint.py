# This scales down the trackwaypoints made with a bigger map to be used on a smaller map resolution
from data.models import TrackWaypoint

if __name__ == '__main__':
    track_id = 102
    old_resolution_x = 3675
    new_resolution_x = 2000
    scale_factor = new_resolution_x / old_resolution_x
    waypoints = TrackWaypoint.select().where(TrackWaypoint.track == track_id)
    trackway_waypoint: TrackWaypoint
    for trackway_waypoint in waypoints:
        trackway_waypoint.position_x = float(trackway_waypoint.position_x) * scale_factor
        trackway_waypoint.position_y = float(trackway_waypoint.position_y) * scale_factor
        # Outcomment the line if you really want it
        # trackway_waypoint.save()
