""" Add ranging and pdr to the track, then create a new track it """
import json

from algorithms.particlefilter.path_generator import PathGenerator
from algorithms.ranging.ranging import Ranging
from algorithms.rotations.rotations import Rotations
from data.models import Track
from misc.json_encoder import APIJSONEncoder
from misc.track_preparer import TrackPreparer

if __name__ == '__main__':
    track_ids = [133, 134, 135]
    walk_vector_settings = [
        [1.0, 0.2, 1, 5],
        [1.0, 0.2, 2, 10],
        [1.0, 0.2, 3, 15],
        [1.0, 0.2, 4, 20],
        [1.0, 0.2, 5, 25],
        [1.0, 0.2, 6, 30],
    ]
    tracks = Track.select().where(Track.id.in_(track_ids))
    for settings in walk_vector_settings:
        for track in tracks:
            track_preparer = TrackPreparer(track=track, ranging=Ranging(track.location.id), real_rotations=Rotations(),
                                           track_rotations=PathGenerator(track))
            if not track.generatedtrack:
                # track_preparer.append_rotations()
                if len(track.waypoints) > 0:
                    print("Generating exact vectors")
                    track_preparer.append_exact_vectors()
                    track_preparer.append_walk_vectors(stretch_average=settings[0], strech_deviation=settings[1],
                                                       angle_average=settings[2], angle_deviation=settings[3])
                else:
                    track_preparer.append_location_start_position()
                    print("No or wrong number of track waypoints found. "
                          "Expecting two more than the button was pressed (no pressing at start and end, but a waypoint there)")
                track_preparer.append_ranging()
                track_preparer.append_trilaterated_positions()
            track_preparer.set_time_deltas()
            track_preparer.generate_cm_positions()
            new_track = Track()
            new_track.timestamp = track.timestamp
            new_track.client = track.client
            new_track.location = track.location
            new_track.measurements = json.dumps(track_preparer.get_measurements(), cls=APIJSONEncoder)
            new_track.save()
