# This removes measurings from access points (for example when it was not positioned correctly)
import json
from typing import Dict, List

from data.models import Track

if __name__ == '__main__':
    track_ids = list(range(94, 103))
    access_point2remove_mac = 'c0:a0:bb:18:83:8a'
    waypoints = Track.select().where(Track.id.in_(track_ids))
    track: Track
    for track in waypoints:
        measurements: List[Dict[str, object]] = json.loads(track.measurements)
        rangings: List[Dict[str, int]] = [measurement['r'] for measurement in measurements if 'r' in measurement]
        values2remove = [ranging.pop(access_point2remove_mac) for ranging in rangings if
                         access_point2remove_mac in ranging]
        print("Removes " + str(len(values2remove)) + "records")
        # Remove dictionaries of 'r' that are now empty
        for measurement in measurements:
            if 'r' in measurement and not measurement['r']:
                measurement.pop('r')
        track.measurements = json.dumps(measurements)
        track.save()
