# This adds position numbers to measurements when timestamps are separately recorded.
import datetime
import json
from typing import Dict, List

import numpy

from data.models import Track
from misc.utils import datetime_from_uploaded_timestamp

if __name__ == '__main__':
    track_id = 135
    track = Track.get(Track.id == track_id)
    measurements = json.loads(track.measurements)
    measurement: List[Dict[str, object]] = measurements[0]
    first = datetime_from_uploaded_timestamp(measurement['t']).timestamp()
    measurements_with_a = [m for m in measurements if 'a' in m]
    for i, measurement in enumerate(measurements_with_a[3:]):
        if measurement['a'][0] == measurements_with_a[i - 1]['a'][0] \
                and measurements_with_a[i - 1]['a'][0] == measurements_with_a[i - 2]['a'][0]:
            print("Same at " + str(i - 2))
            last = datetime_from_uploaded_timestamp(measurements_with_a[i - 2]['t']).timestamp()
            print("After " + str(last - first))
            break
