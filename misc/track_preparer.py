# Creates a dictionary chronologically ordered with all data from a track and computation results in requested level
import copy
import json
from datetime import datetime
from datetime import timedelta
from typing import List, Dict, Union, Optional, Tuple

from numpy import cumsum

from algorithms.particlefilter.particle_filter import ParticleFilter
from algorithms.particlefilter.path_generator import PathGenerator
from algorithms.particlefilter.trilateration import Trilateration
from algorithms.particlefilter.vector import Vector
from algorithms.ranging.ranging import Ranging
from algorithms.rotations.rotations import Rotations
from data.models import Track, RangingDevice
from misc.json_encoder import APIJSONEncoder
from misc.measurement import Measurement, RangingData, PositionType, PDRData


class TrackPreparer(object):
    measurements: List[Dict[Union[datetime, str], object]]
    track: Track
    particle_filter: ParticleFilter
    path_generator: PathGenerator
    rotations: Optional[Rotations]
    ranging: Ranging
    relative_timed: bool

    def __init__(self, track: Track, ranging: object = None, real_rotations: object = None,
                 track_rotations: object = None, particle_filter: object = None) -> object:
        self.rotations = real_rotations
        self.ranging = ranging
        self.path_generator = track_rotations
        self.particle_filter = particle_filter
        self.track = track
        self.measurements: List[Measurement] = Measurement.from_track(track)
        self.relative_timed = False

    def set_time_deltas(self):
        """
            Changes the timestamp keys to be datetime as offset from the first record
        """
        first = self.measurements[0].timestamp
        self.measurements[0].delta = timedelta()
        for i, measurement in enumerate(self.measurements[1:]):
            measurement.delta = measurement.timestamp - self.measurements[i - 1].timestamp
        self.relative_timed = True

    def filter_ranging_device(self, identifier: str):
        """
            Removes an access point from measurings
        Args:
            identifier: Ranging device identifier
        """
        for measurment in self.measurements:
            if measurment.ranging and identifier in measurment.ranging:
                for ranging_device in copy.deepcopy(measurment.ranging.keys()):
                    if identifier is ranging_device.identifier:
                        measurment.ranging.pop(ranging_device)

    def append_rotations(self):
        """
            Adds the rotations
        """
        # i = 0
        step_times = []
        vectors = []
        for measurment in self.measurements:
            if measurment.pdr:
                vector = self.rotations.update(measurment)
                if vector:
                    step_times.append(measurment.timestamp)
                    vectors.append(vector)
        self._append_vectors(step_times, vectors, PositionType.pdr)
        # i += 1

    def observeRotation(self, x: int, y: int):
        self.current_measurement.pdr[PDRData.vector] = Vector(x, y)

    def append_exact_vectors(self):
        """
            Adds the step vectors generated from track waypoints as key (timestamp) with a vector as value
            and sets relative times inserted such that the chronological order is kept
        """
        if any(PositionType.ground_truth in measurement.positions_cm for measurement in self.measurements):
            return
        self.set_time_deltas()
        assert isinstance(self.path_generator, PathGenerator)
        step_vectors = self.path_generator.generate_exact_vectors()
        step_times = self.path_generator.generate_exact_vectors_step_times()
        self._append_vectors(step_times, step_vectors, PositionType.ground_truth)

    def append_walk_vectors(self, stretch_average=1.0, strech_deviation=0.2, angle_average=0, angle_deviation=0):
        """
            Adds the step vectors generated from track waypoints as key (timestamp) with a vector as value
            and sets relative times inserted such that the chronological order is kept
        """
        if any(PositionType.pdr in measurement.positions_cm for measurement in self.measurements):
            return
        self.set_time_deltas()
        assert isinstance(self.path_generator, PathGenerator)
        step_vectors = self.path_generator.generate_walk_vectors(angle_average, angle_deviation, stretch_average,
                                                                 strech_deviation)
        step_times = self.path_generator.generate_exact_vectors_step_times()
        self._append_vectors(step_times, step_vectors, PositionType.pdr)

    # def append_rotations_vectors(self):
    #     """
    #         Adds the vectors from rotations
    #     """
    #     assert isinstance(self.rotations, Rotations)
    #     self.rotations.update()

    def _append_vectors(self, step_times, step_vectors, key: PositionType):
        measurement_index = 0
        step_index = 0
        positions: List[Vector] = cumsum(step_vectors)
        for step_time in step_times:
            # print("B " + str(measurement_index))
            while not (self.measurements[measurement_index].timestamp <= step_time <=
                       self.measurements[measurement_index + 1].timestamp
                       and measurement_index + 1 < len(self.measurements)):
                # print("A " + str(measurement_index))
                measurement_index += 1
            measurement_index += 1
            if step_time == self.measurements[measurement_index].timestamp:
                self.measurements[measurement_index].positions_cm[key] = positions[step_index]
            else:
                measurement_index += 1
                measurement = Measurement(step_time)
                measurement.positions_cm[key] = step_vectors[step_index]
                self.measurements.insert(measurement_index, measurement)
            step_index += 1

    def append_ranging(self):
        """
            Calculates rangings and append it to measurements with key by converting the current
            'r' value to a dictionary with keys RSSI (origin value) and cm, both int values
        """

        def measurement_with_cm(measurement: Measurement):
            return len(measurement.ranging) > 0 and RangingData.measured_cm in list(measurement.ranging.values())[0]

        if any(measurement_with_cm(measurement) for measurement in self.measurements):
            return
        assert isinstance(self.ranging, Ranging)
        for measurement in self.measurements:
            ranging_results = self.ranging.update([measurement.get_ranging_json()],
                                                  self.track.client.identifier)
            to_be_popped = []
            for device in measurement.ranging:
                if ranging_results[device.identifier] and ranging_results[device.identifier] >= 0:
                    measurement.ranging[device][RangingData.measured_cm] = ranging_results[device.identifier]
                    measurement.ranging[device][RangingData.measured_pixel] = \
                        ranging_results[device.identifier] * self.track.location.get_pixels_per_cm()
                else:
                    to_be_popped.append(device)
                    print("Malformed response from ranging or malformed measurement "
                          "for ap_identifier " + device.identifier + " at time " + str(measurement['t']))
            for device in to_be_popped:
                measurement.ranging.pop(device)

    def append_particle_filter_start_position(self):
        """
            Adds the starting position from TrackWaypoints to the particle filter.
        """
        assert isinstance(self.path_generator, PathGenerator)
        position = self.path_generator.get_start_position()
        self.particle_filter.start_position = position

    def append_location_start_position(self):
        """
            Adds the starting position from TrackWaypoints to the particle filter.
        """
        position = self.path_generator.get_start_position()
        self.measurements[0].positions_cm[PositionType.start_position] = position
        print(json.dumps(position, cls=APIJSONEncoder))

    def append_particle_filter(self, with_particles: bool = False):
        """
            Applies the particle filter to the ranging, the IMU data (if rotations provided) and TrackRotations.
        """
        assert isinstance(self.particle_filter, ParticleFilter)
        if not with_particles and any(PositionType.particle_filter in m.positions_cm for m in self.measurements) \
                or any(PositionType.particles in m.positions_cm for m in self.measurements):
            return
        idx = 0
        start_index = next((i for i, measurement in enumerate(self.measurements) if measurement.last_waypoint == 1))
        for measurement in self.measurements[start_index:]:
            # if isinstance(measurement.last_waypoint, int) and measurement.last_waypoint > 4:
            #     break
            if PositionType.particle_filter in measurement.positions_cm:
                measurement.positions_cm.pop(PositionType.particle_filter)  # <- recompute
            if PositionType.particle_filter not in measurement.positions_cm:
                idx += 1
                if PositionType.pdr in measurement.positions_cm:
                    # print("Update pf pdr " + str(idx) + " " + str(position))
                    self.particle_filter.update(rotations=measurement.positions_cm[PositionType.pdr])
                    # print("-> Update pf done")
                if len(measurement.ranging.values()) > 0 and \
                        RangingData.measured_cm in list(measurement.ranging.values())[0]:
                    # print("Update pf ranging " + str(idx))
                    position, msg = self.particle_filter.update(ranging_data=measurement.ranging)
                    # print("-> Update pf")
                    if msg:
                        measurement.message = msg
                    if position:
                        measurement.positions_cm[PositionType.particle_filter] = position
                        if with_particles:
                            measurement.positions_cm[PositionType.particles] = copy.deepcopy(
                                self.particle_filter.particles)

    def append_trilaterated_positions(self):
        """Adds the positions calculated from triangulated rssi"""
        ranging_devices = set([])
        if any(PositionType.rssi_trilaterated in measurement.positions_cm for measurement in self.measurements):
            return
        for measurement in self.measurements:
            ranging_devices = ranging_devices.union(measurement.ranging.keys())
        measurements_with_ranging = [measurement for measurement in self.measurements if measurement.ranging]
        # For each measurement find the previous and next rssi reading of each ranging device
        # Structure: Previous ranging at time of device is found at time and data is ...
        previous_rangings: Dict[datetime, Dict[RangingDevice, Tuple[datetime, Dict[RangingData, float]]]] = {}
        next_rangings: Dict[datetime, Dict[RangingDevice, Tuple[datetime, Dict[RangingData, float]]]] = {}
        # rangings_till_now = {device: {self.measurements[0].timestamp: ()} for device in ranging_devices}
        rangings_till_now = {}
        for measurement in measurements_with_ranging:
            new_rangings = {device: (measurement.timestamp, data) for device, data in measurement.ranging.items()}
            rangings_till_now.update(new_rangings)
            previous_rangings[measurement.timestamp] = {de: copy.copy(t) for de, t in rangings_till_now.items()}
        # rangings_after_now = {}
        # for measurement in measurements_with_ranging[:-1:]:
        #     new_rangings = {device: (measurement.timestamp, data) for device, data in measurement.ranging.items()}
        #     rangings_after_now.update(new_rangings)
        #     next_rangings[measurement.timestamp] = {de: copy.copy(t) for de, t in rangings_after_now.items()}
        # extrapolated_rangings = {}
        # for time, previous_ranging in previous_rangings.items():
        #     for device, ranging_tuple in previous_ranging.items():
        #         if next_rangings[time][device]:
        #             extrapolated_rangings
        rangings_as_available = [{device: tuple[1][RangingData.measured_cm] for device, tuple in values.items()}
                                 for values in previous_rangings.values()]
        trilateration = Trilateration()
        positions = trilateration.ap_distances(rangings_as_available)
        # Add the positions to the corresponding measurements again
        for position, measurement in zip(positions, measurements_with_ranging):
            if position:
                measurement.positions_cm[PositionType.rssi_trilaterated] = position

    def get_measurements(self):
        return self.measurements

    def generate_cm_positions(self):
        for measurement in self.measurements:
            measurement.compute_pixel_positions(self.track.location.get_pixels_per_cm())
