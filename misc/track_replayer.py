import json
from datetime import datetime
from threading import Timer
from typing import Dict, List

from algorithms.particlefilter.particle_filter import ParticleFilter
from algorithms.particlefilter.path_generator import PathGenerator
from algorithms.particlefilter.vector import Vector
from algorithms.ranging.ranging import Ranging
from algorithms.rotations.rotations import Rotations
from data.models import Track
from misc.json_encoder import APIJSONEncoder
from misc.track_preparer import TrackPreparer
from servers.websocket import WebSocket


class TrackReplayer(object):
    track: Track
    measurements: [Dict[str, object]]
    particle_filter: ParticleFilter
    exact_start_times: List[datetime]
    exact_vectors: List[Vector]
    start_time: datetime
    start_times: List[datetime]

    def __init__(self, websocket: WebSocket, track: Track, ranging: Ranging, rotations: Rotations,
                 track_rotations: PathGenerator, particle_filter: ParticleFilter):
        self.rotations = rotations
        self.start_time = None
        self.measurement_index = 0
        self.track_rotations = track_rotations
        self.ranging = ranging
        self.particle_filter = particle_filter
        self.websocket = websocket
        self.client_id = track.client.id
        self.track = track
        self.current_exact_vector = 0

    def start(self):
        track_preparer = TrackPreparer(track=self.track, ranging=self.ranging, real_rotations=self.rotations,
                                       track_rotations=self.track_rotations, particle_filter=self.particle_filter)
        if not self.track.generatedtrack:
            if len(self.track.waypoints_fetched) > 0:
                print("Generating exact vectors")
                # track_preparer.append_rotations()
                track_preparer.append_exact_vectors()
                track_preparer.append_walk_vectors()
            else:
                print("No or wrong number of track waypoints found. "
                      "Expecting two more than the button was pressed (no pressing at start and end, but a waypoint there)")
        track_preparer.append_ranging()
        track_preparer.append_trilaterated_positions()
        track_preparer.append_particle_filter_start_position()
        track_preparer.append_particle_filter(with_particles=True)
        track_preparer.append_location_start_position()
        track_preparer.set_time_deltas()
        track_preparer.generate_cm_positions()
        self.measurements = track_preparer.get_measurements()
        self.start_time = datetime.now()
        # self.start_times = [self.start_time + measurement.timestamp for measurement in self.measurements]
        Timer(self.measurements[0].delta.total_seconds(), self.update).start()

    def update(self):
        # print("Sending measurement " + str(self.measurement_index))
        current_measurement = self.measurements[self.measurement_index]
        # position = self.particle_filter.update(ranging_results, steps_to_send)
        self.websocket.send_message(json.dumps(current_measurement, cls=APIJSONEncoder))
        self.measurement_index += 1
        if self.measurement_index < len(self.measurements):
            # print("Next in " + str(next_delta) + "seconds.")
            Timer(self.measurements[self.measurement_index].delta.total_seconds(), self.update).start()
        # else:
        #     self.start_time = datetime.now()
        #     self.measurement_index = 0
        #     Timer(20, self.update).start()
