from datetime import datetime


def get_or_list(element_or_list) -> []:
    """
        Wraps the passed element in a list if it's not already a list
    Args:
        element_or_list:

    Returns:

    """
    return element_or_list if type(element_or_list) == list else [element_or_list]


# def put_or_create(dict, element) -> {}:
#     """
#         Adds an element to dict if that is a dict, otherwise creates and returns a new dict
#     Args:
#         element:
#
#     Returns:
#
#     """

def datetime_from_uploaded_timestamp(timestamp: int) -> datetime:
    """
        Returns a datetime object from a uploaded timestamp (millis or YYYYmmddHHMMssMMM)
    Args:
        timestamp: string YYYYmmddHHMMssMMM (M = milliseconds) or millis since epoch

    Returns:
        A datetime object
    """
    if timestamp > 20180000000000000:
        return datetime.strptime(str(timestamp * 1000), '%Y%m%d%H%M%S%f')
    else:
        return datetime.fromtimestamp(timestamp / 1000)


def epoch_millis_from_timestamp(timestamp: int) -> int:
    """
        Converts an uploaded timestamp to an epoch timestamp in millis.
        If format doesn't match, nothing is done.
    Args:
        timestamp: string YYYYmmddHHMMssMMM (M = milliseconds)

    Returns:
        Millis since 1970
    """
    try:
        return int(datetime_from_uploaded_timestamp(timestamp).timestamp() * 1000 + timestamp % 1000)
    except OSError:
        return timestamp