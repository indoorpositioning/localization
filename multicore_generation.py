import sys
from multiprocessing.pool import ThreadPool
from typing import List, Tuple

from algorithms.particlefilter.test_particle_filter import generate_tracks
from data.models import TrackGeneratorModel, GeneratedTrack

progress: List[str] = []


def update():
    to_print = ""
    for id, status in enumerate(progress):
        to_print += str(id) + ": " + status + ", "
    print(to_print, "\r", flush=True)


def generate_by_ids(ids_thread_tuple: Tuple):
    thread_id, ids = ids_thread_tuple
    done = 0
    for id in ids:
        if len(progress) == 1:
            progress[0] = "Thread " + str(thread_id) + ": " + str(done) + "/" + str(len(ids)) + " " + str(id)
        else:
            progress[thread_id] = str(done) + "/" + str(len(ids))
        update()
        generate_tracks(id)
        done += 1


if __name__ == '__main__':
    generator_models_done: List[TrackGeneratorModel] = TrackGeneratorModel.select(TrackGeneratorModel.id) \
        .join(GeneratedTrack)
    track_generator_models: List[TrackGeneratorModel] = TrackGeneratorModel.select(TrackGeneratorModel) \
        .where(TrackGeneratorModel.id.not_in(generator_models_done))
    generated_track_ids = [generated_track.id for generated_track in track_generator_models]
    tracks = len(generated_track_ids)
    threads = 32
    tracks_per_thread: List[List[int]] = [[] for i in range(threads)]
    progress = [[] for i in range(threads)]
    for index, track_id in enumerate(generated_track_ids):
        tracks_per_thread[index % threads].append(track_id)
    if len(sys.argv) > 0:
        thread_id = int(sys.argv[1])
        progress = [""]
        generate_by_ids((thread_id, tracks_per_thread[thread_id]))
    else:
        pool = ThreadPool(threads)
        pool.map(generate_by_ids, enumerate(tracks_per_thread))
