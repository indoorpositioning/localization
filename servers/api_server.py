import json
from collections import namedtuple
from shutil import copyfile

import tornado.web
from PIL import Image

import settings
from algorithms.graph.graph import Graph
from algorithms.ranging.enhanced.final.controller import EnhancedRanging
from algorithms.ranging.ranging_measurements import RangingMeasurements
from data.dummy_data import insert_dummy_data
from data.models import *


def split_path(path) -> (str, int):
    """Splits the path to the area (=function) and id as the called parameter
    Args:
        path: The called URI-path (in form of area[/id])

    Returns: A named tuple containing the area and the id or False if no id is provided

    """
    elements = path.split('/')
    Result = namedtuple('Path', 'area id')
    area = elements[0]
    id = elements[1] if len(elements) == 2 else False
    return Result(area, id)


def reset_database():
    Auth.drop_table(True)
    Location.drop_table(True)
    RangingDevice.drop_table(True)
    Client.drop_table(True)
    Group.drop_table(True)
    Track.drop_table(True)
    RangingLocation.drop_table(True)
    RangingData.drop_table(True)
    RangingModel.drop_table(True)
    db.create_tables(
        [Auth, Location, RangingDevice, Client, Group, Track, RangingLocation, RangingData, RangingModel])
    insert_dummy_data()
    return json.dumps({'success': 1})


def get_locations():
    locations = []
    for location in Location.select():
        locations.append({
            'id': location.id,
            'name': location.name,
            'floor': location.floor
        })
    return json.dumps(locations)


def get_location(id):
    location = Location.get(Location.id == id)
    location_data = ({
        'id': location.id,
        'name': location.name,
        'floor': location.floor,
        'gps_latitude': float(location.gps_latitude) if location.gps_latitude else 0,
        'gps_longitude': float(location.gps_longitude) if location.gps_longitude else 0,
        'map_display': location.map_display,
        'map_grid': location.map_grid,
        'map_orientation': float(location.map_orientation) if location.map_orientation else 0,
        'map_width': float(location.map_width) if location.map_width else 0,
        'map_scale_start_x': float(location.map_scale_start_x) if location.map_scale_start_x else 0,
        'map_scale_start_y': float(location.map_scale_start_y) if location.map_scale_start_y else 0,
        'map_scale_end_x': float(location.map_scale_end_x) if location.map_scale_end_x else 0,
        'map_scale_end_y': float(location.map_scale_end_y) if location.map_scale_end_y else 0,
        'map_start_x': float(location.map_start_x) if location.map_start_x else 0,
        'map_start_y': float(location.map_start_y) if location.map_start_y else 0,
    })

    ranging_devices = []
    for device in RangingDevice.select().where(RangingDevice.location == location):
        ranging_devices.append({
            'id': device.id,
            'identifier': device.identifier,
            'group': device.group,
            'signal_strength': float(device.signal_strength) if device.signal_strength else 0,
            'position_x': float(device.position_x) if device.position_x else 0,
            'position_y': float(device.position_y) if device.position_y else 0,
            'alpha': float(device.alpha) if device.alpha else 0,
            'beta': float(device.beta) if device.beta else 0,
            'type': device.type
        })

    ranging_locations = []
    for ranging_location in RangingLocation.select().where(RangingLocation.location == location):
        ranging_locations.append({
            'id': ranging_location.id,
            'position_x': float(ranging_location.position_x) if ranging_location.position_x else 0,
            'position_y': float(ranging_location.position_y) if ranging_location.position_y else 0
        })

    return ({
        'location': location_data,
        'ranging_devices': ranging_devices,
        'ranging_locations': ranging_locations
    })


def delete_location(id):
    location = Location.get(Location.id == id)
    location.delete_instance()
    return '{"success": 1}'


def post_location(name):
    Location.create(name=name)
    return '{"success": 1}'


def patch_location(id, field, value):
    query = Location.update(**{field: value}).where(Location.id == id)
    query.execute()
    if field == "map_width":
        # pass
        _update_image_array(id)  # TODO (Patrick): Produces an error -> @Stefan: Because wrong user input
    return '{"success": 1}'


def post_file(id, self):
    type = self.get_argument('type')
    fileinfo = self.request.files['map'][0]

    filename = fileinfo['filename']
    extension = os.path.splitext(filename)[1]
    new_name = str(id) + '_' + type + extension
    fh = open(settings.MAPS_BASE_PATH + '/' + new_name, 'wb')
    fh.write(fileinfo['body'])

    db_field = 'map_' + type
    query = Location.update(**{db_field: extension}).where(Location.id == id)
    query.execute()
    _update_image_array(id)  # TODO (Patrick): Produces an error -> @Stefan: Because wrong user input

    return '{"success": 1}'


def post_waypoint(waypointNumber, trackId, left, top):
    waypointId = TrackWaypoint.create(position_number=waypointNumber, track=trackId, position_x=left, position_y=top).id
    return '{"success": 1, "id": ' + str(waypointId) + '}'


def post_tool(locationID, toolClass, top, left):
    location = Location.get(Location.id == locationID)
    if toolClass == 'wifi' or toolClass == 'bluetooth':
        RangingDevice.create(position_x=left, position_y=top, type=toolClass, location=location)
        last_id = RangingDevice.select().order_by(RangingDevice.id.desc()).get()
        return '{"success": 1, "id": ' + str(last_id.id) + '}'
    elif toolClass == 'plus':  # measuring
        if location.map_scale_start_x:
            location.map_scale_end_x = left
            location.map_scale_end_y = top
        else:
            location.map_scale_start_x = left
            location.map_scale_start_y = top
        location.save()
        # _update_image_array(location.id) # TODO (Patrick): Results in error: line 76, in _get_grid_map_name:  TypeError: must be str, not NoneType
    elif toolClass == 'media-play':  ## start
        query = Location.update(map_start_x=left, map_start_y=top).where(Location.id == locationID)
        query.execute()
    elif toolClass == 'audio':  ## Ranging Location
        RangingLocation.create(position_x=left, position_y=top, location=location)
        last_id = RangingLocation.select().order_by(RangingLocation.id.desc()).get()
        return '{"success": 1, "id": ' + str(last_id.id) + '}'

    return '{"success": 1}'


def patch_waypoint(waypoint_id, self):
    data = {}
    for k in self.request.arguments:
        data[k] = self.get_argument(k)

    update = {}

    for k in data:
        if k == 'left':
            update['position_x'] = data['left']
        elif k == 'top':
            update['position_y'] = data['top']

    if len(update) > 0:
        query = TrackWaypoint.update(**update).where(TrackWaypoint.id == waypoint_id)
        query.execute()
    return '{"success": 1}'


def patch_tool(toolID, self):
    data = {}
    for k in self.request.arguments:
        data[k] = self.get_argument(k)

    update = {}

    if data['toolClass'] == 'wifi' or data['toolClass'] == 'bluetooth':

        for k in data:
            if k == 'left':
                update['position_x'] = data['left']
            elif k == 'top':
                update['position_y'] = data['top']
            elif hasattr(RangingDevice, k):
                update[k] = data[k]

        if len(update) > 0:
            query = RangingDevice.update(**update).where(RangingDevice.id == toolID)
            query.execute()

        return '{"success": 1}'

    elif data['toolClass'] == 'audio':

        for k in data:
            if k == 'left':
                update['position_x'] = data['left']
            elif k == 'top':
                update['position_y'] = data['top']
            elif hasattr(RangingLocation, k):
                update[k] = data[k]

        if len(update) > 0:
            query = RangingLocation.update(**update).where(RangingLocation.id == toolID)
            query.execute()

        return '{"success": 1}'

    elif data['toolClass'] == 'plus':

        if toolID == 'start':
            if 'left' in data:
                update['map_scale_start_x'] = data['left']
            if 'top' in data:
                update['map_scale_start_y'] = data['top']
        if toolID == 'end':
            if 'left' in data:
                update['map_scale_end_x'] = data['left']
            if 'top' in data:
                update['map_scale_end_y'] = data['top']

    elif data['toolClass'] == 'media-play':
        if 'left' in data:
            update['map_start_x'] = data['left']
        if 'top' in data:
            update['map_start_y'] = data['top']

    if len(update) > 0:
        location_id = data['locationID']
        # _update_image_array(location_id) # TODO (Patrick): Same error...
        query = Location.update(**update).where(Location.id == location_id)
        query.execute()
    return '{"success": 1}'


def _update_image_array(location_id: int):
    location = Location.get(Location.id == location_id)
    if location.can_create_image_array():
        Graph().generate_floorplan_graph(location.get_grid_image_path(), location.get_pixels_per_cm(), location.id)
        img = Image.open(location.get_grid_image_path())
        location.map_pixel_width = img.size[0]
        location.map_pixel_height = img.size[1]
        location.save()



def delete_tool(tool_id):
    device = RangingDevice.get(RangingDevice.id == tool_id)
    device.delete_instance()

    query = RangingData.delete().where(RangingData.ranging_device_id == tool_id)
    query.execute()

    query = RangingModel.delete().where(RangingModel.ranging_device_id == tool_id)
    query.execute()

    return '{"success": 1}'


def get_groups():
    groups = []
    for group in Group.select():
        groups.append({
            'id': group.id,
            'name': group.name
        })
    return json.dumps(groups)


def post_group(name):
    Group.create(name=name)
    return '{"success": 1}'


def delete_group(group_id):
    group = Group.get(Group.id == group_id)
    group.delete_instance()
    return '{"success": 1}'


def get_clients():
    clients = []
    for client in Client.select():
        clients.append({
            'id': client.id,
            'identifier': client.identifier,
            'group_id': client.group_id
        })
    return json.dumps(clients)


def patch_client(client_id, group_id):
    if group_id:
        group = Group.get(Group.id == group_id)
    else:
        group = None
    client = Client.get(Client.id == client_id)
    client.group = group
    client.save()
    return '{"success": 1}'


def get_tracks(start, end):
    start = int(start)
    end = int(end)
    end += 24 * 3600;
    tracks = []
    for track in Track.select(Track, Client, Location).join(Client).switch(Track).join(Location).where(
            (Track.timestamp >= start) & (Track.timestamp <= end) & (Track.client != 18)) \
            .order_by(Track.timestamp.desc()):
        tracks.append({
            'id': track.id,
            'timestamp': track.timestamp.timestamp(),
            'client': track.client.identifier,
            'location': track.location.name
        })
    return json.dumps(tracks)


def get_track(track_id):
    track = Track.get(Track.id == track_id)
    location = Location.get(Location.id == track.location)
    waypoint_objects = TrackWaypoint.select().where(TrackWaypoint.track == track_id)
    waypoints = []  # type: [TrackWaypoint]
    for waypoint in waypoint_objects:  # type: TrackWaypoint
        waypoints.append({
            'position_x': str(waypoint.position_x),
            'position_y': str(waypoint.position_y),
            'waypointNumber': str(waypoint.position_number),
            'id': waypoint.id,
        })

    track_info = {}
    track_info['waypoints'] = waypoints
    track_info['location_name'] = track.location.name
    track_info['location'] = {
        'map_id': location.id,
        'map_display': location.map_display
    }
    track_info['measurements'] = track.measurements
    track_info['client_identifier'] = track.client.identifier
    track_info['id'] = track.id

    return json.dumps(track_info)


def delete_track(track_id):
    track = Track.get(Track.id == track_id)
    track.delete_instance()
    return '{"success": 1}'


def get_devices():
    clients = [];
    for client in Client.select().order_by(Client.identifier.asc()):
        clients.append({
            'id': client.id,
            'name': client.identifier
        })
    return json.dumps(clients);


def delete_device(client_id):
    client = Client.get(Client.id == client_id)
    client.delete_instance();
    return '{"success": 1}'


def post_device(identifier):
    Client.create(identifier=identifier)
    last_id = Client.select().order_by(Client.id.desc()).get()
    return '{"success": 1, "id": ' + str(last_id.id) + '}'


def patch_device(client_id, identifier):
    client = Client.get(Client.id == client_id);
    client.identifier = identifier
    client.save()
    return '{"success": 1}'


def get_rangingdata(ranging_location_id, self):
    client_id = self.get_argument('deviceID')

    if int(ranging_location_id) != -1:
        data = []
        for ranging_data in RangingData.select(RangingData.id,
                                               RangingData.ranging_device_id,
                                               fn.Avg(RangingData.value).alias('avg'),
                                               fn.Count(RangingData.id).alias('count')) \
                .where(
            (RangingData.client_id == client_id) &
            (RangingData.ranging_location_id == ranging_location_id)).group_by(RangingData.ranging_device_id):
            data.append({
                "ranging_device_id": ranging_data.ranging_device_id,
                "ranging_device_identifier": ranging_data.ranging_device.identifier,
                "avg": float(ranging_data.avg),
                "count": float(ranging_data.count)
            })
        return json.dumps(data)
    else:
        data = EnhancedRanging.get_ranging_status()
        return json.dumps(data)


def ranging_measurements_control(self, ranging_location_id):
    action = self.get_argument('action')
    client_id = self.get_argument('deviceID')
    position = self.get_argument('position')

    if int(ranging_location_id) != -1:
        if action == 'Start':
            RangingMeasurements.start_measurements(ranging_location_id, client_id)
        elif action == 'End':
            RangingMeasurements.end_measurements(ranging_location_id, client_id)
    else:
        EnhancedRanging.ranging_control(action)


def delete_rangingdata(ranging_location_id, ranging_device_id, client_id):
    query = RangingData.delete().where(
        (RangingData.ranging_device_id == ranging_device_id) & (
                RangingData.ranging_location_id == ranging_location_id) & (RangingData.client_id == client_id))
    query.execute()
    return '{"success": 1}'


def delete_ranginglocation(ranging_location_id):
    query = RangingData.delete().where(RangingData.ranging_location_id == ranging_location_id)
    query.execute()
    ranging_location = RangingLocation.get(RangingLocation.id == ranging_location_id)
    ranging_location.delete_instance()
    return '{"success": 1}'


def database_backup():
    copyfile('data/indoor_positioning.db', 'data/db_backup/indoor_positioning.db')
    return '{"success": 1}'


def enhancedranging_set_ranging_device(id):
    EnhancedRanging.update_ranging_device(id)


def get_display_location(location_id: int, device_id: int):
    location = get_location(location_id)
    location["tracking_devices"] = ([{
        # TODO: Return the correct device icon
        "icon": "iphone",
        "id": device_id
    }])
    return location


###############################
# Routing
###############################
class APIServer(tornado.web.RequestHandler):
    def initialize(self, **kwargs):
        pass

    def get(self, path):
        path = split_path(path)  # type:

        if path.area == 'locations' and not path.id:
            output = get_locations()
        elif path.area == 'locations' and path.id:
            output = json.dumps(get_location(path.id))
        elif path.area == 'groups' and not path.id:
            output = get_groups()
        elif path.area == 'clients' and not path.id:
            output = get_clients()
        elif path.area == 'tracks' and not path.id:
            output = get_tracks(self.get_argument('from'), self.get_argument('to'))
        elif path.area == 'tracks' and path.id:
            output = get_track(path.id)
        elif path.area == 'devices' and not path.id:
            output = get_devices()
        elif path.area == 'rangingdata' and path.id:
            output = get_rangingdata(path.id, self)
        elif path.area == 'ranging_measurements_control' and path.id:
            ranging_measurements_control(self, path.id)

        if not 'output' in locals():
            output = '{"success": 0}'

        self.write(output)

    def post(self, path):
        path = split_path(path)

        if path.area == 'locations' and not path.id:
            output = post_location(self.get_argument("name"))
        elif path.area == 'locations' and path.id:  # File Upload
            output = post_file(path.id, self)
        elif path.area == 'database':
            output = reset_database()
        elif path.area == 'tools' and not path.id:
            output = post_tool(self.get_argument('locationID'), self.get_argument('toolClass'),
                               self.get_argument('top'), self.get_argument('left'))
        elif path.area == 'waypoints' and not path.id:
            output = post_waypoint(self.get_argument('waypointNumber'), self.get_argument('trackId'),
                                   self.get_argument('left'), self.get_argument('top'))
        elif path.area == 'groups' and not path.id:
            output = post_group(self.get_argument('name'))
        elif path.area == 'devices' and not path.id:
            output = post_device(self.get_argument('name'))
        elif path.area == 'databasebackup' and not path.id:
            output = database_backup()
        elif path.area == 'enhancedranging' and path.id:
            output = enhancedranging_set_ranging_device(path.id)

        if not output:
            output = '{"success": 0}'

        self.write(output)

    def patch(self, path):
        path = split_path(path)

        if path.area == 'locations' and path.id:
            output = patch_location(path.id, self.get_argument('field'), self.get_argument('value'))
        elif path.area == 'tools' and path.id:
            output = patch_tool(path.id, self)
        elif path.area == 'waypoints' and path.id:
            output = patch_waypoint(path.id, self)
        elif path.area == 'clients' and path.id:
            output = patch_client(path.id, self.get_argument('group'))
        elif path.area == 'devices' and path.id:
            output = patch_device(path.id, self.get_argument('name'));

        if not output:
            output = '{"success": 0}'

        self.write(output)

    def delete(self, path):
        path = split_path(path)

        if path.area == 'locations' and path.id:
            output = delete_location(path.id)
        elif path.area == 'tools' and path.id:
            output = delete_tool(path.id)
        elif path.area == 'groups' and path.id:
            output = delete_group(path.id)
        elif path.area == 'tracks' and path.id:
            output = delete_track(path.id)
        elif path.area == 'devices' and path.id:
            output = delete_device(path.id)
        elif path.area == 'rangingdata' and path.id:
            output = delete_rangingdata(path.id, self.get_argument('rangingDeviceID'), self.get_argument('deviceID'))
        elif path.area == 'ranginglocation' and path.id:
            output = delete_ranginglocation(path.id)

        if not output:
            output = '{"success": 0}'

        self.write(output)
