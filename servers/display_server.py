import os

from tornado.web import RequestHandler

from data.datastore import DataStore
from settings import DISPLAY_CLIENT_PATH


class DisplayServer(RequestHandler):
    def __init__(self, application, request, data_store: DataStore, **kwargs):
        super().__init__(application, request, **kwargs)
        self.data_store = data_store

    def data_received(self, chunk):
        pass

    def initialize(self, **kwargs):
        pass

    def get(self, path):
        if "upload-id" in path:
            upload_ids = path.split("/")[1]
            download_id = self.data_store.generate_client_id()
            self.render(os.path.join(DISPLAY_CLIENT_PATH, "map.html"), download_id=download_id, upload_id=upload_ids,
                        track_id="")
        elif "track-id" in path:
            track_id = path.split("/")[1]
            download_id = self.data_store.generate_client_id()
            self.render(os.path.join(DISPLAY_CLIENT_PATH, "map.html"), download_id=download_id, upload_id="",
                        track_id=track_id)
        elif "test" in path:
            self.render(os.path.join(DISPLAY_CLIENT_PATH, "map.html"), download_id="test", upload_id="test",
                        track_id="")
        else:
            upload_ids = str(self.data_store.get_upload_clients(path))
            self.render(os.path.join(DISPLAY_CLIENT_PATH, "map.html"), download_id=path, upload_id=upload_ids,
                        track_id="")
