import tornado


from data.models import Auth


class LoginHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('../clients/admin/login.html')

    def post(self):
        username = tornado.escape.xhtml_escape(self.get_argument("username"))
        password = tornado.escape.xhtml_escape(self.get_argument("password"))

        good_login = Auth.select().where((Auth.username == username) & (Auth.password == password)).count()
        if good_login:
            self.set_secure_cookie("user", username)
            self.redirect('/')
        else:
            self.redirect('/login')
