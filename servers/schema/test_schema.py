import json
import os
from unittest import TestCase

from jsonschema import validate


class TestSchema(TestCase):
    def test__subscribe(self):
        subscribe = json.load(open(os.path.join(os.getcwd(), 'subscribe.json'), 'r'))
        dtd = json.load(open(os.path.join(os.getcwd(), 'subscribe-schema.json'), 'r'))
        validate(subscribe, dtd)
