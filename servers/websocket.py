import json
import time
import uuid

import tornado.websocket


def get_ms():
    return int(round(time.time() * 1000))


class WebSocket(tornado.websocket.WebSocketHandler):
    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)
        self.id = uuid.uuid4()
        self.last_transmission_time = -1

    def __eq__(self, other):
        return self.id == other.id

    def initialize(self, **kwargs):
        self.controller = kwargs['controller']

    def data_received(self, chunk):
        pass

    def check_origin(self, origin):
        return True

    def open(self):
        print("connected")

    def on_message(self, message):
        data = self.try_parse_json(message)
        if data:

            # Flow Control if required
            if 'c' in data:
                self.send_message({'c': data['c']})

            #print(data)
            #print("Delta: " + str(get_ms()-self.last_transmission_time))
            #self.last_transmission_time = get_ms()
            #print("Sent: " + str(data['c']))
            #print("Len: " + str(len(message)))

            # Update
            self.controller.update(self, data)

    def on_close(self):
        print("disconnected")

    def send_message(self, message):
        if not isinstance(message, str):
            message = json.dumps(message)

        # print("Response: " + message)
        self.write_message(message)
        #print("T: " + message)

    def try_parse_json(self, msg):
        try:
            return json.loads(msg)
        except TypeError:
            return json.loads(msg.decode())
        except ValueError:
            print("Bad JSON:")
            print(msg)
            self.write_message({'t': 'e', 'd': {'error': 'bad_json_format', 'msg': msg}})
            return False
