import os

db_settings = {
    'type': 'sqlite',
    'path': 'data/indoor_positioning.db'
}

datastore_settings = {
    'cleaning_interval': 60
}

ranging_settings = {
    'active_models': ['linear', 'exponential', 'power', 'poly2', 'poly3'], # , 'poly2', 'poly3', 'power', 'poly2leastsquares', 'leastsquarescauchy' 'leastsquaressoftl1'
    'live_model': 'poly3',
    'aggregate_last_n_measurements': 4,
    'gps_detection_range': 0.5,
    'measurements_per_direction': 10
}

MAPS_BASE_PATH = os.path.join(os.getcwd(), 'data', 'maps')  # type: str
"""The relative path to the maps"""
MAP_ARRAY_SUFFIX = "_array.txt"
"""The suffix of the array with booleans for the tiles (id preceding)"""

DISPLAY_CLIENT_PATH = os.path.join(os.getcwd(), 'clients', 'display', 'dist')  # type: str
"""The relative path to the display client and its frontend files"""

NODE_MODULES_PATH = os.path.join(os.getcwd(), 'node_modules')  # type: str
